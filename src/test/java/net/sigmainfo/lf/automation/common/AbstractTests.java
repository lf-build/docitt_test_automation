package net.sigmainfo.lf.automation.common;

import com.mongodb.*;
import net.sigmainfo.lf.automation.api.function.ApiPropertiesReader;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;
import net.sigmainfo.lf.automation.portal.function.StringEncrypter;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.SignUpPage;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.VerifyMobileNumberPage;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.WelcomePage;
import net.sigmainfo.lf.automation.api.config.ApiConfig;
import net.sigmainfo.lf.automation.api.constant.ApiParam;
import net.sigmainfo.lf.automation.portal.config.PortalConfig;
import net.sigmainfo.lf.automation.portal.constant.PortalParam;
import net.sigmainfo.lf.automation.portal.function.PortalPropertiesReader;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.ResetPasswordPage;
import net.sigmainfo.lf.automation.portal.pages.lenderportal.LenderLoginPage;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;
import org.testng.Assert;
import org.testng.annotations.*;

import javax.annotation.PostConstruct;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.testng.Assert.fail;

/**
 * Created by           : Shaishav.s on 08-02-2017.
 * Test class           : AbstractTests.java
 * Description          : Drives automation suite and delegates testng annotations
 * Includes             : 1. Setup and quit method for browser opening and closing for portal cases
 *                        2. Initializes test data and test property files
 *                        3. Custom Reporting methods
 *                        4. Reads,deletes emails from gmail account
 */

@ContextConfiguration(classes = {PortalConfig.class, ApiConfig.class})
@TestExecutionListeners(inheritListeners = false, listeners = {
        DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class})
@WebAppConfiguration
@Test
public class AbstractTests extends AbstractTestNGSpringContextTests {


    static Server automationServer = new Server(9096);

    static boolean setupDone = false;
    public static boolean ifFileExist=false;
    public static long auto_start = 0;
    public static String Execution_start_time = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss").format(new Date());
    public static long auto_finish = 0;
    public static String sResBackUp = "res/TestReport.txt";
    public enum browser_list {
        IE, FF, Chrome;
    }

    public static WebDriver getDriver() {
        return driver;
    }

    public static WebDriver driver;
    public static int profile_delay = 60;
    public static int ShortSleep = 60;
    public static File casefile,portal_scenfile,portal_feafile,feafile;
    public static int SUCCESS_WITH_CONTENT=200;
    public static int SUCCESS_WITH_NO_CONTENT=204;
    public static int AUTHORIZATION_INCORRECT=401;
    public static int BAD_REQUEST=400;


    @Autowired
    ApiPropertiesReader apiPropertiesReader;

    @Autowired
    public
    PortalParam portalParam;

    @Autowired
    PortalPropertiesReader portalPropertiesReader;

    @Autowired
    public
    PortalFuncUtils portalFuncUtils;

    @Autowired
    StringEncrypter strEncrypter;

    @BeforeSuite(alwaysRun = true)
    public void setUpOnce() throws Exception {
        logger.info("======================== Before Suite Invoked ==============================");
        setupJetty("docitt-automation", automationServer);
        logger.info("Jetty Server Started..........");
    }



    @AfterSuite(alwaysRun = true)
    public void tearDown() throws Exception {
        logger.info("======================== After Suite Invoked. ==============================");
        automationServer.getServer().stop();
        logger.info("Jetty Server Stopped..........");
    }

    @PostConstruct
    private void postConstruct() throws SQLException {
        if (!setupDone) {
            logger.info("======================== Post Construct Invoked. ==============================");
            setupDBParams();
            setupDone = true;
        }
    }

    public void setupDBParams() {

        logger.info("--------------- READING PORTAL PROPERTIES FILE ----------------");

        portalParam.baseweburl = portalPropertiesReader.getBaseweburl();
        logger.info("portalParam.baseweburl :" + portalParam.baseweburl);
        portalParam.docittwebuserid = portalPropertiesReader.getDocittwebuserid();
        logger.info("portalParam.docittwebuserid :" + portalParam.docittwebuserid);
        portalParam.borrowerurl = portalPropertiesReader.getBorrowerurl();
        logger.info("portalParam.borrowerurl :" + portalParam.borrowerurl);
        portalParam.lenderurl = portalPropertiesReader.getLenderurl();
        logger.info("portalParam.lenderurl :"+portalParam.lenderurl);
        portalParam.lenderUserLogin = portalPropertiesReader.getLenderUserLogin();
        logger.info("portalParam.lenderUserLogin :"+portalParam.lenderurl);
        portalParam.encryptedPassword = portalPropertiesReader.getEncyptedPassword();
        logger.info("portalParam.encryptedPassword :" + portalParam.encryptedPassword);
        portalParam.custom_report_location = portalPropertiesReader.getCustom_report_location();
        logger.info("portalParam.custom_report_loaction :" + portalParam.custom_report_location);
        portalParam.upload_file_loaction = portalPropertiesReader.getUpload_file_loaction();
        logger.info("portalParam.upload_file_loaction :" + portalParam.upload_file_loaction);
        portalParam.browser = portalPropertiesReader.getBrowser();
        logger.info("portalParam.browser :" + portalParam.browser);
        portalParam.mongoHost = portalPropertiesReader.getmongoHost();
        logger.info("portalParam.mongoDbHost :" + portalParam.mongoHost);
        portalParam.mongoPort = Integer.parseInt(portalPropertiesReader.getmongoPort());
        logger.info("portalParam.mongoDbPort :" + portalParam.mongoPort);
        portalParam.gmailId = portalPropertiesReader.getGmailId();
        logger.info("portalParam.gmailId :" + portalParam.gmailId);
        portalParam.gmailPassword = portalPropertiesReader.getGmailPassword();
        logger.info("portalParam.gmailPassword :" + portalParam.gmailPassword);
        portalParam.isEmailVerification = portalPropertiesReader.isEmailVerification();
        logger.info("portalParam.isEmailVerification :" + portalParam.isEmailVerification);
        portalParam.isMobileVerification = portalPropertiesReader.isMobileVerification();
        logger.info("portalParam.isMobileVerification :" + portalParam.isMobileVerification);
        portalParam.plaidUsername = portalPropertiesReader.getPlaid_username();
        logger.info("portalParam.plaidUsername :" + portalParam.plaidUsername);
        portalParam.plaidPassword = portalPropertiesReader.getPlaid_password();
        logger.info("portalParam.plaidPassword :" + portalParam.plaidPassword);
        portalParam.plaidPin = portalPropertiesReader.getPlaid_pin();
        logger.info("portalParam.plaidUsername :" + portalParam.plaidPin);

    }

    private void setupJetty(final String contextRoot, Server server) throws Exception {
        final AnnotationConfigWebApplicationContext applicationContext = new AnnotationConfigWebApplicationContext();
        applicationContext.register(ApiConfig.class,PortalConfig.class);
//        applicationContext.register(SmppConfig.class);
        final ServletHolder servletHolder = new ServletHolder(new DispatcherServlet(applicationContext));
        final ServletContextHandler context = new ServletContextHandler();
        context.setErrorHandler(null);
        context.setContextPath("/" + contextRoot);
        context.addServlet(servletHolder, "/*");

        server.setHandler(context);
        server.start();
    }

    @Test(groups = { "Result", "", "" }, description = "Results file creation")
    public static void AAResultsBackUp() throws Exception {
        System.out.println("*****Begining of ResultsBackUp *********************");

        try {
            TestResults.CreateResultTxt();
            //TestResults.ExportResultToTxt("Portal Url:=",GlobalVariables.portalUrl);
            Assert.assertTrue(true, "ResultsBackUp test case has been Passed.");
            System.out.println("*****Ending of ResultsBackUp *********************");

        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Exception happened in executing test case ResultsBackUp");
            fail("ResultsBackUp test case has been failed.");
        }
    }

    protected void initializeData(String FuncMod, String Test_ID) throws Exception {

        try {

            logger.info("Reading test data from " + FuncMod + " worksheet.");
            logger.info("-----------------------------------------------");

            if (FuncMod.equalsIgnoreCase("Docitt_Portal")) {
                String inviteId = new SimpleDateFormat("ddmmHHmmss").format(Calendar.getInstance().getTime());
                portalParam.borrowerEmail = "shaishav.sigma"+"+"+inviteId+"@gmail.com";
                logger.info("portalParam.borrowerEmail  :"+portalParam.borrowerEmail);
                portalParam.spouseEmail = "sigma.shaishav"+"+"+inviteId+"@gmail.com";
                logger.info("portalParam.spouseEmail  :"+portalParam.spouseEmail);
                portalParam.nonSpouseEmail= "shaishav.s"+"+"+inviteId+"@sigmainfo.net";
                logger.info("portalParam.nonSpouseEmail  :"+portalParam.nonSpouseEmail);
                portalParam.inviteUserType = portalFuncUtils.getTestData(FuncMod,Test_ID,"InviteUserType");
                logger.info("portalParam.inviteUserType: " + portalParam.inviteUserType);
                portalParam.testid = portalFuncUtils.getTestData(FuncMod, Test_ID, "Test_ID");
                logger.info("portalParam.testid: " + portalParam.testid);
                portalParam.firstName = portalFuncUtils.getTestData(FuncMod, Test_ID, "firstName");
                logger.info("portalParam.firstName: " + portalParam.firstName);
                portalParam.middleName = portalFuncUtils.getTestData(FuncMod, Test_ID, "middleName");
                logger.info("portalParam.middleName: " + portalParam.middleName);
                portalParam.lastName = portalFuncUtils.getTestData(FuncMod, Test_ID, "lastName");
                logger.info("portalParam.lastName: " + portalParam.lastName);
                portalParam.suffix = portalFuncUtils.getTestData(FuncMod, Test_ID, "suffix");
                logger.info("portalParam.suffix: " + portalParam.suffix);
                portalParam.phone = portalFuncUtils.getTestData(FuncMod, Test_ID, "phone");
                logger.info("portalParam.phone: " + portalParam.phone);
                portalParam.modeOfCommunication = portalFuncUtils.getTestData(FuncMod, Test_ID, "communication");
                logger.info("portalParam.modeOfCommunication: " + portalParam.modeOfCommunication);
                portalParam.propertyType = portalFuncUtils.getTestData(FuncMod, Test_ID, "propertyType");
                logger.info("portalParam.propertyType: " + portalParam.propertyType);
                portalParam.isProperty = Boolean.valueOf(portalFuncUtils.getTestData(FuncMod, Test_ID, "property"));
                logger.info("portalParam.isProperty: " + portalParam.isProperty);
                portalParam.isContract = Boolean.valueOf(portalFuncUtils.getTestData(FuncMod, Test_ID, "contract"));
                logger.info("portalParam.isContract: " + portalParam.isContract);
                portalParam.propertyUse = portalFuncUtils.getTestData(FuncMod, Test_ID, "propertyUse");
                logger.info("portalParam.propertyUse: " + portalParam.propertyUse);
                portalParam.firstAddress = portalFuncUtils.getTestData(FuncMod, Test_ID, "firstAddress");
                logger.info("portalParam.firstAddress: " + portalParam.firstAddress);
                portalParam.purchasePrice = portalFuncUtils.getTestData(FuncMod, Test_ID, "purchasePrice");
                logger.info("portalParam.purchasePrice: " + portalParam.purchasePrice);
                portalParam.downPayment = portalFuncUtils.getTestData(FuncMod, Test_ID, "downPayment");
                logger.info("portalParam.downPayment: " + portalParam.downPayment);
                portalParam.married = Boolean.valueOf(portalFuncUtils.getTestData(FuncMod, Test_ID, "married"));
                logger.info("portalParam.married: " + portalParam.married);
                portalParam.addSpouce = Boolean.valueOf(portalFuncUtils.getTestData(FuncMod, Test_ID, "addSpouce"));
                logger.info("portalParam.addSpouce: " + portalParam.addSpouce);
                portalParam.addCoborrower = Boolean.valueOf(portalFuncUtils.getTestData(FuncMod, Test_ID, "addCoborrower"));
                logger.info("portalParam.addCoborrower: " + portalParam.addCoborrower);
                portalParam.addDependant = Boolean.valueOf(portalFuncUtils.getTestData(FuncMod, Test_ID, "addDependant"));
                logger.info("portalParam.addDependant: " + portalParam.addDependant);
                portalParam.spouceFirstName = portalFuncUtils.getTestData(FuncMod, Test_ID, "spouceFirstName");
                logger.info("portalParam.spouceFirstName: " + portalParam.spouceFirstName);
                portalParam.spouceMiddleName = portalFuncUtils.getTestData(FuncMod, Test_ID, "spouseMiddleName");
                logger.info("portalParam.spouceMiddleName: " + portalParam.spouceMiddleName);
                portalParam.spouceLastName = portalFuncUtils.getTestData(FuncMod, Test_ID, "spouceLastName");
                logger.info("portalParam.spouceLastName: " + portalParam.spouceLastName);
                portalParam.spouceSuffix = portalFuncUtils.getTestData(FuncMod, Test_ID, "spouseSuffix");
                logger.info("portalParam.spouceSuffix: " + portalParam.spouceSuffix);
                portalParam.spoucePhone = portalFuncUtils.getTestData(FuncMod, Test_ID, "spoucePhone");
                logger.info("portalParam.spoucePhone: " + portalParam.spoucePhone);
                portalParam.spouceComm = portalFuncUtils.getTestData(FuncMod, Test_ID, "spouceComm");
                logger.info("portalParam.spouceComm: " + portalParam.spouceComm);
                portalParam.spouseEmployer = portalFuncUtils.getTestData(FuncMod, Test_ID, "spouseEmployer");
                logger.info("portalParam.spouseEmployer: " + portalParam.spouseEmployer);
                portalParam.spouseTitle = portalFuncUtils.getTestData(FuncMod, Test_ID, "spouseTitle");
                logger.info("portalParam.spouseTitle: " + portalParam.spouseTitle);
                portalParam.spouseEmpStartDate = portalFuncUtils.getTestData(FuncMod, Test_ID, "spouseEmpStartDate");
                logger.info("portalParam.spouseEmpStartDate: " + portalParam.spouseEmpStartDate);
                portalParam.spouseMonthlybaseSalary = portalFuncUtils.getTestData(FuncMod, Test_ID, "spouseMonthlybaseSalary");
                logger.info("portalParam.spouseMonthlybaseSalary: " + portalParam.spouseMonthlybaseSalary);
                portalParam.spouseBonusAmount = portalFuncUtils.getTestData(FuncMod, Test_ID, "spouseBonusAmount");
                logger.info("portalParam.spouseBonusAmount: " + portalParam.spouseBonusAmount);
                portalParam.spouseEmpAddress = portalFuncUtils.getTestData(FuncMod, Test_ID, "spouseEmpAddress");
                logger.info("portalParam.spouseEmpAddress: " + portalParam.spouseEmpAddress);
                portalParam.spouseAlimonyChildName = portalFuncUtils.getTestData(FuncMod, Test_ID, "spouseAlimonyChildName");
                logger.info("portalParam.spouseAlimonyChildName: " + portalParam.spouseAlimonyChildName);
                portalParam.spouseAlimonyChildDoB = portalFuncUtils.getTestData(FuncMod, Test_ID, "spouseAlimonyChildDoB");
                logger.info("portalParam.spouseAlimonyChildDoB: " + portalParam.spouseAlimonyChildDoB);
                portalParam.spouseSupportByCourt = Boolean.valueOf(portalFuncUtils.getTestData(FuncMod, Test_ID, "spouseSupportByCourt"));
                logger.info("portalParam.spouseSupportByCourt: " + portalParam.spouseSupportByCourt);
                portalParam.SpouseWillReceiveOrderByCourt = Boolean.valueOf(portalFuncUtils.getTestData(FuncMod, Test_ID, "SpouseWillReceiveOrderByCourt"));
                logger.info("portalParam.SpouseWillReceiveOrderByCourt: " + portalParam.SpouseWillReceiveOrderByCourt);
                portalParam.spouseTwoMonthSupport = Boolean.valueOf(portalFuncUtils.getTestData(FuncMod, Test_ID, "spouseTwoMonthSupport"));
                logger.info("portalParam.spouseTwoMonthSupport: " + portalParam.spouseTwoMonthSupport);
                portalParam.NonSpouseFirstName = portalFuncUtils.getTestData(FuncMod, Test_ID, "nonSpouseFirstName");
                logger.info("portalParam.NonSpouseFirstName: " + portalParam.NonSpouseFirstName);
                portalParam.spouseBusinessMonthlyIncome = portalFuncUtils.getTestData(FuncMod, Test_ID, "spouseBusinessMonthlyIncome");
                logger.info("portalParam.spouseBusinessMonthlyIncome: " + portalParam.spouseBusinessMonthlyIncome);
                portalParam.spouseBusinessCompanyName = portalFuncUtils.getTestData(FuncMod, Test_ID, "spouseBusinessCompanyName");
                logger.info("portalParam.spouseBusinessCompanyName: " + portalParam.spouseBusinessCompanyName);
                portalParam.spouseBusinessTitle = portalFuncUtils.getTestData(FuncMod, Test_ID, "spouseBusinessTitle");
                logger.info("portalParam.spouseBusinessTitle: " + portalParam.spouseBusinessTitle);
                portalParam.spouseBusinessAddress = portalFuncUtils.getTestData(FuncMod, Test_ID, "spouseBusinessAddress");
                logger.info("portalParam.spouseBusinessAddress: " + portalParam.spouseBusinessAddress);
                portalParam.spouseBusinessPhone = portalFuncUtils.getTestData(FuncMod, Test_ID, "spouseBusinessPhone");
                logger.info("portalParam.spouseBusinessPhone: " + portalParam.spouseBusinessPhone);
                portalParam.spouseBusinessStartDate = portalFuncUtils.getTestData(FuncMod, Test_ID, "spouseBusinessStartDate");
                logger.info("portalParam.spouseBusinessStartDate: " + portalParam.spouseBusinessStartDate);
                portalParam.spouseInstitutionName = portalFuncUtils.getTestData(FuncMod, Test_ID, "spouseInstitutionName");
                logger.info("portalParam.spouseInstitutionName: " + portalParam.spouseInstitutionName);
                portalParam.spouseAccountType = portalFuncUtils.getTestData(FuncMod, Test_ID, "spouseAccountType");
                logger.info("portalParam.spouseAccountType: " + portalParam.spouseAccountType);
                portalParam.spouseCurrentBalance = portalFuncUtils.getTestData(FuncMod, Test_ID, "spouseCurrentBalance");
                logger.info("portalParam.spouseCurrentBalance: " + portalParam.spouseCurrentBalance);
                portalParam.spouseAccountNumber = portalFuncUtils.getTestData(FuncMod, Test_ID, "spouseAccountNumber");
                logger.info("portalParam.spouseAccountNumber: " + portalParam.spouseAccountNumber);
                portalParam.spouseAccountName = portalFuncUtils.getTestData(FuncMod, Test_ID, "spouseAccountName");
                logger.info("portalParam.spouseAccountName: " + portalParam.spouseAccountName);
                portalParam.spouseTypeOfCompany = portalFuncUtils.getTestData(FuncMod, Test_ID, "spouseTypeOfCompany");
                logger.info("portalParam.spouseTypeOfCompany: " + portalParam.spouseTypeOfCompany);
                portalParam.spoucePercentOwnership = Boolean.valueOf(portalFuncUtils.getTestData(FuncMod, Test_ID, "spoucePercentOwnership"));
                logger.info("portalParam.spoucePercentOwnership: " + portalParam.spousePercentOwnership);
                portalParam.NonSpouseMiddleName = portalFuncUtils.getTestData(FuncMod, Test_ID, "nonSpouseMiddleName");
                logger.info("portalParam.NonSpouseMiddleName: " + portalParam.NonSpouseMiddleName);
                portalParam.NonSpouseLastName = portalFuncUtils.getTestData(FuncMod, Test_ID, "nonSpouseLastName");
                logger.info("portalParam.NonSpouseLastName: " + portalParam.NonSpouseLastName);
                portalParam.NonSpouseSuffix = portalFuncUtils.getTestData(FuncMod, Test_ID, "nonSpouseSuffix");
                logger.info("portalParam.NonSpouseSuffix: " + portalParam.NonSpouseSuffix);
                portalParam.NonSpousePhone = portalFuncUtils.getTestData(FuncMod, Test_ID, "nonSpousePhone");
                logger.info("portalParam.NonSpousePhone: " + portalParam.NonSpousePhone);
                portalParam.eligibleLoan = Boolean.valueOf(portalFuncUtils.getTestData(FuncMod, Test_ID, "eligibleLoan"));
                logger.info("portalParam.eligibleLoan: " + portalParam.eligibleLoan);
                portalParam.currentLoan = Boolean.valueOf(portalFuncUtils.getTestData(FuncMod, Test_ID, "currentLoan"));
                logger.info("portalParam.currentLoan: " + portalParam.currentLoan);
                portalParam.realEstateAgent = Boolean.valueOf(portalFuncUtils.getTestData(FuncMod, Test_ID, "realEstateAgent"));
                logger.info("portalParam.realEstateAgent: " + portalParam.realEstateAgent);
                portalParam.withLoanOfficer = Boolean.valueOf(portalFuncUtils.getTestData(FuncMod, Test_ID, "withLoanOfficer"));
                logger.info("portalParam.withLoanOfficer: " + portalParam.withLoanOfficer);
                portalParam.loanOfficerName = portalFuncUtils.getTestData(FuncMod, Test_ID, "loanOfficerName");
                logger.info("portalParam.loanOfficerName: " + portalParam.loanOfficerName);
                portalParam.sameResidence = Boolean.valueOf(portalFuncUtils.getTestData(FuncMod, Test_ID, "sameResidence"));
                logger.info("portalParam.sameResidence: " + portalParam.sameResidence);
                portalParam.residenceStatus = portalFuncUtils.getTestData(FuncMod, Test_ID, "residenceStatus");
                logger.info("portalParam.residenceStatus: " + portalParam.residenceStatus);
                portalParam.currentAddress = portalFuncUtils.getTestData(FuncMod, Test_ID, "currentAddress");
                logger.info("portalParam.currentAddress: " + portalParam.currentAddress);
                portalParam.stayingSince = portalFuncUtils.getTestData(FuncMod, Test_ID, "stayingSince");
                logger.info("portalParam.stayingSince: " + portalParam.stayingSince);
                portalParam.planningToSale = Boolean.valueOf(portalFuncUtils.getTestData(FuncMod, Test_ID, "planningToSale"));
                logger.info("portalParam.planningToSale: " + portalParam.planningToSale);
                portalParam.additionalProperty = Boolean.valueOf(portalFuncUtils.getTestData(FuncMod, Test_ID, "additionalProperty"));
                logger.info("portalParam.additionalProperty: " + portalParam.additionalProperty);
                portalParam.propertyAddress = portalFuncUtils.getTestData(FuncMod, Test_ID, "propertyAddress");
                logger.info("portalParam.propertyAddress: " + portalParam.propertyAddress);
                portalParam.typeOfProperty = portalFuncUtils.getTestData(FuncMod, Test_ID, "propertyType");
                logger.info("portalParam.typeOfProperty: " + portalParam.typeOfProperty);
                portalParam.propertyStatus = portalFuncUtils.getTestData(FuncMod, Test_ID, "propertyStatus");
                logger.info("portalParam.propertyStatus: " + portalParam.propertyStatus);
                portalParam.propertyValue = portalFuncUtils.getTestData(FuncMod, Test_ID, "propertyValue");
                logger.info("portalParam.propertyValue: " + portalParam.propertyValue);
                portalParam.rentalIncome = portalFuncUtils.getTestData(FuncMod, Test_ID, "rentalIncome");
                logger.info("portalParam.rentalIncome: " + portalParam.rentalIncome);
                portalParam.expenses = portalFuncUtils.getTestData(FuncMod, Test_ID, "expenses");
                logger.info("portalParam.expenses: " + portalParam.expenses);
                portalParam.institutionName = portalFuncUtils.getTestData(FuncMod, Test_ID, "institutionName");
                logger.info("portalParam.institutionName: " + portalParam.institutionName);
                portalParam.accountType = portalFuncUtils.getTestData(FuncMod, Test_ID, "accountType");
                logger.info("portalParam.accountType: " + portalParam.accountType);
                portalParam.currentBalance = portalFuncUtils.getTestData(FuncMod, Test_ID, "currentBalance");
                logger.info("portalParam.currentBalance: " + portalParam.currentBalance);
                portalParam.accountNumber = portalFuncUtils.getTestData(FuncMod, Test_ID, "accountNumber");
                logger.info("portalParam.accountNumber: " + portalParam.accountNumber);
                portalParam.accountName = portalFuncUtils.getTestData(FuncMod, Test_ID, "accountName");
                logger.info("portalParam.accountName: " + portalParam.accountName);
                portalParam.dateOfBirth = portalFuncUtils.getTestData(FuncMod, Test_ID, "dateOfBirth");
                logger.info("portalParam.dateOfBirth: " + portalParam.dateOfBirth);
                portalParam.ssnNumber = portalFuncUtils.getTestData(FuncMod, Test_ID, "ssnNumber");
                logger.info("portalParam.ssnNumber: " + portalParam.ssnNumber);
                portalParam.currentEmployer = portalFuncUtils.getTestData(FuncMod, Test_ID, "currentEmployer");
                logger.info("portalParam.currentEmployer: " + portalParam.currentEmployer);
                portalParam.employmentTitle = portalFuncUtils.getTestData(FuncMod, Test_ID, "title");
                logger.info("portalParam.employmentTitle: " + portalParam.employmentTitle);
                portalParam.employmentStartDate = portalFuncUtils.getTestData(FuncMod, Test_ID, "employmentStartDate");
                logger.info("portalParam.employmentStartDate: " + portalParam.employmentStartDate);
                portalParam.inThisLineYear = portalFuncUtils.getTestData(FuncMod, Test_ID, "inThisLineYear");
                logger.info("portalParam.inThisLineYear: " + portalParam.inThisLineYear);
                portalParam.inThisLineMonth = portalFuncUtils.getTestData(FuncMod, Test_ID, "inThisLineMonth");
                logger.info("portalParam.inThisLineMonth: " + portalParam.inThisLineMonth);
                portalParam.monthlyBaseSalary = portalFuncUtils.getTestData(FuncMod, Test_ID, "monthlyBaseSalary");
                logger.info("portalParam.monthlyBaseSalary: " + portalParam.monthlyBaseSalary);
                portalParam.businessAddress = portalFuncUtils.getTestData(FuncMod, Test_ID, "businessAddress");
                logger.info("portalParam.businessAddress: " + portalParam.businessAddress);
                portalParam.businessPhone = portalFuncUtils.getTestData(FuncMod, Test_ID, "businessPhone");
                logger.info("portalParam.businessPhone: " + portalParam.businessPhone);
                portalParam.monthlyAlimony = portalFuncUtils.getTestData(FuncMod, Test_ID, "monthlyAlimony");
                logger.info("portalParam.monthlyAlimony: " + portalParam.monthlyAlimony);
                portalParam.alimonyStartDate = portalFuncUtils.getTestData(FuncMod, Test_ID, "alimonyStartDate");
                logger.info("portalParam.alimonyStartDate: " + portalParam.alimonyStartDate);
                portalParam.monthlyChildSupport = portalFuncUtils.getTestData(FuncMod, Test_ID, "monthlyChildSupport");
                logger.info("portalParam.monthlyChildSupport: " + portalParam.monthlyChildSupport);
                portalParam.bonusAmount = portalFuncUtils.getTestData(FuncMod, Test_ID, "bonusAmount");
                logger.info("portalParam.bonusAmount: " + portalParam.bonusAmount);
                portalParam.childName = portalFuncUtils.getTestData(FuncMod, Test_ID, "childName");
                logger.info("portalParam.childName: " + portalParam.childName);
                portalParam.childDoB = portalFuncUtils.getTestData(FuncMod, Test_ID, "childDoB");
                logger.info("portalParam.childDoB: " + portalParam.childDoB);
                portalParam.supportByCourt = Boolean.valueOf(portalFuncUtils.getTestData(FuncMod, Test_ID, "supportByCourt"));
                logger.info("portalParam.supportByCourt: " + portalParam.supportByCourt);
                portalParam.willReceiveOrderByCourt = Boolean.valueOf(portalFuncUtils.getTestData(FuncMod, Test_ID, "willReceiveOrderByCourt"));
                logger.info("portalParam.willReceiveOrderByCourt: " + portalParam.willReceiveOrderByCourt);
                portalParam.twoMonthSupport = Boolean.valueOf(portalFuncUtils.getTestData(FuncMod, Test_ID, "twoMonthSupport"));
                logger.info("portalParam.twoMonthSupport: " + portalParam.twoMonthSupport);
                portalParam.selfEmploymentMonthlyIncome = portalFuncUtils.getTestData(FuncMod, Test_ID, "selfEmploymentMonthlyIncome");
                logger.info("portalParam.selfEmploymentMonthlyIncome: " + portalParam.selfEmploymentMonthlyIncome);
                portalParam.selfEmploymentCompanyName = portalFuncUtils.getTestData(FuncMod, Test_ID, "selfEmploymentCompanyName");
                logger.info("portalParam.selfEmploymentCompanyName: " + portalParam.selfEmploymentCompanyName);
                portalParam.selfEmploymentTitle = portalFuncUtils.getTestData(FuncMod, Test_ID, "selfEmploymentTitle");
                logger.info("portalParam.selfEmploymentTitle: " + portalParam.selfEmploymentTitle);
                portalParam.selfEmploymentBusinessAddress = portalFuncUtils.getTestData(FuncMod, Test_ID, "selfEmploymentBusinessAddress");
                logger.info("portalParam.selfEmploymentBusinessAddress: " + portalParam.selfEmploymentBusinessAddress);
                portalParam.selfEmploymentBusinessPhone = portalFuncUtils.getTestData(FuncMod, Test_ID, "selfEmploymentBusinessPhone");
                logger.info("portalParam.selfEmploymentBusinessPhone: " + portalParam.selfEmploymentBusinessPhone);
                portalParam.selfEmploymentBusinessStartDate = portalFuncUtils.getTestData(FuncMod, Test_ID, "selfEmploymentBusinessStartDate");
                logger.info("portalParam.selfEmploymentBusinessStartDate: " + portalParam.selfEmploymentBusinessStartDate);
                portalParam.typeOfCompany = portalFuncUtils.getTestData(FuncMod, Test_ID, "typeOfCompany");
                logger.info("portalParam.typeOfCompany: " + portalParam.typeOfCompany);
                portalParam.percentageOwnership = Boolean.valueOf(portalFuncUtils.getTestData(FuncMod, Test_ID, "percentageOwnership"));
                logger.info("portalParam.percentageOwnership: " + portalParam.percentageOwnership);
                portalParam.monthlyMilitaryPay = portalFuncUtils.getTestData(FuncMod, Test_ID, "monthlyMilitoryPay");
                logger.info("portalParam.monthlyMilitaryPay: " + portalParam.monthlyMilitaryPay);
                portalParam.monthlyRentalIncome = portalFuncUtils.getTestData(FuncMod, Test_ID, "monthlyRentalIncome");
                logger.info("portalParam.monthlyRentalIncome: " + portalParam.monthlyRentalIncome);
                portalParam.rentalPropertyAddress = portalFuncUtils.getTestData(FuncMod, Test_ID, "rentalPropertyAddress");
                logger.info("portalParam.rentalPropertyAddress: " + portalParam.rentalPropertyAddress);
                portalParam.spouseRentalPropertyAddress = portalFuncUtils.getTestData(FuncMod, Test_ID, "spouseRentalPropertyAddress");
                logger.info("portalParam.spouseRentalPropertyAddress: " + portalParam.spouseRentalPropertyAddress);
                portalParam.typeOfRentalProperty = portalFuncUtils.getTestData(FuncMod, Test_ID, "typeOfRentalProperty");
                logger.info("portalParam.typeOfRentalProperty: " + portalParam.typeOfRentalProperty);
                portalParam.socialSecurityIncome = portalFuncUtils.getTestData(FuncMod, Test_ID, "socialSecurityIncome");
                logger.info("portalParam.socialSecurityIncome: " + portalParam.socialSecurityIncome);
                portalParam.interestLastYear = portalFuncUtils.getTestData(FuncMod, Test_ID, "interestLastYear");
                logger.info("portalParam.interestLastYear: " + portalParam.interestLastYear);
                portalParam.interestPreviousYear = portalFuncUtils.getTestData(FuncMod, Test_ID, "interestPreviousYear");
                logger.info("portalParam.interestPreviousYear: " + portalParam.interestPreviousYear);
                portalParam.otherIncomePerMonth = portalFuncUtils.getTestData(FuncMod, Test_ID, "otherIncomePerMonth");
                logger.info("portalParam.otherIncomePerMonth: " + portalParam.otherIncomePerMonth);
                portalParam.sourceOfIncome = portalFuncUtils.getTestData(FuncMod, Test_ID, "sourceOfIncome");
                logger.info("portalParam.sourceOfIncome: " + portalParam.sourceOfIncome);
                portalParam.continuousIncome = Boolean.valueOf(portalFuncUtils.getTestData(FuncMod, Test_ID, "continuousIncome"));
                logger.info("portalParam.continuousIncome: " + portalParam.continuousIncome);
                portalParam.spouseDateOfBirth = portalFuncUtils.getTestData(FuncMod, Test_ID, "spouseDateOfBirth");
                logger.info("portalParam.spouseDateOfBirth: " + portalParam.spouseDateOfBirth);
                portalParam.spouseSsnNumber = portalFuncUtils.getTestData(FuncMod, Test_ID, "spouseSsnNumber");
                logger.info("portalParam.spouseSsnNumber: " + portalParam.spouseSsnNumber);

                portalParam.worthOfProperty = portalFuncUtils.getTestData(FuncMod, Test_ID, "worthOfProperty");
                logger.info("portalParam.worthOfProperty: " + portalParam.worthOfProperty);
                portalParam.firstOwe = portalFuncUtils.getTestData(FuncMod, Test_ID, "firstOwe");
                logger.info("portalParam.firstOwe: " + portalParam.firstOwe);
                portalParam.secondOwe = portalFuncUtils.getTestData(FuncMod, Test_ID, "secondOwe");
                logger.info("portalParam.secondOwe: " + portalParam.secondOwe);
                portalParam.otherOwe = portalFuncUtils.getTestData(FuncMod, Test_ID, "otherOwe");
                logger.info("portalParam.otherOwe: " + portalParam.otherOwe);
                portalParam.ifCashOut = Boolean.valueOf(portalFuncUtils.getTestData(FuncMod, Test_ID, "ifCashOut"));
                logger.info("portalParam.ifCashOut: " + portalParam.ifCashOut);
                portalParam.cashOutAmount = portalFuncUtils.getTestData(FuncMod, Test_ID, "cashOutAmount");
                logger.info("portalParam.cashOutAmount: " + portalParam.cashOutAmount);
                portalParam.cashOutPurpose = portalFuncUtils.getTestData(FuncMod, Test_ID, "cashOutPurpose");
                logger.info("portalParam.cashOutPurpose: " + portalParam.cashOutPurpose);


                portalParam.declarationQueA = portalFuncUtils.getTestData(FuncMod, Test_ID, "DeclarationQuestionA");
                logger.info("portalParam.declarationQueA : " + portalParam.declarationQueA );
                portalParam.DeclarationQuestionAText = portalFuncUtils.getTestData(FuncMod, Test_ID, "DeclarationQuestionAText");
                logger.info("portalParam.DeclarationQuestionAText: " + portalParam.DeclarationQuestionAText );
                portalParam.declarationQueB = portalFuncUtils.getTestData(FuncMod, Test_ID, "DeclarationQuestionB");
                logger.info("portalParam.declarationQueB : " + portalParam.declarationQueB );
                portalParam.DeclarationQuestionBText = portalFuncUtils.getTestData(FuncMod, Test_ID, "DeclarationQuestionBText");
                logger.info("portalParam.DeclarationQuestionBText: " + portalParam.DeclarationQuestionBText );
                portalParam.declarationQueC = portalFuncUtils.getTestData(FuncMod, Test_ID, "DeclarationQuestionC");
                logger.info("portalParam.declarationQueC : " + portalParam.declarationQueC );
                portalParam.DeclarationQuestionCText = portalFuncUtils.getTestData(FuncMod, Test_ID, "DeclarationQuestionCText");
                logger.info("portalParam.DeclarationQuestionCText: " + portalParam.DeclarationQuestionCText );
                portalParam.declarationQueD = portalFuncUtils.getTestData(FuncMod, Test_ID, "DeclarationQuestionD");
                logger.info("portalParam.declarationQueD : " + portalParam.declarationQueD );
                portalParam.DeclarationQuestionDText = portalFuncUtils.getTestData(FuncMod, Test_ID, "DeclarationQuestionDText");
                logger.info("portalParam.DeclarationQuestionDText: " + portalParam.DeclarationQuestionDText );
                portalParam.declarationQueE = portalFuncUtils.getTestData(FuncMod, Test_ID, "DeclarationQuestionE");
                logger.info("portalParam.declarationQueE : " + portalParam.declarationQueE );
                portalParam.DeclarationQuestionEText = portalFuncUtils.getTestData(FuncMod, Test_ID, "DeclarationQuestionEText");
                logger.info("portalParam.DeclarationQuestionEText: " + portalParam.DeclarationQuestionEText );
                portalParam.declarationQueF = portalFuncUtils.getTestData(FuncMod, Test_ID, "DeclarationQuestionF");
                logger.info("portalParam.declarationQueF : " + portalParam.declarationQueF );
                portalParam.DeclarationQuestionFText = portalFuncUtils.getTestData(FuncMod, Test_ID, "DeclarationQuestionFText");
                logger.info("portalParam.DeclarationQuestionFText: " + portalParam.DeclarationQuestionFText );
                portalParam.declarationQueG = portalFuncUtils.getTestData(FuncMod, Test_ID, "DeclarationQuestionG");
                logger.info("portalParam.declarationQueG : " + portalParam.declarationQueG );
                portalParam.DeclarationQuestionGText = portalFuncUtils.getTestData(FuncMod, Test_ID, "DeclarationQuestionGText");
                logger.info("portalParam.DeclarationQuestionGText: " + portalParam.DeclarationQuestionGText );
                portalParam.declarationQueH = portalFuncUtils.getTestData(FuncMod, Test_ID, "DeclarationQuestionH");
                logger.info("portalParam.declarationQueH : " + portalParam.declarationQueH );
                portalParam.DeclarationQuestionHText = portalFuncUtils.getTestData(FuncMod, Test_ID, "DeclarationQuestionHText");
                logger.info("portalParam.DeclarationQuestionHText: " + portalParam.DeclarationQuestionHText );
                portalParam.declarationQueI = portalFuncUtils.getTestData(FuncMod, Test_ID, "DeclarationQuestionI");
                logger.info("portalParam.declarationQueI : " + portalParam.declarationQueI );
                portalParam.DeclarationQuestionIText = portalFuncUtils.getTestData(FuncMod, Test_ID, "DeclarationQuestionIText");
                logger.info("portalParam.DeclarationQuestionIText: " + portalParam.DeclarationQuestionIText );
                portalParam.declarationQueJ = portalFuncUtils.getTestData(FuncMod, Test_ID, "DeclarationQuestionJ");
                logger.info("portalParam.declarationQueJ : " + portalParam.declarationQueJ );
                portalParam.declarationQueK = portalFuncUtils.getTestData(FuncMod, Test_ID, "DeclarationQuestionK");
                logger.info("portalParam.declarationQueK : " + portalParam.declarationQueK );
                portalParam.declarationQueL = portalFuncUtils.getTestData(FuncMod, Test_ID, "DeclarationQuestionL");
                logger.info("portalParam.declarationQueL : " + portalParam.declarationQueL );
                portalParam.declarationQueM = portalFuncUtils.getTestData(FuncMod, Test_ID, "DeclarationQuestionM");
                logger.info("portalParam.declarationQueM : " + portalParam.declarationQueM );
                portalParam.DeclarationBorrowerWishToFurnish = portalFuncUtils.getTestData(FuncMod, Test_ID, "DeclarationBorrowerWishToFurnish");
                logger.info("portalParam.DeclarationBorrowerWishToFurnish : " + portalParam.DeclarationBorrowerWishToFurnish);
                portalParam.DeclarationEthnicity = portalFuncUtils.getTestData(FuncMod, Test_ID, "DeclarationEthnicity");
                logger.info(" portalParam.DeclarationEthnicity : " +  portalParam.DeclarationEthnicity);
                portalParam.DeclarationRace = portalFuncUtils.getTestData(FuncMod, Test_ID, "DeclarationRace");
                logger.info("portalParam.DeclarationRace : " + portalParam.DeclarationRace);
                portalParam.DeclarationSex = portalFuncUtils.getTestData(FuncMod, Test_ID, "DeclarationSex");
                logger.info("portalParam.DeclarationSex: " + portalParam.DeclarationSex);
                portalParam.spouseSex = portalFuncUtils.getTestData(FuncMod, Test_ID, "spouseSex");
                logger.info("portalParam.spouseSex: " + portalParam.spouseSex);
                portalParam.postAppInstitutionName = portalFuncUtils.getTestData(FuncMod, Test_ID, "postAppInstitutionName");
                logger.info("portalParam.postAppInstitutionName: " + portalParam.postAppInstitutionName);
                portalParam.postAppInstitutionOwner = portalFuncUtils.getTestData(FuncMod, Test_ID, "postAppInstitutionOwner");
                logger.info("portalParam.postAppInstitutionOwner: " + portalParam.postAppInstitutionOwner);
                portalParam.postAppInstitutionType = portalFuncUtils.getTestData(FuncMod, Test_ID, "postAppInstitutionType");
                logger.info("portalParam.postAppInstitutionType: " + portalParam.postAppInstitutionType);
                portalParam.postAppDate = portalFuncUtils.getTestData(FuncMod, Test_ID, "postAppDate");
                logger.info("portalParam.postAppDate: " + portalParam.postAppDate);
                portalParam.postAppCompany = portalFuncUtils.getTestData(FuncMod, Test_ID, "postAppCompany");
                logger.info("portalParam.postAppCompany: " + portalParam.postAppCompany);
                portalParam.postAppAmount = portalFuncUtils.getTestData(FuncMod, Test_ID, "postAppAmount");
                logger.info("portalParam.postAppAmount: " + portalParam.postAppAmount);
                portalParam.TaxInstitution = portalFuncUtils.getTestData(FuncMod, Test_ID, "TaxServiceInstitution");
                logger.info("portalParam.TaxInstitution: " + portalParam.TaxInstitution);

                portalParam.TaxuserName = portalFuncUtils.getTestData(FuncMod, Test_ID, "TaxUsername");
                logger.info("portalParam.TaxuserName: " + portalParam.TaxuserName);

                portalParam.Taxpassword = portalFuncUtils.getTestData(FuncMod, Test_ID, "TaxPassword");
                logger.info("portalParam.Taxpassword: " + portalParam.Taxpassword);


                portalParam.ExplanationSummary = portalFuncUtils.getTestData(FuncMod, Test_ID, "ExplanationSummary");
                logger.info("portalParam.ExplanationSummary: " + portalParam.ExplanationSummary);

            } 
            logger.info("-----------------------------------------------");
        } catch (Exception e) {
            logger.info("    *****    Field not present in \"" + FuncMod + "\" worksheet.");
        }
    }

    public void writeToReport(String funcMod,String sTestID, String result) throws IOException {

        String base64String=null;
        JSONObject obj = new JSONObject();
        try {
            obj.put("name", sTestID);
            obj.put("status", result);
            if((funcMod.contains("Docitt_Portal") && (result.equalsIgnoreCase("Failed"))))
            {
                File file = new File(portalParam.custom_report_location+sTestID+".png");
                byte[] encoded = Base64.encodeBase64(FileUtils.readFileToByteArray(file));
                base64String = new String(encoded, StandardCharsets.US_ASCII);
                obj.put("screenshot", "data:image/png;base64,"+base64String);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            File file = new File(portalParam.custom_report_location + "test.json");
            ifFileExist = file.createNewFile();
            if (ifFileExist) {
                FileWriter f = new FileWriter(file);
                f.write("cases : [" + obj.toString());
                f.flush();
                f.close();
            } else {
                FileWriter f = new FileWriter(file, true);
                f.write("," + obj.toString());
                f.flush();
                f.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @BeforeMethod(groups = {"verifyPurchaseWorkflow","Sanity","verifyRefinanceWorkflow"})
    @Parameters("browser")
    public void setUp() throws Exception {
        //String ibrowser = GlobalVariables.browser;

        browser_list browser = browser_list.valueOf(portalPropertiesReader.getBrowser());
        String nodeUrl;
        switch (browser) {

            case IE:

                // IEDriverServer available @
                // http://code.google.com/p/selenium/downloads/list
                System.setProperty("webdriver.ie.driver", "./webdriver/IEDriverServer.exe");
                DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
                capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
                driver = new InternetExplorerDriver(capabilities);
                break;

            case Chrome:
                // chromedriver available @http://code.google.com/p/chromedriver/downloads/list
                System.setProperty("webdriver.chrome.driver", "./webdriver/chromedriver.exe");
                driver = new ChromeDriver();

                break;

            case FF:
                System.setProperty("webdriver.gecko.driver" ,"./webdriver/geckodriver.exe");
                driver = new FirefoxDriver();
        }

        Thread.sleep(profile_delay);
        driver.manage().timeouts().implicitlyWait(ShortSleep, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        logger.info("Loading url :" + portalPropertiesReader.getLenderurl());
        driver.get(portalPropertiesReader.getLenderurl());

    }



    @AfterMethod(groups = {"verifyPurchaseWorkflow","Sanity","verifyRefinanceWorkflow"})
    public void quit()
    {
        // pass the parameter to this method --> ITestResult result
        /*result = Reporter.getCurrentTestResult();
        logger.info("result.getStatus():"+result.getStatus());
        if(result.getStatus() == ITestResult.FAILURE)
        {
            try
            {
               TakesScreenshot ts=(TakesScreenshot)driver;
               File source=ts.getScreenshotAs(OutputType.FILE);
               FileUtils.copyFile(source, new File("./Screenshots/"+result.getName()+".png"));
               logger.info("Screenshot taken");
            }
            catch (Exception e)
            {

                logger.info("Exception while taking screenshot "+e.getMessage());
            }



        }*/
        driver.quit();
    }
    /**
     * Waits for the expected condition to complete
     *
     * @param e the expected condition to wait until it becomes true
     * @param timeout  how long to wait for the expected condition to turn true
     * @return true if the expected condition returned true and false if not
     */
    public boolean waitForCondition(ExpectedCondition<Boolean> e, int timeout) {
        WebDriverWait w = new WebDriverWait(driver, timeout);
        boolean returnValue = true;
        try {
            w.until(e);
        } catch (TimeoutException te) {
            logger.error("Failed to find the expected condition", te);
            returnValue = false;
        }
        return returnValue;
    }

    /**
     * Wait for the element to disappear from the screen
     *
     * @param finder the element to wait to disappear
     * @param timeout  how long to wait for the item to disappear (in seconds)
     * @return true if the element disappeared and false if it did not
     */
    protected boolean waitForElementRemoval(final By finder, int timeout) {
        ExpectedCondition<Boolean> e = new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                return driver.findElements(finder).size() == 0;
            }
        };
        boolean returnValue = waitForCondition(e, timeout);
        return returnValue;
    }

    /**
     * Wait for the element to appear on the screen
     * @param finder  the element to wait to appear
     * @param timeout  how long to wait for the item to appear (in seconds)
     * @return true if the element appeared and false if it did not
     */
    protected boolean waitForElement(final By finder, int timeout) {
        //logger.info("Waiting for element to load");
        ExpectedCondition<Boolean> e = new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                return driver.findElements(finder).size() > 0;
            }
        };
        boolean returnValue = waitForCondition(e, timeout);
        return returnValue;
    }

    /**
     * Sleeps for the desired amount of time
     *
     * @param time
     *            the amount of time to sleep in ms
     */
    protected void sleep(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException ie) {
        }
    }

    public void waitForPageLoad(WebDriver driver) {
        ExpectedCondition<Boolean> pageLoadCondition = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
                    }
                };
        WebDriverWait wait = new WebDriverWait(driver, 30);
        driver.manage().window().maximize();
        wait.until(pageLoadCondition);
    }

    /*public void loadAnotherProfile(){
        String userProfile= "C:\\Users\\shaishav.s\\AppData\\Local\\Temp\\AutomationProfile\\";
        ChromeOptions options = new ChromeOptions();
        options.addArguments("user-data-dir="+userProfile);
        options.addArguments("--start-maximized");

        driver = new ChromeDriver(options);
    }*/

    public VerifyMobileNumberPage readDocittEmail(WebDriver driver, String mainTab) throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver,30);
        driver.get("https://accounts.google.com/ServiceLogin?");
        loginToGmail(portalParam.getUsername(),portalParam.gmailPassword);
        driver.findElement(By.xpath("//*[@title='Google apps']")).click();
        driver.findElement(By.id("gb23")).click();
        List<WebElement> unreademail = driver.findElements(By.xpath("//*[@class='zF']"));
        //String docittMailer = getMailSender(System.getProperty("envParam"));
        String docittMailer = "Docitt Development";
        for(int i=0;i<unreademail.size();i++) {
            if (unreademail.get(i).isDisplayed() == true) {
                // Verify whether email received from Docitt
                if (unreademail.get(i).getText().equals(docittMailer)) {
                    //driver.get(baseUrl + "/mail/#inbox");
                    wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span[name=\""+docittMailer+"\"]")));
                    driver.findElement(By.cssSelector("span[name=\""+docittMailer+"\"]")).click();
                    String gmailTab = driver.getWindowHandle();
                    driver.findElement(By.linkText("Verify your account.")).click();
                    ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
                    newTab.remove(gmailTab);
                    if(newTab.size() != 0) {
                        driver.switchTo().window(newTab.get(0));
                    }

                } else {
                    System.out.println("No mail form " + docittMailer);
                }
            }
        }
        return new VerifyMobileNumberPage(driver);
    }

    public WelcomePage loginToBorrowerPortal(WebDriver driver, String mainTab) throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver,60);
        //driver.get("https://accounts.google.com/ServiceLogin?");
        driver.get("https://mail.google.com/mail/u/0/#inbox");
        loginToGmail(portalParam.gmailId,portalParam.gmailPassword);
        /*wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/[@title='Google apps']")));
        driver.findElement(By.xpath("/[@title='Google apps']")).click();
        driver.findElement(By.xpath("//a[@aria-label='Mail']"));
        driver.findElement(By.id("gb23")).click();*/
        List<WebElement> unreademail = driver.findElements(By.xpath("//*[@class='zF']"));
        //String docittMailer = getMailSender(System.getProperty("envParam"));
        String docittMailer = "Docitt Development";
        for(int i=0;i<unreademail.size();i++) {
            if (unreademail.get(i).isDisplayed() == true) {
                // Verify whether email received from Docitt
                if (unreademail.get(i).getText().equals(docittMailer)) {
                    //driver.get(baseUrl + "/mail/#inbox");
                    wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span[name=\""+docittMailer+"\"]")));
                    driver.findElement(By.cssSelector("span[name=\""+docittMailer+"\"]")).click();
                    String gmailTab = driver.getWindowHandle();
                    driver.findElement(By.linkText("Verify your account.")).click();
                    ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
                    newTab.remove(gmailTab);
                    if(newTab.size() != 0) {
                        driver.switchTo().window(newTab.get(0));
                    }

                } else {
                    System.out.println("No mail form " + docittMailer);
                }
            }
        }
        return new WelcomePage(driver);
    }

    public SignUpPage signUpAndLogin(WebDriver driver, String mainTab,String refId){
        WebDriverWait wait = new WebDriverWait(driver,60);
        //driver.get("https://accounts.google.com/ServiceLogin?");
        driver.get("http://docitt.qa.lendfoundry.com:9005/#/sign-up/refid:"+refId);
        return new SignUpPage(driver);
    }

    public LenderLoginPage navigateToLenderPortal(WebDriver driver,String mainTab){
        WebDriverWait wait = new WebDriverWait(driver,60);
        driver.get(portalPropertiesReader.getLenderurl());
        return new LenderLoginPage(driver);
    }
    
    public WelcomePage readDocittEmailAndLogin(WebDriver driver, String mainTab) throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver,30);
        driver.get("https://accounts.google.com/ServiceLogin?");
        loginToGmail(portalParam.getUsername(),portalParam.gmailPassword);
        driver.findElement(By.xpath("//*[@title='Google apps']")).click();
        driver.findElement(By.id("gb23")).click();
        List<WebElement> unreademail = driver.findElements(By.xpath("//*[@class='zF']"));
        //String docittMailer = getMailSender(System.getProperty("envParam"));
        String docittMailer = "Docitt Development";
        for(int i=0;i<unreademail.size();i++) {
            if (unreademail.get(i).isDisplayed() == true) {
                // Verify whether email received from Docitt
                if (unreademail.get(i).getText().equals(docittMailer)) {
                    //driver.get(baseUrl + "/mail/#inbox");
                    wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span[name=\""+docittMailer+"\"]")));
                    driver.findElement(By.cssSelector("span[name=\""+docittMailer+"\"]")).click();
                    String gmailTab = driver.getWindowHandle();
                    driver.findElement(By.linkText("Verify your account.")).click();
                    ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
                    newTab.remove(gmailTab);
                    if(newTab.size() != 0) {
                        driver.switchTo().window(newTab.get(0));
                    }

                } else {
                    System.out.println("No mail form " + docittMailer);
                }
            }
        }
        return new WelcomePage(driver);
    }

    private void loginToGmail(String id, String password) {
        WebDriverWait wait = new WebDriverWait(driver,30);
        boolean newGmail = driver.findElements( By.xpath("//div[contains(text(),'More options')]") ).size() != 0;
        if(newGmail)
        {
            driver.findElement(By.xpath("//input[@id='identifierId']")).sendKeys(id);
            driver.findElement(By.xpath("//span[contains(text(),'Next')]")).click();
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(),'Forgot password?')]"))).isDisplayed();
            driver.findElement(By.xpath("//input[@name='password']")).sendKeys(password);
            driver.findElement(By.xpath("//span[contains(text(),'Next')]")).click();
        }
        else {
            driver.findElement(By.id("Email")).sendKeys(id);
            driver.findElement(By.id("next")).click();
            driver.findElement(By.id("Passwd")).sendKeys(password);
            driver.findElement(By.id("signIn")).click();

        }
    }

    public String getMailSender(String envt)
    {
        HashMap<String,String> mailSender=new HashMap<String,String>();
        mailSender.put("dev","Docitt Development");
        mailSender.put("uat","Docitt UAT");
        mailSender.put("qa","Docitt QA");

        if(envt.contains("dev"))
            return mailSender.get("dev");
        else if (envt.contains("uat"))
            return mailSender.get("uat");
        else
            return mailSender.get("qa");

    }

    public ResetPasswordPage readResetPasswordEmail(WebDriver driver, String mainTab) throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver,30);
        String baseUrl = "https://mail.google.com/";
        driver.get("https://accounts.google.com/ServiceLogin?");
        loginToGmail(portalParam.gmailId,portalParam.gmailPassword);
        driver.findElement(By.xpath("//*[@title='Google apps']")).click();
        driver.findElement(By.id("gb23")).click();
        List<WebElement> unreademail = driver.findElements(By.xpath("//*[@class='zF']"));
        //String docittMailer = getMailSender(System.getProperty("envParam"));
        String docittMailer = "Docitt Development";
        for(int i=0;i<unreademail.size();i++) {
            if (unreademail.get(i).isDisplayed() == true) {
                // Verify whether email received from Docitt
                if (unreademail.get(i).getText().equals(docittMailer)) {
                    //driver.get(baseUrl + "/mail/#inbox");
                    wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span[name=\""+docittMailer+"\"]")));
                    driver.findElement(By.cssSelector("span[name=\""+docittMailer+"\"]")).click();
                    String gmailTab = driver.getWindowHandle();
                    driver.findElement(By.linkText("Reset your password.")).click();
                    ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
                    newTab.remove(gmailTab);
                    if(newTab.size() != 0) {
                        driver.switchTo().window(newTab.get(0));
                    }
                } else {
                    System.out.println("No mail form " + docittMailer);
                }
            }
        }
        return new ResetPasswordPage(driver);
    }

    @BeforeSuite(alwaysRun=true)
    public void  startCaseReport() throws Exception {
        logger.info("Initializing reporting framework");
        logger.info("---------------------------------");
        auto_start = System.currentTimeMillis();

        logger.info("portalParam.custom_report_location:"+portalParam.custom_report_location);

        FileUtils.copyFile(new File(portalParam.custom_report_location + "header.txt"),new File(portalParam.custom_report_location + "header_template.txt"));
        FileUtils.copyFile(new File(portalParam.custom_report_location + "trailor.txt"),new File(portalParam.custom_report_location + "trailor_template.txt"));

        logger.info("Calling Before Suite for generating reporting files.");

        casefile = new File(portalParam.custom_report_location+ "test.json");
        portal_scenfile = new File(portalParam.custom_report_location + "portal_scenario.json");
        portal_feafile = new File(portalParam.custom_report_location + "portal_feature.json");
        
        feafile = new File(portalParam.custom_report_location + "feature.json");

        casefile.createNewFile();
        FileWriter casef = new FileWriter(casefile);
        portal_scenfile.createNewFile();
        FileWriter portal_scenf = new FileWriter(portal_scenfile);
        portal_feafile.createNewFile();
        FileWriter portal_feaf = new FileWriter(portal_feafile);
        
        feafile.createNewFile();
        FileWriter featurefw = new FileWriter(feafile);
        portal_feaf.write("features: [");
        portal_scenf.write("scenarios: [");
        casef.write("cases : [");
        casef.flush();
        casef.close();
        portal_scenf.flush();
        portal_scenf.close();
        portal_feaf.flush();
        portal_feaf.close();
        featurefw.write("features: [");
        featurefw.flush();
        featurefw.close();



    }

    public void generateReport(String className, String description,String funcModule) throws IOException, JSONException {

        String classStatus = "";
        File scenFile = null;

        if(funcModule.equalsIgnoreCase("Docitt_Portal"))
        {
            scenFile = portal_scenfile;
        }

        logger.info("scenFile:"+scenFile);
        logger.info("casefile:"+casefile);
        BufferedReader scenreader = new BufferedReader(new FileReader(scenFile));
        BufferedReader reader = new BufferedReader(new FileReader(casefile));
        String line = "", oldtext = "";
        while ((line = reader.readLine()) != null) {
            oldtext += line + "\r";
        }

        reader.close();
        String newtext = oldtext.replaceAll("\\[\\,", "[");
        newtext = newtext.replaceAll("\n", "");
        newtext = newtext + "]";
        FileWriter f = new FileWriter(casefile, true);
        f.write(newtext);
        f.flush();
        f.close();

        JSONObject jobj = new JSONObject();
        jobj.put("name", className);
        jobj.put("description", description);
        jobj.put("tags", "");
        if (newtext.contains("Fail")) {
            classStatus = "Fail";
        } else {
            classStatus = "Pass";
        }
        jobj.put("status", classStatus);
        jobj.put("automated", "true");

        line = "";
        oldtext = "";
        while ((line = scenreader.readLine()) != null) {
            oldtext += line + "\r";
        }
        FileWriter scenf = new FileWriter(scenFile, true);
        if (!oldtext.contains("scenarios: [")) {
            scenf.write("scenarios: [\n");
        }
        scenf.write(jobj.toString().replaceAll("\\}", "") + "," + newtext + "\n},");
        scenf.flush();
        scenf.close();
        casefile.delete();
    }

    @AfterSuite (alwaysRun = true)
    public void prepareFinalReport() throws IOException, JSONException,java.lang.Exception {
        auto_finish = System.currentTimeMillis();

        String testEnvtString =  setTestEnvtDetails(auto_start,auto_finish,Execution_start_time);
        testEnvtString = testEnvtString.toString().replaceAll("\"values\"","values").replaceAll("\\[\\{","\\[").replaceAll("\\}\\]","\\]").replaceAll("\":\"", ":");

        UpdateEnvtParams(testEnvtString, portalParam.custom_report_location+ "header_template.txt");
        System.out.println(testEnvtString);
        logger.info("Calling After Suite for preparing html report.");

        if((portal_feafile.isFile()) && (portal_scenfile.isFile()))
        {
            BufferedReader portal_feareader = new BufferedReader(new FileReader(portal_feafile));
            BufferedReader portal_scenreader = new BufferedReader(new FileReader(portal_scenfile));

            String line = "", oldtext = "";
            while((line = portal_scenreader.readLine()) != null)
            {
                oldtext += line + "\r";
            }
            portal_scenreader.close();
            //String newfeatext = oldtext.replaceAll("\\}\\,\\r",",\n");
            JSONObject fobj = new JSONObject();
            fobj.put("name","Docitt_Portal");
            fobj.put("description","Docitt Portal Test Cases");

            FileWriter feaf = new FileWriter(portal_feafile,true);
            feaf.write(fobj.toString() + ",\n"+ oldtext);
            feaf.flush();
            feaf.close();

            oldtext = "";
            while((line = portal_feareader.readLine()) != null)
            {
                oldtext += line + "\r";
            }
            portal_feareader.close();
            oldtext = oldtext.replaceAll("\\\"\\}\\,\\r", "\\\",\n");
            oldtext = oldtext.replaceAll("\\^\\,", "");
            String finalFeatureText = oldtext + "]},";
            FileWriter finalfeaf = new FileWriter(portal_feafile);
            finalfeaf.write(finalFeatureText);
            finalfeaf.flush();
            finalfeaf.close();
        }

        casefile.delete();

        BufferedReader portal_feareader = new BufferedReader(new FileReader(portal_feafile));
        
        String oldtext="",line="";
        
        while((line = portal_feareader.readLine()) != null)
        {
            oldtext += line + "\r";
        }

        portal_feareader.close();
        
        String[] parts = oldtext.split("features: \\[", 2);
        oldtext = parts[0] + parts[1].replaceAll("features: \\[", "");

        FileWriter finalfeaf = new FileWriter(feafile,true);
        finalfeaf.write(oldtext);
        finalfeaf.flush();
        finalfeaf.close();

        File htmlReportFile = new File(portalParam.custom_report_location + "Docitt_Automation.html");
        File reportHeader = new File(portalParam.custom_report_location + "header_template.txt");
        File reportFeature = new File(portalParam.custom_report_location + "feature.json");
        File reportTrailer = new File(portalParam.custom_report_location + "trailor_template.txt");
        oldtext = "";line="";
        BufferedReader reportReader = new BufferedReader(new FileReader(reportHeader));
        while((line = reportReader.readLine()) != null)
        {
            oldtext += line + "\r";
        }

        BufferedReader featureReader = new BufferedReader(new FileReader(reportFeature));
        while((line = featureReader.readLine()) != null)
        {
            oldtext += line + "\r";
        }

        BufferedReader trailerReader = new BufferedReader(new FileReader(reportTrailer));
        while((line = trailerReader.readLine()) != null)
        {
            oldtext += line + "\r";
        }

        htmlReportFile.createNewFile();

        FileWriter htmlf = new FileWriter(htmlReportFile);
        htmlf.write(oldtext);
        htmlf.flush();
        htmlf.close();
        UpdateEnvtParams("ENVT_DETAILS", portalParam.custom_report_location + "header_template.txt");
    }

    public void UpdateEnvtParams(String testEnvtString,String fileName){
        try
        {
            String newtext=null;
            File file = new File(fileName);
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line = "", oldtext = "";
            while((line = reader.readLine()) != null)
            {
                oldtext += line + "";
            }
            reader.close();
            // replace a word in a file
            //String newtext = oldtext.replaceAll("drink", "Love");

            //To replace a line in a file
            if(!testEnvtString.equalsIgnoreCase("ENVT_DETAILS")) {
                newtext = oldtext.replaceAll("ENVT_DETAILS", testEnvtString);
            }
            else {
                newtext = oldtext.replaceAll(testEnvtString, "ENVT_DETAILS");
            }
            FileWriter writer = new FileWriter(fileName);
            writer.write(newtext);writer.close();
        }
        catch (IOException ioe)
        {
            ioe.printStackTrace();
        }

    }

    public String setTestEnvtDetails(long startTime,long endTime,String execution_start_time) throws java.lang.Exception{
        JSONObject jo = new JSONObject();
        jo.put("ProjectName","Docitt-Automation");
        jo.put("Operating System",System.getProperty("os.name").toLowerCase());
        jo.put("Testing environment",getMailSender(System.getProperty("envParam")));
        jo.put("Date",execution_start_time);
        jo.put("Total Execution Time",toHHMMDD(endTime - startTime));

        JSONArray ja = new JSONArray();
        ja.put(jo);

        JSONObject mainObj = new JSONObject();
        mainObj.put("values", ja);
        return mainObj.toString();
    }

    public String toHHMMDD(long time){

        String hms = String.format("%02d"+ " hrs" +":%02d"+ " mins" + ":%02d"+ " sec", TimeUnit.MILLISECONDS.toHours(time),
                TimeUnit.MILLISECONDS.toMinutes(time) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(time)),
                TimeUnit.MILLISECONDS.toSeconds(time) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(time)));
        return hms;
    }

    public void deleteGmailEmails(WebDriver driver, String mainTab){
        WebDriverWait wait = new WebDriverWait(driver,30);
        String baseUrl = "https://mail.google.com/";
        driver.get("https://accounts.google.com/ServiceLogin?");
        /*driver.findElement(By.id("Email")).sendKeys(portalParam.getUsername());
        driver.findElement(By.id("next")).click();*/
        boolean newGmail = driver.findElements( By.xpath("//div[contains(text(),'Forgot password?')]") ).size() != 0;
        if(newGmail)
        {
            driver.findElement(By.name("password")).sendKeys("my_sigma");
            driver.findElement(By.xpath("//div[@id='passwordNext']")).click();
        }
        else
        {
            driver.findElement(By.id("Passwd")).sendKeys("my_sigma");
            driver.findElement(By.id("signIn")).click();
        }
        driver.findElement(By.xpath("//*[@title='Google apps']")).click();
        driver.findElement(By.id("gb23")).click();
        driver.findElement(By.cssSelector("div.T-Jo-auh")).click();
        driver.findElement(By.xpath("//div[@id=':5']/div/div/div/div/div/div[2]/div[3]/div/div")).click();
        driver.findElement(By.cssSelector("span.gb_9a.gbii")).click();
        driver.findElement(By.id("gb_71")).click();
        if (isPopedUp()) {
            driver.switchTo().alert().accept();
        }
        logger.info("Emails deleted from Gmail.");
    }

    public boolean isPopedUp() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException Ex) {
            return false;
        }   // catch
    }

    //@AfterSuite(groups = {"LoginPageTests","SignUpPageTests","PortalTests"})
    @AfterSuite()
    public void deleteUserInMongoDb(String username) throws Exception {
        logger.info("Querying mongoDb for the user "+username);
        logger.info("==================");
        MongoClient mongoClient = null;

        /*mongoClient = new MongoClient(portalParam.mongoHost, portalParam.mongoPort);
        DB db = mongoClient.getDB("identity");
        //auth = db.authenticate(portalPropertiesReader.getMongoDbUser(), portalPropertiesReader.getMongoDbPwd().toCharArray());
        DBCollection collection = db.getCollection("users");
        BasicDBObject b1 = new BasicDBObject();
        b1.append("Email", "shaishav.sigma@gmail.com");
        collection.remove(b1);*/

        MongoCredential credential = MongoCredential.createCredential("docitt", "admin", "docSigma".toCharArray());
        mongoClient = new MongoClient(new ServerAddress(portalParam.mongoHost, portalParam.mongoPort), Arrays.asList(credential));
        DB db = mongoClient.getDB("identity");
        DBCollection collection = db.getCollection("users");
        BasicDBObject query = new BasicDBObject();
        query.append("Email", "shaishav.sigma@gmail.com");
        collection.remove(query);

        BasicDBObject b2 = new BasicDBObject();
        b2.append("Email", "sigma.shaishav@gmail.com");
        collection.remove(b2);

        logger.info("User "+ username +"deleted from mongoDb.");
    }

    public void scrollOnTopOfthePage(){
        sleep(3000);
        JavascriptExecutor jse = (JavascriptExecutor) AbstractTests.driver;
        jse.executeScript("window.scrollTo(0,0)");
        logger.info("Scrolling on top of the page for error message.");
    }

    public String getOnlyStrings(String s) {
        Pattern pattern = Pattern.compile("[^a-z A-Z]");
        Matcher matcher = pattern.matcher(s);
        String str = matcher.replaceAll("");
        return str;
    }

    public String getReferenceId(String username){
        logger.info("Querying mongoDb for the user "+username);
        logger.info("==================");
        MongoClient mongoClient = null;


        MongoCredential credential = MongoCredential.createCredential("docitt", "admin", "docSigma".toCharArray());
        mongoClient = new MongoClient(new ServerAddress(portalParam.mongoHost, portalParam.mongoPort), Arrays.asList(credential));
        DB db = mongoClient.getDB("assignment");
        DBCollection collection = db.getCollection("Invites");
        BasicDBObject query = new BasicDBObject();
        query.append("InviteEmail", username);
        DBObject dbObject = collection.findOne(query);
        return (String) dbObject.get("InvitationToken");
    }
}

