package net.sigmainfo.lf.automation.portal.pages.borrowerportal;

import net.sigmainfo.lf.automation.common.AbstractTests;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by           : Shaishav.s on 07-02-2017.
 * Test class           : VerifyYourAccountPage.java
 * Includes             : 1. Objects on VerifyYourAccountPage
 *                        2. Methods implementation on VerifyYourAccountPage
 */
public class VerifyYourAccountPage extends AbstractTests {

    private Logger logger = LoggerFactory.getLogger(VerifyYourAccountPage.class);

    public VerifyYourAccountPage(WebDriver driver) {
        this.driver = driver;
    }

    public static By verifyMobileTextBox = By.xpath("//input[@id='input']");
    public static By verifyCodeTextBox = By.xpath("//input[@placeholder='Enter the 4-digit code']");
    public static By sendCodeButton = By.xpath("//span[contains(text(),'SEND CODE')]");
    public static By signUpButton = By.xpath("//span[contains(text(),'SIGN UP')]");

    public String getPageTitle() {
        String title = driver.getTitle();
        return title;
    }

    public boolean verifyPageTitle() {
        String pageTitle = "";
        return getPageTitle().contains(pageTitle);
    }

    private void sendMobileNumber(String s) {
        logger.info("Entering Mobile Number");
        Actions actions = new Actions(driver);
        WebElement mobileTextBox = driver.findElement(verifyMobileTextBox);
        actions.moveToElement(mobileTextBox);
        actions.click();
        actions.sendKeys(s);
        actions.build().perform();
        driver.findElement(sendCodeButton).click();
    }

    public void sendVerificationCode(WebDriver driver, String s) {
        logger.info("Entering verification code");
        Actions actions = new Actions(driver);
        WebElement verificationCodeTextBox = driver.findElement(verifyCodeTextBox);
        actions.moveToElement(verificationCodeTextBox);
        actions.click();
        actions.sendKeys(s);
        actions.build().perform();
        driver.findElement(signUpButton).click();
    }
}
