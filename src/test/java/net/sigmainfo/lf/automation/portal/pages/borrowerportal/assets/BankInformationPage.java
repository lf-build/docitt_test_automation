package net.sigmainfo.lf.automation.portal.pages.borrowerportal.assets;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.LoginPage;

public class BankInformationPage  extends AbstractTests{
	private Logger logger = LoggerFactory.getLogger(BankInformationPage.class);
	WebDriverWait wait = new WebDriverWait(driver,60);

	BankInformationPage(){}
	
	public BankInformationPage(WebDriver driver){
		this.driver = driver;
        logger.info("========= BankInformationPage page is loaded===========");
	}
	
	public static By plaidUserID=By.xpath("//label[contains(text(),' ID')]");
	public static By plaidPassword=By.xpath("//ui-form[@id='credentialsForm']/div[2]/ui-container/div/ui-input/div/div[1]/input");
	public static By plaidPin=By.xpath("//ui-form[@id='credentialsForm']/div[3]/ui-container/div/ui-input/div/div[1]/input");
	public static By enterBtn=By.xpath("//button[@action='assets-submit']");
	//public static By enterSpouseBtn = By.xpath("//div[@id='sigma_shaishav_gmail_com_coBorrowerAssets']//button[.='Enter']");
	
	
	public BankAndInvestmentPage fillAccountCredentials(String username,String pwd,String pin) throws Exception{
		//new WebDriverWait(driver,40).until(ExpectedConditions.visibilityOfElementLocated(enterBtn));
		enterValueinField(plaidUserID,username);
		enterValueinField(plaidPassword,pwd);
		/*if(driver.findElement(plaidPin).isDisplayed() && driver.findElement(plaidPin).isEnabled())
		{
			enterValueinField(plaidPin,pin);
		}
		*//*if(driver.findElements(plaidPin).size() > 0)
		{
			enterValueinField(plaidPin,pin);
		}*//*
		Thread.sleep(2000);*/
		clickEnter();
		return new BankAndInvestmentPage(driver);
	}

	/*public BankAndInvestmentPage fillAccountCredentials(String username,String pwd,String pin,boolean addSpouse) throws Exception{
		//new WebDriverWait(driver,40).until(ExpectedConditions.visibilityOfElementLocated(enterBtn));
		enterValueinField(plaidUserID,username);
		enterValueinField(plaidPassword,pwd);
		if(driver.findElements(plaidPin).size() > 0)
		{
			enterValueinField(plaidPin,pin);
		}
		clickEnter(addSpouse);
		return new BankAndInvestmentPage(driver);
	}

	private void clickEnter(boolean addSpouse) {
		wait.until(ExpectedConditions.elementToBeClickable(enterSpouseBtn));
		driver.findElement(enterSpouseBtn).click();
		logger.info("Clicking on NEXT button");
	}*/

	public void clickEnter() throws Exception {
        wait.until(ExpectedConditions.elementToBeClickable(enterBtn));
        driver.findElement(enterBtn).click();
        logger.info("Clicking on NEXT button");
    }
	
	public void enterValueinField(By element,String value)  {
		try {
			Actions actions = new Actions(driver);
			new WebDriverWait(driver,30).until(ExpectedConditions.visibilityOfElementLocated(element));
			new WebDriverWait(driver,30).until(ExpectedConditions.elementToBeClickable(element));
			WebElement Element = driver.findElement(element);
			actions.moveToElement(Element);
			actions.click();
			actions.sendKeys(value);
			actions.build().perform();
			logger.info("Entering :"+ value);
		} catch (Exception e) {
			logger.error("Unable to enter "+ value + " value");
		}
	}
	
	
	
	
}
