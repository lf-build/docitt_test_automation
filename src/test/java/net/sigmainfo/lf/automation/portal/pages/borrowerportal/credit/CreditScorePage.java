package net.sigmainfo.lf.automation.portal.pages.borrowerportal.credit;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.summary.SummaryPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sigmainfo.lf.automation.portal.pages.borrowerportal.profile.ApplicantInformationPage;

public class CreditScorePage extends AbstractTests{

    WebDriverWait wait = new WebDriverWait(driver,60);

    private Logger logger = LoggerFactory.getLogger(ApplicantInformationPage.class);
    public static By CheckmyCreditBtn= By.xpath("//button[@name='next']");
    
    public CreditScorePage(WebDriver driver) {
        this.driver = driver;
        logger.info("========= CreditScorePage is loaded===========");
    }

    public CreditScorePage() {
    }
    
    
    public SpouseCreditScorePage checkCredit(){
    	wait.until(ExpectedConditions.elementToBeClickable(CheckmyCreditBtn));
    	driver.findElement(CheckmyCreditBtn).click();
    	return new SpouseCreditScorePage(driver);
    }
    

    
    
}
