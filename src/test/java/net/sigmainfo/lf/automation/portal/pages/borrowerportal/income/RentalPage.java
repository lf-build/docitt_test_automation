package net.sigmainfo.lf.automation.portal.pages.borrowerportal.income;

import net.sigmainfo.lf.automation.common.AbstractTests;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Month;

/**
 * Created by shaishav.s on 07-09-2017.
 */
public class RentalPage extends AbstractTests {
    WebDriverWait wait = new WebDriverWait(driver,60);
    WebElement PropertyTypeListItem;

    private Logger logger = LoggerFactory.getLogger(RentalPage.class);

    public RentalPage(WebDriver driver) {
        this.driver = driver;
        logger.info("========= RentalPage is loaded===========");
    }

    public RentalPage() {}

    public static By MonthlyRentalIncomeTextBox = By.xpath("//platform-shell/div/borrower-private-layout/div/micro-app/div/questioner-sections/div[1]/div[3]/div/div/ui-form/right-content-area/div/div/sub-sections/div[9]/questioner-question-set/div[2]/div[2]/div/questioner-question/ui-container/div/ui-input/div/div[1]/input");
    public static By RentalPropertyAddressTextBox = By.xpath("//platform-shell/div/borrower-private-layout/div/micro-app/div/questioner-sections/div[1]/div[3]/div/div/ui-form/right-content-area/div/div/sub-sections/div[9]/questioner-question-set/div[3]/div[2]/div/questioner-question/ui-container/div/ui-business-address/div/div[1]/ui-container/div/ui-input/div/div[1]/input");
    public static By TypeOfRentalPropertyDropdown = By.xpath("//platform-shell/div/borrower-private-layout/div/micro-app/div/questioner-sections/div[1]/div[3]/div/div/ui-form/right-content-area/div/div/sub-sections/div[9]/questioner-question-set/div[4]/div[2]/div/questioner-question/ui-container/div/ui-options/div[1]/div/input");
    public static By SingleFamilyType = By.xpath("//span[contains(text(),'Single Family Residence']");
    public static By CondoType = By.xpath("//span[contains(text(),'Condo']");
    public static By UnitsType = By.xpath("//span[contains(text(),'2-4 Units']");
    public static By iAmAllDoneButton = By.xpath("//button[@name='next']");


    public SocialSecurityPage enterRentalIncomeDetails(String monthlyRentalIncome, String rentalPropertyAddress, String typeOfRentalProperty) throws Exception {
        enterMonthlyRentalIncome(MonthlyRentalIncomeTextBox,monthlyRentalIncome);
        enterRentalPropertyAddress(RentalPropertyAddressTextBox,rentalPropertyAddress);
        wait.until(ExpectedConditions.visibilityOfElementLocated(TypeOfRentalPropertyDropdown));
        selectTypeOfProperty(TypeOfRentalPropertyDropdown,typeOfRentalProperty);
        clickButton();
        return new SocialSecurityPage(driver);
    }

    private void enterRentalPropertyAddress(By locator,String currentAddress) throws InterruptedException {
        enterText(locator,currentAddress);
        selectOptionWithText(currentAddress);
        Thread.sleep(3000);
    }

    private void selectOptionWithText(String currentAddress) {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='pac-item']//span[contains(text(),'"+currentAddress+"')]")));
        driver.findElement(By.xpath("//div[@class='pac-item']//span[contains(text(),'"+currentAddress+"')]")).click();
        logger.info("Selected address :" + currentAddress);
    }

    private void enterMonthlyRentalIncome(By locator,String monthlyRentalIncome) {
        enterText(locator,monthlyRentalIncome);
    }

    public void enterText(By locator,String text){
        Actions actions = new Actions(driver);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        actions.moveToElement(element);
        actions.click();
        actions.sendKeys(text);
        actions.build().perform();
        logger.info("Entering :"+text);
    }

    public void clickButton() throws Exception {
        wait.until(ExpectedConditions.elementToBeClickable(iAmAllDoneButton));
        driver.findElement(iAmAllDoneButton).click();
        logger.info("Clicking on I am done button");
    }

    private void selectTypeOfProperty(By locator, String typeOfRentalProperty) throws InterruptedException {
        driver.findElement(TypeOfRentalPropertyDropdown).click();

        for(int i=3;i<=6;i++){
            WebElement proeprtyType = driver.findElement(By.cssSelector("ui-options[name='dobMonth'] div[class*='select-wrapper'] li:nth-child("+i+")"));
            Thread.sleep(2000);
            String text = proeprtyType.findElement(By.xpath("//span[contains(text(),'"+typeOfRentalProperty+"')]")).getText();
            if(text.contains(typeOfRentalProperty)){
                proeprtyType.findElement(By.xpath("//span[contains(text(),'"+typeOfRentalProperty+"')]")).click();
            }

        }
        logger.info("Selected :"+typeOfRentalProperty);
    }
}
