package net.sigmainfo.lf.automation.portal.pages.borrowerportal.income;

import net.sigmainfo.lf.automation.common.AbstractTests;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by shaishav.s on 07-09-2017.
 */
public class AlimonyChildIncomePage extends AbstractTests {

    WebDriverWait wait = new WebDriverWait(driver,60);
    WebElement PropertyTypeListItem;

    private Logger logger = LoggerFactory.getLogger(AlimonyChildIncomePage.class);

    public AlimonyChildIncomePage(WebDriver driver) {
        this.driver = driver;
        logger.info("========= AlimonyChildIncomePage is loaded===========");
    }

    public AlimonyChildIncomePage() {}

    public static By MonthlyAlimonySupportTextbox = By.xpath("//platform-shell/div/borrower-private-layout/div/micro-app/div/questioner-sections/div[1]/div[3]/div/div/ui-form/right-content-area/div/div/sub-sections/div[5]/questioner-question-set/div[3]/div/div[1]/questioner-question/ui-container/div/ui-input/div/div[1]/input");
    public static By SpouseMonthlyAlimonySupportTextbox = By.xpath("//div[starts-with(@id,'ID')][13]/questioner-question-set/div[3]/div/div[1]//input[@id='input']");
    public static By AlimonyStartDateTextBox = By.xpath("//platform-shell/div/borrower-private-layout/div/micro-app/div/questioner-sections/div[1]/div[3]/div/div/ui-form/right-content-area/div/div/sub-sections/div[5]/questioner-question-set/div[3]/div/div[2]/questioner-question/ui-container/div/ui-input/div/div[1]/input");
    public static By SpouseAlimonyStartDateTextBox = By.xpath("//div[starts-with(@id,'ID')][13]/questioner-question-set/div[3]/div/div[2]//input[@id='input']");
    public static By MonthlyChildSupportTextBox = By.xpath("//platform-shell/div/borrower-private-layout/div/micro-app/div/questioner-sections/div[1]/div[3]/div/div/ui-form/right-content-area/div/div/sub-sections/div[5]/questioner-question-set/div[5]/div/div/questioner-question/ui-container/div/ui-input/div/div[1]/input");
    public static By SpouseMonthlyChildSupportTextBox = By.xpath("//div[starts-with(@id,'ID')][13]/questioner-question-set/div[5]//input[@id='input']");
    public static By ChildNameTextBox = By.xpath("//platform-shell/div/borrower-private-layout/div/micro-app/div/questioner-sections/div[1]/div[3]/div/div/ui-form/right-content-area/div/div/sub-sections/div[5]/questioner-question-set/div[6]/div/div[1]/questioner-question/ui-container/div/ui-input/div/div[1]/input");
    public static By SpouseChildNameTextBox = By.xpath("//div[starts-with(@id,'ID')][13]//div[6]/div[1]/div[1]//input[@id='input']");
    public static By ChildDoBTextBox = By.xpath("//platform-shell/div/borrower-private-layout/div/micro-app/div/questioner-sections/div[1]/div[3]/div/div/ui-form/right-content-area/div/div/sub-sections/div[5]/questioner-question-set/div[6]/div/div[2]/questioner-question/ui-container/div/ui-input/div/div[1]/input");
    public static By SpouseChildDoBTextBox = By.xpath("//div[starts-with(@id,'ID')][13]//div[6]/div[1]/div[2]//input[@id='input']");
    public static By NextButton = By.xpath("//button[@name='next']");

    public AlimonyChildSupportPage enterAlimonyIncomeDetails(String monthlyAlimony, String alimonyStartDate, String monthlyChildSupport,String childName,String childDoB) throws Exception {
    	enterMonthlyAlimony(MonthlyAlimonySupportTextbox,monthlyAlimony);
    	enterAlimonyStartDate(AlimonyStartDateTextBox,alimonyStartDate);
    	enterMonthlyChildSupport(MonthlyChildSupportTextBox,monthlyChildSupport);
    	wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(ChildNameTextBox));
    	enterChildName(ChildNameTextBox,childName);
    	enterChildDoB(ChildDoBTextBox,childDoB);
    	wait.until(ExpectedConditions.elementToBeClickable(ChildNameTextBox));
    	clickButton();
    	return new AlimonyChildSupportPage(driver);
    }
    
    private void enterChildDoB(By locator, String childDoB) {
    	enterText(locator,childDoB);
		
	}

	private void enterChildName(By locator, String childName) {
		enterText(locator,childName);
		
	}

	public void clickButton() throws Exception {
        wait.until(ExpectedConditions.elementToBeClickable(NextButton));
        driver.findElement(NextButton).click();
        logger.info("Clicking on Next button");
    }
    
    private void enterMonthlyChildSupport(By locator, String monthlyChildSupport) {
    	enterText(locator,monthlyChildSupport);
		
	}

	private void enterAlimonyStartDate(By locator, String alimonyStartDate) {
    	enterText(locator,alimonyStartDate);		
	}

	private void enterMonthlyAlimony(By locator,String monthlyAlimony) {
		enterText(locator,monthlyAlimony);
		
	}

	public void enterText(By locator,String text){
        Actions actions = new Actions(driver);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        actions.moveToElement(element);
        actions.click();
        actions.sendKeys(text);
        actions.build().perform();
        logger.info("Entering :"+text);
    }

    public AlimonyChildSupportPage provideSpouseAlimonyIncomeDetails(String monthlyAlimony, String alimonyStartDate, String monthlyChildSupport, String childName, String childDoB) throws Exception {
        enterMonthlyAlimony(SpouseMonthlyAlimonySupportTextbox,monthlyAlimony);
        enterAlimonyStartDate(SpouseAlimonyStartDateTextBox,alimonyStartDate);
        enterMonthlyChildSupport(SpouseMonthlyChildSupportTextBox,monthlyChildSupport);
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(SpouseChildNameTextBox));
        enterChildName(SpouseChildNameTextBox,childName);
        enterChildDoB(SpouseChildDoBTextBox,childDoB);
        wait.until(ExpectedConditions.elementToBeClickable(SpouseChildNameTextBox));
        clickButton();
        return new AlimonyChildSupportPage(driver);
    }
}
