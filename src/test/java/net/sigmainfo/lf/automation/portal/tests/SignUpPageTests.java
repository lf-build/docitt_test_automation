package net.sigmainfo.lf.automation.portal.tests;

import static org.testng.Assert.assertEquals;

import java.io.IOException;

import org.json.JSONException;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.common.CaptureScreenshot;
import net.sigmainfo.lf.automation.common.TestResults;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.LoginPage;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.SignUpPage;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.VerifyAccessCodePage;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.VerifyMobileNumberPage;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.VerifyYourAccountPage;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.WelcomePage;

/**
 * Created by shaishav.s on 08-02-2017.
 */
@Listeners(CaptureScreenshot.class)
public class SignUpPageTests extends AbstractTests {

    private Logger logger = LoggerFactory.getLogger(SignUpPageTests.class);
    public static String funcMod="Docitt_Portal";
   // public WebDriver driver;

    @Autowired
    TestResults testResults;

    @Autowired
    SignUpPage signUpPage;

    @Autowired
    LoginPage loginPage;

    public SignUpPageTests() {}

    @AfterClass(alwaysRun=true)
    public void endCasereport() throws IOException, JSONException {

        String funcModule = "Docitt_Portal";
        String className = org.apache.commons.lang.StringUtils.substringAfterLast(this.getClass().getName(), ".");
        String description = "Verify  "+ org.apache.commons.lang.StringUtils.substringAfterLast(this.getClass().getName(), ".");
        generateReport(className, description,funcModule);
    }

    /**
     * Description          : verifySuccessfulSignUp
     * Includes             : 1. Verifying whether user signs up successfully  :
     *                          a. Create questionnaire
     *                          b. Answer the question in coborrowerSection
     *                          c. Verify the questionnaire in mongodb that answer is updated
     */

    @Test(priority=1,description = "", groups = {"PortalTests","SignUpPageTests","verifySuccessfulSignUp"})
    public void verifySuccessfulSignUp() throws Exception {
        String sTestID = "verifySuccessfulSignUp";
        String result = "Failed";
        WebDriverWait wait = new WebDriverWait(driver,30);
        String mainTab=null;
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            signUpPage = new SignUpPage(driver);
            wait.until(ExpectedConditions.elementToBeClickable(By.name("email")));
            signUpPage.enterEmail(portalParam.getUsername());
            signUpPage.enterPassword(portalParam.getPassword());
            signUpPage.enterConfirmPassword(portalParam.getPassword());
            
            if(portalParam.isEmailVerification && portalParam.isMobileVerification)
            {
            	logger.info("EmailVerification:"+portalParam.isEmailVerification +" MobileVerification:"+portalParam.isMobileVerification);
	            VerifyYourAccountPage verifyYourAccountPage = signUpPage.clickSignUp();
	            wait.until(ExpectedConditions.urlMatches("http://docitt."+ getOnlyStrings(System.getProperty("envParam"))+".lendfoundry.com:9005/#/signup-success"));
	            assertEquals(driver.getCurrentUrl(),"http://docitt."+ getOnlyStrings(System.getProperty("envParam"))+".lendfoundry.com:9005/#/signup-success");
	            mainTab = driver.getWindowHandle();
	            VerifyMobileNumberPage verifyMobileNumberPage = readDocittEmail(driver,mainTab);
	            wait.until(ExpectedConditions.visibilityOfElementLocated(verifyMobileNumberPage.mobileNumberTextBox));
	            VerifyAccessCodePage verifyAccessCodePage = verifyMobileNumberPage.sendMobileNumber(driver,"9999999999");
	            sleep(2000);
	            //wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//class[contains(text(),'A 6-digit code has been sent to']")));
	            wait.until(ExpectedConditions.visibilityOfElementLocated(verifyAccessCodePage.enterCodeInstruction));
	            WelcomePage welcomePage = verifyAccessCodePage.sendVerificationCode(driver,"123456");	            
            }
            if(portalParam.isEmailVerification && !portalParam.isMobileVerification)
            {
            	logger.info("EmailVerification:"+portalParam.isEmailVerification +" MobileVerification:"+portalParam.isMobileVerification);
            	VerifyYourAccountPage verifyYourAccountPage = signUpPage.clickSignUpAndCheckEmail();
            	wait.until(ExpectedConditions.urlMatches("http://docitt."+ getOnlyStrings(System.getProperty("envParam"))+".lendfoundry.com:9005/#/signup-success"));
            	assertEquals(driver.getCurrentUrl(),"http://docitt."+ getOnlyStrings(System.getProperty("envParam"))+".lendfoundry.com:9005/#/signup-success");
            	mainTab = driver.getWindowHandle();
            	WelcomePage welcomePage = readDocittEmailAndLogin(driver,mainTab);
            }
            
            if(!portalParam.isEmailVerification && portalParam.isMobileVerification)
            {
            	logger.info("EmailVerification:"+portalParam.isEmailVerification +" MobileVerification:"+portalParam.isMobileVerification);
            	VerifyMobileNumberPage verifyMobileNumberPage = signUpPage.clickSignUpAndEnterMobile();
            	wait.until(ExpectedConditions.urlContains("http://docitt."+ getOnlyStrings(System.getProperty("envParam"))+".lendfoundry.com:9005/#/accountVerification/refid"));
            	wait.until(ExpectedConditions.elementToBeClickable(verifyMobileNumberPage.sendCodeButton));
                VerifyAccessCodePage verifyAccessCodePage = verifyMobileNumberPage.sendMobileNumber(driver,"9999999999");
                sleep(2000);
                wait.until(ExpectedConditions.visibilityOfElementLocated(verifyAccessCodePage.accessMyPortalButton));
                WelcomePage welcomePage = verifyAccessCodePage.sendVerificationCode(driver,"123456");
                
            }
            if(!portalParam.isEmailVerification && !portalParam.isMobileVerification)
            {
            	logger.info("EmailVerification:"+portalParam.isEmailVerification +" MobileVerification:"+portalParam.isMobileVerification);
            	WelcomePage welcomePage = signUpPage.clickSignUpAndEnterDocittHome();
            }
            wait.until(ExpectedConditions.urlContains("http://docitt."+ getOnlyStrings(System.getProperty("envParam"))+".lendfoundry.com:9005/#/home"));
            assertEquals(driver.getCurrentUrl(), "http://docitt."+ getOnlyStrings(System.getProperty("envParam"))+".lendfoundry.com:9005/#/home");
            logger.info("Home page is displayed");
            //deleteGmailEmails(driver,mainTab);
            //deleteUserInMongoDb(portalParam.getUsername());
            result="Passed";
           }
        catch(Exception e) {

            e.printStackTrace();
            logger.info("******************" + sTestID +  "  failed. *****\n" +e);
        }

        finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
            if(result.equalsIgnoreCase("Failed"))
                Assert.fail();
        }
    }

    @Test(priority=3,description = "", groups = {"PortalTests","SignUpPageTests","IncorrectUsername"})
    public void verifyIncorrectUsername() throws Exception {
        String sTestID = "verifyIncorrectUsername";
        String result = "Failed";
        String errorMessage="Email address is not valid.";
        WebDriverWait wait = new WebDriverWait(driver,30);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            String currentUrl = driver.getCurrentUrl();
            signUpPage = new SignUpPage(driver);
            wait.until(ExpectedConditions.elementToBeClickable(By.name("email")));
            signUpPage.enterEmail(portalParam.getUsername());
            signUpPage.enterPassword(portalParam.getPassword());
            signUpPage.enterConfirmPassword(portalParam.getPassword());
            VerifyYourAccountPage verifyYourAccountPage = signUpPage.clickSignUp();
            assertEquals(driver.findElement(By.xpath("//div[contains(text(),'"+errorMessage+"')]")).getText(),errorMessage);
            assertEquals(driver.getCurrentUrl().contains(""+ currentUrl+""),true);
            logger.info("Error message :" +errorMessage +" is captured.");
            result="Passed";
        }
        catch(Exception e) {
            e.printStackTrace();
            logger.info("******************" + sTestID +  "  failed. *****\n" +e);
        }

        finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
            if(result.equalsIgnoreCase("Failed"))
                Assert.fail();
        }
    }

    @Test(priority=4,description = "", groups = {"PortalTests","SignUpPageTests","IncorrectPassword"})
    public void verifyIncorrectPassword() throws Exception {
        String sTestID = "verifyIncorrectPassword";
        String result = "Failed";
        String errorMessage="Password criteria don't match.";
        WebDriverWait wait = new WebDriverWait(driver,30);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            String currentUrl = driver.getCurrentUrl();
            signUpPage = new SignUpPage(driver);
            wait.until(ExpectedConditions.elementToBeClickable(By.name("email")));
            signUpPage.enterEmail(portalParam.getUsername());
            signUpPage.enterPassword(portalParam.getPassword());
            signUpPage.enterConfirmPassword(portalParam.getPassword());
            VerifyYourAccountPage verifyYourAccountPage = signUpPage.clickSignUp();
            assertEquals(driver.findElement(By.xpath("//div[contains(text(),\"Password criteria don't match.\")]")).getText(),errorMessage);
            assertEquals(driver.getCurrentUrl().contains(""+ currentUrl+""),true);
            logger.info("Error message :" +errorMessage +" is captured.");
            result="Passed";
        }
        catch(Exception e) {
            e.printStackTrace();
            logger.info("******************" + sTestID +  "  failed. *****\n" +e);
        }

        finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
            if(result.equalsIgnoreCase("Failed"))
                Assert.fail();
        }
    }

    @Test(priority=5,description = "", groups = {"PortalTests","SignUpPageTests","IncorrectConfirmPassword","regression","Sanity"})
    public void verifyIncorrectConfirmPassword() throws Exception {
        String sTestID = "verifyIncorrectConfirmPassword";
        String result = "Failed";
        String errorMessage="Password and Confirm Password must match.";
        WebDriverWait wait = new WebDriverWait(driver,30);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            String currentUrl = driver.getCurrentUrl();
            signUpPage = new SignUpPage(driver);
            wait.until(ExpectedConditions.elementToBeClickable(By.name("email")));
            signUpPage.enterEmail(portalParam.getUsername());
            signUpPage.enterPassword(portalParam.getPassword());
            signUpPage.enterConfirmPassword(portalParam.getPassword()+"test");
            VerifyYourAccountPage verifyYourAccountPage = signUpPage.clickSignUp();
            assertEquals(driver.findElement(By.xpath("//div[contains(text(),'"+errorMessage+"')]")).getText(),errorMessage);
            assertEquals(driver.getCurrentUrl().contains(""+ currentUrl+""),true);
            logger.info("Error message :" +errorMessage +" is captured.");
            result="Passed";
        }
        catch(Exception e) {
            e.printStackTrace();
            logger.info("******************" + sTestID +  "  failed. *****\n" +e);
        }

        finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
            if(result.equalsIgnoreCase("Failed"))
                Assert.fail();
        }
    }

    /*@Test(priority=2,description = "", groups = {"PortalTests","SignUpPageTests","SuccessfulSignIn"})
    public void verifyNavigateToLoginPage() throws Exception {
        String sTestID = "verifySuccessfulSignIn";
        String result = "Failed";
        WebDriverWait wait = new WebDriverWait(driver,30);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            signUpPage = new SignUpPage(driver);
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(@href, 'javascript:void(0);')]")));
            LoginPage loginPage= signUpPage.clickOnSignInButton();
            wait.until(ExpectedConditions.urlMatches("http://docitt."+ getOnlyStrings(System.getProperty("envParam"))+".lendfoundry.com:9005/#/login"));
            assertEquals(driver.getCurrentUrl(),"http://docitt."+ getOnlyStrings(System.getProperty("envParam"))+".lendfoundry.com:9005/#/login");
            logger.info(driver.getCurrentUrl() +" is displayed.");
            result="Passed";
        }
        catch(Exception e) {
            e.printStackTrace();
            logger.info("******************" + sTestID +  "  failed. *****\n" +e);
        }

        finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
            if(result.equalsIgnoreCase("Failed"))
                Assert.fail();
        }
    }*/
}
