package net.sigmainfo.lf.automation.portal.pages.borrowerportal.profile;

import net.sigmainfo.lf.automation.common.AbstractTests;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;


/**
 * Created by shaishav.s on 06-09-2017.
 */
public class ApplicantInformationPage extends AbstractTests {

    WebDriverWait wait = new WebDriverWait(driver,60);
    WebElement PropertyTypeListItem;

    private Logger logger = LoggerFactory.getLogger(ApplicantInformationPage.class);

    public ApplicantInformationPage(WebDriver driver) {
        this.driver = driver;
        logger.info("========= ApplicantInformationPage is loaded===========");
    }

    public ApplicantInformationPage() {
    }

    public static By NextButton = By.xpath("//button[contains(text(),'Next')]");
    public static By CoborrowerLabel = By.xpath("//div[contains(text(),'(Co-Borrower)')]");
    public static By CoborrwerNameLabel = By.xpath(".//*[@id='undefined']/div[2]/div[2]");
    public static By progressBarLabel = By.xpath(".//*[@id='micro-app-host']/questioner-sections/div[1]/div[1]/left-navigation/div/div[2]/questioner-progress-bar/div/div");

    public EligibilityPage clickNext() throws Exception {
        wait.until(ExpectedConditions.visibilityOfElementLocated(CoborrowerLabel));
        wait.until(ExpectedConditions.visibilityOfElementLocated(CoborrwerNameLabel));
        wait.until(ExpectedConditions.elementToBeClickable(NextButton));
        driver.findElement(NextButton).click();
        logger.info("Clicking on NEXT button");
        return new EligibilityPage(driver);
    }

    public EligibilityPage verifyApplicants() throws Exception {
        //verifyTotalApplicants();
        verifyApplicantNames();
        clickNext();
        return new EligibilityPage(driver);
    }

    private void verifyApplicantNames() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(progressBarLabel));
        assertTrue(driver.findElement(By.xpath("//div[starts-with(@id,'ID')][3]//div[contains(text(),'"+portalParam.firstName+" "+portalParam.lastName+"')]")).isDisplayed());
        assertTrue(driver.findElement(By.xpath("//div[starts-with(@id,'ID')][3]//div[contains(text(),'"+portalParam.spouceFirstName+" "+portalParam.spouceLastName+"')]")).isDisplayed());
        assertTrue(driver.findElement(By.xpath("//div[starts-with(@id,'ID')][3]//div[contains(text(),'"+portalParam.NonSpouseFirstName+" "+portalParam.NonSpouseLastName+"')]")).isDisplayed());
        assertTrue(driver.findElement(By.xpath("//label[@for='SS1ui-select-one']")).isDisplayed());
    }

    private void verifyTotalApplicants() {
        assertEquals(driver.findElement(By.xpath("//input[@type='radio']")).getSize(),3);
    }
}
