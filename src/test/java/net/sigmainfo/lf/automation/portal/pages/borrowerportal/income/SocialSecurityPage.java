package net.sigmainfo.lf.automation.portal.pages.borrowerportal.income;

import net.sigmainfo.lf.automation.common.AbstractTests;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by shaishav.s on 07-09-2017.
 */
public class SocialSecurityPage extends AbstractTests {
    WebDriverWait wait = new WebDriverWait(driver,60);
    WebElement PropertyTypeListItem;

    private Logger logger = LoggerFactory.getLogger(SocialSecurityPage.class);

    public SocialSecurityPage(WebDriver driver) {
        this.driver = driver;
        logger.info("========= SocialSecurityPage is loaded===========");
    }

    public SocialSecurityPage() {}

    public static By SocialSecurityIncomeTextBox = By.xpath("//platform-shell/div/borrower-private-layout/div/micro-app/div/questioner-sections/div[1]/div[3]/div/div/ui-form/right-content-area/div/div/sub-sections/div[10]/questioner-question-set/div[2]/div/div/questioner-question/ui-container/div/ui-input/div/div[1]/input");
    public static By iAmAllDoneButton = By.xpath("//button[@name='next']");

    public InterestDividendPage enterSocialSecurityIncomeDetails(String socialSecurityIncome) throws Exception {
        enterSocialSecurityIncome(SocialSecurityIncomeTextBox,socialSecurityIncome);
        clickButton();
        return new InterestDividendPage(driver);
    }

    private void enterSocialSecurityIncome(By locator, String socialSecurityIncome) {
        enterText(locator,socialSecurityIncome);
    }

    public void clickButton() throws Exception {
        wait.until(ExpectedConditions.elementToBeClickable(iAmAllDoneButton));
        driver.findElement(iAmAllDoneButton).click();
        logger.info("Clicking on I am done button");
    }

    public void enterText(By locator,String text){
        Actions actions = new Actions(driver);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        actions.moveToElement(element);
        actions.click();
        actions.sendKeys(text);
        actions.build().perform();
        logger.info("Entering :"+text);
    }
}
