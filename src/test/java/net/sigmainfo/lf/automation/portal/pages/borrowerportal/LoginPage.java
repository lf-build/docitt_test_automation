package net.sigmainfo.lf.automation.portal.pages.borrowerportal;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.function.StringEncrypter;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.borrowerdashboard.BorrowerDashboardPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import static net.sigmainfo.lf.automation.portal.pages.lenderportal.LenderLoginPage.passwordTextbox;

/**
 * Created by           : Shaishav.s on 07-02-2017.
 * Test class           : LoginPage.java
 * Includes             : 1. Objects on LoginPage
 *                        2. Methods implementation on LoginPage
 */

public class LoginPage extends AbstractTests {
    private Logger logger = LoggerFactory.getLogger(LoginPage.class);
    WebDriverWait wait = new WebDriverWait(driver,60);

    public LoginPage(WebDriver driver) {

        this.driver = driver;

        logger.info("=========== LoginPage is loaded============");
    }
    public LoginPage(){}

    public static By emailTextBox = By.name("email");
    public static By passwordTextBox = By.name("password");
    public static By signInButton = By.xpath("//span[contains(text(),'LOG IN')]");
    public static By forgetPasswordLink = By.xpath("//a[contains(text(),'Forgot it?')]");
    public static By signUpLink = By.xpath("//a[contains(text(),'SIGN UP')]");
    public static By termsAndConditionLink = By.xpath("//a[contains(text(),'Terms of Service')]");

//    WebDriverWait wait = new WebDriverWait(driver,60);

    public String getPageTitle() {
        String title = driver.getTitle();
        return title;
    }

    public boolean verifyPageTitle() {
        String pageTitle = "";
        return getPageTitle().contains(pageTitle);
    }

    public void enterEmail(String username)  {


        /*Actions actions = new Actions(driver);
        new WebDriverWait(driver,30).until(ExpectedConditions.visibilityOfElementLocated(emailTextBox));
        WebElement emailTxtBox = driver.findElement(emailTextBox);
        actions.moveToElement(emailTxtBox);
        actions.click();
        actions.sendKeys(username);
        actions.build().perform();*/
        enterText(emailTextBox,username);
        logger.info("Entering Email:"+username);
    }


    public void enterPassword(String password) throws StringEncrypter.EncryptionException {
        StringEncrypter stringEncrypter = new StringEncrypter();
        enterText(passwordTextbox,StringEncrypter.createNewEncrypter().decrypt(password));
        logger.info("Entered Password");
    }

    public WelcomePage clickLogIn() {
        logger.info("Clicking Login button");
        WebElement signInBtn = driver.findElement(signInButton);
        if((signInBtn.isDisplayed()) || (signInBtn.isEnabled()))
            signInBtn.click();
        else
            new Exception("Login button is not found");
        return new WelcomePage(driver);
    }

    public ForgotPasswordPage clickOnForgetPasswordLink(){
        logger.info("Clicking Forget Password link");
        WebElement fpLink = driver.findElement(forgetPasswordLink);
        if((fpLink.isDisplayed()) || (fpLink.isEnabled()))
            fpLink.click();
        else
            new Exception("Forget it link is not found");
        return new ForgotPasswordPage(driver);
    }
    
    public WelcomePage loginToDocitt(String username, String password) throws StringEncrypter.EncryptionException {
        enterEmail(username);
    	enterPassword(password);
    	clickLogIn();
    	return new WelcomePage(driver);
    }

    public void enterText(By locator,String text)  {
        Actions actions = new Actions(driver);
        new WebDriverWait(driver,30).until(ExpectedConditions.visibilityOfElementLocated(locator));
        WebElement passwordTxtBox = driver.findElement(locator);
        actions.moveToElement(passwordTxtBox);
        actions.click();
        actions.sendKeys(text);
        actions.build().perform();
    }

    public BorrowerDashboardPage loginToDashboard(String username, String password) throws StringEncrypter.EncryptionException {
        wait.until(ExpectedConditions.visibilityOfElementLocated(termsAndConditionLink));
        enterEmail(username);
        enterPassword(password);
        clickLogIn();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(),'Upcoming Events')]")));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(),'Conditions Met')]")));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(),'Members')]")));
        return new BorrowerDashboardPage(driver);
    }
}
