package net.sigmainfo.lf.automation.portal.pages.borrowerportal.profile;

import net.sigmainfo.lf.automation.common.AbstractTests;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import static net.sigmainfo.lf.automation.portal.constant.PortalParam.sourceOfIncome;
import static org.testng.AssertJUnit.assertTrue;

/**
 * Created by shaishav.s on 09-10-2017.
 */
public class RefinancePropertyDetailsPage extends AbstractTests {
    WebDriverWait wait = new WebDriverWait(driver,60);
    WebElement PropertyTypeListItem;

    private Logger logger = LoggerFactory.getLogger(RefinancePropertyDetailsPage.class);

    public RefinancePropertyDetailsPage(WebDriver driver) {
        this.driver = driver;
        Assert.assertTrue(driver.findElement(By.xpath("//div[@id='SS2']/h3")).getText().contains(portalParam.firstName+"'s property details"),"RefinancePropertyDetailsPage header is not displayed.");
        logger.info("========= RefinancePropertyDetailsPage page is loaded===========");
    }

    public RefinancePropertyDetailsPage() {
    }

    public static By PropertyTypeDropdown = By.xpath("//input[@value='Property Type']");
    public static By PropertyUseDropdown = By.xpath("//input[@value='Property Use']");
    public static By AddressTextBox = By.xpath("//div[@id='SS2']//ui-input[@id='AddressAutocomplete']/div/div[1]/input");
    public static By NextButton = By.xpath("//button[@name='next']");

    public RefinanceLoanAmountPage enterRefinancePropertyDetails(String propertyType,String propertyUse,String firstAddress) throws Exception {
        selectFromDropdown(PropertyTypeDropdown,propertyType);
        selectFromDropdown(PropertyUseDropdown,propertyUse);
        enterRefinancePropertyAddress(AddressTextBox,firstAddress);
        clickNext();
        return new RefinanceLoanAmountPage(driver);
    }

    private void enterRefinancePropertyAddress(By locator,String address) throws InterruptedException {
        enterText(locator,address);
        selectOptionWithText(address);
        Thread.sleep(1500);
    }

    private void enterText(By locator, String address) {
        Actions actions = new Actions(driver);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        actions.moveToElement(element);
        actions.click();
        actions.sendKeys(address);
        actions.build().perform();
        logger.info("Entering :"+address);
    }

    public void clickNext() throws Exception {
        new WebDriverWait(driver,60).until(ExpectedConditions.elementToBeClickable(NextButton));
        driver.findElement(NextButton).click();
        logger.info("Clicking on NEXT button");
    }

    private void selectFromDropdown(By locator, String propertyUse) throws InterruptedException {
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        wait.until(ExpectedConditions.elementToBeClickable(locator));
        driver.findElement(locator).click();
        Thread.sleep(1000);
        for(int i=3;i<=18;i++){
            WebElement incomeType = driver.findElement(By.xpath("//div[@id='SS2']/questioner-question-set/div[1]//li["+i+"]"));
            Thread.sleep(1500);
            incomeType.findElement(By.xpath("//span[contains(text(),'"+propertyUse+"')]")).click();
            break;
        }
        logger.info("Selected :"+propertyUse);
        Thread.sleep(1000);
    }


    private void selectOptionWithText(String firstAddress) {
        driver.findElement(By.xpath("//div[@class='pac-item']//span[contains(text(),'"+firstAddress+"')]")).click();
        logger.info("Selected address :" + firstAddress);
    }


}
