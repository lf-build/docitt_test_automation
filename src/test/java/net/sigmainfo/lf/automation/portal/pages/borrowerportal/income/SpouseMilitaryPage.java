package net.sigmainfo.lf.automation.portal.pages.borrowerportal.income;

import net.sigmainfo.lf.automation.common.AbstractTests;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by shaishav.s on 26-09-2017.
 */
public class SpouseMilitaryPage extends AbstractTests {
    WebDriverWait wait = new WebDriverWait(driver,60);
    WebElement PropertyTypeListItem;

    private Logger logger = LoggerFactory.getLogger(SpouseMilitaryPage.class);

    public SpouseMilitaryPage(WebDriver driver) {
        this.driver = driver;
        logger.info("========= SpouseMilitaryPage is loaded===========");
    }

    public SpouseMilitaryPage() {}

    public static By SpouseMilitaryPayPerMonthTextBox = By.xpath("//div[starts-with(@id,'ID-')][16]//div[3]//input[@id='input']");
    public static By NextButton = By.xpath("//button[@name='next']");

    public SpouseRentalPage provideMilitaryIncomeDetails(String monthlyMilitaryPay) throws Exception {
        enterMilitaryIncome(monthlyMilitaryPay);
        clickButton();
        return new SpouseRentalPage(driver);
    }

    private void enterMilitaryIncome(String monthlyMilitaryPay) {
        enterText(SpouseMilitaryPayPerMonthTextBox,monthlyMilitaryPay);
    }

    public void enterText(By locator,String text){
        Actions actions = new Actions(driver);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        actions.moveToElement(element);
        actions.click();
        actions.sendKeys(text);
        actions.build().perform();
        logger.info("Entering :"+text);
    }

    public void clickButton() throws Exception {
        wait.until(ExpectedConditions.elementToBeClickable(NextButton));
        driver.findElement(NextButton).click();
        logger.info("Clicking on Next button");
    }
}
