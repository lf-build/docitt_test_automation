package net.sigmainfo.lf.automation.portal.pages.borrowerportal;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * Created by           : Shaishav.s on 07-02-2017.
 * Test class           : ForgotPasswordPage.java
 * Includes             : 1. Objects on ForgotPasswordPage
 *                        2. Methods implementation on ForgotPasswordPage
 */
@Component
public class ForgotPasswordPage extends AbstractTests {

    @Autowired
    PortalFuncUtils portalFuncUtils;

    private Logger logger = LoggerFactory.getLogger(ForgotPasswordPage.class);

    public ForgotPasswordPage(WebDriver driver) {
        this.driver = driver;
    }
    public ForgotPasswordPage(){}

    public static By emailTextBox = By.xpath("//input[@type='text']");
    public static By sendEmailButton = By.xpath("//span[contains(text(),'SEND EMAIL')]");
    public static By cancelLink = By.linkText("Cancel");
    public static By forgotPasswordLabel = By.xpath("//h1[contains(text(),'Forgot Password?')]");

    public void enterEmail(WebDriver driver, String username){
        sleep(5000);
        driver.findElement(emailTextBox).click();
        driver.findElement(emailTextBox).sendKeys(username);
        logger.info("Entered Email: "+username);
        /*JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("document.getElementById('input').click");
        js.executeScript("document.getElementById('input').value='"+username+"'");*/
    }

    public CheckYourEmailPage clickSendEmail() throws InterruptedException {
        logger.info("Clicking Send Email button");
        WebDriverWait wait = new WebDriverWait(driver,30);
        WebElement sendEmailBtn = driver.findElement(sendEmailButton);
        if((sendEmailBtn.isDisplayed()) || (sendEmailBtn.isEnabled()))
            sendEmailBtn.click();
        else
            new Exception("Send Email button is not found");
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h1[contains(text(),'Check your email')]")));
        Thread.sleep(20000);
        return new CheckYourEmailPage(driver);
    }

    public LoginPage clickCancelLink(){
        logger.info("Clicking Cancel link");
        WebElement cancelLnk = driver.findElement(cancelLink);
        if((cancelLnk.isDisplayed()) || (cancelLnk.isEnabled()))
            cancelLnk.click();
        else
            new Exception("Send Email button is not found");
        return new LoginPage(driver);
    }

    public void IgnoreStaleElementReferenceAndMakeElementActive(WebDriver driver, By element) {
        int count = 0;
        boolean clicked = false;
        while (count < 10 || !clicked) {
            try {
                WebElement yourSlipperyElement = driver.findElement(element);
                yourSlipperyElement.click();
                clicked = true;
                break;
            } catch (StaleElementReferenceException e) {
                e.toString();
                System.out.println("Trying to recover from a stale element :" + e.getMessage());
                count = count + 1;
            }
        }
    }
}
