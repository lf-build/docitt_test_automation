package net.sigmainfo.lf.automation.portal.pages.borrowerportal.income;

import net.sigmainfo.lf.automation.common.AbstractTests;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by shaishav.s on 06-09-2017.
 */
public class IncomeTypesPage extends AbstractTests {
    WebDriverWait wait = new WebDriverWait(driver,60);
    WebElement PropertyTypeListItem;

    private Logger logger = LoggerFactory.getLogger(IncomeTypesPage.class);

    public IncomeTypesPage(WebDriver driver) {
        this.driver = driver;
        logger.info("========= IncomeTypesPage is loaded===========");
    }

    public IncomeTypesPage() {
    }


    public static By AlimonyButton = By.xpath("//div[@id='SS2']//span[.='Alimony / Child Support']");
    public static By EmploymentButton = By.xpath("//div[@id='SS2']//span[.='Employment']");
    public static By InterestButton = By.xpath("//div[@id='SS2']//span[.='Interest & Dividend']");
    public static By OtherButton = By.xpath("//div[@id='SS2']//li[.='  Other ']");
    public static By MilitaryButton = By.xpath("//div[@id='SS2']//li[.='  Military Pay ']");
    public static By BusinessButton = By.xpath("//div[@id='SS2']//span[.='Business / Self Employment']");
    public static By SocialSecurityButton = By.xpath("//div[@id='SS2']//span[.='Social Security']");
    public static By RentalButton = By.xpath("//div[@id='SS2']//span[.='Rental']");
    public static By BeginButton = By.name("next");

    
    public BorrowerEmploymentPage enterTypesOfIncome() throws Exception {
    	clickIncomeType(AlimonyButton);
    	clickIncomeType(EmploymentButton);
    	clickIncomeType(InterestButton);
    	clickIncomeType(OtherButton);
    	clickIncomeType(MilitaryButton);
    	clickIncomeType(BusinessButton);
    	clickIncomeType(SocialSecurityButton);
    	clickIncomeType(RentalButton);    	
    	clickBegin(BeginButton);
    	return new BorrowerEmploymentPage(driver);
    }
    
    public void clickIncomeType(By locator) throws InterruptedException
    {
        wait.until(ExpectedConditions.elementToBeClickable(locator));
    	driver.findElement(locator).click();
    	logger.info("Selecting Income type:"+ locator);
    	Thread.sleep(1500);
    }
    
    public void clickBegin(By locator) throws Exception {
        wait.until(ExpectedConditions.elementToBeClickable(locator));
        driver.findElement(locator).click();
        logger.info("Clicking on BEGIN button");
    }


    
}
