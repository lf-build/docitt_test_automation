package net.sigmainfo.lf.automation.portal.pages.borrowerportal.income;

import net.sigmainfo.lf.automation.common.AbstractTests;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.testng.AssertJUnit.assertTrue;

/**
 * Created by shaishav.s on 25-09-2017.
 */
public class SpouseIncomeTypePage extends AbstractTests {

    WebDriverWait wait = new WebDriverWait(driver,60);
    WebElement PropertyTypeListItem;

    private Logger logger = LoggerFactory.getLogger(SpouseIncomeTypePage.class);

    public SpouseIncomeTypePage(WebDriver driver) {
        this.driver = driver;
        logger.info("========= SpouseIncomeTypePage is loaded===========");
        assertTrue(driver.findElement(By.xpath("//div[contains(@id,'coBorrowerIncome')]//h3[contains(text(),\"Jesica's income\")]")).isDisplayed());
    }

    public SpouseIncomeTypePage() {
    }

    public static By SpouseAlimonyButton = By.xpath("//div[starts-with(@id,'sigma_shaishav')]//span[.='Alimony / Child Support']");
    public static By SpouseEmploymentButton = By.xpath("//div[starts-with(@id,'sigma_shaishav')]//span[.='Employment']");
    public static By SpouseInterestButton = By.xpath("//div[starts-with(@id,'sigma_shaishav')]//span[.='Interest & Dividend']");
    public static By SpouseOtherButton = By.xpath("//div[starts-with(@id,'sigma_shaishav')]//li[.='  Other ']");
    public static By SpouseMilitaryButton = By.xpath("//div[starts-with(@id,'sigma_shaishav')]//span[.='Military Pay']");
    public static By SpouseBusinessButton = By.xpath("//div[starts-with(@id,'sigma_shaishav')]//span[.='Business / Self Employment']");
    public static By SpouseSocialSecurityButton = By.xpath("//div[starts-with(@id,'sigma_shaishav')]//span[.='Social Security']");
    public static By SpouseRentalButton = By.xpath("//div[starts-with(@id,'sigma_shaishav')]//span[.='Rental']");
    public static By SpouseBeginButton = By.xpath("//button[@name='next']");

    public SpouseEmploymentPage provideTypesOfIncome() throws Exception {
        clickIncomeType(SpouseAlimonyButton);
        clickIncomeType(SpouseEmploymentButton);
        clickIncomeType(SpouseInterestButton);
        clickIncomeType(SpouseOtherButton);
        clickIncomeType(SpouseMilitaryButton);
        clickIncomeType(SpouseBusinessButton);
        clickIncomeType(SpouseSocialSecurityButton);
        clickIncomeType(SpouseRentalButton);
        clickBegin(SpouseBeginButton);
        return new SpouseEmploymentPage(driver);
    }

    public void clickIncomeType(By locator) throws InterruptedException
    {
        wait.until(ExpectedConditions.elementToBeClickable(locator));
        driver.findElement(locator).click();
        logger.info("Selecting Income type:"+ locator);
        Thread.sleep(1500);
    }

    public void clickBegin(By locator) throws Exception {
        wait.until(ExpectedConditions.elementToBeClickable(locator));
        driver.findElement(locator).click();
        logger.info("Clicking on BEGIN button");
    }
}
