package net.sigmainfo.lf.automation.portal.pages.borrowerportal.credit;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.profile.ApplicantInformationPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by shaishav.s on 11-10-2017.
 */
public class SpouseCreditScorePage extends AbstractTests {
    WebDriverWait wait = new WebDriverWait(driver,60);

    private Logger logger = LoggerFactory.getLogger(SpouseCreditScorePage.class);
    public static By CheckmyCreditBtn= By.xpath("//button[@name='submit']");

    public SpouseCreditScorePage(WebDriver driver) {
        this.driver = driver;
        logger.info("========= SpouseCreditScorePage is loaded===========");
    }

    public SpouseCreditScorePage() {
    }

    public CreditCompletedPage checkCredit(){
        wait.until(ExpectedConditions.elementToBeClickable(CheckmyCreditBtn));
        driver.findElement(CheckmyCreditBtn).click();
        return new CreditCompletedPage(driver);
    }
}
