package net.sigmainfo.lf.automation.portal.pages.borrowerportal.income;

import net.sigmainfo.lf.automation.common.AbstractTests;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by shaishav.s on 27-09-2017.
 */
public class SpouseRentalPage extends AbstractTests {

    WebDriverWait wait = new WebDriverWait(driver,60);
    WebElement PropertyTypeListItem;

    private Logger logger = LoggerFactory.getLogger(SpouseRentalPage.class);

    public SpouseRentalPage(WebDriver driver) {
        this.driver = driver;
        logger.info("========= SpouseRentalPage is loaded===========");
    }

    public SpouseRentalPage() {}

    public static By SpouseMonthlyRentalTextBox = By.xpath("//div[starts-with(@id,'ID-')][17]/questioner-question-set/div[2]//input[@id='input']");
    public static By SpouseRentalPropertyAddressTextBox = By.xpath("//div[starts-with(@id,'ID-')][17]//ui-input[@id='AddressAutocomplete']");
    public static By SpouseTypeOfPropertyDropdown = By.xpath("//div[starts-with(@id,'ID-')][17]/questioner-question-set/div[4]//ui-options[@name='dobMonth']");
    public static By iAmAllDoneButton = By.xpath("//button[@name='next']");


    public SpouseSocialSecurityPage provideRentalIncomeDetails(String monthlyRentalIncome, String rentalPropertyAddress, String typeOfRentalProperty) throws Exception {
        enterMonthlyRentalIncome(SpouseMonthlyRentalTextBox,monthlyRentalIncome);
        enterRentalPropertyAddress(SpouseRentalPropertyAddressTextBox,rentalPropertyAddress);
        selectTypeOfProperty(SpouseTypeOfPropertyDropdown,typeOfRentalProperty);
        clickButton();
        return new SpouseSocialSecurityPage(driver);
    }

    private void enterRentalPropertyAddress(By locator, String currentAddress) throws InterruptedException {
        enterText(locator,currentAddress);
        Thread.sleep(3000);
        selectOptionWithText(currentAddress);
        Thread.sleep(3000);

    }

    private void selectOptionWithText(String currentAddress) {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='pac-item']//span[contains(text(),'"+currentAddress+"')]")));
        driver.findElement(By.xpath("//div[@class='pac-item']//span[contains(text(),'"+currentAddress+"')]")).click();
        logger.info("Selected address :" + currentAddress);
    }

    private void enterMonthlyRentalIncome(By locator,String monthlyRentalIncome) {
        enterText(locator,monthlyRentalIncome);
    }

    public void enterText(By locator,String text){
        Actions actions = new Actions(driver);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        actions.moveToElement(element);
        actions.click();
        actions.sendKeys(text);
        actions.build().perform();
        logger.info("Entering :"+text);
    }

    public void clickButton() throws Exception {
        wait.until(ExpectedConditions.elementToBeClickable(iAmAllDoneButton));
        driver.findElement(iAmAllDoneButton).click();
        logger.info("Clicking on I am done button");
    }

    private void selectTypeOfProperty(By locator, String typeOfRentalProperty) throws InterruptedException {
        driver.findElement(locator).click();
        for(int i=3;i<=6;i++){
            WebElement proeprtyType = driver.findElement(By.cssSelector("ui-options[name='dobMonth'] div[class*='select-wrapper'] li:nth-child("+i+")"));
            String text = proeprtyType.findElement(By.xpath("//div[starts-with(@id,'ID-')][17]//span[contains(text(),'"+typeOfRentalProperty+"')]")).getText();
            if(text.contains(typeOfRentalProperty)){
                proeprtyType.findElement(By.xpath("//div[starts-with(@id,'ID-')][17]//span[contains(text(),'"+typeOfRentalProperty+"')]")).click();
                break;
            }

        }
        logger.info("Selected :"+typeOfRentalProperty);
    }

}
