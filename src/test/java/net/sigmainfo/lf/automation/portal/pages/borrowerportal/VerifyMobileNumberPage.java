package net.sigmainfo.lf.automation.portal.pages.borrowerportal;

import net.sigmainfo.lf.automation.common.AbstractTests;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
/**
 * Created by           : Shaishav.s on 08-02-2017.
 * Test class           : VerifyMobileNumberPage.java
 * Includes             : 1. Objects on VerifyMobileNumberPage
 *                        2. Methods implementation on VerifyMobileNumberPage
 */
@Component
public class VerifyMobileNumberPage extends AbstractTests {

    private Logger logger = LoggerFactory.getLogger(VerifyMobileNumberPage.class);
    public VerifyMobileNumberPage(WebDriver driver){this.driver = driver;}
    public VerifyMobileNumberPage(){}

    public static By mobileNumberTextBox = By.xpath("//input[@type='text']");
    public static By sendCodeButton = By.xpath("//span[contains(text(),'SEND CODE')]");

    public VerifyAccessCodePage sendMobileNumber(WebDriver driver, String mobileNumber){
        WebDriverWait wait = new WebDriverWait(driver,30);
        logger.info("Entering mobile number for verification");
        Actions actions = new Actions(driver);
        WebElement emailTxtBox = driver.findElement(mobileNumberTextBox);
        actions.moveToElement(emailTxtBox);
        actions.click();
        actions.sendKeys(mobileNumber);
        actions.build().perform();
        driver.findElement(sendCodeButton).click();
        return new VerifyAccessCodePage(this.driver);
    }
}
