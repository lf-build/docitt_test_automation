package net.sigmainfo.lf.automation.portal.pages.borrowerportal.borrowerdashboard;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.constant.PortalParam;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.LoginPage;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.postapplication.DashbordTourPage;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.postapplication.PostApplicationPage;
import net.sigmainfo.lf.automation.portal.pages.lenderportal.LenderPipeLinePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by shaishav.s on 26-10-2017.
 */
public class BorrowerDashboardPage extends AbstractTests {

    PortalParam portalParam;
    WebDriverWait wait = new WebDriverWait(driver,60);
    private Logger logger = LoggerFactory.getLogger(DashbordTourPage.class);

    public BorrowerDashboardPage(WebDriver driver) {
        this.driver = driver;
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(),'Upcoming Events')]")));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(),'Conditions Met')]")));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(),'Members')]")));
        logger.info("BorrowerDashboardPage is loaded");

    }
    public BorrowerDashboardPage(){}

    public static By LoanId=By.xpath(".//*[@id='micro-app-host']/dashboard-view/div/div/div[1]/ul/li[4]/div/loan-detail/p[2]/span");
    public static By TotaloutStandingCount=By.xpath(".//*[@id='micro-app-host']/dashboard-view/div/div/div[1]/ul/li[1]/div/h3/outstanding-conditions-count/div");

    public static By totalConditions=By.xpath(".//*[@id='micro-app-host']/dashboard-view/div/div/div[3]/tab-group/div[1]/ul/li[1]/a/span[3]");
    public static By outStandingCount=By.xpath(".//*[@id='micro-app-host']/application-actions/div/div[2]/ul/li[1]/a/span[2]");
    public static By receivedCount=By.xpath(".//*[@id='micro-app-host']/application-actions/div/div[2]/ul/li[2]/a/span[2]");

    public static By monthName=By.xpath(".//*[@id='micro-app-host']/dashboard-view/div/div/div[3]/tab-group/div[1]/ul/li[3]/a/span[2]");
    public static By address=By.xpath(".//*[@id='micro-app-host']/dashboard-view/div/div/div[1]/ul/li[5]/div[2]/property-info/address");

    public static By FirstOutstandingItem=By.xpath(".//*[@id='outstandingList']/li[1]");

    public static By fileUploadArea=By.xpath(".//*[@id='docu-drop']/table/tbody/tr/td");
    public static By writeExplanantion = By.xpath(".//*[@id='componentHolder']/document-activities/div/ul/li[2]/a");
    public static By writeArea=By.xpath(".//*[@id='editor-container']/div[1]");
    public static By noIamFinishedBtn=By.xpath(".//*[@id='componentHolder']/document-activities/div/div[2]/button");
    public static By addressInfo=By.xpath(".//*[@class='style-scope property-info']");
    public static By profileDropdown = By.xpath("//div[@id='micro-app-host']/header-view//profile-summary/context-menu-list/div/a/i");
    public static By logoutLink = By.xpath("//div[@id='micro-app-host']/header-view//profile-summary//a[contains(text(),'Log out')]");
    public static By upcomingEventTab = By.xpath("//span[contains(text(),'Upcoming Events')]");

    public static By conditionsTab = By.xpath("//span[contains(text(),'Conditions Met')]");
    public static By calendarTab = By.xpath("//span[contains(text(),'Upcoming Events')]");
    public static By communicationTab = By.xpath("//span[contains(text(),'Communication')]");
    public static By outstandingConditionTab = By.xpath("//span[contains(text(),'Outstanding')]");
    public static By addedCustomConditionEC = By.xpath("//ul[@id='outstandingList']/li/a/div/h4[contains(text(),'EC')]");
    public static By addedCustomConditionGift = By.xpath("//ul[@id='outstandingList']/li/a/div/h4[contains(text(),'//ul[@id='outstandingList']/li/a/div/h4[contains(text(),'Gift Letter')]')]");

    public LoginPage verifyDashboardFunctionality() throws InterruptedException, AWTException {
        wait.until(ExpectedConditions.visibilityOfElementLocated(upcomingEventTab));
        verifyLoanId();
        verifyCurrentMonth();
        checktotalConditionCount();
        uploadDocumentandExplaination();
        verifyAddress();
        logout();
        return new LoginPage(driver);
    }

    private void logout(){
        wait.until(ExpectedConditions.presenceOfElementLocated(profileDropdown));
        driver.findElement(profileDropdown).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(logoutLink));
        driver.findElement(logoutLink).click();
        logger.info("Logging out of borrower dashboard");
    }

    private void verifyAddress() {
        String addressFromUI=driver.findElement(addressInfo).getText();
        addressFromUI=addressFromUI.split(",")[0];

        if(addressFromUI.equals(PortalParam.firstAddress)){
            logger.info("Address is same");
        }else{
            logger.error("Address is not same");
        }



    }
    private void verifyLoanId(){
        wait.until(ExpectedConditions.urlContains("http://docitt.qa.lendfoundry.com:9005/#/dashboard/id:"));
        String URL=driver.getCurrentUrl();
        String LoanIDfromURL[]=URL.split("id:");
        String loanIDfromUI[]=driver.findElement(LoanId).getText().split(":");
        if(LoanIDfromURL[1].equals(loanIDfromUI[1].trim())){
            logger.info("Loan Id :"+ LoanIDfromURL[1]+" verified");
        }else{
            logger.error("Loan Id :"+ LoanIDfromURL[1]+" is not same");
        }
    }


    private void verifyCurrentMonth(){
        Calendar cal = Calendar.getInstance();
        String month=new SimpleDateFormat("MMMMMMM").format(cal.getTime());
        if(month.equals(driver.findElement(monthName).getText())){
            logger.info("Current month verified");
        }else{
            logger.error("Month name does not match");
        }
    }

    private void checktotalConditionCount(){
        String OutStandingCount=driver.findElement(outStandingCount).getText().replaceAll("[()]", "");
        String ReceivedCount=driver.findElement(receivedCount).getText().replaceAll("[()]", "");

        int totalCount=Integer.parseInt(OutStandingCount)+Integer.parseInt(ReceivedCount);

        String value=driver.findElement(totalConditions).getText();
        int  ConditionCount=Integer.parseInt(value.split("of")[1].split("Conditions")[0].trim());

        if(totalCount==ConditionCount){
            logger.info("Total Conditions matches");
        }else{
            logger.error("Total Conditions does not match");
        }
    }

    private void uploadDocumentandExplaination() throws InterruptedException, AWTException{
        WebDriverWait wait= new WebDriverWait(driver, 40);


        //Get Older conditions count
        String OldOutStandingCount=driver.findElement(outStandingCount).getText().replaceAll("[()]", "");
        String OldOutStandingCountHeader=driver.findElement(TotaloutStandingCount).getText();
        String OldReceivedCount=driver.findElement(receivedCount).getText().replaceAll("[()]", "");

        wait.until(ExpectedConditions.elementToBeClickable(FirstOutstandingItem));
        driver.findElement(FirstOutstandingItem).click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(fileUploadArea));
        PortalFuncUtils.scrollToElementandClick(driver, fileUploadArea);

        Thread.sleep(3000);

        PostApplicationPage.fileupload();

        Thread.sleep(3000);

        PortalFuncUtils.scrollPageUp(driver);
        driver.findElement(writeExplanantion).click();



        wait.until(ExpectedConditions.visibilityOfElementLocated(writeArea));
        driver.findElement(writeArea).sendKeys(PortalParam.ExplanationSummary);


        PortalFuncUtils.scrollToElementandClick(driver, noIamFinishedBtn);

        Thread.sleep(3000);

        //Get New Count
        String NewOutStandingCount=driver.findElement(outStandingCount).getText().replaceAll("[()]", "");
        String NewReceivedCount=driver.findElement(receivedCount).getText().replaceAll("[()]", "");

        String NewStandingCountinHeader=driver.findElement(TotaloutStandingCount).getText();

        if(Integer.parseInt(NewOutStandingCount)==(Integer.parseInt(OldOutStandingCount)-1)){
            logger.info("OutStanding Count matches");
        }else{
            logger.error("OutStanding Count not  matches");
        }
        if(Integer.parseInt(NewStandingCountinHeader)==(Integer.parseInt(OldOutStandingCountHeader)-1)){
            logger.info("OutStanding Header Count matches");
        }else{
            logger.error("OutStanding Header Count not  matches");
        }


        if(Integer.parseInt(NewReceivedCount)==(Integer.parseInt(OldReceivedCount)+1)){
            logger.info("Received  Count matches");
        }else{
            logger.error("Received Count not  matches");
        }

    }


    public LoginPage verifyEvent() throws InterruptedException {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(),'1 Upcoming Events')]")));
        driver.findElement(upcomingEventTab).click();
        portalFuncUtils.scrollToElement(driver, LenderPipeLinePage.tomorrowDateCell);
        Thread.sleep(3000);
        /*wait.until(ExpectedConditions.presenceOfElementLocated(LenderPipeLinePage.tomorrowDateCell));
        wait.until(ExpectedConditions.visibilityOfElementLocated(LenderPipeLinePage.tomorrowDateCell));*/
        driver.findElement(LenderPipeLinePage.tomorrowDateCell).click();
        Assert.assertTrue(driver.findElement(By.xpath("//ul[@id='calContent']/li/div[contains(text(),'Description')]")).isDisplayed(),"Created event description is not visible");
        Thread.sleep(3000);
        driver.findElement(By.xpath("//ui-modal[@id='fullCalModal']/div[@id='modal']/div/div/div[1]/button/span")).click();
        verifyCustomCondition();
        logout();
        return new LoginPage(driver);
    }

    private void verifyCustomCondition() throws InterruptedException {
        Thread.sleep(2500);
        portalFuncUtils.scrollOnTopOfThePage(driver);
        wait.until(ExpectedConditions.presenceOfElementLocated(conditionsTab));
        wait.until(ExpectedConditions.elementToBeClickable(conditionsTab));
        driver.findElement(conditionsTab).click();
        Thread.sleep(2500);
        String totalConditions[] = driver.findElement(conditionsTab).getText().split("of");
        int metCondition = Integer.parseInt(totalConditions[0].trim());
        String receivedConditions[] = totalConditions[1].split("Conditions");
        int rcvdConditions = Integer.parseInt(receivedConditions[0].trim());
        boolean ecVerified = false;
        boolean giftVerified = false;
        for(int i=1;i<=(rcvdConditions-metCondition);i++)
        {
            WebElement conditionList = driver.findElement(By.cssSelector("ul[id='outstandingList'] li:nth-child("+i+")"));
            Thread.sleep(2500);
            if(!ecVerified) {
                if (driver.findElement(By.xpath("//ul[@id='outstandingList']/li[" + i + "]/a/div/h4")).getText().contains("EC")) {
                    Assert.assertTrue(conditionList.findElement(By.xpath("//a/div/h4[contains(text(),'EC')]")).isDisplayed());
                    ecVerified = true;
                }
            }
            if(!ecVerified) {
                if (driver.findElement(By.xpath("//ul[@id='outstandingList']/li[" + i + "]/a/div/h4")).getText().contains("Gift")) {
                    Assert.assertTrue(conditionList.findElement(By.xpath("//a/div/h4[contains(text(),'Gift')]")).isDisplayed());
                    giftVerified = true;
                }
            }
            if(ecVerified && giftVerified)
            break;
        }
    }
}
