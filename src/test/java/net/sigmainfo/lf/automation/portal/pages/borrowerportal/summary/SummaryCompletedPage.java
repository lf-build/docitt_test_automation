package net.sigmainfo.lf.automation.portal.pages.borrowerportal.summary;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.credit.CreditScorePage;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.postapplication.BeginPostApplicationPage;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.postapplication.PostApplicationPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by shaishav.s on 20-09-2017.
 */
public class SummaryCompletedPage extends AbstractTests {
    private Logger logger = LoggerFactory.getLogger(SummaryPage.class);
    WebDriverWait wait = new WebDriverWait(driver,60);

    SummaryCompletedPage(){}



    public SummaryCompletedPage(WebDriver driver){
        logger.info("SummaryCompletedPage is loaded");
        this.driver=driver;
    }
    
    public static By BeginSectionButton = By.xpath("//ui-button[@id='goNextSection']/button");

    public BeginPostApplicationPage submitApplication()
    {
        wait.until(ExpectedConditions.presenceOfElementLocated(BeginSectionButton));
        wait.until(ExpectedConditions.elementToBeClickable(BeginSectionButton));
        driver.findElement(BeginSectionButton).click();
        logger.info("Application is submitted.");
        return new BeginPostApplicationPage(driver);
    }
    
    



}
