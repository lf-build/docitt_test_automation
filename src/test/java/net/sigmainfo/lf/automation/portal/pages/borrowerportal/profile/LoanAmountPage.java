package net.sigmainfo.lf.automation.portal.pages.borrowerportal.profile;

import net.sigmainfo.lf.automation.common.AbstractTests;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertTrue;

/**
 * Created by shaishav.s on 22-08-2017.
 */
public class LoanAmountPage extends AbstractTests {

    private Logger logger = LoggerFactory.getLogger(LoanAmountPage.class);

    public LoanAmountPage(WebDriver driver){
        this.driver = driver;
        logger.info("========= LoanAmountPage page is loaded===========");
        assertTrue(driver.findElement(By.xpath("//div[@id='SS3']/h3[contains(text(),\""+portalParam.firstName+", tell us about the loan you want\")]")).isDisplayed());
        assertTrue(driver.findElement(By.cssSelector("div[id='SS3'] questioner-question-set[class*='sub-sections'] div:nth-child(2) div[class*=col-md-12] questioner-question[class*=style-scope]")).getText().contains("Purchase"));
    }
    public LoanAmountPage(){}

    WebDriverWait wait = new WebDriverWait(driver,60);

    public static By PurchasePriceTextbox = By.xpath("//ui-input[@id='purchasePrice']/div/div[1]/input");
    public static By DownPaymentTextbox = By.xpath("//ui-input[@id='downPayment']/div/div[1]/input");
    public static By LoanPercentageTextBox = By.xpath("//ui-input[@id='loanPercentage']/div/div[1]/input");
    public static By LoanAmountTextBox = By.xpath("//ui-input[@id='loanAmount']/div/div[1]/input");
    public static By NextButton = By.xpath("//button[contains(text(),'Next')]");

    public void enterText(By locator,String text){
        Actions actions = new Actions(driver);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        actions.moveToElement(element);
        actions.click();
        actions.sendKeys(text);
        actions.build().perform();

    }


    public WhosOnLoanPage enterLoanAmountDetails(String purchasePrice, String downPayment) throws Exception {
        enterPurchasePrice(purchasePrice);
        enterDownPayment(downPayment);
        verifyLoanAmount();
        clickNext();
        return new WhosOnLoanPage(driver);
    }

    private void enterPurchasePrice(String purchasePrice) {
        enterText(PurchasePriceTextbox,purchasePrice);
        logger.info("Entering Purchase Price :"+purchasePrice);
    }

    private void enterDownPayment(String downPayment) {
        enterText(DownPaymentTextbox,downPayment);
        logger.info("Entering Down Payment :"+downPayment);
    }

    public void clickNext() throws Exception {
        wait.until(ExpectedConditions.elementToBeClickable(NextButton));
        driver.findElement(NextButton).click();
        logger.info("Clicking on NEXT button");
    }

    public void verifyLoanAmount() {
        assertEquals(Integer.parseInt(driver.findElement(LoanPercentageTextBox).getAttribute("value")),20);
        assertEquals(driver.findElement(LoanAmountTextBox).getAttribute("value"),"$ 400,000");
    }
}
