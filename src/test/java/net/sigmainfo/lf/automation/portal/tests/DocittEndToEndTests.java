package net.sigmainfo.lf.automation.portal.tests;

import java.io.IOException;

import net.sigmainfo.lf.automation.portal.constant.PortalParam;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.*;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.assets.AssetsPage;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.assets.BankAndInvestmentPage;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.assets.BankInformationPage;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.assets.SavingAndInvestmentPage;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.borrowerdashboard.BorrowerDashboardPage;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.credit.CreditCompletedPage;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.credit.CreditScorePage;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.credit.SpouseCreditScorePage;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.declaration.DeclarationPage;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.declaration.DeclarationSectionCompletePage;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.declaration.SpouseDeclarationPage;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.income.*;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.postapplication.BeginPostApplicationPage;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.postapplication.DashbordTourPage;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.postapplication.PostApplicationPage;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.profile.*;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.summary.AgreementTermsPage;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.summary.SummaryCompletedPage;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.summary.SummaryPage;
import net.sigmainfo.lf.automation.portal.pages.lenderportal.LenderLoginPage;
import net.sigmainfo.lf.automation.portal.pages.lenderportal.LenderPipeLinePage;
import org.json.JSONException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.common.CaptureScreenshot;
import net.sigmainfo.lf.automation.common.TestResults;

/**
 * Created by           : Shaishav.s
 * Test class           : DocittEndToEndTests.java
 * Includes             : 1. Verified end to end flow of pre application
 *                        
 */

@Listeners(CaptureScreenshot.class)
public class DocittEndToEndTests extends AbstractTests{
	
	private Logger logger = LoggerFactory.getLogger(DocittEndToEndTests.class);
    public static String funcMod="Docitt_Portal";
   // public WebDriver driver;

    @Autowired
    TestResults testResults;
    
    @Autowired
	SignUpPage signUpPage;

    @Autowired
	LenderLoginPage lenderLoginPage;

   	 	@AfterClass(alwaysRun=true)
	    public void endCasereport() throws IOException, JSONException {

	        String funcModule = "Docitt_Portal";
	        String className = org.apache.commons.lang.StringUtils.substringAfterLast(this.getClass().getName(), ".");
	        String description = "Verify  "+ org.apache.commons.lang.StringUtils.substringAfterLast(this.getClass().getName(), ".");
	        generateReport(className, description,funcModule);
	    }
	 
	 	/*@Test(priority=1,description = "", groups = {"verifyEndToEndFlow"})
	 	public void borrowerEndToEndFlow() throws Exception {
	 		String sTestID = "verifyEndToEndFlow";
	        String result = "Failed";
	        WebDriverWait wait = new WebDriverWait(driver,60);
			EligibilityPage eligibilityPage = null;
	        String mainTab=null;
	        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
	        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
	        initializeData(funcMod,sTestID);
	 		try
	 		{
				signUpPage = new SignUpPage(driver);
				wait.until(ExpectedConditions.elementToBeClickable(signUpPage.loginButton));
				Thread.sleep(5000);
				LoginPage loginPage= signUpPage.clickOnSignInButton(driver);
				wait.until(ExpectedConditions.visibilityOfElementLocated(loginPage.signInButton));
				WelcomePage welcomePage = loginPage.loginToDocitt(portalParam.username, portalParam.password);
	 			WelcomeVideoPage welcomeVideoPage = welcomePage.clickPurposeButton("Purchase");
	 			LetsGetStartedPage letsGetStartedPage = welcomeVideoPage.clickGetStartedButton(driver);
	 			AccountInformationPage accountInformationPage = letsGetStartedPage.clickBeginSectionButton();
				wait.until(ExpectedConditions.presenceOfElementLocated(accountInformationPage.progressBarLabel));
				accountInformationPage.verifyAccountInformationPageUI();
				PurchasePropertyDetailsPage purchasePropertyDetailsPage = accountInformationPage.enterAccountInfo(portalParam.firstName, portalParam.middleName, portalParam.lastName, portalParam.suffix, portalParam.preferredEmail, portalParam.phone, portalParam.modeOfCommunication);
				LoanAmountPage loanAmountPage = purchasePropertyDetailsPage.enterPurchasePropertyDetails(portalParam.propertyType,portalParam.isProperty,portalParam.isContract,portalParam.propertyUse,portalParam.firstAddress,portalParam.city,portalParam.state,portalParam.zip);
				WhosOnLoanPage whosOnLoanPage = loanAmountPage.enterLoanAmountDetails(portalParam.purchasePrice,portalParam.downPayment);
				eligibilityPage = whosOnLoanPage.selectApplicant(portalParam.married, portalParam.addSpouce, portalParam.addCoborrwer, portalParam.addDependant);
				RealEstateAgentPage realEstateAgentPage = eligibilityPage.selectLoanInfo(portalParam.eligibleLoan,portalParam.currentLoan);
				LoanOfficerPage loanOfficerPage = realEstateAgentPage.selectAgentInfo(portalParam.isRealEstateAgent());
				CurrentResidenceDetailsPage currentResidenceDetailsPage = loanOfficerPage.enterLoanOfficerDetail(portalParam.withLoanOfficer,portalParam.loanOfficerName);
				RealEstatePage realEstatePage = currentResidenceDetailsPage.enterCurrentResidenceDetails(portalParam.sameResidence,portalParam.residenceStatus,portalParam.currentAddress,portalParam.stayingSince,portalParam.planningToSale);
				ProfileCompletedPage profileCompletedPage = realEstatePage.enterRealEstateDetails(portalParam.additionalProperty,portalParam.propertyAddress,portalParam.propertyType,portalParam.propertyStatus,portalParam.propertyValue,portalParam.rentalIncome,portalParam.expenses);
				AssetsPage assetsPage = profileCompletedPage.clickBeginSectionBtn();
				SavingAndInvestmentPage savingAndInvestmentPage= assetsPage.clickEnterManually();
				BankAndInvestmentPage bankAndInvestmentPage = savingAndInvestmentPage.enterManualAssetsDetails(portalParam.institutionName,portalParam.accountType,portalParam.currentBalance,portalParam.accountNumber,portalParam.accountName);
				//bankAndInvestmentPage.validateAccountInfo(portalParam.institutionName,portalParam.accountType,portalParam.currentBalance,portalParam.accountNumber,portalParam.accountName);
				BankInformationPage bankInformationPage = bankAndInvestmentPage.addInstitution();
				BankAndInvestmentPage bankAndInvestmentPage1 = bankInformationPage.fillAccountCredentials(portalParam.plaidUsername,portalParam.plaidPassword,portalParam.plaidPin);
				//bankAndInvestmentPage1.validateAccountInfo();
				AssetCompletedPage assetCompletedPage = bankAndInvestmentPage1.clickSubmit();
				IncomeSearchPage incomeSearchPage = assetCompletedPage.clickBeginSectionBtn();
				IncomeTypesPage incomeTypesPage = incomeSearchPage.enterIncomeSearchDetails(portalParam.dateOfBirth,portalParam.ssnNumber);
				BorrowerEmploymentPage borrowerEmploymentPage = incomeTypesPage.enterTypesOfIncome();
				BorrowerEmpAddressPage borrowerEmpAddressPage = borrowerEmploymentPage.enterBorrowerEmploymentDetails(portalParam.currentEmployer,portalParam.employmentTitle,portalParam.employmentStartDate,portalParam.inThisLineYear,portalParam.inThisLineMonth,portalParam.monthlyBaseSalary,portalParam.bonusAmount);
				AlimonyChildIncomePage alimonyChildIncomePage = borrowerEmpAddressPage.enterBorrowerEmpAddressDetails(portalParam.businessAddress,portalParam.businessPhone);
				AlimonyChildSupportPage alimonyChildSupportPage = alimonyChildIncomePage.enterAlimonyIncomeDetails(portalParam.monthlyAlimony,portalParam.alimonyStartDate,portalParam.monthlyChildSupport,portalParam.childName,portalParam.childDoB);
				BusinessSelfEmploymentPage businessSelfEmploymentPage = alimonyChildSupportPage.enterAlimonySupportDetails(portalParam.supportByCourt,portalParam.willReceiveOrderByCourt,portalParam.twoMonthSupport);
				MilitaryPage militaryPage = businessSelfEmploymentPage.provideSelfEmploymentDetails(portalParam.selfEmploymentMonthlyIncome,portalParam.selfEmploymentCompanyName,portalParam.selfEmploymentTitle,portalParam.selfEmploymentBusinessAddress,portalParam.selfEmploymentBusinessPhone,portalParam.selfEmploymentBusinessStartDate,portalParam.percentageOwnership,portalParam.typeOfCompany);
				RentalPage rentalPage = militaryPage.enterMilitaryIncomeDetails(portalParam.monthlyMilitaryPay);
				SocialSecurityPage socialSecurityPage = rentalPage.enterRentalIncomeDetails(portalParam.monthlyRentalIncome,portalParam.rentalPropertyAddress,portalParam.typeOfRentalProperty);
				InterestDividendPage interestDividendPage = socialSecurityPage.enterSocialSecurityIncomeDetails(portalParam.socialSecurityIncome);
				OtherIncomePage otherIncomePage = interestDividendPage.enterInterestDividendIncomeDetails(portalParam.interestLastYear,portalParam.interestPreviousYear);
				IncomeSectionCompletePage incomeSectionCompletePage = otherIncomePage.enterOtherIncomeDetails(portalParam.otherIncomePerMonth,portalParam.sourceOfIncome,portalParam.continuousIncome);
				DeclarationPage declarationPage = incomeSectionCompletePage.clickBeginSectionBtn();
				DeclarationSectionCompletePage declarationSectionCompletePage = declarationPage.fillDeclarationInfo();
				CreditScorePage creditScorePage = declarationSectionCompletePage.clickBeginSectionBtn();
				CreditCompletedPage creditCompletedPage = creditScorePage.checkCredit();
				SummaryPage summaryPage = creditCompletedPage.clickBeginSectionBtn();
				AgreementTermsPage agreementTermsPage = summaryPage.verifySummaryInfo();
				SummaryCompletedPage summaryCompletedPage = agreementTermsPage.provideConsent();
				result="Pass";
				
	 		}catch(Exception e) {

	 	            e.printStackTrace();
	 	            logger.info("******************" + sTestID +  "  failed. *****\n" +e);
	 	        }

	 	        finally {
	 	            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
	 	            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
	 	            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
	 	            if(result.equalsIgnoreCase("Failed"))
	 	                Assert.fail();
	 	        }
	 	}*/


	@Test(priority=1,description = "", groups = {"verifyPurchaseWorkflow","Sanity"})
	public void verifyPurchaseWorkflow() throws Exception {
		String sTestID = "verifyPurchaseWorkflow";
		String result = "Failed";
		WebDriverWait wait = new WebDriverWait(driver,60);
		String mainTab=null;
		AssetCompletedPage assetCompletedPage;
		IncomeSectionCompletePage incomeSectionCompletePage = null;
		testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
		logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
		initializeData(funcMod,sTestID);
		try
		{
			lenderLoginPage = new LenderLoginPage(driver);
			LenderPipeLinePage lenderPipeLinePage = lenderLoginPage.loginToLenderPortal(portalParam.lenderUserLogin,portalParam.encryptedPassword);
			lenderPipeLinePage.inviteBorrower(portalParam.borrowerEmail);
			LenderLoginPage lenderLoginPage = lenderPipeLinePage.logoutLender(portalParam.lenderUserLogin);
			mainTab = driver.getWindowHandle();
			String referenceId = getReferenceId(portalParam.borrowerEmail);
			SignUpPage signUpPage = signUpAndLogin(driver,mainTab,referenceId);
			WelcomePage welcomePage = signUpPage.signUp(portalParam.borrowerEmail,portalParam.encryptedPassword);
			/*signUpPage = new SignUpPage(driver);
			wait.until(ExpectedConditions.elementToBeClickable(signUpPage.loginButton));
			Thread.sleep(5000);
			LoginPage loginPage= signUpPage.clickOnSignInButton(driver);
			wait.until(ExpectedConditions.visibilityOfElementLocated(loginPage.signInButton));
			WelcomePage welcomePage = loginPage.loginToDocitt("shaishav.sigma+1649144940@gmail.com", portalParam.encryptedPassword);*/
			WelcomeVideoPage welcomeVideoPage = welcomePage.clickPurposeButton("Purchase");
			LetsGetStartedPage letsGetStartedPage = welcomeVideoPage.clickGetStartedButton(driver);
			AccountInformationPage accountInformationPage = letsGetStartedPage.clickBeginSectionButton();
			wait.until(ExpectedConditions.presenceOfElementLocated(accountInformationPage.progressBarLabel));
			accountInformationPage.verifyAccountInformationPageUI();
			PurchasePropertyDetailsPage purchasePropertyDetailsPage = accountInformationPage.enterPurchaseAccountInfo(portalParam.firstName, portalParam.middleName, portalParam.lastName, portalParam.suffix, portalParam.phone, portalParam.modeOfCommunication);
			LoanAmountPage loanAmountPage = purchasePropertyDetailsPage.enterPurchasePropertyDetails(portalParam.propertyType,portalParam.isProperty,portalParam.isContract,portalParam.propertyUse,portalParam.firstAddress,portalParam.city,portalParam.state,portalParam.zip);
			WhosOnLoanPage whosOnLoanPage = loanAmountPage.enterLoanAmountDetails(portalParam.purchasePrice,portalParam.downPayment);
			SpouseInformationPage spouseInformationPage = whosOnLoanPage.selectApplicants(portalParam.married, portalParam.addSpouce, portalParam.addCoborrower, portalParam.addDependant);
			NonSpouseInformationPage nonSpouseInformationPage = spouseInformationPage.enterSpouseDetails(portalParam.spouceFirstName,portalParam.spouceMiddleName,portalParam.spouceLastName,portalParam.spouceSuffix,portalParam.spouseEmail,portalParam.spoucePhone,portalParam.spouceComm);
			ApplicantInformationPage applicantInformationPage = nonSpouseInformationPage.enterNonSpouseDetails(portalParam.NonSpouseFirstName,portalParam.NonSpouseMiddleName,portalParam.NonSpouseLastName,portalParam.NonSpouseSuffix,portalParam.nonSpouseEmail,portalParam.NonSpousePhone);
			EligibilityPage eligibilityPage = applicantInformationPage.verifyApplicants();
			RealEstateAgentPage realEstateAgentPage = eligibilityPage.selectLoanInfo(portalParam.eligibleLoan,portalParam.currentLoan);
			LoanOfficerPage loanOfficerPage = realEstateAgentPage.selectAgentInfo(portalParam.isRealEstateAgent());
			CurrentResidenceDetailsPage currentResidenceDetailsPage = loanOfficerPage.enterLoanOfficerDetail(portalParam.withLoanOfficer,portalParam.loanOfficerName);
			RealEstatePage realEstatePage = currentResidenceDetailsPage.enterCurrentResidenceDetails(portalParam.sameResidence,portalParam.residenceStatus,portalParam.currentAddress,portalParam.stayingSince,portalParam.planningToSale);
			ProfileCompletedPage profileCompletedPage = realEstatePage.enterRealEstateDetails(portalParam.additionalProperty,portalParam.propertyAddress,portalParam.propertyType,portalParam.propertyStatus,portalParam.propertyValue,portalParam.rentalIncome,portalParam.expenses);
			AssetsPage assetsPage = profileCompletedPage.clickBeginSectionBtn();
			SavingAndInvestmentPage savingAndInvestmentPage= assetsPage.clickEnterManually();
			BankAndInvestmentPage bankAndInvestmentPage = savingAndInvestmentPage.enterManualAssetsDetails(portalParam.institutionName,portalParam.accountType,portalParam.currentBalance,portalParam.accountNumber,portalParam.accountName);
			//bankAndInvestmentPage.validateAccountInfo(portalParam.institutionName,portalParam.accountType,portalParam.currentBalance,portalParam.accountNumber,portalParam.accountName);
			BankInformationPage bankInformationPage = bankAndInvestmentPage.addInstitution();
			BankAndInvestmentPage bankAndInvestmentPage1 = bankInformationPage.fillAccountCredentials(portalParam.plaidUsername,portalParam.plaidPassword,portalParam.plaidPin);
			//bankAndInvestmentPage1.validateAccountInfo();

			if(!portalParam.addSpouce) {
				assetCompletedPage = bankAndInvestmentPage1.clickSubmit();
			}
			else
			{
				AssetsPage assetsPage1 = bankAndInvestmentPage1.clickSubmit(portalParam.addSpouce);
				SavingAndInvestmentPage savingAndInvestmentPage1 = assetsPage1.clickEnterManually();
				BankAndInvestmentPage bankAndInvestmentPage2 = savingAndInvestmentPage1.enterManualAssetsDetails(portalParam.spouseInstitutionName,portalParam.spouseAccountType,portalParam.spouseCurrentBalance,portalParam.spouseAccountNumber,portalParam.spouseAccountName);
				BankInformationPage bankInformationPage1 = bankAndInvestmentPage2.addInstitution();
				BankAndInvestmentPage bankAndInvestmentPage3 = bankInformationPage1.fillAccountCredentials(portalParam.plaidUsername,portalParam.plaidPassword,portalParam.plaidPin);
				assetCompletedPage = bankAndInvestmentPage3.clickSubmit();
			}
			IncomeSearchPage incomeSearchPage = assetCompletedPage.clickBeginSectionBtn();
			IncomeTypesPage incomeTypesPage = incomeSearchPage.enterIncomeSearchDetails(portalParam.dateOfBirth,portalParam.ssnNumber);
			BorrowerEmploymentPage borrowerEmploymentPage = incomeTypesPage.enterTypesOfIncome();
			BorrowerEmpAddressPage borrowerEmpAddressPage = borrowerEmploymentPage.enterBorrowerEmploymentDetails(portalParam.currentEmployer,portalParam.employmentTitle,portalParam.employmentStartDate,portalParam.inThisLineYear,portalParam.inThisLineMonth,portalParam.monthlyBaseSalary,portalParam.bonusAmount);
			AlimonyChildIncomePage alimonyChildIncomePage = borrowerEmpAddressPage.enterBorrowerEmpAddressDetails(portalParam.businessAddress,portalParam.businessPhone);
			AlimonyChildSupportPage alimonyChildSupportPage = alimonyChildIncomePage.enterAlimonyIncomeDetails(portalParam.monthlyAlimony,portalParam.alimonyStartDate,portalParam.monthlyChildSupport,portalParam.childName,portalParam.childDoB);
			BusinessSelfEmploymentPage businessSelfEmploymentPage = alimonyChildSupportPage.enterAlimonySupportDetails(portalParam.supportByCourt,portalParam.willReceiveOrderByCourt,portalParam.twoMonthSupport);
			MilitaryPage militaryPage = businessSelfEmploymentPage.provideSelfEmploymentDetails(portalParam.selfEmploymentMonthlyIncome,portalParam.selfEmploymentCompanyName,portalParam.selfEmploymentTitle,portalParam.selfEmploymentBusinessAddress,portalParam.selfEmploymentBusinessPhone,portalParam.selfEmploymentBusinessStartDate,portalParam.percentageOwnership,portalParam.typeOfCompany);
			RentalPage rentalPage = militaryPage.enterMilitaryIncomeDetails(portalParam.monthlyMilitaryPay);
			SocialSecurityPage socialSecurityPage = rentalPage.enterRentalIncomeDetails(portalParam.monthlyRentalIncome,portalParam.rentalPropertyAddress,portalParam.typeOfRentalProperty);
			InterestDividendPage interestDividendPage = socialSecurityPage.enterSocialSecurityIncomeDetails(portalParam.socialSecurityIncome);
			OtherIncomePage otherIncomePage = interestDividendPage.enterInterestDividendIncomeDetails(portalParam.interestLastYear,portalParam.interestPreviousYear);
			if(!portalParam.addSpouce) {
				incomeSectionCompletePage = otherIncomePage.enterOtherIncomeDetails(portalParam.otherIncomePerMonth, portalParam.sourceOfIncome, portalParam.continuousIncome);
			}
			if(portalParam.addSpouce) {
				SpouseIncomeSearchPage spouseIncomeSearchPage = otherIncomePage.provieOtherIncomeDetails(portalParam.otherIncomePerMonth, portalParam.sourceOfIncome, portalParam.continuousIncome);
				SpouseIncomeTypePage spouseIncomeTypePage = spouseIncomeSearchPage.enterSpouseIncomeSearchDetails(portalParam.spouseDateOfBirth, portalParam.spouseSsnNumber);
				SpouseEmploymentPage spouseEmploymentPage = spouseIncomeTypePage.provideTypesOfIncome();
				SpouseEmpAddressPage spouseEmpAddressPage = spouseEmploymentPage.provideSpouseEmploymentDetails(portalParam.spouseEmployer, portalParam.spouseTitle, portalParam.spouseEmpStartDate, portalParam.inThisLineYear, portalParam.inThisLineMonth, portalParam.spouseMonthlybaseSalary, portalParam.spouseBonusAmount);
				SpouseAlimonyChildIncomePage spouseAlimonyChildIncomePage = spouseEmpAddressPage.provideSpouseEmpAddressDetails(portalParam.spouseEmpAddress, portalParam.businessPhone);
				SpouseAlimonyChildSupportPage spouseAlimonyChildSupportPage = spouseAlimonyChildIncomePage.provideSpouseAlimonyIncomeDetails(portalParam.monthlyAlimony, portalParam.alimonyStartDate, portalParam.monthlyChildSupport, portalParam.spouseAlimonyChildName, portalParam.spouseAlimonyChildDoB);
				SpouseBusinessSelfEmploymentPage spouseBusinessSelfEmploymentPage = spouseAlimonyChildSupportPage.provideChildSupportDetails(portalParam.spouseSupportByCourt, portalParam.SpouseWillReceiveOrderByCourt, portalParam.spouseTwoMonthSupport);
				SpouseMilitaryPage spouseMilitaryPage = spouseBusinessSelfEmploymentPage.provideSpouseBusinessDetails(portalParam.spouseBusinessMonthlyIncome, portalParam.spouseBusinessCompanyName, portalParam.spouseBusinessTitle, portalParam.spouseBusinessAddress, portalParam.spouseBusinessPhone, portalParam.spouseBusinessStartDate, portalParam.spousePercentOwnership, portalParam.spouseTypeOfCompany);
				SpouseRentalPage spouseRentalPage = spouseMilitaryPage.provideMilitaryIncomeDetails(portalParam.monthlyMilitaryPay);
				SpouseSocialSecurityPage spouseSocialSecurityPage = spouseRentalPage.provideRentalIncomeDetails(portalParam.monthlyRentalIncome, portalParam.spouseRentalPropertyAddress, portalParam.typeOfRentalProperty);
				SpouseInterestDividendPage spouseInterestDividendPage = spouseSocialSecurityPage.provideSocialSecurityIncomeDetails(portalParam.socialSecurityIncome);
				SpouseOtherIncomePage spouseOtherIncomePage = spouseInterestDividendPage.provideOtherIncomeDetails(portalParam.interestLastYear, portalParam.interestPreviousYear);
				incomeSectionCompletePage = spouseOtherIncomePage.provideOtherIncomeDetails(portalParam.otherIncomePerMonth, portalParam.sourceOfIncome, portalParam.continuousIncome);

			}
			DeclarationPage declarationPage = incomeSectionCompletePage.clickBeginSectionBtn();
			SpouseDeclarationPage spouseDeclarationPage = declarationPage.fillDeclarationInfo();
			DeclarationSectionCompletePage declarationSectionCompletePage = spouseDeclarationPage.fillDeclarationInfo();
			CreditScorePage creditScorePage = declarationSectionCompletePage.clickBeginSectionBtn();
			SpouseCreditScorePage spouseCreditScorePage = creditScorePage.checkCredit();
			CreditCompletedPage creditCompletedPage = spouseCreditScorePage.checkCredit();
			SummaryPage summaryPage = creditCompletedPage.clickBeginSectionBtn();
			AgreementTermsPage agreementTermsPage = summaryPage.verifySummaryInfo();
			SummaryCompletedPage summaryCompletedPage = agreementTermsPage.provideConsent();
			BeginPostApplicationPage beginPostApplicationPage=summaryCompletedPage.submitApplication();
			PostApplicationPage postApplicationPage = beginPostApplicationPage.clickBeginPostApplication();
			DashbordTourPage dashbordTourPage = postApplicationPage.fillingPostApp();
			BorrowerDashboardPage borrowerDashboardPage = dashbordTourPage.navigateToDashboard();
			String loanid[] = driver.getCurrentUrl().split("id:");
			LoginPage loginPage1 = borrowerDashboardPage.verifyDashboardFunctionality();
			driver.get(portalParam.lenderurl);
			lenderLoginPage = new LenderLoginPage(driver);
			//String loanid = "DOC001307";
			LenderPipeLinePage lenderPipeLinePage1 = lenderLoginPage.loginToLenderPortal(portalParam.lenderUserLogin,portalParam.encryptedPassword);
			LenderLoginPage lenderLoginPage2 = lenderPipeLinePage1.createEventAndCondition(loanid[1]);
			driver.get(portalParam.borrowerurl);
			LoginPage loginPage2 = new LoginPage(driver);
			BorrowerDashboardPage borrowerDashboardPage1 = loginPage2.loginToDashboard(portalParam.borrowerEmail,portalParam.encryptedPassword);
			LoginPage loginPage3 = borrowerDashboardPage1.verifyEvent();
			result="Passed";
		}catch(Exception e) {

			e.printStackTrace();
			logger.info("******************" + sTestID +  "  failed. *****\n" +e);
		}

		finally {
			logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
			testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
			writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
			if(result.equalsIgnoreCase("Failed"))
				Assert.fail();
		}
	}

	@Test(priority=1,description = "", groups = {"verifyRefinanceWorkflow","Sanity"})
	public void VerifyRefinanceWorkflow() throws Exception {
		String sTestID = "verifyRefinanceWorkflow";
		String result = "Failed";
		WebDriverWait wait = new WebDriverWait(driver,60);
		AssetCompletedPage assetCompletedPage;
		IncomeSectionCompletePage incomeSectionCompletePage = null;
		String mainTab=null;
		testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
		logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
		initializeData(funcMod,sTestID);
		try
		{
			lenderLoginPage = new LenderLoginPage(driver);
            LenderPipeLinePage lenderPipeLinePage = lenderLoginPage.loginToLenderPortal(portalParam.lenderUserLogin,portalParam.encryptedPassword);
            lenderPipeLinePage.inviteBorrower(portalParam.borrowerEmail);
            LenderLoginPage lenderLoginPage = lenderPipeLinePage.logoutLender(portalParam.lenderUserLogin);
            mainTab = driver.getWindowHandle();
            String referenceId = getReferenceId(portalParam.borrowerEmail);
            SignUpPage signUpPage = signUpAndLogin(driver,mainTab,referenceId);
            WelcomePage welcomePage = signUpPage.signUp(portalParam.borrowerEmail,portalParam.encryptedPassword);
			WelcomeVideoPage welcomeVideoPage = welcomePage.clickPurposeButton("Refinance");
			LetsGetStartedPage letsGetStartedPage = welcomeVideoPage.clickGetStartedButton(driver);
			AccountInformationPage accountInformationPage = letsGetStartedPage.clickBeginSectionButton();
			wait.until(ExpectedConditions.presenceOfElementLocated(accountInformationPage.progressBarLabel));
			accountInformationPage.verifyAccountInformationPageUI();
			RefinancePropertyDetailsPage refinancePropertyDetailsPage = accountInformationPage.enterAccountInfo(portalParam.firstName, portalParam.middleName, portalParam.lastName, portalParam.suffix, portalParam.phone, portalParam.modeOfCommunication);
			RefinanceLoanAmountPage refinanceLoanAmountPage = refinancePropertyDetailsPage.enterRefinancePropertyDetails(portalParam.propertyType,portalParam.propertyUse,portalParam.firstAddress);
			WhosOnLoanPage whosOnLoanPage = refinanceLoanAmountPage.enterRefinanceLoanDetails(portalParam.worthOfProperty,portalParam.firstOwe,portalParam.secondOwe,portalParam.otherOwe,portalParam.ifCashOut,portalParam.cashOutAmount,portalParam.cashOutPurpose);
			SpouseInformationPage spouseInformationPage = whosOnLoanPage.selectApplicants(portalParam.married, portalParam.addSpouce, portalParam.addCoborrower, portalParam.addDependant);
			NonSpouseInformationPage nonSpouseInformationPage = spouseInformationPage.enterSpouseDetails(portalParam.spouceFirstName,portalParam.spouceMiddleName,portalParam.spouceLastName,portalParam.spouceSuffix,portalParam.spouseEmail,portalParam.spoucePhone,portalParam.spouceComm);
			ApplicantInformationPage applicantInformationPage = nonSpouseInformationPage.enterNonSpouseDetails(portalParam.NonSpouseFirstName,portalParam.NonSpouseMiddleName,portalParam.NonSpouseLastName,portalParam.NonSpouseSuffix,portalParam.nonSpouseEmail,portalParam.NonSpousePhone);
			EligibilityPage eligibilityPage = applicantInformationPage.verifyApplicants();
			RealEstateAgentPage realEstateAgentPage = eligibilityPage.selectLoanInfo(portalParam.eligibleLoan,portalParam.currentLoan);
			LoanOfficerPage loanOfficerPage = realEstateAgentPage.selectAgentInfo(portalParam.isRealEstateAgent());
			CurrentResidenceDetailsPage currentResidenceDetailsPage = loanOfficerPage.enterLoanOfficerDetail(portalParam.withLoanOfficer,portalParam.loanOfficerName);
			RealEstatePage realEstatePage = currentResidenceDetailsPage.enterCurrentResidenceDetails(portalParam.sameResidence,portalParam.residenceStatus,portalParam.currentAddress,portalParam.stayingSince,portalParam.planningToSale);
			ProfileCompletedPage profileCompletedPage = realEstatePage.enterRealEstateDetails(portalParam.additionalProperty,portalParam.propertyAddress,portalParam.propertyType,portalParam.propertyStatus,portalParam.propertyValue,portalParam.rentalIncome,portalParam.expenses);
			AssetsPage assetsPage = profileCompletedPage.clickBeginSectionBtn();
			SavingAndInvestmentPage savingAndInvestmentPage= assetsPage.clickEnterManually();
			BankAndInvestmentPage bankAndInvestmentPage = savingAndInvestmentPage.enterManualAssetsDetails(portalParam.institutionName,portalParam.accountType,portalParam.currentBalance,portalParam.accountNumber,portalParam.accountName);
			//bankAndInvestmentPage.validateAccountInfo(portalParam.institutionName,portalParam.accountType,portalParam.currentBalance,portalParam.accountNumber,portalParam.accountName);
			BankInformationPage bankInformationPage = bankAndInvestmentPage.addInstitution();
			BankAndInvestmentPage bankAndInvestmentPage1 = bankInformationPage.fillAccountCredentials(portalParam.plaidUsername,portalParam.plaidPassword,portalParam.plaidPin);
			//bankAndInvestmentPage1.validateAccountInfo();

			if(!portalParam.addSpouce) {
				assetCompletedPage = bankAndInvestmentPage1.clickSubmit();
			}
			else
			{
				AssetsPage assetsPage1 = bankAndInvestmentPage1.clickSubmit(portalParam.addSpouce);
				SavingAndInvestmentPage savingAndInvestmentPage1 = assetsPage1.clickEnterManually();
				BankAndInvestmentPage bankAndInvestmentPage2 = savingAndInvestmentPage1.enterManualAssetsDetails(portalParam.spouseInstitutionName,portalParam.spouseAccountType,portalParam.spouseCurrentBalance,portalParam.spouseAccountNumber,portalParam.spouseAccountName);
				BankInformationPage bankInformationPage1 = bankAndInvestmentPage2.addInstitution();
				BankAndInvestmentPage bankAndInvestmentPage3 = bankInformationPage1.fillAccountCredentials(portalParam.plaidUsername,portalParam.plaidPassword,portalParam.plaidPin);
				assetCompletedPage = bankAndInvestmentPage3.clickSubmit();
			}
			IncomeSearchPage incomeSearchPage = assetCompletedPage.clickBeginSectionBtn();
			IncomeTypesPage incomeTypesPage = incomeSearchPage.enterIncomeSearchDetails(portalParam.dateOfBirth,portalParam.ssnNumber);
			BorrowerEmploymentPage borrowerEmploymentPage = incomeTypesPage.enterTypesOfIncome();
			BorrowerEmpAddressPage borrowerEmpAddressPage = borrowerEmploymentPage.enterBorrowerEmploymentDetails(portalParam.currentEmployer,portalParam.employmentTitle,portalParam.employmentStartDate,portalParam.inThisLineYear,portalParam.inThisLineMonth,portalParam.monthlyBaseSalary,portalParam.bonusAmount);
			AlimonyChildIncomePage alimonyChildIncomePage = borrowerEmpAddressPage.enterBorrowerEmpAddressDetails(portalParam.businessAddress,portalParam.businessPhone);
			AlimonyChildSupportPage alimonyChildSupportPage = alimonyChildIncomePage.enterAlimonyIncomeDetails(portalParam.monthlyAlimony,portalParam.alimonyStartDate,portalParam.monthlyChildSupport,portalParam.childName,portalParam.childDoB);
			BusinessSelfEmploymentPage businessSelfEmploymentPage = alimonyChildSupportPage.enterAlimonySupportDetails(portalParam.supportByCourt,portalParam.willReceiveOrderByCourt,portalParam.twoMonthSupport);
			MilitaryPage militaryPage = businessSelfEmploymentPage.provideSelfEmploymentDetails(portalParam.selfEmploymentMonthlyIncome,portalParam.selfEmploymentCompanyName,portalParam.selfEmploymentTitle,portalParam.selfEmploymentBusinessAddress,portalParam.selfEmploymentBusinessPhone,portalParam.selfEmploymentBusinessStartDate,portalParam.percentageOwnership,portalParam.typeOfCompany);
			RentalPage rentalPage = militaryPage.enterMilitaryIncomeDetails(portalParam.monthlyMilitaryPay);
			SocialSecurityPage socialSecurityPage = rentalPage.enterRentalIncomeDetails(portalParam.monthlyRentalIncome,portalParam.rentalPropertyAddress,portalParam.typeOfRentalProperty);
			InterestDividendPage interestDividendPage = socialSecurityPage.enterSocialSecurityIncomeDetails(portalParam.socialSecurityIncome);
			OtherIncomePage otherIncomePage = interestDividendPage.enterInterestDividendIncomeDetails(portalParam.interestLastYear,portalParam.interestPreviousYear);
			if(!portalParam.addSpouce) {
				incomeSectionCompletePage = otherIncomePage.enterOtherIncomeDetails(portalParam.otherIncomePerMonth, portalParam.sourceOfIncome, portalParam.continuousIncome);
			}
			if(portalParam.addSpouce) {
				SpouseIncomeSearchPage spouseIncomeSearchPage = otherIncomePage.provieOtherIncomeDetails(portalParam.otherIncomePerMonth, portalParam.sourceOfIncome, portalParam.continuousIncome);
				SpouseIncomeTypePage spouseIncomeTypePage = spouseIncomeSearchPage.enterSpouseIncomeSearchDetails(portalParam.spouseDateOfBirth, portalParam.spouseSsnNumber);
				SpouseEmploymentPage spouseEmploymentPage = spouseIncomeTypePage.provideTypesOfIncome();
				SpouseEmpAddressPage spouseEmpAddressPage = spouseEmploymentPage.provideSpouseEmploymentDetails(portalParam.spouseEmployer, portalParam.spouseTitle, portalParam.spouseEmpStartDate, portalParam.inThisLineYear, portalParam.inThisLineMonth, portalParam.spouseMonthlybaseSalary, portalParam.spouseBonusAmount);
				SpouseAlimonyChildIncomePage spouseAlimonyChildIncomePage = spouseEmpAddressPage.provideSpouseEmpAddressDetails(portalParam.spouseEmpAddress, portalParam.businessPhone);
				SpouseAlimonyChildSupportPage spouseAlimonyChildSupportPage = spouseAlimonyChildIncomePage.provideSpouseAlimonyIncomeDetails(portalParam.monthlyAlimony, portalParam.alimonyStartDate, portalParam.monthlyChildSupport, portalParam.spouseAlimonyChildName, portalParam.spouseAlimonyChildDoB);
				SpouseBusinessSelfEmploymentPage spouseBusinessSelfEmploymentPage = spouseAlimonyChildSupportPage.provideChildSupportDetails(portalParam.spouseSupportByCourt, portalParam.SpouseWillReceiveOrderByCourt, portalParam.spouseTwoMonthSupport);
				SpouseMilitaryPage spouseMilitaryPage = spouseBusinessSelfEmploymentPage.provideSpouseBusinessDetails(portalParam.spouseBusinessMonthlyIncome, portalParam.spouseBusinessCompanyName, portalParam.spouseBusinessTitle, portalParam.spouseBusinessAddress, portalParam.spouseBusinessPhone, portalParam.spouseBusinessStartDate, portalParam.spousePercentOwnership, portalParam.spouseTypeOfCompany);
				SpouseRentalPage spouseRentalPage = spouseMilitaryPage.provideMilitaryIncomeDetails(portalParam.monthlyMilitaryPay);
				SpouseSocialSecurityPage spouseSocialSecurityPage = spouseRentalPage.provideRentalIncomeDetails(portalParam.monthlyRentalIncome, portalParam.spouseRentalPropertyAddress, portalParam.typeOfRentalProperty);
				SpouseInterestDividendPage spouseInterestDividendPage = spouseSocialSecurityPage.provideSocialSecurityIncomeDetails(portalParam.socialSecurityIncome);
				SpouseOtherIncomePage spouseOtherIncomePage = spouseInterestDividendPage.provideOtherIncomeDetails(portalParam.interestLastYear, portalParam.interestPreviousYear);
				incomeSectionCompletePage = spouseOtherIncomePage.provideOtherIncomeDetails(portalParam.otherIncomePerMonth, portalParam.sourceOfIncome, portalParam.continuousIncome);

			}
			DeclarationPage declarationPage = incomeSectionCompletePage.clickBeginSectionBtn();
			SpouseDeclarationPage spouseDeclarationPage = declarationPage.fillDeclarationInfo();
			DeclarationSectionCompletePage declarationSectionCompletePage = spouseDeclarationPage.fillDeclarationInfo();
			CreditScorePage creditScorePage = declarationSectionCompletePage.clickBeginSectionBtn();
			SpouseCreditScorePage spouseCreditScorePage = creditScorePage.checkCredit();
			CreditCompletedPage creditCompletedPage = spouseCreditScorePage.checkCredit();
			SummaryPage summaryPage = creditCompletedPage.clickBeginSectionBtn();
			AgreementTermsPage agreementTermsPage = summaryPage.verifySummaryInfo();
			SummaryCompletedPage summaryCompletedPage = agreementTermsPage.provideConsent();
			BeginPostApplicationPage beginPostApplicationPage=summaryCompletedPage.submitApplication();
			PostApplicationPage postApplicationPage = beginPostApplicationPage.clickBeginPostApplication();
			DashbordTourPage dashbordTourPage = postApplicationPage.fillingPostApp();
			BorrowerDashboardPage borrowerDashboardPage = dashbordTourPage.navigateToDashboard();
			String loanid[] = driver.getCurrentUrl().split("id:");
			LoginPage loginPage1 = borrowerDashboardPage.verifyDashboardFunctionality();
            driver.get(portalParam.lenderurl);
            lenderLoginPage = new LenderLoginPage(driver);
            //String loanid = "DOC001307";
            LenderPipeLinePage lenderPipeLinePage1 = lenderLoginPage.loginToLenderPortal(portalParam.lenderUserLogin,portalParam.encryptedPassword);
            LenderLoginPage lenderLoginPage2 = lenderPipeLinePage1.createEventAndCondition(loanid[1]);
            driver.get(portalParam.borrowerurl);
            LoginPage loginPage2 = new LoginPage(driver);
            BorrowerDashboardPage borrowerDashboardPage1 = loginPage2.loginToDashboard(portalParam.borrowerEmail,portalParam.encryptedPassword);
            LoginPage loginPage3 = borrowerDashboardPage1.verifyEvent();
            result="Passed";
		}catch(Exception e) {

			e.printStackTrace();
			logger.info("******************" + sTestID +  "  failed. *****\n" +e);
		}

		finally {
			logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
			testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
			writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
			if(result.equalsIgnoreCase("Failed"))
				Assert.fail();
		}
	}
	
}