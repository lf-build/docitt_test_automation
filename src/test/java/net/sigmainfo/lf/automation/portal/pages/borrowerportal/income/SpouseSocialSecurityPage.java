package net.sigmainfo.lf.automation.portal.pages.borrowerportal.income;

import net.sigmainfo.lf.automation.common.AbstractTests;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by shaishav.s on 27-09-2017.
 */
public class SpouseSocialSecurityPage extends AbstractTests {

    WebDriverWait wait = new WebDriverWait(driver,60);
    WebElement PropertyTypeListItem;

    private Logger logger = LoggerFactory.getLogger(SpouseSocialSecurityPage.class);

    public SpouseSocialSecurityPage(WebDriver driver) {
        this.driver = driver;
        logger.info("========= SpouseSocialSecurityPage is loaded===========");
    }

    public SpouseSocialSecurityPage() {}

    public static By SpousePerMonthSocialSecurityTextBox = By.xpath("//div[starts-with(@id,'ID-')][18]//div[2]//input[@id='input']");
    public static By iAmAllDoneButton = By.xpath("//button[@name='next']");

    public SpouseInterestDividendPage provideSocialSecurityIncomeDetails(String socialSecurityIncome) throws Exception {
        enterSocialSecurityIncome(SpousePerMonthSocialSecurityTextBox,socialSecurityIncome);
        clickButton();
        return new SpouseInterestDividendPage(driver);
    }

    private void enterSocialSecurityIncome(By locator, String socialSecurityIncome) {
        enterText(locator,socialSecurityIncome);
    }

    public void clickButton() throws Exception {
        wait.until(ExpectedConditions.elementToBeClickable(iAmAllDoneButton));
        driver.findElement(iAmAllDoneButton).click();
        logger.info("Clicking on I am done button");
    }

    public void enterText(By locator,String text){
        Actions actions = new Actions(driver);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        actions.moveToElement(element);
        actions.click();
        actions.sendKeys(text);
        actions.build().perform();
        logger.info("Entering :"+text);
    }
}
