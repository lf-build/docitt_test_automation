package net.sigmainfo.lf.automation.portal.pages.borrowerportal.profile;

import net.sigmainfo.lf.automation.common.AbstractTests;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by shaishav.s on 06-09-2017.
 */
public class RealEstateAgentPage extends AbstractTests {

    WebDriverWait wait = new WebDriverWait(driver,60);
    WebElement PropertyTypeListItem;

    private Logger logger = LoggerFactory.getLogger(RealEstateAgentPage.class);

    public RealEstateAgentPage(WebDriver driver) {
        this.driver = driver;
        logger.info("========= RealEstateAgentPage is loaded===========");
    }

    public RealEstateAgentPage() {
    }

    public static By HaveAgentButton = By.xpath("//ui-switch[@id='35']//label[normalize-space(.)='Yes']");
    public static By NoAgentButton = By.xpath("//ui-switch[@id='35']//label[normalize-space(.)='No']");
    public static By ReferAgentButton = By.xpath("//ui-switch[@id='36']//label[normalize-space(.)='Yes']");
    public static By NoReferAgentButton = By.xpath("//ui-switch[@id='36']//label[normalize-space(.)='No']");
    public static By NextButton = By.xpath("//button[contains(text(),'Next')]");

    public void selectButton(By locator,String value){
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        if(value.equalsIgnoreCase("Yes")) {
            driver.findElement(locator).click();
        }
        else
        {
            driver.findElement(locator).click();
        }
        logger.info("Clicking on "+ value +" button");
    }

    public void clickNext() throws Exception {
        wait.until(ExpectedConditions.elementToBeClickable(NextButton));
        driver.findElement(NextButton).click();
        logger.info("Clicking on NEXT button");
    }

    public LoanOfficerPage selectAgentInfo(boolean realEstateAgent) throws Exception {
        if(realEstateAgent)
        {
            selectButton(HaveAgentButton,"Real Estate Agent");

        }
        else
        {
            selectButton(NoAgentButton,"No Real Estate Agent");
            wait.until(ExpectedConditions.elementToBeClickable(ReferAgentButton));
            selectButton(ReferAgentButton,"Refer agent");
        }
        clickNext();
        return new LoanOfficerPage(driver);
    }
}
