package net.sigmainfo.lf.automation.portal.pages.borrowerportal.summary;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sigmainfo.lf.automation.portal.constant.PortalParam;

import static org.junit.Assert.*;

public class SummaryPage extends AbstractTests{

	PortalFuncUtils portalFuncUtils;

	private Logger logger = LoggerFactory.getLogger(SummaryPage.class);
	WebDriver driver;



	SummaryPage(){}

	public SummaryPage(WebDriver driver){
		logger.info("SummaryPage is loaded");
		this.driver=driver;
	}


	//Account Information
	public static By fullName=By.xpath(".//*[@id='tab0']/div[1]/borrower-summary/ul/div/div[1]/li/span[2]");
	public static By email=By.xpath(".//*[@id='tab0']/div[1]/borrower-summary/ul/div/div[2]/li/span[2]");
	public static By phoneNumber=By.xpath(".//*[@id='tab0']/div[1]/borrower-summary/ul/div/div[3]/li/span[2]");
	//Income Information
	public static By doB = By.xpath(".//*[@id='tab0']/div[2]/borrower-summary/ul/div[1]/div/li/span[2]");
	public static By SSN = By.xpath(".//*[@id='tab0']/div[2]/borrower-summary/ul/div[2]/div/li/span[2]");
	//Purchase Property Details
	public static By purchasingAddress=By.xpath(".//*[@id='tab0']/div[3]/borrower-summary/ul/div/div[1]/li[1]/span[2]");
	public static By propertyType=By.xpath(".//*[@id='tab0']/div[3]/borrower-summary/ul/div/div[2]/li/span[2]");
	public static By propertyUse=By.xpath(".//*[@id='tab0']/div[3]/borrower-summary/ul/div/div[3]/li/span[2]");
	//Loan Details
	public static By purchasePrice=By.xpath(".//*[@id='tab0']/div[5]/borrower-summary/ul/div/div/li[1]/div[1]/span[2]");
	public static By downPaymentPrice=By.xpath(".//*[@id='tab0']/div[5]/borrower-summary/ul/div/div/li[1]/div[2]/span[2]");
	public static By loanAmount=By.xpath(".//*[@id='tab0']/div[5]/borrower-summary/ul/div/div/li[2]/span[2]");
	//Employment Address
	public static By businessAddress=By.xpath(".//*[@id='tab0']/div[6]/borrower-summary/ul/div[1]/div/li[1]/span[2]");
	public static By businessPhone = By.xpath(".//*[@id='tab0']/div[6]/borrower-summary/ul/div[2]/div/li/span[2]");


	//EmployMent Fields
	public static By currentEmployer=By.xpath(".//*[@id='tab0']/div[4]/borrower-summary/ul/div[1]/div/li/span[2]");
	public static By employmentTitle=By.xpath(".//*[@id='tab0']/div[4]/borrower-summary/ul/div[2]/div/li/span[2]");
	public static By employmentStartDate=By.xpath(".//*[@id='tab0']/div[4]/borrower-summary/ul/div[3]/div/li/span[2]");
	public static By lineofWorkYears=By.xpath(".//*[@id='tab0']/div[4]/borrower-summary/ul/div[5]/div/div[1]/li/span[2]");
	public static By lineofWorkMonths=By.xpath(".//*[@id='tab0']/div[4]/borrower-summary/ul/div[5]/div/div[2]/li/span[2]");
	public static By monthlyBaseSalary=By.xpath(".//*[@id='tab0']/div[4]/borrower-summary/ul/div[6]/div/li/span[2]");
	public static By monthlyBonus=By.xpath(".//*[@id='tab0']/div[4]/borrower-summary/ul/div[8]/div/li/span[2]");
	public static By monthlyCommision=By.xpath(".//*[@id='tab0']/div[4]/borrower-summary/ul/div[8]/div/li/span[2]");
	public static By monthlyOverTime=By.xpath(".//*[@id='tab0']/div[4]/borrower-summary/ul/div[9]/div/li/span[2]");

	//Marital Status
	public static By maritalStatus = By.xpath(".//*[@id='tab0']/div[7]/borrower-summary/ul/div/div/li/span[2]");

	//Military Loan
	public static By militaryLoan = By.xpath(".//*[@id='tab0']/div[8]/borrower-summary/ul/div/div/li/span[2]");

	//Alimony OR Child's Support
	public static By childDoB = By.xpath(".//*[@id='tab0']/div[9]/borrower-summary/ul/div[5]/div/div[2]/li/span[2]");
	public static By childName = By.xpath(".//*[@id='tab0']/div[9]/borrower-summary/ul/div[5]/div/div[1]/li/span[2]");
	public static By alimonyPerMonth = By.xpath(".//*[@id='tab0']/div[9]/borrower-summary/ul/div[2]/div/div[1]/li/span[2]");
	public static By alimonystartDate=By.xpath(".//*[@id='tab0']/div[9]/borrower-summary/ul/div[2]/div/div[2]/li/span[2]");
	public static By childSupportPerMonth=By.xpath(".//*[@id='tab0']/div[9]/borrower-summary/ul/div[4]/div/li/span[2]");
	public static By officialyOrderedByCourt=By.xpath(".//*[@id='tab0']/div[10]/borrower-summary/ul/div[1]/div/li/span[2]");
	public static By courtIssueSupport=By.xpath(".//*[@id='tab0']/div[10]/borrower-summary/ul/div[2]/div/li/span[2]");
	public static By supportFor24Months=By.xpath(".//*[@id='tab0']/div[10]/borrower-summary/ul/div[3]/div/li/span[2]");

	//Real Estate Agent
	public static By realEstateAgent=By.xpath(".//*[@id='tab0']/div[11]/borrower-summary/ul/div/div/li/span[2]");

	//Business OR Self Employment
	public static By montlyIncome=By.xpath(".//*[@id='tab0']/div[12]/borrower-summary/ul/div[1]/div/li/span[2]");
	public static By businessTitle=By.xpath(".//*[@id='tab0']/div[12]/borrower-summary/ul/div[2]/div/li/span[2]");
	public static By businessStartDate=By.xpath(".//*[@id='tab0']/div[12]/borrower-summary/ul/div[3]/div/li/span[2]");
	public static By businessPercentageOwnership=By.xpath(".//*[@id='tab0']/div[12]/borrower-summary/ul/div[4]/div/li/span[2]");
	public static By percentageOwnership = By.xpath(".//*[@id='tab0']/div[12]/borrower-summary/ul/div[4]/div/li/span[2]");
	public static By typeofCompany=By.xpath(".//*[@id='tab0']/div[12]/borrower-summary/ul/div[5]/div/li/span[2]");


	//Current Resident Address
	public static By currentResidenceAddress = By.xpath(".//*[@id='tab0']/div[13]/borrower-summary/ul/div/div[1]/li[1]/span[2]");
	public static By livedHereSince = By.xpath(".//*[@id='tab0']/div[13]/borrower-summary/ul/div[2]/div/li/span[2]");

	//Military Pay
	public static By militaryPay = By.xpath(".//*[@id='tab0']/div[14]/borrower-summary/ul/div/div/li/span[2]");

	//Rental Income
	public static By rentalIncomePerMonth = By.xpath("//*[@id='tab0']/div[15]/borrower-summary/ul/div[1]/div[2]/li/span[2]");
	public static By rentalAddress = By.xpath(".//*[@id='tab0']/div[15]/borrower-summary/ul/div[2]/div[2]/li[1]/span[2]");
	public static By rentalPropertyType = By.xpath(".//*[@id='tab0']/div[15]/borrower-summary/ul/div[3]/div/li/span[2]");

	// Social security income

	public static By ssnIncome = By.xpath(".//*[@id='tab0']/div[16]/borrower-summary/ul/div/div/li/span[2]");

	//Interest OR Dividend
	public static By interestLastYear=By.xpath(".//*[@id='tab0']/div[17]/borrower-summary/ul/div[1]/div/li/span[2]");
	public static By interestBeforeLastYear=By.xpath(".//*[@id='tab0']/div[17]/borrower-summary/ul/div[2]/div/li/span[2]");

	//Other Income
	public static By incomeReceivePerMonth=By.xpath(".//*[@id='tab0']/div[18]/borrower-summary/ul/div[1]/div/li/span[2]");
	public static By sourceOfIncome=By.xpath(".//*[@id='tab0']/div[18]/borrower-summary/ul/div[2]/div/li/span[2]");
	public static By receiveIncomefor2OrMore=By.xpath(".//*[@id='tab0']/div[18]/borrower-summary/ul/div[3]/div/li/span[2]");

	//Confirm Phone Number
	public static By confirmPhoneNumber = By.xpath(".//*[@id='tab0']/div[19]/borrower-summary/ul/div/div/li/span[2]");


	//-----------------------------------Questions List --------------------------------------------------------------------------------------//

	public static By QueA=By.xpath(".//*[@id='tab0']/borrower-declarations/div/div[1]/strong[1]");
	public static By QueASummary=By.xpath(".//*[@id='tab0']/borrower-declarations/div/div[1]/div[1]");

	public static By QueB=By.xpath(".//*[@id='tab0']/borrower-declarations/div/div[1]/strong[2]");
	public static By QueBSummary=By.xpath(".//*[@id='tab0']/borrower-declarations/div/div[1]/div[2]");

	public static By QueC=By.xpath(".//*[@id='tab0']/borrower-declarations/div/div[1]/strong[3]");
	public static By QueCSummary=By.xpath(".//*[@id='tab0']/borrower-declarations/div/div[1]/div[3]");

	public static By QueD=By.xpath(".//*[@id='tab0']/borrower-declarations/div/div[1]/strong[4]");
	public static By QueDSummary=By.xpath(".//*[@id='tab0']/borrower-declarations/div/div[1]/div[4]");

	public static By QueE=By.xpath(".//*[@id='tab0']/borrower-declarations/div/div[1]/strong[5]");
	public static By QueESummary=By.xpath(".//*[@id='tab0']/borrower-declarations/div/div[1]/div[5]");

	public static By QueF=By.xpath(".//*[@id='tab0']/borrower-declarations/div/div[1]/strong[6]");
	public static By QueFSummary=By.xpath(".//*[@id='tab0']/borrower-declarations/div/div[1]/div[6]");

	public static By QueG=By.xpath(".//*[@id='tab0']/borrower-declarations/div/div[1]/strong[7]");
	public static By QueGSummary=By.xpath(".//*[@id='tab0']/borrower-declarations/div/div[1]/div[7]");

	public static By QueH=By.xpath(".//*[@id='tab0']/borrower-declarations/div/div[1]/strong[8]");
	public static By QueHSummary=By.xpath(".//*[@id='tab0']/borrower-declarations/div/div[1]/div[8]");

	public static By QueI=By.xpath(".//*[@id='tab0']/borrower-declarations/div/div[1]/strong[9]");
	public static By QueISummary=By.xpath(".//*[@id='tab0']/borrower-declarations/div/div[1]/div[9]");

	public static By QueJ=By.xpath(".//*[@id='tab0']/borrower-declarations/div/div[1]/strong[10]");
	public static By QueK=By.xpath(".//*[@id='tab0']/borrower-declarations/div/div[2]/strong[1]");
	public static By QueL=By.xpath(".//*[@id='tab0']/borrower-declarations/div/div[2]/strong[2]");
	public static By QueM=By.xpath(".//*[@id='tab0']/borrower-declarations/div/div[2]/strong[3]");
	public static By typeofProperty=By.xpath(".//*[@id='tab0']/borrower-declarations/div/div[2]/strong[4]");
	public static By titletoHome=By.xpath(".//*[@id='tab0']/borrower-declarations/div/div[2]/strong[5]");
	public static By wishtoFurnishInfo=By.xpath(".//*[@id='tab0']/borrower-declarations/div/div[2]/strong[6]");
	public static By Ethinicity=By.xpath(".//*[@id='tab0']/borrower-declarations/div/div[2]/strong[7]");
	public static By Race=By.xpath(".//*[@id='tab0']/borrower-declarations/div/div[2]/strong[8]");
	public static By Sex=By.xpath(".//*[@id='tab0']/borrower-declarations/div/div[2]/strong[9]");
	public static By declarationLabel = By.xpath(".//*[@id='tab0']/borrower-declarations/div/h3");
	public static By acknowledgementLabel = By.xpath(".//*[@id='SS1']/questioner-question-set/div[3]/div[1]/div/questioner-question/ui-container/div/ui-label/div");
	public static By AgreementCheckbox = By.xpath("//input[@id='C-5']");
	public static By ThisIsCorrectButton = By.xpath("//button[@name='next']");


	public AgreementTermsPage verifySummaryInfo() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver,60);

		/*// ACCOUNT INFOMRATION
		String fullNameExcel = PortalParam.firstName+" "+PortalParam.middleName+" "+PortalParam.lastName+" "+PortalParam.suffix;
		assertEquals("Full name does not match",fullNameExcel,driver.findElement(fullName).getText());
		assertEquals("Email does not match",PortalParam.preferredEmail,driver.findElement(email).getText());
		String portalPhoneNumber=(driver.findElement(phoneNumber).getText()).replaceAll("[\\s()-]","");
		assertEquals("Phone Number does not match",PortalParam.phone,portalPhoneNumber);
		// INCOME DETAILS
		String DateofBirth=removeSpecialChar(driver.findElement(doB).getText());
		assertEquals("Date Of Birth does not match",PortalParam.dateOfBirth,DateofBirth);
		String SSNNumber=removeSpecialChar(driver.findElement(SSN).getText());
		assertEquals("SSN number does not match",PortalParam.ssnNumber,SSNNumber);
		// PURCHASE PROPERTY DETAILS
		assertEquals("Purchasing address does not match",PortalParam.firstAddress,driver.findElement(purchasingAddress).getText());
		portalFuncUtils.scrollUntil(businessAddress,driver);
		assertEquals("Property Type does not match",PortalParam.propertyType,driver.findElement(propertyType).getText());
		assertEquals("Property Use does not match",PortalParam.propertyUse,driver.findElement(propertyUse).getText());
		// EMPLOYMENT DETAILS
		assertEquals("Current Employer does not match",PortalParam.currentEmployer,driver.findElement(currentEmployer).getText());
		assertEquals("Employment Title does not match",PortalParam.employmentTitle,driver.findElement(employmentTitle).getText());
		String PurchasePrice =removeSpecialChar(driver.findElement(purchasePrice).getText());
		// LOAN DETAILS
		assertEquals("Purchase price does not match",PortalParam.purchasePrice,PurchasePrice);
		assertEquals("Employment Start Date does not match",PortalParam.employmentStartDate,driver.findElement(employmentStartDate).getText());
		String DownPayment=removeSpecialChar(driver.findElement(downPaymentPrice).getText());
		assertEquals("Down payment does not match",PortalParam.downPayment,DownPayment);
		String loanAmountSummary = "$ " +((Integer.parseInt(portalParam.purchasePrice))-(Integer.parseInt(portalParam.downPayment)));
		assertEquals("Loan amount does not match",loanAmountSummary,(driver.findElement(loanAmount).getText()).replace(",",""));
		assertTrue("Business address does not match",driver.findElement(businessAddress).getText().contains(portalParam.businessAddress));
		assertEquals("Line of Work years does not match",PortalParam.inThisLineYear,driver.findElement(lineofWorkYears).getText());
		assertEquals("Line of Work month does not match",PortalParam.inThisLineMonth,driver.findElement(lineofWorkMonths).getText());
		portalFuncUtils.scrollUntil(alimonystartDate,driver);
		logger.info("Scrolling till alimony start date");
		String bsnsPhone = removeSpecialChar(driver.findElement(businessPhone).getText()).trim();
		assertEquals("Business Phone does not match",portalParam.businessPhone,bsnsPhone);
		String MonthlyBaseSalary =removeSpecialChar(driver.findElement(monthlyBaseSalary).getText());
		assertEquals("Monthly Base Salary does not match",PortalParam.monthlyBaseSalary,MonthlyBaseSalary);
		String MonthlyBaseBonus =removeSpecialChar(driver.findElement(monthlyBonus).getText());
		assertEquals("Monthly bonus does not match",PortalParam.bonusAmount,MonthlyBaseBonus);
		portalFuncUtils.scrollUntil(businessStartDate,driver);
		logger.info("Scrolling till business start date");
		assertEquals("Marital status not match",(PortalParam.married)?"Married":"Single",driver.findElement(maritalStatus).getText());
		//assertEquals("VA eligibility not match",(PortalParam.eligibleLoan)?"Yes":"No",driver.findElement(militaryLoan).getText());
		assertEquals("Per month alimony does not match",portalParam.monthlyAlimony,driver.findElement(alimonyPerMonth).getText());
		assertEquals("Alimony start date does not match",portalParam.alimonyStartDate,driver.findElement(alimonystartDate).getText());
		portalFuncUtils.scrollUntil(percentageOwnership,driver);
		logger.info("Scrolling till percentage ownership in business");
		assertEquals("Per month child support does not match",portalParam.monthlyChildSupport,driver.findElement(childSupportPerMonth).getText());
		assertEquals("Child name does not match",portalParam.childName,driver.findElement(childName).getText());
		assertEquals("Child Date of birth does not match",portalParam.childDoB,removeSpecialChar(driver.findElement(childDoB).getText()));
		assertEquals("Support by court does not match",(portalParam.supportByCourt)?"Yes":"No",driver.findElement(officialyOrderedByCourt).getText());
		assertEquals("Will receive Order By Court does not match",(portalParam.willReceiveOrderByCourt)?"Yes":"No",driver.findElement(courtIssueSupport).getText());
		assertEquals("Two month support does not match",(portalParam.twoMonthSupport)?"Yes":"No",driver.findElement(supportFor24Months).getText());
		assertEquals("Working with real estate agent does not match",(portalParam.realEstateAgent)?"Yes":"No",driver.findElement(realEstateAgent).getText());
		//BUSINESS DETAILS
		assertEquals("Business monthly income does not match",portalParam.selfEmploymentMonthlyIncome,driver.findElement(montlyIncome).getText());
		assertEquals("Business title does not match",portalParam.selfEmploymentTitle,driver.findElement(businessTitle).getText());
		assertEquals("Business start date does not match",portalParam.selfEmploymentBusinessStartDate,driver.findElement(businessStartDate).getText());
		portalFuncUtils.scrollUntil(rentalIncomePerMonth,driver);
		logger.info("Scrolling till rental income per month");
		assertEquals("Percentage ownership does not match",(portalParam.percentageOwnership)?"Yes":"No",driver.findElement(businessPercentageOwnership).getText());
		assertEquals("Type of company does not match",portalParam.typeOfCompany,driver.findElement(typeofCompany).getText());
		assertTrue(driver.findElement(currentResidenceAddress).getText().contains(portalParam.currentAddress));
		assertEquals("Staying since in current address does not match",portalParam.stayingSince,driver.findElement(livedHereSince).getText());
		assertEquals("Military pay per month does not match",portalParam.monthlyMilitaryPay,driver.findElement(militaryPay).getText());
		assertEquals("Rental income per month does not match",portalParam.monthlyRentalIncome,driver.findElement(rentalIncomePerMonth).getText());
		portalFuncUtils.scrollUntil(interestBeforeLastYear,driver);
		logger.info("Scrolling till interest received before last year");
		assertEquals("Rental property address does not match",portalParam.rentalPropertyAddress,driver.findElement(rentalAddress).getText());
		assertEquals("Rental property type does not match",portalParam.typeOfRentalProperty,driver.findElement(rentalPropertyType).getText());
		assertEquals("Per month social security income does not match",portalParam.socialSecurityIncome,driver.findElement(ssnIncome).getText());
		assertEquals("Interest last year does not match",portalParam.interestLastYear,driver.findElement(interestLastYear).getText());
		assertEquals("Interest previous year does not match",portalParam.interestPreviousYear,driver.findElement(interestBeforeLastYear).getText());
		portalFuncUtils.scrollUntil(declarationLabel,driver);
		assertEquals("Question A answer does not match",PortalParam.declarationQueA,driver.findElement(QueA).getText());
		assertEquals("Question A explanation does not match",PortalParam.DeclarationQuestionAText,driver.findElement(QueASummary).getText());
		assertEquals("Question B answer does not match",PortalParam.declarationQueB,driver.findElement(QueB).getText());
		assertEquals("Question C answer does not match",PortalParam.declarationQueC,driver.findElement(QueC).getText());
		assertEquals("Question C explanation does not match",PortalParam.DeclarationQuestionCText,driver.findElement(QueCSummary).getText());
		assertEquals("Question D answer does not match",PortalParam.declarationQueD,driver.findElement(QueD).getText());
		assertEquals("Question E answer does not match",PortalParam.declarationQueE,driver.findElement(QueE).getText());
		assertEquals("Question E explanation does not match",PortalParam.DeclarationQuestionEText,driver.findElement(QueESummary).getText());
		assertEquals("Question F answer does not match",PortalParam.declarationQueF,driver.findElement(QueF).getText());
		assertEquals("Question G answer does not match",PortalParam.declarationQueG,driver.findElement(QueG).getText());
		assertEquals("Question G explanation does not match",PortalParam.DeclarationQuestionGText,driver.findElement(QueGSummary).getText());
		assertEquals("Question H answer does not match",PortalParam.declarationQueH,driver.findElement(QueH).getText());
		assertEquals("Question I answer does not match",PortalParam.declarationQueI,driver.findElement(QueI).getText());
		assertEquals("Question I explanation does not match",PortalParam.DeclarationQuestionIText,driver.findElement(QueISummary).getText());
		assertEquals("Question J answer does not match",PortalParam.declarationQueJ,driver.findElement(QueJ).getText());
		assertEquals("Question K answer does not match",PortalParam.declarationQueK,driver.findElement(QueK).getText());
		assertEquals("Question L answer does not match",PortalParam.declarationQueL,driver.findElement(QueL).getText());
		*//*assertEquals("Wish To Furnish Informatation  does not match",PortalParam.DeclarationBorrowerWishToFurnish,driver.findElement(wishtoFurnishInfo).getText());
		assertEquals("Ethnicity does not match",PortalParam.DeclarationEthnicity,driver.findElement(Ethinicity).getText());
		assertEquals("Race does not match",PortalParam.DeclarationRace,driver.findElement(Race).getText());
		assertEquals("Sex does not match",PortalParam.DeclarationSex,driver.findElement(Sex).getText());*//*
		portalFuncUtils.scrollUntil(acknowledgementLabel,driver);
		//assertEquals("Question M answer does not match",PortalParam.declarationQueM,driver.findElement(QueM).getText());*/

		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(ThisIsCorrectButton));
		Thread.sleep(2000);
		//wait.until(ExpectedConditions.visibilityOfElementLocated(AgreementCheckbox));
		driver.findElement(AgreementCheckbox).click();
		wait.until(ExpectedConditions.elementToBeClickable(ThisIsCorrectButton));
		driver.findElement(ThisIsCorrectButton).click();
		return new AgreementTermsPage(driver);


	}

	public String removeSpecialChar(String value){
		 value=value.replaceAll("[#%$,()/-]", "").trim();
		 value = value.replaceAll("\\s","");
		 return value;
	}
}
