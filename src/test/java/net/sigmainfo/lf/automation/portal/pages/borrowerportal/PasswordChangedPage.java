package net.sigmainfo.lf.automation.portal.pages.borrowerportal;

import net.sigmainfo.lf.automation.common.AbstractTests;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Created by           : Shaishav.s on 10-04-2017.
 * Test class           : PasswordChangedPage.java
 * Includes             : 1. Objects on PasswordChangedPage
 *                        2. Methods implementation on PasswordChangedPage
 */
@Component
public class PasswordChangedPage extends AbstractTests{

    private Logger logger = LoggerFactory.getLogger(PasswordChangedPage.class);

    public PasswordChangedPage(WebDriver driver) {
        this.driver = driver;
    }
    public PasswordChangedPage(){}

    public static By passwordChangedSuccessMsg = By.xpath("//p[contains(text(),'You have successfully changed the password')]");
    public static By accessMyPortalBtn = By.xpath("//span[contains(text(),'Access my portal')]");

    public LoginPage clickOnAccessMyPortal(WebDriver driver){
        logger.info("Clicking Access my portal button");
        WebElement accessMyPortalButton = driver.findElement(accessMyPortalBtn);

        if((accessMyPortalButton.isDisplayed()) || (accessMyPortalButton.isEnabled()))
            accessMyPortalButton.click();
        else
            new Exception("Access my portal button is not clickable");
        return new LoginPage(driver);
    }
}
