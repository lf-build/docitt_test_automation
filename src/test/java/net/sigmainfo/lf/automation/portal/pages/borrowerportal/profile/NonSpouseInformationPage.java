package net.sigmainfo.lf.automation.portal.pages.borrowerportal.profile;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.assets.SavingAndInvestmentPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.testng.AssertJUnit.assertTrue;

/**
 * Created by shaishav.s on 22-09-2017.
 */
public class NonSpouseInformationPage extends AbstractTests {

    private Logger logger = LoggerFactory.getLogger(NonSpouseInformationPage.class);

    WebDriverWait wait = new WebDriverWait(driver,60);

    public NonSpouseInformationPage(WebDriver driver) {
        this.driver = driver;
        logger.info("========= NonSpouseInformationPage is loaded===========");
        assertTrue(driver.findElement(By.xpath("//div[starts-with(@id,'ID')][2]/h3[contains(text(),\""+portalParam.firstName+" what's your co-borrower's information\")]")).isDisplayed());
    }
    public NonSpouseInformationPage(){}

    public static By NonSpouseFirstNameTextbox = By.cssSelector("div[class*='section-bx']>sub-sections[class*='right-content-area']>div:nth-child(6) questioner-question-set[class*='sub-sections']>div:nth-child(1) div:nth-child(2) input[id='input']");
    public static By NonSpouseMiddleNameTextbox = By.cssSelector("div[class*='section-bx']>sub-sections[class*='right-content-area']>div:nth-child(6) questioner-question-set[class*='sub-sections']>div:nth-child(1) div:nth-child(3) input[id='input']");
    public static By NonSpouseLastNameTextbox = By.xpath("//div[starts-with(@id,'ID')][2]//label[contains(text(),'Last Name')]");
    public static By NonSpouseSuffixTextbox = By.xpath("//div[starts-with(@id,'ID')][2]//label[contains(text(),'Suffix')]");
    public static By NonSpousePreferredEmailTextbox = By.xpath("//div[starts-with(@id,'ID')][2]//label[contains(text(),'Preferred Email')]");
    public static By NonSpousePhoneTextbox = By.xpath("//div[starts-with(@id,'ID')][2]//label[contains(text(),'Phone #')]");
    public static By BackButton = By.xpath("//button[contains(text(),'Back')]");
    public static By NextButton = By.xpath("//button[contains(text(),'Next')]");

    public void clickNext() throws Exception {
        wait.until(ExpectedConditions.elementToBeClickable(NextButton));
        driver.findElement(NextButton).click();
        logger.info("Clicking on NEXT button");
    }

    public void enterText(By locator,String text){
        Actions actions = new Actions(driver);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        actions.moveToElement(element);
        actions.click();
        actions.sendKeys(text);
        actions.build().perform();
    }

    public ApplicantInformationPage enterNonSpouseDetails(String nonSpouseFirstName, String nonSpouseMiddleName, String nonSpouseLastName, String nonSpouseSuffix, String nonSpousePreferredEmail, String nonSpousePhone) throws Exception {
        enterFirstName(NonSpouseFirstNameTextbox,nonSpouseFirstName);
        enterMiddleName(NonSpouseMiddleNameTextbox,nonSpouseMiddleName);
        enterLastName(NonSpouseLastNameTextbox,nonSpouseLastName);
        enterSuffix(NonSpouseSuffixTextbox,nonSpouseSuffix);
        enterPreferredEmail(NonSpousePreferredEmailTextbox,nonSpousePreferredEmail);
        enterPhone(NonSpousePhoneTextbox,nonSpousePhone);
        clickNext();
        return new ApplicantInformationPage(driver);
    }

    private void enterPhone(By nonSpousePhoneTextbox, String nonSpousePhone) throws InterruptedException {
        Thread.sleep(1500);
        enterText(nonSpousePhoneTextbox,nonSpousePhone);
        logger.info("Entered phone :"+nonSpousePhone);
    }

    private void enterPreferredEmail(By nonSpousePreferredEmailTextbox, String nonSpousePreferredEmail) throws InterruptedException {
        Thread.sleep(1500);
        enterText(nonSpousePreferredEmailTextbox,nonSpousePreferredEmail);
        logger.info("Entered email :"+nonSpousePreferredEmail);
    }

    private void enterSuffix(By nonSpouseSuffixTextbox, String nonSpouseSuffix) throws InterruptedException {
        Thread.sleep(1500);
        enterText(nonSpouseSuffixTextbox,nonSpouseSuffix);
        logger.info("Entered suffix :"+nonSpouseSuffix);
    }

    private void enterLastName(By nonSpouseLastNameTextbox, String nonSpouseLastName) throws InterruptedException {
        Thread.sleep(1500);
        enterText(nonSpouseLastNameTextbox,nonSpouseLastName);
        logger.info("Entered last name :"+nonSpouseLastName);
    }

    private void enterMiddleName(By nonSpouseMiddleNameTextbox, String nonSpouseMiddleName) throws InterruptedException {
        Thread.sleep(1500);
        enterText(nonSpouseMiddleNameTextbox,nonSpouseMiddleName);
        logger.info("Entered middle name :"+nonSpouseMiddleName);
    }

    private void enterFirstName(By nonSpouseFirstNameTextbox, String nonSpouseFirstName) throws InterruptedException {
        Thread.sleep(1500);
        enterText(NonSpouseFirstNameTextbox,nonSpouseFirstName);
        logger.info("Entered first name :"+nonSpouseFirstName);
    }
}
