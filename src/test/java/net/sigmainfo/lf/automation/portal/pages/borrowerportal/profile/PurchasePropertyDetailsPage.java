package net.sigmainfo.lf.automation.portal.pages.borrowerportal.profile;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sigmainfo.lf.automation.common.AbstractTests;
import org.testng.Assert;

import java.util.List;
import java.util.NoSuchElementException;

import static org.testng.AssertJUnit.assertTrue;

public class PurchasePropertyDetailsPage extends AbstractTests {

	WebDriverWait wait = new WebDriverWait(driver,60);
	WebElement PropertyTypeListItem;

	private Logger logger = LoggerFactory.getLogger(PurchasePropertyDetailsPage.class);

	public PurchasePropertyDetailsPage(WebDriver driver) {
		this.driver = driver;
		logger.info("========= PurchasePropertyDetailsPage page is loaded===========");
		assertTrue(driver.findElement(By.xpath("//div[@id='SS2']/h3[contains(text(),\""+portalParam.firstName+"'s property details\")]")).isDisplayed());
		assertTrue(driver.findElement(BackButton).isDisplayed());
	}

	public PurchasePropertyDetailsPage() {
	}

	public static By PropertyTypeDropdown = By.xpath("//input[@value='Property Type']");
	public static By SingleFamilyPropertyType = By.xpath("//span[contains(text(),'Single Family')]");
	public static By CondoPropertyType = By.xpath("//span[contains(text(),'Condo')]");
	public static By UnitsType = By.xpath("//span[contains(text(),'2-4 Units')]");
	public static By MixedUsePropertyType = By.xpath("//span[contains(text(),'Mixed Use')]");
	public static By OthersPropertyType = By.xpath("//span[contains(text(),'Other')]");

	public static By YesPropertyButton = By.xpath("//ui-switch[@id='9']//label[normalize-space(.)='Yes']");
	public static By NoPropertyButton = By.xpath("//ui-switch[@id='9']//label[normalize-space(.)='No']");

	public static By AreYouInContractLabel = By.xpath("//span[contains(text(),'Are you in contract?')]");

	public static By YesContractButton = By.xpath("//ui-switch[@id='10']//label[normalize-space(.)='Yes']");
	public static By NoContractButton = By.xpath("//ui-switch[@id='10']//label[normalize-space(.)='No']");

	public static By PropertyUseDropdown = By.xpath("//input[@value='Property Use']");
	public static By PrimaryPropertyUse = By.xpath("//span[contains(text(),'Primary')]");
	public static By SecondHomePropertyUse = By.xpath("//span[contains(text(),'Second Home')]");
	public static By InvestmentPropertyUse = By.xpath("//span[contains(text(),'Investment Property')]");

	public static By AddressTextBox = By.xpath("//ui-input[@id='AddressAutocomplete']/div/div[1]/input");
	public static By InformationIcon = By.xpath("//ui-popover[@class='style-scope ui-container' and @custom-icon=\"true\"]");
	public static By BackButton = By.xpath("//button[@name='back']");
	public static By CityTextBox = By.xpath("//div[@id='SS2']/questioner-question-set/div[6]/div/div/questioner-question/ui-container/div/ui-address/div/div[2]/ui-container/div/ui-input/div/div[1]/input");
	public static By NextButton = By.xpath("//button[@name='next']");


	public void selectFromDropdown(By locator,String value){
		Actions actions = new Actions(driver);
		wait.until(ExpectedConditions.elementToBeClickable(locator));
		driver.findElement(locator).click();
		if(value.equalsIgnoreCase("Single Family"))
		{
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(SingleFamilyPropertyType)));
			PropertyTypeListItem = driver.findElement(SingleFamilyPropertyType);
		}
		else if(value.equalsIgnoreCase("Condo")){
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(CondoPropertyType)));
			PropertyTypeListItem = driver.findElement(CondoPropertyType);
		}
		else if(value.equalsIgnoreCase("2-4 Units")){
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(UnitsType)));
			PropertyTypeListItem = driver.findElement(UnitsType);
		}
		else if(value.equalsIgnoreCase("Mixed Use")){
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(MixedUsePropertyType)));
			PropertyTypeListItem = driver.findElement(MixedUsePropertyType);
		}
		else if(value.equalsIgnoreCase("Other")){
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(OthersPropertyType)));
			PropertyTypeListItem = driver.findElement(OthersPropertyType);
		}
		else if(value.equalsIgnoreCase("Primary")){
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(PrimaryPropertyUse)));
			PropertyTypeListItem = driver.findElement(PrimaryPropertyUse);
		}
		else if(value.equalsIgnoreCase("Second Home")){
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(SecondHomePropertyUse)));
			PropertyTypeListItem = driver.findElement(SecondHomePropertyUse);
		}
		else if(value.equalsIgnoreCase("Investment Property")){
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(InvestmentPropertyUse)));
			PropertyTypeListItem = driver.findElement(InvestmentPropertyUse);
		}
		actions.moveToElement(PropertyTypeListItem);
		actions.click();
		actions.build().perform();
		logger.info("Selected from dropdown :"+value);
	}

	public void selectBoolean(By locator,boolean value)
	{
		new WebDriverWait(driver,60).until(ExpectedConditions.visibilityOfElementLocated(locator));
		WebElement element = driver.findElement(locator);
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click()", element);
		logger.info("Selected boolean :"+value);
	}

	public void selectButton(By locator,String value){
		new WebDriverWait(driver,60).until(ExpectedConditions.visibilityOfElementLocated(locator));
		if(value.equalsIgnoreCase("Yes")) {
			driver.findElement(locator).click();
		}
		else
		{
			driver.findElement(locator).click();
		}
		logger.info("Clicking on "+ value +" button");
	}

	public void enterText(By locator,String text){
		Actions actions = new Actions(driver);
		new WebDriverWait(driver,60).until(ExpectedConditions.presenceOfElementLocated(locator));
		new WebDriverWait(driver,60).until(ExpectedConditions.elementToBeClickable(locator));
		/*WebElement element = driver.findElement(locator);
		actions.moveToElement(element);
		actions.click();
		actions.sendKeys(text);
		actions.build().perform();*/
		driver.findElement(locator).sendKeys(text);
		logger.info("Entered text :"+text);
	}

	public LoanAmountPage enterPurchasePropertyDetails(String propertyTyoe, boolean isProperty, boolean isContract, String propertyUse, String firstAddress, String city, String state, String zip) throws Exception {
		selectFromDropdown(PropertyTypeDropdown,propertyTyoe);
		selectBoolean(YesPropertyButton,isProperty);
		selectBoolean(YesContractButton,isContract);
		selectFromDropdown(PropertyUseDropdown,propertyUse);
		selectAddress(firstAddress,city,state,zip);
		return new LoanAmountPage(driver);
	}

	public void selectAddress(String firstAddress, String city, String state, String zip) throws Exception {
		enterText(AddressTextBox,firstAddress);
		Thread.sleep(3000);
		selectOptionWithText(firstAddress);
		Thread.sleep(3000);
		clickNext();
	}

	private void selectOptionWithText(String firstAddress) {
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='pac-item']//span[contains(text(),'"+firstAddress+"')]")));
		driver.findElement(By.xpath("//div[@class='pac-item']//span[contains(text(),'"+firstAddress+"')]")).click();
		logger.info("Selected address :" + firstAddress);
	}

	public void clickNext() throws Exception {
		new WebDriverWait(driver,60).until(ExpectedConditions.elementToBeClickable(NextButton));
		driver.findElement(NextButton).click();
		logger.info("Clicking on NEXT button");
	}
}


