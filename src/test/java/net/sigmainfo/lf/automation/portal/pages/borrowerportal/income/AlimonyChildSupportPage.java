package net.sigmainfo.lf.automation.portal.pages.borrowerportal.income;

import net.sigmainfo.lf.automation.common.AbstractTests;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by shaishav.s on 07-09-2017.
 */
public class AlimonyChildSupportPage  extends AbstractTests{
    WebDriverWait wait = new WebDriverWait(driver,60);
    WebElement PropertyTypeListItem;

    private Logger logger = LoggerFactory.getLogger(AlimonyChildSupportPage.class);

    public AlimonyChildSupportPage(WebDriver driver) {
        this.driver = driver;
        logger.info("========= AlimonyChildSupportPage is loaded===========");
    }

    public AlimonyChildSupportPage() {}

    public static By SupportByCourtButton =By.xpath("//ui-switch[@id='36']//label[normalize-space(.)='Yes']");
    public static By willReceiveOrderByCourtButton =By.xpath("//ui-switch[@id='37']//label[normalize-space(.)='Yes']");
    public static By twoMonthSupportButton =By.xpath("//ui-switch[@id='38']//label[normalize-space(.)='Yes']");

    public static By NoSupportByCourtButton =By.xpath("//ui-switch[@id='36']//label[normalize-space(.)='No']");
    public static By willNotReceiveOrderByCourtButton =By.xpath("//ui-switch[@id='37']//label[normalize-space(.)='No']");
    public static By NoTwoMonthSupportButton =By.xpath("//ui-switch[@id='38']//label[normalize-space(.)='No']");

    public static By NextButton = By.xpath("//button[@name='next']");
    
    public void selectBoolean(By locator,boolean value)
    {
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("arguments[0].click()", element);
        logger.info("Selected boolean :"+value);
    }
    
    public void clickNext() throws Exception {
        wait.until(ExpectedConditions.elementToBeClickable(NextButton));
        driver.findElement(NextButton).click();
        logger.info("Clicking on NEXT button");
    }

	public BusinessSelfEmploymentPage enterAlimonySupportDetails(boolean supportByCourt,
			boolean receiveOrderByCourt, boolean twoMonthSupport) throws Exception {
		if(supportByCourt)
		{
			selectBoolean(SupportByCourtButton, supportByCourt);
		}
		else
		{
			selectBoolean(NoSupportByCourtButton, supportByCourt);
		}
		Thread.sleep(3000);
        new WebDriverWait(driver,30).until(ExpectedConditions.elementToBeClickable(willReceiveOrderByCourtButton));
        if(receiveOrderByCourt)
		{
			selectBoolean(willReceiveOrderByCourtButton, supportByCourt);
		}
		else
		{
			selectBoolean(willNotReceiveOrderByCourtButton, supportByCourt);
		}
        Thread.sleep(3000);
        new WebDriverWait(driver,30).until(ExpectedConditions.elementToBeClickable(twoMonthSupportButton));
        if(receiveOrderByCourt)
		{
			selectBoolean(twoMonthSupportButton, twoMonthSupport);
		}
		else
		{
			selectBoolean(NoTwoMonthSupportButton, twoMonthSupport);
		}
        new WebDriverWait(driver,30).until(ExpectedConditions.elementToBeClickable(NextButton));
        clickNext();
		return new BusinessSelfEmploymentPage(driver);
	}
}
