package net.sigmainfo.lf.automation.portal.pages.borrowerportal.profile;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.constant.PortalParam;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import static org.testng.AssertJUnit.assertTrue;

/**
 * Created by shaishav.s on 09-10-2017.
 */
public class RefinanceLoanAmountPage extends AbstractTests {

    PortalFuncUtils portalFuncUtils;

    WebDriverWait wait = new WebDriverWait(driver,60);

    private Logger logger = LoggerFactory.getLogger(RefinanceLoanAmountPage.class);

    public RefinanceLoanAmountPage(WebDriver driver) {
        this.driver = driver;
        Assert.assertTrue(driver.findElement(By.xpath("//div[@id='SS3']/h3")).getText().contains(portalParam.firstName+", tell us about the loan you want"),"RefinanceLoanAmountPage header is not displayed.");
        logger.info("========= RefinanceLoanAmountPage is loaded===========");
    }

    public static By worthOfPropertyTextbox = By.xpath("//div[@id='SS3']/questioner-question-set/div[3]//input[@id='input']");
    public static By firstOweTextbox = By.xpath("//div[@id='SS3']/questioner-question-set/div[5]/div/div[1]//input[@id='input']");
    public static By secondOweTextbox = By.xpath("//div[@id='SS3']/questioner-question-set/div[5]/div/div[2]//input[@id='input']");
    public static By otherOweTextbox = By.xpath("//div[@id='SS3']/questioner-question-set/div[6]//input[@id='input']");
    public static By cashOutYes = By.xpath("//ui-switch[@id='85']//label[normalize-space(.)='Yes']");
    public static By cashOutNo = By.xpath("//ui-switch[@id='85']//label[normalize-space(.)='No']");
    public static By cashOutForHomeEquityTextbox = By.xpath("//div[@id='SS3']/questioner-question-set/div[8]//input[@id='input']");
    public static By debtConsolidationButton = By.xpath("//div[@id='SS3']//ui-single-toggle/div[1]/label");
    public static By investmentButton = By.xpath("//div[@id='SS3']//ui-single-toggle/div[2]/label");
    public static By otherButton = By.xpath("//div[@id='SS3']//ui-single-toggle/div[3]/label");
    public static By NextButton = By.xpath("//button[@name='next']");

    public RefinanceLoanAmountPage() {
    }

    public WhosOnLoanPage enterRefinanceLoanDetails(String worthOfProperty, String firstOwe, String secondOwe, String otherOwe, boolean ifCashOut, String cashOutAmount, String cashOutPurpose) throws Exception {
        wait.until(ExpectedConditions.elementToBeClickable(worthOfPropertyTextbox));
        enterText(worthOfPropertyTextbox,worthOfProperty);
        enterText(firstOweTextbox,firstOwe);
        enterText(secondOweTextbox,secondOwe);
        enterText(otherOweTextbox,otherOwe);
        selectIfCashOut(ifCashOut);
        clickNext();
        return new WhosOnLoanPage(driver);
    }

    public void clickNext() throws Exception {
        portalFuncUtils.scrollUntil(NextButton,driver);
        wait.until(ExpectedConditions.elementToBeClickable(NextButton));
        driver.findElement(NextButton).click();
        logger.info("Clicking on NEXT button");
    }

    private void selectIfCashOut(boolean ifCashOut) {
        if(ifCashOut) {
            driver.findElement(cashOutYes).click();
            wait.until(ExpectedConditions.presenceOfElementLocated(cashOutForHomeEquityTextbox));
            enterText(cashOutForHomeEquityTextbox, portalParam.cashOutAmount);
            selectCashOutPurpose(portalParam.cashOutPurpose);
        }
        else {
            driver.findElement(cashOutNo).click();
        }
    }

    private void selectCashOutPurpose(String cashOutPurpose) {
        if(cashOutPurpose.equalsIgnoreCase("Debt Consolidation"))
        {
            wait.until(ExpectedConditions.presenceOfElementLocated(debtConsolidationButton));
            driver.findElement(debtConsolidationButton).click();
        }
        else if(cashOutPurpose.equalsIgnoreCase("Investment"))
        {
            wait.until(ExpectedConditions.presenceOfElementLocated(debtConsolidationButton));
            driver.findElement(investmentButton).click();
        }
        else if(cashOutPurpose.equalsIgnoreCase("Other"))
        {
            wait.until(ExpectedConditions.presenceOfElementLocated(debtConsolidationButton));
            driver.findElement(otherButton).click();
        }
        logger.info("Selected :"+cashOutPurpose);
    }

    public void enterText(By locator,String text){
        Actions actions = new Actions(driver);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        actions.moveToElement(element);
        actions.click();
        actions.sendKeys(text);
        actions.build().perform();
        logger.info("Entering :"+text);
    }
}
