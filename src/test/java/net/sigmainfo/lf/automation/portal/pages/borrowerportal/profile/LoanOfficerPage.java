package net.sigmainfo.lf.automation.portal.pages.borrowerportal.profile;

import net.sigmainfo.lf.automation.common.AbstractTests;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by shaishav.s on 06-09-2017.
 */
public class LoanOfficerPage extends AbstractTests {
    WebDriverWait wait = new WebDriverWait(driver,60);
    WebElement PropertyTypeListItem;

    private Logger logger = LoggerFactory.getLogger(LoanOfficerPage.class);

    public LoanOfficerPage(WebDriver driver) {
        this.driver = driver;
        logger.info("========= LoanOfficerPage is loaded===========");
    }

    public LoanOfficerPage() {
    }

    public static By WorkingWithLoanOfficerButton = By.xpath("//ui-switch[@id='60']//label[normalize-space(.)='Yes']");
    public static By NotWorkingWithLoanOfficerButton = By.xpath("//ui-switch[@id='60']//label[normalize-space(.)='No']");
    public static By SearchLoanOfficerTextBox = By.xpath("//label[contains(text(),'Search for loan officer')]");
    public static By NextButton = By.xpath("//button[contains(text(),'Next')]");

    public CurrentResidenceDetailsPage enterLoanOfficerDetail(boolean withLoanOfficer, String loanOfficerName) throws Exception {
        enterLoanOfficerDetails(withLoanOfficer,loanOfficerName);
        Thread.sleep(3000);
        clickNext();
        return new CurrentResidenceDetailsPage(driver);
    }

    private void enterLoanOfficerDetails(boolean withLoanOfficer, String loanOfficerName) {
        if(withLoanOfficer)
        {
            selectButton(WorkingWithLoanOfficerButton,"Working with Loan officer");
            enterLoanOfficerName(loanOfficerName);

        }
        else
        {
            selectButton(NotWorkingWithLoanOfficerButton,"Not working with Loan officer");
        }
    }

    private void enterLoanOfficerName(String loanOfficerName) {
        enterText(SearchLoanOfficerTextBox,loanOfficerName);
    }

    public void selectButton(By locator,String value){
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        if(value.equalsIgnoreCase("Yes")) {
            driver.findElement(locator).click();
        }
        else
        {
            driver.findElement(locator).click();
        }
        logger.info("Clicking on "+ value +" button");
    }

    public void clickNext() throws Exception {
        wait.until(ExpectedConditions.elementToBeClickable(NextButton));
        driver.findElement(NextButton).click();
        logger.info("Clicking on NEXT button");
    }

    public void enterText(By locator,String text){
        Actions actions = new Actions(driver);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        actions.moveToElement(element);
        actions.click();
        actions.sendKeys(text);
        actions.build().perform();
    }
}
