package net.sigmainfo.lf.automation.portal.pages.borrowerportal.income;

import net.sigmainfo.lf.automation.common.AbstractTests;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.testng.AssertJUnit.assertTrue;

/**
 * Created by shaishav.s on 25-09-2017.
 */
public class SpouseIncomeSearchPage extends AbstractTests {
    WebDriverWait wait = new WebDriverWait(driver,60);
    WebElement PropertyTypeListItem;

    private Logger logger = LoggerFactory.getLogger(SpouseIncomeSearchPage.class);

    public SpouseIncomeSearchPage(WebDriver driver) {
        this.driver = driver;
        logger.info("========= SpouseIncomeSearchPage is loaded===========");
//        assertTrue(driver.findElement(By.xpath("//div[starts-with(@id,'sigma_shaishav')']/h3[contains(text(),\""+portalParam.spouceFirstName+"'s income search\")]")).isDisplayed());
    }

    public SpouseIncomeSearchPage() {
    }

    public static By SpouseDoBTextBox = By.xpath("//div[starts-with(@id,'sigma_shaishav')]//label[contains(text(),'Date of Birth (MM/DD/YYYY)')]");
    public static By SpouseSsnTextBox = By.xpath("//div[starts-with(@id,'sigma_shaishav')]//label[contains(text(),'Social Security #')]");
    public static By FindMyIncomeButton = By.xpath("//button[@name='next']");

    public void enterText(By locator,String text) throws InterruptedException {
        Actions actions = new Actions(driver);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        actions.moveToElement(element);
        actions.click();
        actions.sendKeys(text);
        actions.build().perform();
        logger.info("Entering :"+text);
        Thread.sleep(1500);
    }

    public void clickButton() throws Exception {
        wait.until(ExpectedConditions.elementToBeClickable(FindMyIncomeButton));
        driver.findElement(FindMyIncomeButton).click();
        logger.info("Clicking on Find My Income button");
    }


    public SpouseIncomeTypePage enterSpouseIncomeSearchDetails(String spouseDateOfBirth, String spouseSsnNumber) throws Exception {
        enterText(SpouseDoBTextBox, spouseDateOfBirth);
        enterText(SpouseSsnTextBox, spouseSsnNumber);
        clickButton();
        return new SpouseIncomeTypePage(driver);
    }
}
