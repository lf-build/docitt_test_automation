package net.sigmainfo.lf.automation.portal.constant;

import org.springframework.stereotype.Component;

/**
 * Created by           : Shaishav.s on 08-02-2017.
 * Test class           : PortalParam.java
 * Description          : Contains all placeholders declared within the test package
 * Includes             : 1. Declares the place holders being used
 *                        2. Contains getter and setter methods
 */
@Component
public class PortalParam {

    public static String baseweburl;
    public static String lenderurl;
    public static String borrowerurl;
    public static String docittwebuserid;
    public static String lenderUserLogin;
    public static String encryptedPassword;
    public static String docittwebuserpassword;
    public static String custom_report_location;
    public static String upload_file_loaction;
    public static String username;
    public static String password;
    public static String browser;
    public static String test_id;
    public static String mongoHost;
    public static int mongoPort;
    public static String mongoUser;
    public static String mongoPwd;
    public static String companyName;
    public static String mailId;
    public static String pwd;
    public static String confirmPassword;
    public static String firstName;
    public static String middleName;
    public static String lastName;
    public static String suffix;
    public static String phone;
    public static String modeOfCommunication;
    public static String propertyType;
    public static boolean isProperty;
    public static boolean isContract;
    public static String propertyUse;
    public static boolean married;
    public static boolean addSpouce;
    public static boolean addCoborrower;
    public static boolean addDependant;
    public static String firstAddress;
    public static String city;
    public static String state;
    public static String zip;
    public static boolean isEmailVerification;
    public static boolean isMobileVerification;
    public static String purchasePrice;
    public static String downPayment;
    public static String spouceFirstName;
    public static String spouceMiddleName;
    public static String spouceLastName;
    public static String spouceSuffix;
    public static String spoucePhone;
    public static String spouceComm;
    public static String spouseEmployer;
    public static String spouseTitle;
    public static String spouseEmpStartDate;
    public static String spouseAlimonyChildName;
    public static boolean spouseSupportByCourt;
    public static boolean SpouseWillReceiveOrderByCourt;
    public static boolean spouseTwoMonthSupport;
    public static String spouseBusinessMonthlyIncome;
    public static String spouseBusinessCompanyName;
    public static String spouseBusinessTitle;
    public static String spouseBusinessAddress;
    public static String spouseBusinessPhone;
    public static String spouseBusinessStartDate;
    public static String spouseInstitutionName;
    public static String spouseAccountType;
    public static String spouseCurrentBalance;
    public static String spouseAccountNumber;
    public static String spouseAccountName;
    public static boolean spoucePercentOwnership;
    public static String spouseTypeOfCompany;
    public static boolean spousePercentOwnership;
    public static String spouseAlimonyChildDoB;
    public static String spouseEmpAddress;
    public static String spouseMonthlybaseSalary;
    public static String spouseBonusAmount;
    public static boolean eligibleLoan;
    public static boolean currentLoan;
    public static boolean realEstateAgent;
    public static boolean withLoanOfficer;
    public static String loanOfficerName;
    public static boolean sameResidence;
    public static String residenceStatus;
    public static String currentAddress;
    public static String stayingSince;
    public static boolean planningToSale;
    public static boolean additionalProperty;
    public static String propertyAddress;
    public static String typeOfProperty;
    public static String propertyStatus;
    public static String propertyValue;
    public static String rentalIncome;
    public static String expenses;
    public static String institutionName;
    public static String accountType;
    public static String currentBalance;
    public static String accountNumber;
    public static String accountName;
    public static String dateOfBirth;
    public static String ssnNumber;
    public static String currentEmployer;
    public static String employmentTitle;
    public static String employmentStartDate;
    public static String inThisLineYear;
    public static String inThisLineMonth;
    public static String monthlyBaseSalary;
    public static String bonusAmount;
    public static String businessAddress;
    public static String businessPhone;
    public static String monthlyAlimony;
    public static String alimonyStartDate;
    public static String monthlyChildSupport;
    public static String childName;
    public static String childDoB;
    public static boolean supportByCourt;
    public static boolean willReceiveOrderByCourt;
    public static boolean twoMonthSupport;
    public static String selfEmploymentMonthlyIncome;
    public static String selfEmploymentCompanyName;
    public static String selfEmploymentTitle;
    public static String selfEmploymentBusinessAddress;
    public static String selfEmploymentBusinessPhone;
    public static String selfEmploymentBusinessStartDate;
    public static boolean percentageOwnership;
    public static String typeOfCompany;
    public static String monthlyMilitaryPay;
    public static String monthlyRentalIncome;
    public static String rentalPropertyAddress;
    public static String typeOfRentalProperty;
    public static String socialSecurityIncome;
    public static String interestLastYear;
    public static String interestPreviousYear;
    public static String plaidUsername;
    public static String plaidPassword;
    public static String plaidPin;
    public static boolean continuousIncome;
    public static String otherIncomePerMonth;
    public static String sourceOfIncome;
    public static String spouseRentalPropertyAddress;
    public static String spouseDateOfBirth;
    public static String spouseSsnNumber;
    public static boolean addCoborrwer;
    public static String testid;
    public static String mongoDb;
    public static String gmailPassword;
    public static String borrowerEmail;
    public static String spouseEmail;
    public static String nonSpouseEmail;

    public static String declarationQueA;
    public static String declarationQueB;
    public static String declarationQueC;
    public static String declarationQueD;
    public static String declarationQueE;
    public static String declarationQueF;
    public static String declarationQueG;
    public static String declarationQueH;
    public static String declarationQueI;
    public static String declarationQueJ;
    public static String declarationQueK;
    public static String declarationQueL;
    public static String declarationQueM;
    public static String DeclarationQuestionAText;
    public static String DeclarationQuestionBText;
    public static String DeclarationQuestionCText;
    public static String DeclarationQuestionDText;
    public static String DeclarationQuestionEText;
    public static String DeclarationQuestionFText;
    public static String DeclarationQuestionGText;
    public static String DeclarationQuestionHText;
    public static String DeclarationQuestionIText;
    public static String DeclarationBorrowerWishToFurnish;
    public static String DeclarationEthnicity;
    public static String DeclarationRace;
    public static String DeclarationSex;

    public static String getSpouseSex() {
        return spouseSex;
    }

    public static void setSpouseSex(String spouseSex) {
        PortalParam.spouseSex = spouseSex;
    }

    public static String spouseSex;
    public static String NonSpouseFirstName;
    public static String NonSpouseMiddleName;
    public static String NonSpouseLastName;
    public static String NonSpouseSuffix;
    public static String NonSpousePhone;
    public static String TaxuserName;
    public static String Taxpassword;

    public static String PayStubInstitutionName;
    public static String PayStubOwnerName;
    public static String PayStubDate;
    public static String PayStubCompanyName;
    public static String PayStubTotal;
    public static boolean lenderTeam;
    public static String gmailId;
    public static String ExplanationSummary;

    // REFINANCE VARIABLES

    public static String worthOfProperty;
    public static String firstOwe;
    public static String secondOwe;
    public static String otherOwe;
    public static boolean ifCashOut;
    public static String cashOutAmount;
    public static String cashOutPurpose;

    public static String postAppInstitutionName;
    public static String postAppInstitutionOwner;
    public static String postAppInstitutionType;
    public static String postAppDate;
    public static String postAppCompany;
    public static String postAppAmount;

    public static String inviteFirstName;
    public static String inviteLastName;
    public static String inviteUserType;
    public static String invitePhone;
    public static String inviteBoth;

    //Lender Calendar Event Variables
    public static String event_Title;
    public static String event_Location;
    public static String event_StartTime;
    public static String event_EndTime;
    public static String event_Description;
    public static String event_Priority_Set;
    public static String event_InviteEmail;

    public static String getBorrowerEmail() {
        return borrowerEmail;
    }

    public static void setBorrowerEmail(String borrowerEmail) {
        PortalParam.borrowerEmail = borrowerEmail;
    }

    public static String getSpouseEmail() {
        return spouseEmail;
    }

    public static void setSpouseEmail(String spouseEmail) {
        PortalParam.spouseEmail = spouseEmail;
    }

    public static String getNonSpouseEmail() {
        return nonSpouseEmail;
    }

    public static void setNonSpouseEmail(String nonSpouseEmail) {
        PortalParam.nonSpouseEmail = nonSpouseEmail;
    }

    public static String getInviteFirstName() {
        return inviteFirstName;
    }

    public static void setInviteFirstName(String inviteFirstName) {
        PortalParam.inviteFirstName = inviteFirstName;
    }

    public static String getInviteLastName() {
        return inviteLastName;
    }

    public static void setInviteLastName(String inviteLastName) {
        PortalParam.inviteLastName = inviteLastName;
    }

    public static String getInviteUserType() {
        return inviteUserType;
    }

    public static void setInviteUserType(String inviteUserType) {
        PortalParam.inviteUserType = inviteUserType;
    }

    public static String getInvitePhone() {
        return invitePhone;
    }

    public static void setInvitePhone(String invitePhone) {
        PortalParam.invitePhone = invitePhone;
    }

    public static String getInviteBoth() {
        return inviteBoth;
    }

    public static void setInviteBoth(String inviteBoth) {
        PortalParam.inviteBoth = inviteBoth;
    }

    public static String getEvent_Title() {
        return event_Title;
    }

    public static void setEvent_Title(String event_Title) {
        PortalParam.event_Title = event_Title;
    }

    public static String getEvent_Location() {
        return event_Location;
    }

    public static void setEvent_Location(String event_Location) {
        PortalParam.event_Location = event_Location;
    }

    public static String getEvent_StartTime() {
        return event_StartTime;
    }

    public static void setEvent_StartTime(String event_StartTime) {
        PortalParam.event_StartTime = event_StartTime;
    }

    public static String getEvent_EndTime() {
        return event_EndTime;
    }

    public static void setEvent_EndTime(String event_EndTime) {
        PortalParam.event_EndTime = event_EndTime;
    }

    public static String getEvent_Description() {
        return event_Description;
    }

    public static void setEvent_Description(String event_Description) {
        PortalParam.event_Description = event_Description;
    }

    public static String getEvent_Priority_Set() {
        return event_Priority_Set;
    }

    public static void setEvent_Priority_Set(String event_Priority_Set) {
        PortalParam.event_Priority_Set = event_Priority_Set;
    }

    public static String getEvent_InviteEmail() {
        return event_InviteEmail;
    }

    public static void setEvent_InviteEmail(String event_InviteEmail) {
        PortalParam.event_InviteEmail = event_InviteEmail;
    }




    public static String getLenderurl() {
        return lenderurl;
    }

    public static void setLenderurl(String lenderurl) {
        PortalParam.lenderurl = lenderurl;
    }

    public static String getPostAppInstitutionName() {
        return postAppInstitutionName;
    }

    public static void setPostAppInstitutionName(String postAppInstitutionName) {
        PortalParam.postAppInstitutionName = postAppInstitutionName;
    }

    public static String getPostAppInstitutionOwner() {
        return postAppInstitutionOwner;
    }

    public static void setPostAppInstitutionOwner(String postAppInstitutionOwner) {
        PortalParam.postAppInstitutionOwner = postAppInstitutionOwner;
    }

    public static String getPostAppInstitutionType() {
        return postAppInstitutionType;
    }

    public static void setPostAppInstitutionType(String postAppInstitutionType) {
        PortalParam.postAppInstitutionType = postAppInstitutionType;
    }

    public static String getWorthOfProperty() {
        return worthOfProperty;
    }

    public static void setWorthOfProperty(String worthOfProperty) {
        PortalParam.worthOfProperty = worthOfProperty;
    }

    public static String getFirstOwe() {
        return firstOwe;
    }

    public static void setFirstOwe(String firstOwe) {
        PortalParam.firstOwe = firstOwe;
    }

    public static String getSecondOwe() {
        return secondOwe;
    }

    public static void setSecondOwe(String secondOwe) {
        PortalParam.secondOwe = secondOwe;
    }

    public static String getOtherOwe() {
        return otherOwe;
    }

    public static void setOtherOwe(String otherOwe) {
        PortalParam.otherOwe = otherOwe;
    }

    public static boolean isIfCashOut() {
        return ifCashOut;
    }

    public static void setIfCashOut(boolean ifCashOut) {
        PortalParam.ifCashOut = ifCashOut;
    }

    public static String getCashOutAmount() {
        return cashOutAmount;
    }

    public static void setCashOutAmount(String cashOutAmount) {
        PortalParam.cashOutAmount = cashOutAmount;
    }

    public static String getCashOutPurpose() {
        return cashOutPurpose;
    }

    public static void setCashOutPurpose(String cashOutPurpose) {
        PortalParam.cashOutPurpose = cashOutPurpose;
    }

    public static String getUpload_file_loaction() {
        return upload_file_loaction;
    }

    public static void setUpload_file_loaction(String upload_file_loaction) {
        PortalParam.upload_file_loaction = upload_file_loaction;
    }

    public static String getExplanationSummary() {
        return ExplanationSummary;
    }

    public static void setExplanationSummary(String explanationSummary) {
        ExplanationSummary = explanationSummary;
    }

    public static String getSpouceMiddleName() {
        return spouceMiddleName;
    }

    public static void setSpouceMiddleName(String spouceMiddleName) {
        PortalParam.spouceMiddleName = spouceMiddleName;
    }

    public static String getSpouceSuffix() {
        return spouceSuffix;
    }

    public static void setSpouceSuffix(String spouceSuffix) {
        PortalParam.spouceSuffix = spouceSuffix;
    }


    public static boolean getSpouseSupportByCourt() {
        return spouseSupportByCourt;
    }

    public static void setSpouseSupportByCourt(boolean spouseSupportByCourt) {
        PortalParam.spouseSupportByCourt = spouseSupportByCourt;
    }

    public static String getSpouseRentalPropertyAddress() {
        return spouseRentalPropertyAddress;
    }

    public static void setSpouseRentalPropertyAddress(String spouseRentalPropertyAddress) {
        PortalParam.spouseRentalPropertyAddress = spouseRentalPropertyAddress;
    }

    public static boolean getSpouseWillReceiveOrderByCourt() {
        return SpouseWillReceiveOrderByCourt;
    }

    public static void setSpouseWillReceiveOrderByCourt(boolean spouseWillReceiveOrderByCourt) {
        SpouseWillReceiveOrderByCourt = spouseWillReceiveOrderByCourt;
    }

    public static boolean getSpouseTwoMonthSupport() {
        return spouseTwoMonthSupport;
    }

    public static void setSpouseTwoMonthSupport(boolean spouseTwoMonthSupport) {
        PortalParam.spouseTwoMonthSupport = spouseTwoMonthSupport;
    }


    public static String getSpouseInstitutionName() {
        return spouseInstitutionName;
    }

    public static void setSpouseInstitutionName(String spouseInstitutionName) {
        PortalParam.spouseInstitutionName = spouseInstitutionName;
    }

    public static String getSpouseAccountType() {
        return spouseAccountType;
    }

    public static void setSpouseAccountType(String spouseAccountType) {
        PortalParam.spouseAccountType = spouseAccountType;
    }

    public static String getSpouseCurrentBalance() {
        return spouseCurrentBalance;
    }

    public static void setSpouseCurrentBalance(String spouseCurrentBalance) {
        PortalParam.spouseCurrentBalance = spouseCurrentBalance;
    }

    public static String getSpouseAccountNumber() {
        return spouseAccountNumber;
    }

    public static void setSpouseAccountNumber(String spouseAccountNumber) {
        PortalParam.spouseAccountNumber = spouseAccountNumber;
    }

    public static String getSpouseAccountName() {
        return spouseAccountName;
    }

    public static void setSpouseAccountName(String spouseAccountName) {
        PortalParam.spouseAccountName = spouseAccountName;
    }


    public static boolean isSpoucePercentOwnership() {
        return spoucePercentOwnership;
    }

    public static void setSpoucePercentOwnership(boolean spoucePercentOwnership) {
        PortalParam.spoucePercentOwnership = spoucePercentOwnership;
    }



    public static boolean isSpouseSupportByCourt() {
        return spouseSupportByCourt;
    }

    public static boolean isSpouseWillReceiveOrderByCourt() {
        return SpouseWillReceiveOrderByCourt;
    }

    public static boolean isSpouseTwoMonthSupport() {
        return spouseTwoMonthSupport;
    }

    public static String getSpouseBusinessMonthlyIncome() {
        return spouseBusinessMonthlyIncome;
    }

    public static void setSpouseBusinessMonthlyIncome(String spouseBusinessMonthlyIncome) {
        PortalParam.spouseBusinessMonthlyIncome = spouseBusinessMonthlyIncome;
    }

    public static String getSpouseBusinessCompanyName() {
        return spouseBusinessCompanyName;
    }

    public static void setSpouseBusinessCompanyName(String spouseBusinessCompanyName) {
        PortalParam.spouseBusinessCompanyName = spouseBusinessCompanyName;
    }

    public static String getSpouseBusinessTitle() {
        return spouseBusinessTitle;
    }

    public static void setSpouseBusinessTitle(String spouseBusinessTitle) {
        PortalParam.spouseBusinessTitle = spouseBusinessTitle;
    }

    public static String getSpouseBusinessAddress() {
        return spouseBusinessAddress;
    }

    public static void setSpouseBusinessAddress(String spouseBusinessAddress) {
        PortalParam.spouseBusinessAddress = spouseBusinessAddress;
    }

    public static String getSpouseBusinessPhone() {
        return spouseBusinessPhone;
    }

    public static void setSpouseBusinessPhone(String spouseBusinessPhone) {
        PortalParam.spouseBusinessPhone = spouseBusinessPhone;
    }

    public static String getSpouseBusinessStartDate() {
        return spouseBusinessStartDate;
    }

    public static void setSpouseBusinessStartDate(String spouseBusinessStartDate) {
        PortalParam.spouseBusinessStartDate = spouseBusinessStartDate;
    }

    public static String getSpouseTypeOfCompany() {
        return spouseTypeOfCompany;
    }

    public static void setSpouseTypeOfCompany(String spouseTypeOfCompany) {
        PortalParam.spouseTypeOfCompany = spouseTypeOfCompany;
    }

    public static boolean isSpousePercentOwnership() {
        return spousePercentOwnership;
    }

    public static void setSpousePercentOwnership(boolean spousePercentOwnership) {
        PortalParam.spousePercentOwnership = spousePercentOwnership;
    }

    public static String getSpouseAlimonyChildName() {
        return spouseAlimonyChildName;
    }

    public static void setSpouseAlimonyChildName(String spouseAlimonyChildName) {
        PortalParam.spouseAlimonyChildName = spouseAlimonyChildName;
    }

    public static String getSpouseAlimonyChildDoB() {
        return spouseAlimonyChildDoB;
    }

    public static void setSpouseAlimonyChildDoB(String spouseAlimonyChildDoB) {
        PortalParam.spouseAlimonyChildDoB = spouseAlimonyChildDoB;
    }



    public static String getSpouseEmpAddress() {
        return spouseEmpAddress;
    }

    public static void setSpouseEmpAddress(String spouseEmpAddress) {
        PortalParam.spouseEmpAddress = spouseEmpAddress;
    }



    public static String getSpouseEmployer() {
        return spouseEmployer;
    }

    public static void setSpouseEmployer(String spouseEmployer) {
        PortalParam.spouseEmployer = spouseEmployer;
    }

    public static String getSpouseTitle() {
        return spouseTitle;
    }

    public static void setSpouseTitle(String spouseTitle) {
        PortalParam.spouseTitle = spouseTitle;
    }

    public static String getSpouseEmpStartDate() {
        return spouseEmpStartDate;
    }

    public static void setSpouseEmpStartDate(String spouseEmpStartDate) {
        PortalParam.spouseEmpStartDate = spouseEmpStartDate;
    }

    public static String getSpouseMonthlybaseSalary() {
        return spouseMonthlybaseSalary;
    }

    public static void setSpouseMonthlybaseSalary(String spouseMonthlybaseSalary) {
        PortalParam.spouseMonthlybaseSalary = spouseMonthlybaseSalary;
    }

    public static String getSpouseBonusAmount() {
        return spouseBonusAmount;
    }

    public static void setSpouseBonusAmount(String spouseBonusAmount) {
        PortalParam.spouseBonusAmount = spouseBonusAmount;
    }


    public static String getSpouseDateOfBirth() {
        return spouseDateOfBirth;
    }

    public static void setSpouseDateOfBirth(String spouseDateOfBirth) {
        PortalParam.spouseDateOfBirth = spouseDateOfBirth;
    }

    public static String getSpouseSsnNumber() {
        return spouseSsnNumber;
    }

    public static void setSpouseSsnNumber(String spouseSsnNumber) {
        PortalParam.spouseSsnNumber = spouseSsnNumber;
    }


    public  static String TaxInstitution;
    public static String getTaxInstitution() {
        return TaxInstitution;
    }

    public static void setTaxInstitution(String taxInstitution) {
        TaxInstitution = taxInstitution;
    }

    public static String getTaxuserName() {
        return TaxuserName;
    }

    public static void setTaxuserName(String taxuserName) {
        TaxuserName = taxuserName;
    }

    public static String getTaxpassword() {
        return Taxpassword;
    }

    public static void setTaxpassword(String taxpassword) {
        Taxpassword = taxpassword;
    }

    public static String getPayStubInstitutionName() {
        return PayStubInstitutionName;
    }

    public static void setPayStubInstitutionName(String payStubInstitutionName) {
        PayStubInstitutionName = payStubInstitutionName;
    }

    public static String getPayStubOwnerName() {
        return PayStubOwnerName;
    }

    public static void setPayStubOwnerName(String payStubOwnerName) {
        PayStubOwnerName = payStubOwnerName;
    }

    public static String getPayStubDate() {
        return PayStubDate;
    }

    public static void setPayStubDate(String payStubDate) {
        PayStubDate = payStubDate;
    }

    public static String getPayStubCompanyName() {
        return PayStubCompanyName;
    }

    public static void setPayStubCompanyName(String payStubCompanyName) {
        PayStubCompanyName = payStubCompanyName;
    }

    public static String getPayStubTotal() {
        return PayStubTotal;
    }

    public static void setPayStubTotal(String payStubTotal) {
        PayStubTotal = payStubTotal;
    }



    public static String getNonSpouseFirstName() {
        return NonSpouseFirstName;
    }

    public static void setNonSpouseFirstName(String nonSpouseFirstName) {
        NonSpouseFirstName = nonSpouseFirstName;
    }

    public static String getNonSpouseMiddleName() {
        return NonSpouseMiddleName;
    }

    public static void setNonSpouseMiddleName(String nonSpouseMiddleName) {
        NonSpouseMiddleName = nonSpouseMiddleName;
    }

    public static String getNonSpouseLastName() {
        return NonSpouseLastName;
    }

    public static void setNonSpouseLastName(String nonSpouseLastName) {
        NonSpouseLastName = nonSpouseLastName;
    }

    public static String getNonSpouseSuffix() {
        return NonSpouseSuffix;
    }

    public static void setNonSpouseSuffix(String nonSpouseSuffix) {
        NonSpouseSuffix = nonSpouseSuffix;
    }

    public static String getNonSpousePhone() {
        return NonSpousePhone;
    }

    public static void setNonSpousePhone(String nonSpousePhone) {
        NonSpousePhone = nonSpousePhone;
    }

    public static String getDeclarationQueA() {
        return declarationQueA;
    }

    public static void setDeclarationQueA(String declarationQueA) {
        PortalParam.declarationQueA = declarationQueA;
    }

    public static String getDeclarationQueB() {
        return declarationQueB;
    }

    public static void setDeclarationQueB(String declarationQueB) {
        PortalParam.declarationQueB = declarationQueB;
    }

    public static String getDeclarationQueC() {
        return declarationQueC;
    }

    public static void setDeclarationQueC(String declarationQueC) {
        PortalParam.declarationQueC = declarationQueC;
    }

    public static String getDeclarationQueD() {
        return declarationQueD;
    }

    public static void setDeclarationQueD(String declarationQueD) {
        PortalParam.declarationQueD = declarationQueD;
    }

    public static String getDeclarationQueE() {
        return declarationQueE;
    }

    public static void setDeclarationQueE(String declarationQueE) {
        PortalParam.declarationQueE = declarationQueE;
    }

    public static String getDeclarationQueF() {
        return declarationQueF;
    }

    public static void setDeclarationQueF(String declarationQueF) {
        PortalParam.declarationQueF = declarationQueF;
    }

    public static String getDeclarationQueG() {
        return declarationQueG;
    }

    public static void setDeclarationQueG(String declarationQueG) {
        PortalParam.declarationQueG = declarationQueG;
    }

    public static String getDeclarationQueH() {
        return declarationQueH;
    }

    public static void setDeclarationQueH(String declarationQueH) {
        PortalParam.declarationQueH = declarationQueH;
    }

    public static String getDeclarationQueI() {
        return declarationQueI;
    }

    public static void setDeclarationQueI(String declarationQueI) {
        PortalParam.declarationQueI = declarationQueI;
    }

    public static String getDeclarationQueJ() {
        return declarationQueJ;
    }

    public static void setDeclarationQueJ(String declarationQueJ) {
        PortalParam.declarationQueJ = declarationQueJ;
    }

    public static String getDeclarationQueK() {
        return declarationQueK;
    }

    public static void setDeclarationQueK(String declarationQueK) {
        PortalParam.declarationQueK = declarationQueK;
    }

    public static String getDeclarationQueL() {
        return declarationQueL;
    }

    public static void setDeclarationQueL(String declarationQueL) {
        PortalParam.declarationQueL = declarationQueL;
    }

    public static String getDeclarationQueM() {
        return declarationQueM;
    }

    public static void setDeclarationQueM(String declarationQueM) {
        PortalParam.declarationQueM = declarationQueM;
    }

    public static String getDeclarationQuestionAText() {
        return DeclarationQuestionAText;
    }

    public static void setDeclarationQuestionAText(String declarationQuestionAText) {
        DeclarationQuestionAText = declarationQuestionAText;
    }

    public static String getDeclarationQuestionBText() {
        return DeclarationQuestionBText;
    }

    public static void setDeclarationQuestionBText(String declarationQuestionBText) {
        DeclarationQuestionBText = declarationQuestionBText;
    }

    public static String getDeclarationQuestionCText() {
        return DeclarationQuestionCText;
    }

    public static void setDeclarationQuestionCText(String declarationQuestionCText) {
        DeclarationQuestionCText = declarationQuestionCText;
    }

    public static String getDeclarationQuestionDText() {
        return DeclarationQuestionDText;
    }

    public static void setDeclarationQuestionDText(String declarationQuestionDText) {
        DeclarationQuestionDText = declarationQuestionDText;
    }

    public static String getDeclarationQuestionEText() {
        return DeclarationQuestionEText;
    }

    public static void setDeclarationQuestionEText(String declarationQuestionEText) {
        DeclarationQuestionEText = declarationQuestionEText;
    }

    public static String getDeclarationQuestionFText() {
        return DeclarationQuestionFText;
    }

    public static void setDeclarationQuestionFText(String declarationQuestionFText) {
        DeclarationQuestionFText = declarationQuestionFText;
    }

    public static String getDeclarationQuestionGText() {
        return DeclarationQuestionGText;
    }

    public static void setDeclarationQuestionGText(String declarationQuestionGText) {
        DeclarationQuestionGText = declarationQuestionGText;
    }

    public static String getDeclarationQuestionHText() {
        return DeclarationQuestionHText;
    }

    public static void setDeclarationQuestionHText(String declarationQuestionHText) {
        DeclarationQuestionHText = declarationQuestionHText;
    }

    public static String getDeclarationQuestionIText() {
        return DeclarationQuestionIText;
    }

    public static void setDeclarationQuestionIText(String declarationQuestionIText) {
        DeclarationQuestionIText = declarationQuestionIText;
    }

    public static String getDeclarationBorrowerWishToFurnish() {
        return DeclarationBorrowerWishToFurnish;
    }

    public static void setDeclarationBorrowerWishToFurnish(String declarationBorrowerWishToFurnish) {
        DeclarationBorrowerWishToFurnish = declarationBorrowerWishToFurnish;
    }

    public static String getDeclarationEthnicity() {
        return DeclarationEthnicity;
    }

    public static void setDeclarationEthnicity(String declarationEthnicity) {
        DeclarationEthnicity = declarationEthnicity;
    }

    public static String getDeclarationRace() {
        return DeclarationRace;
    }

    public static void setDeclarationRace(String declarationRace) {
        DeclarationRace = declarationRace;
    }

    public static String getDeclarationSex() {
        return DeclarationSex;
    }

    public static void setDeclarationSex(String declarationSex) {
        DeclarationSex = declarationSex;
    }

    public static boolean isContinuousIncome() {
        return continuousIncome;
    }

    public static void setContinuousIncome(boolean continuousIncome) {
        PortalParam.continuousIncome = continuousIncome;
    }

    public static String getOtherIncomePerMonth() {
        return otherIncomePerMonth;
    }

    public static void setOtherIncomePerMonth(String otherIncomePerMonth) {
        PortalParam.otherIncomePerMonth = otherIncomePerMonth;
    }

    public static String getSourceOfIncome() {
        return sourceOfIncome;
    }

    public static void setSourceOfIncome(String sourceOfIncome) {
        PortalParam.sourceOfIncome = sourceOfIncome;
    }

    public static String getChildName() {
        return childName;
    }

    public static void setChildName(String childName) {
        PortalParam.childName = childName;
    }

    public static String getChildDoB() {
        return childDoB;
    }

    public static void setChildDoB(String childDoB) {
        PortalParam.childDoB = childDoB;
    }

    public static String getDateOfBirth() {
        return dateOfBirth;
    }

    public static void setDateOfBirth(String dateOfBirth) {
        PortalParam.dateOfBirth = dateOfBirth;
    }

    public static String getSsnNumber() {
        return ssnNumber;
    }

    public static void setSsnNumber(String ssnNumber) {
        PortalParam.ssnNumber = ssnNumber;
    }

    public static String getPlaidUsername() {
        return plaidUsername;
    }

    public static void setPlaidUsername(String plaidUsername) {
        PortalParam.plaidUsername = plaidUsername;
    }

    public static String getPlaidPassword() {
        return plaidPassword;
    }

    public static void setPlaidPassword(String plaidPassword) {
        PortalParam.plaidPassword = plaidPassword;
    }

    public static String getPlaidPin() {
        return plaidPin;
    }

    public static void setPlaidPin(String plaidPin) {
        PortalParam.plaidPin = plaidPin;
    }

    public static String getInstitutionName() {
        return institutionName;
    }

    public static void setInstitutionName(String institutionName) {
        PortalParam.institutionName = institutionName;
    }

    public static String getAccountType() {
        return accountType;
    }

    public static void setAccountType(String accountType) {
        PortalParam.accountType = accountType;
    }

    public static String getCurrentBalance() {
        return currentBalance;
    }

    public static void setCurrentBalance(String currentBalance) {
        PortalParam.currentBalance = currentBalance;
    }

    public static String getAccountNumber() {
        return accountNumber;
    }

    public static void setAccountNumber(String accountNumber) {
        PortalParam.accountNumber = accountNumber;
    }

    public static String getAccountName() {
        return accountName;
    }

    public static void setAccountName(String accountName) {
        PortalParam.accountName = accountName;
    }

    public static String getInterestLastYear() {
        return interestLastYear;
    }

    public static void setInterestLastYear(String interestLastYear) {
        PortalParam.interestLastYear = interestLastYear;
    }

    public static String getInterestPreviousYear() {
        return interestPreviousYear;
    }

    public static void setInterestPreviousYear(String interestPreviousYear) {
        PortalParam.interestPreviousYear = interestPreviousYear;
    }

    public static String getSocialSecurityIncome() {
        return socialSecurityIncome;
    }

    public static void setSocialSecurityIncome(String socialSecurityIncome) {
        PortalParam.socialSecurityIncome = socialSecurityIncome;
    }

    public static String getMonthlyMilitaryPay() {
        return monthlyMilitaryPay;
    }

    public static void setMonthlyMilitaryPay(String monthlyMilitaryPay) {
        PortalParam.monthlyMilitaryPay = monthlyMilitaryPay;
    }

    public static String getMonthlyRentalIncome() {
        return monthlyRentalIncome;
    }

    public static void setMonthlyRentalIncome(String monthlyRentalIncome) {
        PortalParam.monthlyRentalIncome = monthlyRentalIncome;
    }

    public static String getRentalPropertyAddress() {
        return rentalPropertyAddress;
    }

    public static void setRentalPropertyAddress(String rentalPropertyAddress) {
        PortalParam.rentalPropertyAddress = rentalPropertyAddress;
    }

    public static String getTypeOfRentalProperty() {
        return typeOfRentalProperty;
    }

    public static void setTypeOfRentalProperty(String typeOfRentalProperty) {
        PortalParam.typeOfRentalProperty = typeOfRentalProperty;
    }

    public static String getSelfEmploymentMonthlyIncome() {
        return selfEmploymentMonthlyIncome;
    }

    public static void setSelfEmploymentMonthlyIncome(String selfEmploymentMonthlyIncome) {
        PortalParam.selfEmploymentMonthlyIncome = selfEmploymentMonthlyIncome;
    }

    public static String getSelfEmploymentCompanyName() {
        return selfEmploymentCompanyName;
    }

    public static void setSelfEmploymentCompanyName(String selfEmploymentCompanyName) {
        PortalParam.selfEmploymentCompanyName = selfEmploymentCompanyName;
    }

    public static String getSelfEmploymentTitle() {
        return selfEmploymentTitle;
    }

    public static void setSelfEmploymentTitle(String selfEmploymentTitle) {
        PortalParam.selfEmploymentTitle = selfEmploymentTitle;
    }

    public static String getSelfEmploymentBusinessAddress() {
        return selfEmploymentBusinessAddress;
    }

    public static void setSelfEmploymentBusinessAddress(String selfEmploymentBusinessAddress) {
        PortalParam.selfEmploymentBusinessAddress = selfEmploymentBusinessAddress;
    }

    public static String getSelfEmploymentBusinessPhone() {
        return selfEmploymentBusinessPhone;
    }

    public static void setSelfEmploymentBusinessPhone(String selfEmploymentBusinessPhone) {
        PortalParam.selfEmploymentBusinessPhone = selfEmploymentBusinessPhone;
    }

    public static String getSelfEmploymentBusinessStartDate() {
        return selfEmploymentBusinessStartDate;
    }

    public static void setSelfEmploymentBusinessStartDate(String selfEmploymentBusinessStartDate) {
        PortalParam.selfEmploymentBusinessStartDate = selfEmploymentBusinessStartDate;
    }

    public static boolean isPercentageOwnership() {
        return percentageOwnership;
    }

    public static void setPercentageOwnership(boolean percentageOwnership) {
        PortalParam.percentageOwnership = percentageOwnership;
    }

    public static String getTypeOfCompany() {
        return typeOfCompany;
    }

    public static void setTypeOfCompany(String typeOfCompany) {
        PortalParam.typeOfCompany = typeOfCompany;
    }

    public static boolean isSupportByCourt() {
        return supportByCourt;
    }

    public static void setSupportByCourt(boolean supportByCourt) {
        PortalParam.supportByCourt = supportByCourt;
    }

    public static boolean isWillReceiveOrderByCourt() {
        return willReceiveOrderByCourt;
    }

    public static void setWillReceiveOrderByCourt(boolean willReceiveOrderByCourt) {
        PortalParam.willReceiveOrderByCourt = willReceiveOrderByCourt;
    }

    public static boolean isTwoMonthSupport() {
        return twoMonthSupport;
    }

    public static void setTwoMonthSupport(boolean twoMonthSupport) {
        PortalParam.twoMonthSupport = twoMonthSupport;
    }

    public static boolean isIsProperty() {
        return isProperty;
    }

    public static boolean isIsContract() {
        return isContract;
    }

    public static boolean isCurrentLoan() {
        return currentLoan;
    }

    public static String getBusinessAddress() {
        return businessAddress;
    }

    public static void setBusinessAddress(String businessAddress) {
        PortalParam.businessAddress = businessAddress;
    }

    public static String getBusinessPhone() {
        return businessPhone;
    }

    public static void setBusinessPhone(String businessPhone) {
        PortalParam.businessPhone = businessPhone;
    }

    public static String getMonthlyAlimony() {
        return monthlyAlimony;
    }

    public static void setMonthlyAlimony(String monthlyAlimony) {
        PortalParam.monthlyAlimony = monthlyAlimony;
    }

    public static String getAlimonyStartDate() {
        return alimonyStartDate;
    }

    public static void setAlimonyStartDate(String alimonyStartDate) {
        PortalParam.alimonyStartDate = alimonyStartDate;
    }

    public static String getMonthlyChildSupport() {
        return monthlyChildSupport;
    }

    public static void setMonthlyChildSupport(String monthlyChildSupport) {
        PortalParam.monthlyChildSupport = monthlyChildSupport;
    }

    public static String getCurrentEmployer() {
        return currentEmployer;
    }

    public static void setCurrentEmployer(String currentEmployer) {
        PortalParam.currentEmployer = currentEmployer;
    }

    public static String getEmploymentTitle() {
        return employmentTitle;
    }

    public static void setEmploymentTitle(String employmentTitle) {
        PortalParam.employmentTitle = employmentTitle;
    }

    public static String getEmploymentStartDate() {
        return employmentStartDate;
    }

    public static void setEmploymentStartDate(String employmentStartDate) {
        PortalParam.employmentStartDate = employmentStartDate;
    }

    public static String getInThisLineYear() {
        return inThisLineYear;
    }

    public static void setInThisLineYear(String inThisLineYear) {
        PortalParam.inThisLineYear = inThisLineYear;
    }

    public static String getInThisLineMonth() {
        return inThisLineMonth;
    }

    public static void setInThisLineMonth(String inThisLineMonth) {
        PortalParam.inThisLineMonth = inThisLineMonth;
    }

    public static String getMonthlyBaseSalary() {
        return monthlyBaseSalary;
    }

    public static void setMonthlyBaseSalary(String monthlyBaseSalary) {
        PortalParam.monthlyBaseSalary = monthlyBaseSalary;
    }

    public static String getBonusAmount() {
        return bonusAmount;
    }

    public static void setBonusAmount(String bonusAmount) {
        PortalParam.bonusAmount = bonusAmount;
    }



    public static String getTypeOfProperty() {
        return typeOfProperty;
    }

    public static void setTypeOfProperty(String typeOfProperty) {
        PortalParam.typeOfProperty = typeOfProperty;
    }



    public static boolean isEligibleLoan() {
        return eligibleLoan;
    }

    public static void setEligibleLoan(boolean eligibleLoan) {
        PortalParam.eligibleLoan = eligibleLoan;
    }

    public static boolean getCurrentLoan() {
        return currentLoan;
    }

    public static void setCurrentLoan(boolean currentLoan) {
        PortalParam.currentLoan = currentLoan;
    }

    public static boolean isRealEstateAgent() {
        return realEstateAgent;
    }

    public static void setRealEstateAgent(boolean realEstateAgent) {
        PortalParam.realEstateAgent = realEstateAgent;
    }

    public static boolean isWithLoanOfficer() {
        return withLoanOfficer;
    }

    public static void setWithLoanOfficer(boolean withLoanOfficer) {
        PortalParam.withLoanOfficer = withLoanOfficer;
    }

    public static String getLoanOfficerName() {
        return loanOfficerName;
    }

    public static void setLoanOfficerName(String loanOfficerName) {
        PortalParam.loanOfficerName = loanOfficerName;
    }

    public static boolean isSameResidence() {
        return sameResidence;
    }

    public static void setSameResidence(boolean sameResidence) {
        PortalParam.sameResidence = sameResidence;
    }

    public static String getResidenceStatus() {
        return residenceStatus;
    }

    public static void setResidenceStatus(String residenceStatus) {
        PortalParam.residenceStatus = residenceStatus;
    }

    public static String getCurrentAddress() {
        return currentAddress;
    }

    public static void setCurrentAddress(String currentAddress) {
        PortalParam.currentAddress = currentAddress;
    }

    public static String getStayingSince() {
        return stayingSince;
    }

    public static void setStayingSince(String stayingSince) {
        PortalParam.stayingSince = stayingSince;
    }

    public static boolean isPlanningToSale() {
        return planningToSale;
    }

    public static void setPlanningToSale(boolean planningToSale) {
        PortalParam.planningToSale = planningToSale;
    }

    public static boolean isAdditionalProperty() {
        return additionalProperty;
    }

    public static void setAdditionalProperty(boolean additionalProperty) {
        PortalParam.additionalProperty = additionalProperty;
    }

    public static String getPropertyAddress() {
        return propertyAddress;
    }

    public static void setPropertyAddress(String propertyAddress) {
        PortalParam.propertyAddress = propertyAddress;
    }

    public static String getPropertyStatus() {
        return propertyStatus;
    }

    public static void setPropertyStatus(String propertyStatus) {
        PortalParam.propertyStatus = propertyStatus;
    }

    public static String getPropertyValue() {
        return propertyValue;
    }

    public static void setPropertyValue(String propertyValue) {
        PortalParam.propertyValue = propertyValue;
    }

    public static String getRentalIncome() {
        return rentalIncome;
    }

    public static void setRentalIncome(String rentalIncome) {
        PortalParam.rentalIncome = rentalIncome;
    }

    public static String getExpenses() {
        return expenses;
    }

    public static void setExpenses(String expenses) {
        PortalParam.expenses = expenses;
    }

    public static String getSpouceFirstName() {
        return spouceFirstName;
    }

    public static void setSpouceFirstName(String spouceFirstName) {
        PortalParam.spouceFirstName = spouceFirstName;
    }

    public static String getSpouceLastName() {
        return spouceLastName;
    }

    public static void setSpouceLastName(String spouceLastName) {
        PortalParam.spouceLastName = spouceLastName;
    }



    public static String getSpoucePhone() {
        return spoucePhone;
    }

    public static void setSpoucePhone(String spoucePhone) {
        PortalParam.spoucePhone = spoucePhone;
    }

    public static String getSpouceComm() {
        return spouceComm;
    }

    public static void setSpouceComm(String spouceComm) {
        PortalParam.spouceComm = spouceComm;
    }
    public static boolean isAddDependant() {
        return addDependant;
    }

    public static void setAddDependant(boolean addDependant) {
        PortalParam.addDependant = addDependant;
    }

    public static String getCity() {
        return city;
    }

    public static void setCity(String city) {
        PortalParam.city = city;
    }

    public static String getState() {
        return state;
    }

    public static void setState(String state) {
        PortalParam.state = state;
    }

    public static String getZip() {
        return zip;
    }

    public static void setZip(String zip) {
        PortalParam.zip = zip;
    }

    public static boolean isMarried() {
        return married;
    }

    public static void setMarried(boolean married) {
        PortalParam.married = married;
    }

    public static boolean isAddSpouce() {
        return addSpouce;
    }

    public static void setAddSpouce(boolean addSpouce) {
        PortalParam.addSpouce = addSpouce;
    }

    public static boolean isAddCoborrwer() {
        return addCoborrwer;
    }

    public static void setAddCoborrwer(boolean addCoborrwer) {
        PortalParam.addCoborrwer = addCoborrwer;
    }




    public static String getPurchasePrice() {
        return purchasePrice;
    }

    public static void setPurchasePrice(String purchasePrice) {
        PortalParam.purchasePrice = purchasePrice;
    }

    public static String getDownPayment() {
        return downPayment;
    }

    public static void setDownPayment(String downPayment) {
        PortalParam.downPayment = downPayment;
    }



    public static void setMongoPort(int mongoPort) {
        PortalParam.mongoPort = mongoPort;
    }

    public static String getSuffix() {
        return suffix;
    }

    public static void setSuffix(String suffix) {
        PortalParam.suffix = suffix;
    }

    public static String getPhone() {
        return phone;
    }

    public static void setPhone(String phone) {
        PortalParam.phone = phone;
    }

    public static String getModeOfCommunication() {
        return modeOfCommunication;
    }

    public static void setModeOfCommunication(String modeOfCommunication) {
        PortalParam.modeOfCommunication = modeOfCommunication;
    }

    public static String getPropertyType() {
        return propertyType;
    }

    public static void setPropertyType(String propertyType) {
        PortalParam.propertyType = propertyType;
    }

    public static boolean getIsProperty() {
        return isProperty;
    }

    public static void setIsProperty(boolean isProperty) {
        PortalParam.isProperty = isProperty;
    }

    public static boolean getIsContract() {
        return isContract;
    }

    public static void setIsContract(boolean isContract) {
        PortalParam.isContract = isContract;
    }

    public static String getPropertyUse() {
        return propertyUse;
    }

    public static void setPropertyUse(String propertyUse) {
        PortalParam.propertyUse = propertyUse;
    }

    public static String getFirstAddress() {
        return firstAddress;
    }

    public static void setFirstAddress(String firstAddress) {
        PortalParam.firstAddress = firstAddress;
    }

    public static boolean isIsEmailVerification() {
        return isEmailVerification;
    }

    public static void setIsEmailVerification(boolean isEmailVerification) {
        PortalParam.isEmailVerification = isEmailVerification;
    }

    public static boolean isIsMobileVerification() {
        return isMobileVerification;
    }

    public static void setIsMobileVerification(boolean isMobileVerification) {
        PortalParam.isMobileVerification = isMobileVerification;
    }



    public static String getTestid() {
        return testid;
    }

    public static void setTestid(String testid) {
        PortalParam.testid = testid;
    }



    public static String getBaseweburl() {
        return baseweburl;
    }

    public static void setBaseweburl(String baseweburl) {
        PortalParam.baseweburl = baseweburl;
    }

    public static String getDocittwebuserid() {
        return docittwebuserid;
    }

    public static void setDocittwebuserid(String docittwebuserid) {
        PortalParam.docittwebuserid = docittwebuserid;
    }

    public static String getDocittwebuserpassword() {
        return docittwebuserpassword;
    }

    public static void setDocittwebuserpassword(String docittwebuserpassword) {
        PortalParam.docittwebuserpassword = docittwebuserpassword;
    }

    public static String getCustom_report_location() {
        return custom_report_location;
    }

    public static void setCustom_report_location(String custom_report_location) {
        PortalParam.custom_report_location = custom_report_location;
    }

    public static String getUsername() {
        return username;
    }

    public static void setUsername(String username) {
        PortalParam.username = username;
    }

    public static String getPassword() {
        return password;
    }

    public static void setPassword(String password) {
        PortalParam.password = password;
    }

    public static String getBrowser() {
        return browser;
    }

    public static void setBrowser(String browser) {
        PortalParam.browser = browser;
    }

    public static String getTest_id() {
        return test_id;
    }

    public static void setTest_id(String test_id) {
        PortalParam.test_id = test_id;
    }



    public static String getMongoHost() {
        return mongoHost;
    }

    public static void setMongoHost(String mongoHost) {
        PortalParam.mongoHost = mongoHost;
    }

    public static int getMongoPort() {
        return mongoPort;
    }

    public static void setMongoDbPort(int mongoPort) {
        PortalParam.mongoPort = mongoPort;
    }

    public static String getMongoUser() {
        return mongoUser;
    }

    public static void setMongoUser(String mongoUser) {
        PortalParam.mongoUser = mongoUser;
    }

    public static String getMongoPwd() {
        return mongoPwd;
    }

    public static void setMongoPwd(String mongoPwd) {
        PortalParam.mongoPwd = mongoPwd;
    }

    public static String getMongoDb() {
        return mongoDb;
    }

    public static void setMongoDb(String mongoDb) {
        PortalParam.mongoDb = mongoDb;
    }



    public static String getCompanyName() {
        return companyName;
    }

    public static void setCompanyName(String companyName) {
        PortalParam.companyName = companyName;
    }

    public static String getMailId() {
        return mailId;
    }

    public static void setMailId(String mailId) {
        PortalParam.mailId = mailId;
    }

    public static String getConfirmPassword() {
        return confirmPassword;
    }

    public static void setConfirmPassword(String confirmPassword) {
        PortalParam.confirmPassword = confirmPassword;
    }

    public static String getFirstName() {
        return firstName;
    }

    public static void setFirstName(String firstName) {
        PortalParam.firstName = firstName;
    }

    public static String getMiddleName() {
        return middleName;
    }

    public static void setMiddleName(String middleName) {
        PortalParam.middleName = middleName;
    }

    public static String getLastName() {
        return lastName;
    }

    public static void setLastName(String lastName) {
        PortalParam.lastName = lastName;
    }

    public static String getPwd() {
        return pwd;
    }

    public static void setPwd(String pwd) {
        PortalParam.pwd = pwd;
    }



    public static boolean isEmailVerification() {
        return isEmailVerification;
    }

    public static void setEmailVerification(boolean isEmailVerification) {
        PortalParam.isEmailVerification = isEmailVerification;
    }

    public static boolean isMobileVerification() {
        return isMobileVerification;
    }

    public static void setMobileVerification(boolean isMobileVerification) {
        PortalParam.isMobileVerification = isMobileVerification;
    }

    public static boolean isLenderTeam() {
        return lenderTeam;
    }

    public static void setLenderTeam(boolean lenderTeam) {
        PortalParam.lenderTeam = lenderTeam;
    }



    public static String getGmailId() {
        return gmailId;
    }

    public static void setGmailId(String gmailId) {
        PortalParam.gmailId = gmailId;
    }

    public static String getGmailPassword() {
        return gmailPassword;
    }

    public static void setGmailPassword(String gmailPassword) {
        PortalParam.gmailPassword = gmailPassword;
    }


    public static String getLenderUserLogin() {
        return lenderUserLogin;
    }

    public static void setLenderUserLogin(String lenderUserLogin) {
        PortalParam.lenderUserLogin = lenderUserLogin;
    }

    public static String getEncryptedPassword() {
        return encryptedPassword;
    }

    public static void setEncryptedPassword(String encryptedPassword) {
        PortalParam.encryptedPassword = encryptedPassword;
    }

    public static String getBorrowerurl() {
        return borrowerurl;
    }

    public static void setBorrowerurl(String borrowerurl) {
        PortalParam.borrowerurl = borrowerurl;
    }
}

