package net.sigmainfo.lf.automation.portal.pages.borrowerportal;

import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import net.sigmainfo.lf.automation.common.AbstractTests;

@Component
public class WelcomeVideoPage extends AbstractTests {
   private Logger logger = LoggerFactory.getLogger(WelcomeVideoPage.class);

   @Autowired
   PortalFuncUtils portalFuncUtils;

   public WelcomeVideoPage(WebDriver driver){
      this.driver = driver;
      logger.info("========= WelcomeVideoPage is loaded===========");
   }
   public WelcomeVideoPage(){}

   public static By getStartedButton = By.xpath("//button[contains(text(),'GET STARTED')]");
   
   public LetsGetStartedPage clickGetStartedButton(WebDriver driver) throws Exception {
      WebDriverWait wait = new WebDriverWait(driver,30);
      wait.until(ExpectedConditions.elementToBeClickable(getStartedButton));
      driver.findElement(getStartedButton).click();
      logger.info("Clicking on GET STARTED button.");
      return new LetsGetStartedPage(driver);
   }
    
   
    
}