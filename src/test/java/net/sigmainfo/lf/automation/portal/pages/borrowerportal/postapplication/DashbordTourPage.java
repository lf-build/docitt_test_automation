package net.sigmainfo.lf.automation.portal.pages.borrowerportal.postapplication;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.borrowerdashboard.BorrowerDashboardPage;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.profile.ApplicantInformationPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by shaishav.s on 26-10-2017.
 */
public class DashbordTourPage extends AbstractTests {
    WebDriverWait wait = new WebDriverWait(driver,60);
    private Logger logger = LoggerFactory.getLogger(DashbordTourPage.class);

    public DashbordTourPage(WebDriver driver) {
        logger.info("DashbordTourPage is loaded");
        this.driver = driver;
    }
    public DashbordTourPage(){}


    private By TakeTourBtn=By.xpath("//*[@id='micro-app-host']/welcome-personal-dashboard//button[1]");
    private By letJustGoBtn=By.xpath(".//*[@id='micro-app-host']/welcome-personal-dashboard//button[2]");





    public BorrowerDashboardPage navigateToDashboard(){
        WebDriverWait wait = new WebDriverWait(driver,50);
        wait.until(ExpectedConditions.elementToBeClickable(letJustGoBtn));
        driver.findElement(letJustGoBtn).click();
        return new BorrowerDashboardPage(driver);
    }
}
