package net.sigmainfo.lf.automation.portal.pages.borrowerportal.income;

import net.sigmainfo.lf.automation.common.AbstractTests;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by shaishav.s on 06-09-2017.
 */
public class BorrowerEmpAddressPage extends AbstractTests {
    WebDriverWait wait = new WebDriverWait(driver,60);
    WebElement PropertyTypeListItem;

    private Logger logger = LoggerFactory.getLogger(BorrowerEmpAddressPage.class);

    public BorrowerEmpAddressPage(WebDriver driver) {
        this.driver = driver;
        logger.info("========= BorrowerEmpAddressPage is loaded===========");
    }

    public BorrowerEmpAddressPage() {}

    public static By AddressTextbox = By.xpath("//ui-input[@id='AddressAutocomplete']/div/div[1]/input");
    public static By SpouseAddressTextBox = By.xpath("//div[starts-with(@id,'ID')][12]//ui-input[@id='AddressAutocomplete']/div/div[1]/input");
    //public static By selectAddressOption = By.xpath("//div[@class='pac-item']//span[.='Glasgow, VA, United States']");
    public static By SelectAddressOption = By.xpath("//div[3]/div[3]/span[3]");
    public static By employmentPhoneTextBox = By.xpath("//platform-shell/div/borrower-private-layout/div/micro-app/div/questioner-sections/div[1]/div[3]/div/div/ui-form/right-content-area/div/div/sub-sections/div[4]/questioner-question-set/div[3]/div/div/questioner-question/ui-container/div/ui-input/div/div[1]/input");
    public static By SpouseEmploymentPhoneTextBox = By.xpath("//div[starts-with(@id,'ID')][12]//div[3]/div/div/questioner-question/ui-container/div//input[@id='input']");
    public static By IamDoneButton = By.xpath("//button[@name='next']");
    
    private void selectEmploymentAddress(By locator,String currentAddress) throws InterruptedException {
        enterText(locator,currentAddress);
        Thread.sleep(3000);
        selectOptionWithText(currentAddress);
        Thread.sleep(3000);
        
    }

    private void selectOptionWithText(String currentAddress) {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='pac-item']//span[contains(text(),'"+currentAddress+"')]")));
        driver.findElement(By.xpath("//div[@class='pac-item']//span[contains(text(),'"+currentAddress+"')]")).click();
        logger.info("Selected address :" + currentAddress);
    }

    public void enterText(By locator,String text){
        Actions actions = new Actions(driver);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        actions.moveToElement(element);
        actions.click();
        actions.sendKeys(text);
        actions.build().perform();
        logger.info("Entering :"+text);
    }

    public void clickButton() throws Exception {
        wait.until(ExpectedConditions.elementToBeClickable(IamDoneButton));
        driver.findElement(IamDoneButton).click();
        logger.info("Clicking on I am done button");
    }


    public AlimonyChildIncomePage enterBorrowerEmpAddressDetails(String businessAddress, String businessPhone) throws Exception {
    	selectEmploymentAddress(AddressTextbox,businessAddress);
    	enterEmploymentPhone(employmentPhoneTextBox,businessPhone);
    	clickButton();
    	return new AlimonyChildIncomePage(driver);
    }

	private void enterEmploymentPhone(By locator, String businessPhone) {
		enterText(locator,businessPhone);
	}

    public AlimonyChildIncomePage provideSpouseEmpAddressDetails(String businessAddress, String businessPhone) throws Exception {
        selectEmploymentAddress(SpouseAddressTextBox,businessAddress);
        enterEmploymentPhone(SpouseEmploymentPhoneTextBox,businessPhone);
        clickButton();
        return new AlimonyChildIncomePage(driver);
    }
}
