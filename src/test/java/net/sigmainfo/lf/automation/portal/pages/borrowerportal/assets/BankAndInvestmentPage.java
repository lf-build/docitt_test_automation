package net.sigmainfo.lf.automation.portal.pages.borrowerportal.assets;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.tests.AssetCompletedPage;

import static org.testng.AssertJUnit.assertTrue;

public class BankAndInvestmentPage extends AbstractTests {
	
	private Logger logger = LoggerFactory.getLogger(BankAndInvestmentPage.class);
	WebDriverWait wait = new WebDriverWait(driver,60);
	
	BankAndInvestmentPage(){}

	public BankAndInvestmentPage(WebDriver driver){
		this.driver = driver;
		logger.info("========= BankAndInvestmentPage is loaded===========");
	}

	public static By addAccountBtn=By.cssSelector("#button");
	public static By enterBtn=By.cssSelector("button[action='assets-submit']");
	public static By bankOfAmericaImage = By.xpath("//li[contains(text(),'Bank of America')]");
	public static By chaseImage = By.xpath("//li[contains(text(),'Chase')]");
	public static By merrillLynchImage = By.xpath("//li[contains(text(),'Merrill Lynch')]");
	public static By etradeImage = By.xpath("//li[contains(text(),'E*TRADE')]");
	public static By usaaImage = By.xpath("//li[contains(text(),'USAA')]");
	public static By schoolFirstImage = By.xpath("//li[contains(text(),'Merrill Lynch')]");
	public static By fidelityImage = By.xpath("//li[contains(text(),'Fidelity')]");
	public static By iAmAllDoneButton = By.xpath("//button[contains(text(),\"I'M ALL DONE\")]");

	private void scrollDownThePage(WebDriver driver) {
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
	}

	public BankInformationPage addInstitution() throws InterruptedException {
		scrollDownThePage(driver);
		wait.until(ExpectedConditions.elementToBeClickable(addAccountBtn));
		driver.findElement(addAccountBtn).click();

		for(int i=2;i<=9;i++){
			WebElement institution = driver.findElement(By.cssSelector("ul[id='optionsContainer'] ui-selector-item[action='assets-select']:nth-child("+i+")"));
			String text = institution.getText();

			if(text.equalsIgnoreCase("Bank of America")){
				institution.click();
				
			}
		}
		return new BankInformationPage(driver);
	}

	public void validateAccountInfo(String institutionName,String accountType,String currentBalance,String accountNumber,String accountName) throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(driver,40);
		wait.until(ExpectedConditions.elementToBeClickable(addAccountBtn));
		//Thread.sleep(10000);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		Object result1 = js.executeScript(" return document.querySelector(\"tbody[id='body-items']\").children.length;");
		int totalAccount = Integer.parseInt(result1.toString());
		ArrayList<String> list = new ArrayList<String>();
		int Rowno=0;

		list.add(institutionName);
		list.add("");
		list.add(accountType);
		list.add(accountNumber);
		list.add(accountName);
		list.add(currentBalance);

		Thread.sleep(10000);
		
		for(int i=1;i<totalAccount;i++){
			String value=driver.findElement(By.cssSelector("tbody[id='body-items'] tr:nth-child("+i+")>td:nth-child(1)")).getText();
			if(list.get(0).equals(value)){
				Rowno=i;
				break;
			}
		}
		
		System.out.println("Row NO is ::"+Rowno);
		
		for(int i=1;i<=6;i++){
			String value=driver.findElement(By.cssSelector("tbody[id='body-items'] tr:nth-child("+Rowno+")>td:nth-child("+i+")")).getText();
			
			if(value.equals(list.get(i-1))){
				logger.info(value +" is same as "+list.get(i-1));
			}else{
				logger.error(value +" is not same as "+list.get(i-1));
			}
			
			//Assert.assertEquals(value, list.get(i-1));
		}

		logger.info("Account information is validated and verified.");
	}

	public AssetCompletedPage clickSubmit() throws Exception {
		scrollDownThePage(driver);
        wait.until(ExpectedConditions.elementToBeClickable(iAmAllDoneButton));
		driver.findElement(iAmAllDoneButton).click();
        logger.info("Clicking on I'm all done button");
        return new AssetCompletedPage(driver);
    }

    public AssetsPage clickSubmit(boolean addSpouse){
		wait.until(ExpectedConditions.elementToBeClickable(iAmAllDoneButton));
		driver.findElement(iAmAllDoneButton).click();
		logger.info("Clicking on I'm all done button");
		return new AssetsPage(driver);
	}


}
