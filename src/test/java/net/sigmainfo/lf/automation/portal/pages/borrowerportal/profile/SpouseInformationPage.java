package net.sigmainfo.lf.automation.portal.pages.borrowerportal.profile;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.assets.SavingAndInvestmentPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.springframework.test.util.AssertionErrors.assertEquals;
import static org.testng.AssertJUnit.assertTrue;

/**
 * Created by shaishav.s on 22-09-2017.
 */
public class SpouseInformationPage extends AbstractTests {
    private Logger logger = LoggerFactory.getLogger(SpouseInformationPage.class);

    WebDriverWait wait = new WebDriverWait(driver,60);

    public SpouseInformationPage(WebDriver driver) {
        this.driver = driver;
        logger.info("========= SpouseInformationPage is loaded===========");
        assertTrue(driver.findElement(By.xpath("//div[starts-with(@id,'ID')][1]/h3[contains(text(),\""+portalParam.firstName+" what's your spouse's information\")]")).isDisplayed());
    }
    public SpouseInformationPage(){}

    public static By SpouseFirstNameTextbox = By.cssSelector("div[class*='section-bx']>sub-sections[class*='right-content-area']>div:nth-child(5) questioner-question-set[class*='sub-sections']>div:nth-child(1) div:nth-child(2) input[id='input']");
    public static By SpouseMiddleNameTextbox = By.cssSelector("div[class*='section-bx']>sub-sections[class*='right-content-area']>div:nth-child(5) questioner-question-set[class*='sub-sections']>div:nth-child(1) div:nth-child(3) input[id='input']");
    public static By SpouseLastNameTextbox = By.xpath("//div[starts-with(@id,'ID')][1]//label[contains(text(),'Last Name')]");
    public static By SpouseSuffixTextbox = By.xpath("//div[starts-with(@id,'ID')][1]//label[contains(text(),'Suffix')]");
    public static By SpousePreferredEmailTextbox = By.xpath("//div[starts-with(@id,'ID')][1]//label[contains(text(),'Preferred Email')]");
    public static By SpousePhoneTextbox = By.xpath("//div[starts-with(@id,'ID')][1]//label[contains(text(),'Phone #')]");
    public static By PhoneButton = By.xpath("//div[starts-with(@id,'ID')][1]/questioner-question-set/div[5]/div/div/questioner-question/ui-container/div/ui-toggle-button-group/div[1]/label");
    public static By EmailButton = By.xpath("//div[starts-with(@id,'ID')][1]/questioner-question-set/div[5]/div/div/questioner-question/ui-container/div/ui-toggle-button-group/div[1]/label");
    public static By TextButton = By.xpath("//div[starts-with(@id,'ID')][1]/questioner-question-set/div[5]/div/div/questioner-question/ui-container/div/ui-toggle-button-group/div[1]/label");
    public static By AllButton = By.xpath("//div[starts-with(@id,'ID')][1]/questioner-question-set/div[5]/div/div/questioner-question/ui-container/div/ui-toggle-button-group/div[1]/label");
    public static By BackButton = By.xpath("//button[contains(text(),'Back')]");
    public static By NextButton = By.xpath("//button[contains(text(),'Next')]");

    public NonSpouseInformationPage enterSpouseDetails(String spouceFirstName, String spouceMiddleName,String spouceLastName, String spouceSuffix, String spouceEmail, String spoucePhone, String modeOfCom) throws Exception {
        enterFirstName(spouceFirstName);
        enterMiddleName(spouceMiddleName);
        enterLastName(spouceLastName);
        enterSuffix(spouceSuffix);
        enterPreferredEmail(spouceEmail);
        enterPhone(spoucePhone);
        selectPreferredMethodOfCommunication(modeOfCom);
        clickNext();
        return new NonSpouseInformationPage(driver);
    }

     public void clickNext() throws Exception {
        wait.until(ExpectedConditions.elementToBeClickable(NextButton));
        driver.findElement(NextButton).click();
        logger.info("Clicking on NEXT button");
    }

    private void enterPhone(String phone) {
        enterText(SpousePhoneTextbox,phone);
        logger.info("Entering spouse's phone :"+phone);
    }

    private void enterPreferredEmail(String preferredEmail) {
        enterText(SpousePreferredEmailTextbox,preferredEmail);
        logger.info("Entering spouse's preferredEmail :"+preferredEmail);
    }

    private void enterSuffix(String suffix) {
        enterText(SpouseSuffixTextbox,suffix);
        logger.info("Entering spouse's suffix :"+suffix);
    }

    private void enterLastName(String lastName) {
        enterText(SpouseLastNameTextbox,lastName);
        logger.info("Entering spouse's last name :"+lastName);
    }

    private void enterMiddleName(String middleName) {
        enterText(SpouseMiddleNameTextbox,middleName);
        logger.info("Entering spouse's middle name :"+middleName);
    }

    private void enterFirstName(String firstName) {
        enterText(SpouseFirstNameTextbox,firstName);
        logger.info("Entering spouse's first name :"+firstName);
    }

    public void enterText(By locator,String text){
        Actions actions = new Actions(driver);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        actions.moveToElement(element);
        actions.click();
        actions.sendKeys(text);
        actions.build().perform();
    }

    public void selectPreferredMethodOfCommunication(String modeOfComm) throws Exception {
        if(modeOfComm.equalsIgnoreCase("Email"))
        {
            wait.until(ExpectedConditions.presenceOfElementLocated(EmailButton));
            wait.until(ExpectedConditions.elementToBeClickable(EmailButton));
            driver.findElement(EmailButton).click();
        }
        else if(modeOfComm.equalsIgnoreCase("Phone"))
        {
            wait.until(ExpectedConditions.presenceOfElementLocated(PhoneButton));
            wait.until(ExpectedConditions.elementToBeClickable(PhoneButton));
            driver.findElement(PhoneButton).click();
        }
        if(modeOfComm.equalsIgnoreCase("Text"))
        {
            wait.until(ExpectedConditions.presenceOfElementLocated(TextButton));
            wait.until(ExpectedConditions.elementToBeClickable(TextButton));
            driver.findElement(TextButton).click();
        }
        else
        {
            wait.until(ExpectedConditions.presenceOfElementLocated(AllButton));
            wait.until(ExpectedConditions.elementToBeClickable(AllButton));
            driver.findElement(AllButton).click();
        }
        Thread.sleep(1500);
        logger.info("Selecting preferred method of communication");

    }

}
