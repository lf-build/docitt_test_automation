package net.sigmainfo.lf.automation.portal.pages.borrowerportal.profile;

import net.sigmainfo.lf.automation.common.AbstractTests;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by shaishav.s on 06-09-2017.
 */
public class EligibilityPage extends AbstractTests {

    WebDriverWait wait = new WebDriverWait(driver,60);
    WebElement PropertyTypeListItem;

    private Logger logger = LoggerFactory.getLogger(EligibilityPage.class);

    public EligibilityPage(WebDriver driver) {
        this.driver = driver;
        logger.info("========= EligibilityPage is loaded===========");
    }

    public EligibilityPage() {
    }

    public static By EligibleForLoanButton = By.xpath("//ui-switch[@id='33']//label[normalize-space(.)='Yes']");
    public static By NotEligibleForLoanButton = By.xpath("//ui-switch[@id='33']//label[normalize-space(.)='No']");
    public static By HaveCurrentLoanButton = By.xpath("//ui-switch[@id='34']//label[normalize-space(.)='Yes']");
    public static By NoCurrentLoanButton = By.xpath("//ui-switch[@id='34']//label[normalize-space(.)='No']");
    public static By NextButton = By.xpath("//button[contains(text(),'Next')]");

    public void selectButton(By locator,String value){
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        if(value.equalsIgnoreCase("Yes")) {
            driver.findElement(locator).click();
        }
        else
        {
            driver.findElement(locator).click();
        }
        logger.info("Clicking on "+ value +" button");
    }

    public void clickNext() throws Exception {
        wait.until(ExpectedConditions.elementToBeClickable(NextButton));
        driver.findElement(NextButton).click();
        logger.info("Clicking on NEXT button");
    }


    public RealEstateAgentPage selectLoanInfo(boolean eligibleLoan, boolean currentLoan) throws Exception {
        selectButton(EligibleForLoanButton,"eligible for a VA/ Military Loan");
        selectButton(NoCurrentLoanButton,"currently have a VA/ Military loan");
        clickNext();
        return new RealEstateAgentPage(driver);
    }
}
