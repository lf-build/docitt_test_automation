package net.sigmainfo.lf.automation.portal.pages.borrowerportal.income;

import net.sigmainfo.lf.automation.common.AbstractTests;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.testng.AssertJUnit.assertTrue;

/**
 * Created by shaishav.s on 25-09-2017.
 */
public class SpouseEmpAddressPage extends AbstractTests {
    WebDriverWait wait = new WebDriverWait(driver,60);
    WebElement PropertyTypeListItem;

    private Logger logger = LoggerFactory.getLogger(SpouseEmpAddressPage.class);

    public SpouseEmpAddressPage(WebDriver driver) {
        this.driver = driver;
        logger.info("========= SpouseEmpAddressPage is loaded===========");
        //assertTrue(driver.findElement(By.xpath("//div[starts-with(@id,'ID-')][12]/h3[contains(text(),\"\"+portalParam.spouceFirstName+\"'s employment\")]")).isDisplayed());
    }

    public SpouseEmpAddressPage() {
    }

    public static By SpouseBusinessAddressTextBox = By.xpath("//div[starts-with(@id,'ID-')][12]//div[2]//label[contains(text(),'Business Address')]");
    public static By SpouseBusinessPhoneTextBox = By.xpath("//div[starts-with(@id,'ID')][12]/questioner-question-set/div[3]//label[contains(text(),'Phone#')]");
    public static By IamDoneButton = By.xpath("//button[@name='next']");

    public SpouseAlimonyChildIncomePage provideSpouseEmpAddressDetails(String businessAddress, String businessPhone) throws Exception {
        selectEmploymentAddress(SpouseBusinessAddressTextBox,businessAddress);
        enterEmploymentPhone(SpouseBusinessPhoneTextBox,businessPhone);
        clickButton();
        return new SpouseAlimonyChildIncomePage(driver);
    }

    private void enterEmploymentPhone(By locator, String businessPhone) {
        enterText(locator,businessPhone);
    }
    private void selectEmploymentAddress(By locator,String currentAddress) throws InterruptedException {
        enterText(locator,currentAddress);
        Thread.sleep(3000);
        selectOptionWithText(currentAddress);
        Thread.sleep(3000);
    }

    private void selectOptionWithText(String currentAddress) {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='pac-item']//span[contains(text(),'"+currentAddress+"')]")));
        driver.findElement(By.xpath("//div[@class='pac-item']//span[contains(text(),'"+currentAddress+"')]")).click();
        logger.info("Selected address :" + currentAddress);
    }

    public void enterText(By locator,String text){
        Actions actions = new Actions(driver);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        actions.moveToElement(element);
        actions.click();
        actions.sendKeys(text);
        actions.build().perform();
        logger.info("Entering :"+text);
    }

    public void clickButton() throws Exception {
        wait.until(ExpectedConditions.elementToBeClickable(IamDoneButton));
        driver.findElement(IamDoneButton).click();
        logger.info("Clicking on Next button");
    }
}
