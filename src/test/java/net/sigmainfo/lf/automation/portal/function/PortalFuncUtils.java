package net.sigmainfo.lf.automation.portal.function;

import net.sigmainfo.lf.automation.api.function.ApiPropertiesReader;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import net.sigmainfo.lf.automation.common.AbstractTests;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.pagefactory.Annotations;
import org.openqa.selenium.support.ui.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.testng.Assert;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Driver;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static java.awt.SystemColor.window;
import static jxl.biff.BaseCellFeatures.logger;
import static net.sigmainfo.lf.automation.portal.constant.PortalParam.typeOfRentalProperty;

/**
 * Created by           : Shaishav.s on 08-02-2017.
 * Test class           : PortalFuncUtils.java
 * Description          : Contains reusable methods used while Portal automation
 * Includes             : 1. Reads classpath resources
 *                        2. Read test data from excel sheet implementation
 */
@Component
public class PortalFuncUtils {

    public static int WAIT_TIME=40;
    private boolean acceptNextAlert = true;

   public String getTestData(String FuncMod,String TestID, String valueName)
    {
        final Sheet sheet;
        String value="";

        try {
            //sheet= Workbook.getWorkbook(getResourceFromClasspath(System.getProperty("envParam")+"/TestData/MPS_Data.xls")).getSheet(FuncMod);
            sheet= Workbook.getWorkbook(getResourceFromClasspath(System.getProperty("envParam")+"/TestData/Docitt_Test_Data.xls")).getSheet(FuncMod);

            Cell t1 = sheet.findCell(TestID);
            Cell c1 = sheet.findCell(valueName);

            if (t1 == null)
                return "";
            else {
                int fndRow = t1.getRow();
                value = sheet.getCell(c1.getColumn(), fndRow).getContents();
            }

        } catch (BiffException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return value;

    }

    public InputStream getResourceFromClasspath(String resourceName) throws IOException {


        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(resourceName);

        if (inputStream == null) {
            throw new FileNotFoundException("resource '" + resourceName  + "' not found in the classpath");
        }

        return inputStream;
    }

    public void enterText(WebDriver driver,By locator,String inputString){
        Actions actions = new Actions(driver);
        WebElement FirstNameTxtBox = driver.findElement(locator);
        actions.moveToElement(FirstNameTxtBox);
        actions.click();
        actions.sendKeys(inputString);
        actions.build().perform();
        logger.info("Entered value :"+inputString);
    }

    /*public void clickButton(WebDriver driver,By locator,String buttonText){
        Actions actions = new Actions(driver);
        WebElement button = driver.findElement(locator);
        actions.moveToElement(button);
        actions.click();
        actions.build().perform();
        logger.info("Clicking button :"+buttonText);
    }*/

    public void clickButton(WebDriver driver,By locator){
        Actions actions = new Actions(driver);
        WebElement button = driver.findElement(locator);
        actions.moveToElement(button);
        actions.click();
        actions.build().perform();
    }

    public void waitForPageLoadAndTitleContains(WebDriver driver,int timeout, String pageTitle) {
        WebDriverWait wait = new WebDriverWait(driver, timeout, 1000);
        wait.until(ExpectedConditions.titleContains(pageTitle));
    }

    public void waitForElementPresence(WebDriver driver,By locator, int seconds) {
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        wait.until(ExpectedConditions.presenceOfElementLocated(locator));
    }

    public void waitForElementClickable(WebDriver driver,By locator, int seconds) {
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        wait.until(ExpectedConditions.elementToBeClickable(locator));
    }

    public void jsWaitForPageToLoad(WebDriver driver,int timeOutInSeconds) throws InterruptedException {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        String jsCommand = "return document.readyState";

        // Validate readyState before doing any waits
        if (js.executeScript(jsCommand).toString().equals("complete")) {
            return;
        }

        for (int i = 0; i < timeOutInSeconds; i++) {
            Thread.sleep(3000);
            if (js.executeScript(jsCommand).toString().equals("complete")) {
                break;
            }
        }
    }

    public void waitForPageToLoad(WebDriver driver){
        ExpectedCondition<Boolean> expectation = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete");
                    }
                };
        try {
            Thread.sleep(1000);
            WebDriverWait wait = new WebDriverWait(driver, 30);
            wait.until(expectation);
        } catch (Throwable error) {
            Assert.fail("Timeout waiting for Page Load Request to complete.");
        }
    }

    /**
     * Looks for a visible OR invisible element via the provided locator for up
     * to maxWaitTime. Returns as soon as the element is found.
     *
     * @param byLocator
     * @param maxWaitTime - In seconds
     * @return
     *
     */
    public WebElement findElementThatIsPresent(WebDriver driver,final By byLocator, int maxWaitTime) {
        FluentWait<WebDriver> wait = new FluentWait<>(driver).withTimeout(maxWaitTime, java.util.concurrent.TimeUnit.SECONDS)
                .pollingEvery(200, java.util.concurrent.TimeUnit.MILLISECONDS);

        try {
            return wait.until((WebDriver webDriver) -> {
                List<WebElement> elems = driver.findElements(byLocator);
                if (elems.size() > 0) {
                    return elems.get(0);
                } else {
                    return null;
                }
            });
        } catch (Exception e) {
            return null;
        }
    }

    public By getBy(String fieldName) {
        try {
            return new Annotations(this.getClass().getDeclaredField(fieldName)).buildBy();
        } catch (NoSuchFieldException e) {
            return null;
        }
    }

    public static void insertText(WebDriver driver, By locator, String value) throws Exception {
        try {
            WebElement field = driver.findElement(locator);
            field.clear();
            field.sendKeys(value);
        }
        catch(Exception e)
        {
            throw  new Exception(value + "can not be entered because of : " + e.getMessage());
        }
    }

    public static void selectDropdownByvalue(WebDriver driver, By locator, String value) throws Exception {
        try {
            new Select(driver.findElement(locator)).selectByVisibleText(value);
        }
        catch(Exception e)
        {
            throw new Exception(value + "can not be selected because of : "+ e.getMessage());
        }
    }

    public static String getTooltipText(WebDriver driver, By locator) throws Exception {
        try {
            String tooltip = driver.findElement(locator).getAttribute("title");
            return tooltip;
        }
        catch (Exception e)
        {
            throw new Exception("Can not fetch the tooltip because of :" +e.getMessage());
        }
    }

    public static void selectSearchDropdown(WebDriver driver, By locator, String value) throws Exception {
        try {
            driver.findElement(locator).click();
            driver.findElement(locator).sendKeys(value);
            driver.findElement(locator).sendKeys(Keys.TAB);
        }
        catch (Exception e)
        {
            throw new Exception("Selecting "+ value +"from the dropdown failed because of : " +e.getMessage() );
        }
    }

    public static void waitUntilElementLocated(WebDriver driver,By locator) throws Exception {
        try {
            WebDriverWait wait = new WebDriverWait(driver, WAIT_TIME);
            wait.until(ExpectedConditions.presenceOfElementLocated(locator));
        }
        catch (Exception e)
        {
            throw new Exception("Element could not be located within wait time");
        }
    }

    public static void waitUntilElementVisible(WebDriver driver,By locator) throws Exception {
        try {
            WebDriverWait wait = new WebDriverWait(driver, WAIT_TIME);
            wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        }
        catch(Exception e)
        {
            throw  new Exception("Element could not be visible within wait time");
        }
    }

    public boolean ifEnabled(WebDriver driver,By locator){
        WebElement element = driver.findElement(locator);
        if(element.isEnabled())
            return true;
        else
            return false;
    }

    public void waitUntilElementInvisible(WebDriver driver,By locator){
        WebDriverWait wait = new WebDriverWait(driver, WAIT_TIME);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
    }

    public void selectRadioButton(WebDriver driver, By locator, String value) {
        List<WebElement> radios = driver.findElements(locator);

        for (WebElement element : radios) {

            if (element.getAttribute("value").equals(value)) {
                element.click();
            }
            break;
        }
    }

    public void selectCheckbox(WebDriver driver, By locator, String value) {
        List<WebElement> checkbox = driver.findElements(locator);

        for (WebElement element : checkbox) {

            if (element.getAttribute("value").equals(value)) {
                element.click();
            }
            break;
        }
    }

    public void clickButton(WebDriver driver, By locator, String value) throws Exception {

        if((driver.findElement(locator).isDisplayed() && (driver.findElement(locator).isEnabled() && (driver.findElement(locator).getText().equalsIgnoreCase(value)))))
        {
            /*driver.findElement(locator).click();
            logger.info("Clicking button :"+value);*/
            WebElement element = driver.findElement(locator);

            Actions actions = new Actions(driver);

            actions.moveToElement(element).click().perform();
        }
        else
            throw  new Exception("Button with name" + value + "can not be clicked.");
    }

    public void waitForTextToBePresent(WebDriver driver, By locator, String value){
        WebDriverWait wait = new WebDriverWait(driver, WAIT_TIME);
        wait.until(ExpectedConditions.textToBePresentInElementLocated(locator, value));
    }

    public void waitForTexyToBeInvisible(WebDriver driver, By locator, String value){
        WebDriverWait wait = new WebDriverWait(driver, 15);
        wait.until(ExpectedConditions.invisibilityOfElementWithText(locator,value));
    }

    public void navigateTo(WebDriver driver,String url){
        driver.navigate().to(url);
    }

    public void navigateBackpage(WebDriver driver,String url){
        driver.navigate().back();
    }

    public boolean isElementPresent(WebDriver driver,By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public boolean isAlertPresent(WebDriver driver) {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    public String closeAlertAndGetItsText(WebDriver driver,String url) {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }

    public static boolean waitForJQueryProcessing(WebDriver webDriver,  int absTimeOut, int pollInterval){
        Wait<WebDriver> wait = new FluentWait<WebDriver>(webDriver)
                .withTimeout(absTimeOut, TimeUnit.SECONDS)
                .pollingEvery(pollInterval, TimeUnit.SECONDS)
                .ignoring(WebDriverException.class);

        boolean jQcondition = false;
        try{
            wait.until(new ExpectedCondition<Boolean>() {
                public Boolean apply(WebDriver webDriver) {
                    JavascriptExecutor js = (JavascriptExecutor) webDriver;
                    Boolean status =
                            (js.executeScript("return document.readyState").toString().equals("complete"))
                                    && ((Boolean)js.executeScript("return jQuery.active == 0"));
                    return status;
                }
            });
            jQcondition = (Boolean) ((JavascriptExecutor) webDriver)
                    .executeScript("return window.jQuery != undefined && jQuery.active === 0");
            return jQcondition;
        }catch(TimeoutException e){
            e.printStackTrace();
        }
        return jQcondition;
    }

    public static void scrollPageDown(WebDriver driver){
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("window.scrollBy(0,250)", "");
    }

    public static void scrollPageUp(WebDriver driver){
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("window.scrollBy(0,-250)", "");
    }

    public static void scrollOnTopOfThePage(WebDriver driver){
        ((JavascriptExecutor)driver).executeScript("window.scrollTo(document.body.scrollHeight,0)");
    }

    public static void scrollUntil(By locator,WebDriver driver){
        WebElement element = driver.findElement(locator);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
    }


    public void selectFromDropdown(WebDriver driver, By locator, String value) throws InterruptedException {
        driver.findElement(locator).click();
        Thread.sleep(2000);
        for(int i=3;i<=6;i++){
            WebElement proeprtyType = driver.findElement(By.cssSelector("ui-options[name='dobMonth'] div[class*='select-wrapper'] li:nth-child("+i+")"));
            String text = proeprtyType.findElement(By.xpath("//span[contains(text(),'"+value+"')]")).getText();
            if(text.contains(value)){
                proeprtyType.findElement(By.xpath("//span[contains(text(),'"+value+"')]")).click();
            }

        }
        logger.info("Selected :"+typeOfRentalProperty);
    }

      public static void scrollToElementandClick(WebDriver driver,By element){
        WebDriverWait wait = new WebDriverWait(driver,50);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(element));
        wait.until(ExpectedConditions.presenceOfElementLocated(element));
        wait.until(ExpectedConditions.visibilityOfElementLocated(element));
        wait.until(ExpectedConditions.elementToBeClickable(element));
        driver.findElement(element).click();
    }


    public static void scrollToElement(WebDriver driver,By element){
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(element));
    }
}
