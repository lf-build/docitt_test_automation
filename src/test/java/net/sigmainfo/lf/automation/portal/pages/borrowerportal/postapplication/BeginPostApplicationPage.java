package net.sigmainfo.lf.automation.portal.pages.borrowerportal.postapplication;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.summary.SummaryPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by shaishav.s on 25-10-2017.
 */
public class BeginPostApplicationPage extends AbstractTests {

    private Logger logger = LoggerFactory.getLogger(BeginPostApplicationPage.class);
    WebDriverWait wait = new WebDriverWait(driver,60);

    BeginPostApplicationPage(){}

    public static By beginSectionButton = By.xpath("//button[contains(text(),'BEGIN SECTION')]");


    public BeginPostApplicationPage(WebDriver driver){
        logger.info("BeginPostApplicationPage is loaded");
        this.driver=driver;
    }

    public PostApplicationPage clickBeginPostApplication() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(beginSectionButton));
        driver.findElement(beginSectionButton).click();
        logger.info("Clicked on BEGIN SECTION button");
        return new PostApplicationPage(driver);
    }
}
