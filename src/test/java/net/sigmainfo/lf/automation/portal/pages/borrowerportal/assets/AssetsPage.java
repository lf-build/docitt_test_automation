package net.sigmainfo.lf.automation.portal.pages.borrowerportal.assets;

import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.LoginPage;

import static org.testng.AssertJUnit.assertTrue;

public class AssetsPage extends AbstractTests{
	private Logger logger = LoggerFactory.getLogger(AssetsPage.class);
	WebDriverWait wait = new WebDriverWait(driver,60);

	public AssetsPage(WebDriver driver) {
		this.driver = driver;
		logger.info("========= AssetsPage is loaded===========");
	}
	public AssetsPage(){}

	public static By enterManuallyBtn = By.cssSelector("button[class*=manual]");
	//public static By enterManuallyBtn=By.xpath("assets-container/div[1]/div[3]/button[1]");



	public SavingAndInvestmentPage clickEnterManually(){
		clickBtn(enterManuallyBtn);
		return new SavingAndInvestmentPage(driver);
	}


	public void clickBtn(By element){
		try {
			logger.info("Clicking Element:"+element);
			//new WebDriverWait(driver,30).until(ExpectedConditions.elementToBeClickable(element));
			driver.findElement(element).click();
		} catch (Exception e) {
			logger.error("Unable to Click "+ element);
			 new Exception("Element "+ element+ " on UI is  button is not found");
		}
	}

	public void chooseBank(){
		new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(enterManuallyBtn));
		
		Random rand = new Random();
		int  n = rand.nextInt(8) + 1;
		System.out.println("Random Number is:"+n);
		driver.findElement(By.xpath("//assets-grid[1]/div[2]/div["+n+"]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/img[1]")).click();
	}
	
	public BankAndInvestmentPage enterManualAssets() {
		return new BankAndInvestmentPage(driver);
	}


}
