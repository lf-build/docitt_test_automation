package net.sigmainfo.lf.automation.portal.pages.borrowerportal.income;

import net.sigmainfo.lf.automation.common.AbstractTests;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.testng.AssertJUnit.assertTrue;

/**
 * Created by shaishav.s on 26-09-2017.
 */
public class SpouseAlimonyChildIncomePage extends AbstractTests {
    WebDriverWait wait = new WebDriverWait(driver,60);
    WebElement PropertyTypeListItem;

    private Logger logger = LoggerFactory.getLogger(SpouseEmpAddressPage.class);

    public SpouseAlimonyChildIncomePage(WebDriver driver) {
        this.driver = driver;
        logger.info("========= SpouseAlimonyChildIncomePage is loaded===========");
        //assertTrue(driver.findElement(By.xpath("//div[starts-with(@id,'ID-')][13]/h3[contains(text(),\"\"+portalParam.spouceFirstName+\"'s Alimony / Child's support\")]")).isDisplayed());
    }

    public SpouseAlimonyChildIncomePage() {
    }

    public static By SpousePerMonthAlimonySupportTextBox = By.xpath("//div[starts-with(@id,'ID-')][13]/questioner-question-set//div[3]/div[1]/div[1]//input[@id='input']");
    public static By SpouseAlimonySupportStartDateTextBox = By.xpath("//div[starts-with(@id,'ID-')][13]/questioner-question-set//div[3]/div[1]/div[2]//input[@id='input']");
    public static By SpousePerMonthChildSupportTextBox = By.xpath("//div[starts-with(@id,'ID-')][13]/questioner-question-set//div[5]//questioner-question//input[@id='input']");
    public static By SpouseAlimonyChildNameTextBox = By.xpath("//div[starts-with(@id,'ID-')][13]/questioner-question-set//div[6]/div[1]/div[1]//input[@id='input']");
    public static By SpouseAlimonyChildDoBTextBox = By.xpath("//div[starts-with(@id,'ID-')][13]/questioner-question-set//div[6]/div[1]/div[2]//input[@id='input']");
    public static By NextButton = By.xpath("//button[@name='next']");
    public static By BackButton = By.xpath("//button[@name='back']");

    public SpouseAlimonyChildSupportPage provideSpouseAlimonyIncomeDetails(String monthlyAlimony, String alimonyStartDate, String monthlyChildSupport, String childName, String childDoB) throws Exception {
        enterText(SpousePerMonthAlimonySupportTextBox, monthlyAlimony);
        enterText(SpouseAlimonySupportStartDateTextBox, alimonyStartDate);
        enterText(SpousePerMonthChildSupportTextBox, monthlyChildSupport);
        Thread.sleep(3000);
        enterText(SpouseAlimonyChildNameTextBox, childName);
        enterText(SpouseAlimonyChildDoBTextBox, childDoB);
        clickButton();
        return new SpouseAlimonyChildSupportPage(driver);
    }
    public void enterText(By locator,String text){
        Actions actions = new Actions(driver);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        actions.moveToElement(element);
        actions.click();
        actions.sendKeys(text);
        actions.build().perform();
        logger.info("Entering :"+text);
    }

    public void clickButton() throws Exception {
        wait.until(ExpectedConditions.elementToBeClickable(NextButton));
        driver.findElement(NextButton).click();
        logger.info("Clicking on Next button");
    }
}
