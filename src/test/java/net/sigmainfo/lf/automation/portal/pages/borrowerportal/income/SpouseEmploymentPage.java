package net.sigmainfo.lf.automation.portal.pages.borrowerportal.income;

import net.sigmainfo.lf.automation.common.AbstractTests;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.testng.AssertJUnit.assertTrue;

/**
 * Created by shaishav.s on 25-09-2017.
 */
public class SpouseEmploymentPage extends AbstractTests {
    WebDriverWait wait = new WebDriverWait(driver,60);
    WebElement PropertyTypeListItem;

    private Logger logger = LoggerFactory.getLogger(SpouseIncomeSearchPage.class);

    public SpouseEmploymentPage(WebDriver driver) {
        this.driver = driver;
        logger.info("========= SpouseEmploymentPage is loaded===========");
        //assertTrue(driver.findElement(By.xpath("//div[starts-with(@id,'ID-')][11]/h3[contains(text(),\"\"+portalParam.spouceFirstName+\"'s employment\")]")).isDisplayed());
    }

    public SpouseEmploymentPage() {
    }

    public static By SpouseEmployerNameTextBox = By.xpath("//div[starts-with(@id,'ID-')][11]//label[contains(text(),'Current Employer')]");
    public static By SpouseTitleTextBox = By.xpath("//div[starts-with(@id,'ID-')][11]//label[contains(text(),'Title')]");
    public static By SpouseEmploymentStartDate = By.xpath("//div[starts-with(@id,'ID-')][11]//label[contains(text(),'Start Date')]");
    public static By SpouseLineOfYearTextBox = By.xpath("//div[starts-with(@id,'ID-')][11]//label[contains(text(),'Years')]");
    public static By SpouseLineOfMonthTextBox = By.xpath("//div[starts-with(@id,'ID-')][11]//label[contains(text(),'Months')]");
    public static By SpouseMonthlyBaseSalaryBox = By.xpath("//div[starts-with(@id,'ID-')][11]//div[7]//input[@id='input']");
    public static By SpouseBonusButton = By.xpath("//div[starts-with(@id,'ID-')][11]//div[8]//div[3]//ui-container/div[1]//div[1]");
    public static By SpouseCommissionButton = By.xpath("//div[starts-with(@id,'ID-')][11]//div[8]//div[3]//ui-container/div[1]//div[1]");
    public static By SpouseBonusTextBox = By.xpath("//div[starts-with(@id,'ID-')][11]//div[9]//input[@id='input']");
    public static By NextButton = By.xpath(".//*[@id='form']/right-content-area//button[contains(text(),'NEXT')]");

    public SpouseEmpAddressPage provideSpouseEmploymentDetails(String currentEmployer, String employmentTitle, String employmentStartDate, String inThisLineYear, String inThisLineMonth, String monthlyBaseSalary,String bonusAmount) throws InterruptedException {
        enterText(SpouseEmployerNameTextBox, currentEmployer);
        enterText(SpouseTitleTextBox, employmentTitle);
        enterText(SpouseEmploymentStartDate, employmentStartDate);
        enterText(SpouseLineOfYearTextBox, inThisLineYear);
        enterText(SpouseLineOfMonthTextBox, inThisLineMonth);
        enterText(SpouseMonthlyBaseSalaryBox, monthlyBaseSalary);
        Thread.sleep(2500);
        selectButton(SpouseBonusButton, "Bonus");
        enterText(SpouseBonusTextBox,bonusAmount);
        scrollDownThePage(driver);
        selectButton(NextButton, "Next");
        return new SpouseEmpAddressPage(driver);
    }

    private void scrollDownThePage(WebDriver driver) {
        WebElement element = driver.findElement(NextButton);
        ((JavascriptExecutor) driver).executeScript(
                "arguments[0].scrollIntoView();", element);
    }

    public void enterText(By locator,String text){
        Actions actions = new Actions(driver);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        actions.moveToElement(element);
        actions.click();
        actions.sendKeys(text);
        actions.build().perform();
        logger.info("Entering :"+text);
    }

    public void clickButton() throws Exception {
        wait.until(ExpectedConditions.elementToBeClickable(NextButton));
        driver.findElement(NextButton).click();
        logger.info("Clicking on Next button");
    }

    public void selectButton(By locator,String value){
        wait.until(ExpectedConditions.elementToBeClickable(locator));
        driver.findElement(locator).click();
        logger.info("Clicking on "+ value +" button");
    }
}
