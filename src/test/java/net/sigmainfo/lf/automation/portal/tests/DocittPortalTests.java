package net.sigmainfo.lf.automation.portal.tests;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.common.TestResults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

/**
 * Created by shaishav.s on 08-02-2017.
 */
public class DocittPortalTests extends AbstractTests {

    private Logger logger = LoggerFactory.getLogger(DocittPortalTests.class);

    @Autowired
    TestResults testResults;

    public static String funcMod="Portal";

    @Test(description = "", groups = {"docittportaltests"})
    public void DocittApiTest() throws Exception{
        String sTestID = "Docitt_Login_Page";
        String result = "Failed";
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
    }
}
