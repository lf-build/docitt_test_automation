package net.sigmainfo.lf.automation.portal.pages.borrowerportal.income;

import net.sigmainfo.lf.automation.common.AbstractTests;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static net.sigmainfo.lf.automation.portal.constant.PortalParam.typeOfRentalProperty;

/**
 * Created by shaishav.s on 27-09-2017.
 */
public class SpouseOtherIncomePage extends AbstractTests {
    WebDriverWait wait = new WebDriverWait(driver,60);
    WebElement IncomeSourceListItem;

    private Logger logger = LoggerFactory.getLogger(SpouseOtherIncomePage.class);

    public SpouseOtherIncomePage(WebDriver driver) {
        this.driver = driver;
        logger.info("========= SpouseOtherIncomePage is loaded===========");
    }

    public SpouseOtherIncomePage() {}

    public static By SpousePerMonthOtherIncomeTextBox = By.xpath("//div[starts-with(@id,'ID-')][20]//div[2]//input[@id='input']");
    public static By SpouseSourceOfIncomeDropdown = By.xpath("//div[starts-with(@id,'ID-')][20]//input[@value='What is the source of this income?']");
    public static By SpouseContinuousIncomeButton = By.xpath("//div[starts-with(@id,'ID-')][20]//ui-switch[@id='77']//label[normalize-space(.)='Yes']");
    public static By SpouseNotContinuousIncomeButton = By.xpath("//div[starts-with(@id,'ID-')][20]//ui-switch[@id='77']//label[normalize-space(.)='No']");
    public static By AutomobileIncomeSource = By.xpath("//span[contains(text(),'Automobile alliance')]");
    public static By DisabilityIncomeSource = By.xpath("//span[contains(text(),'disability')]");
    public static By iAmAllDoneButton = By.xpath("//button[@name='submit']");
    public static By NextButton = By.xpath("//button[@name='next']");

    public IncomeSectionCompletePage provideOtherIncomeDetails(String otherIncomePerMonth, String sourceOfIncome, boolean continuousIncome) throws Exception {
        enterOtherIncome(SpousePerMonthOtherIncomeTextBox,otherIncomePerMonth);
        selectSourceOfIncome(SpouseSourceOfIncomeDropdown,sourceOfIncome);
        selectBoolean(SpouseContinuousIncomeButton,continuousIncome);
        clickButton();
        return new IncomeSectionCompletePage(driver);
    }

    private void selectSourceOfIncome(By locator, String sourceOfIncome) throws InterruptedException {
        /*Actions actions = new Actions(driver);
        wait.until(ExpectedConditions.elementToBeClickable(locator));
        driver.findElement(locator).click();
        if(sourceOfIncome.equalsIgnoreCase("Automobile alliance"))
        {
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(AutomobileIncomeSource)));
            IncomeSourceListItem = driver.findElement(AutomobileIncomeSource);
        }
        else if(sourceOfIncome.equalsIgnoreCase("disability")){
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(DisabilityIncomeSource)));
            IncomeSourceListItem = driver.findElement(DisabilityIncomeSource);
        }

        actions.moveToElement(IncomeSourceListItem);
        actions.click();
        actions.build().perform();
        logger.info("Selected from dropdown :"+sourceOfIncome);*/
        wait.until(ExpectedConditions.elementToBeClickable(locator));
        driver.findElement(locator).click();
        Thread.sleep(2000);
        for(int i=3;i<=18;i++){
            WebElement incomeType = driver.findElement(By.cssSelector("div[id^='ID-']:nth-child(24) ui-options[name='dobMonth'] div[class*='select-wrapper'] li:nth-child("+i+")"));
            incomeType.findElement(By.xpath("//div[starts-with(@id,'ID-')][20]//span[contains(text(),'"+sourceOfIncome+"')]")).click();
            break;
        }
        logger.info("Selected :"+sourceOfIncome);
    }

    private void enterSourceOfIncome(By locator, String sourceOfIncome) {
        enterText(locator,sourceOfIncome);
    }

    public void selectBoolean(By locator,boolean value)
    {
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("arguments[0].click()", element);
        logger.info("Selected boolean :"+value);
    }

    private void enterOtherIncome(By locator, String otherIncomePerMonth) {
        enterText(locator,otherIncomePerMonth);
    }

    public void enterText(By locator,String text){
        Actions actions = new Actions(driver);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        actions.moveToElement(element);
        actions.click();
        actions.sendKeys(text);
        actions.build().perform();
        logger.info("Entering :"+text);
    }

    public void clickButton() throws Exception {
            wait.until(ExpectedConditions.presenceOfElementLocated(iAmAllDoneButton));
            wait.until(ExpectedConditions.elementToBeClickable(iAmAllDoneButton));
            Thread.sleep(2500);
            driver.findElement(iAmAllDoneButton).click();
            logger.info("Clicking on I am done button");

    }
}
