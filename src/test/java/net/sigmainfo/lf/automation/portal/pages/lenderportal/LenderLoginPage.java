package net.sigmainfo.lf.automation.portal.pages.lenderportal;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.function.StringEncrypter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by           : Shaishav.s on 06-11-2017.
 * Test class           : LenderLoginPage.java
 * Includes             : 1. Objects on LenderLoginPage
 *                        2. Methods implementation on LenderLoginPage
 */
@Component
public class LenderLoginPage extends AbstractTests {

    StringEncrypter strEncrypter;

    public LenderLoginPage(WebDriver driver) {

        this.driver = driver;
        logger.info("=========== LenderLoginPage is loaded============");
    }
    public LenderLoginPage(){}

    private Logger logger = LoggerFactory.getLogger(LenderLoginPage.class);


    public static By lenderLoginTextbox = By.cssSelector("div[class*='login-form'] ui-input[name='email'] input[id='input']");
    public static By passwordTextbox = By.cssSelector("div[class*='login-form'] ui-input[name='password'] input[id='input']");
    public static By loginButton = By.xpath("//button/span[contains(text(),'LOG IN')]");
    public static By forgetItLink = By.xpath("//div/a[contains(text(),'Forgot it')]");

    public LenderPipeLinePage clickLogIn() {
        WebDriverWait wait = new WebDriverWait(driver,60);
        logger.info("Clicking Login button");
        wait.until(ExpectedConditions.presenceOfElementLocated(loginButton));
        wait.until(ExpectedConditions.elementToBeClickable(loginButton));
        WebElement loginBtn = driver.findElement(loginButton);
        if((loginBtn.isDisplayed()) && (loginBtn.isEnabled()))
            loginBtn.click();
        else
            new Exception("Login button is not found");
        return new LenderPipeLinePage(driver);
    }

    public LenderPipeLinePage loginToLenderPortal(String username, String password) throws StringEncrypter.EncryptionException {
        enterEmail(portalParam.lenderUserLogin);
        enterPassword(password);
        clickLogIn();
        return new LenderPipeLinePage(driver);
    }

    public void enterPassword(String password) throws StringEncrypter.EncryptionException {
        StringEncrypter stringEncrypter = new StringEncrypter();
        enterText(passwordTextbox,StringEncrypter.createNewEncrypter().decrypt(password));
        logger.info("Entered Password");
    }

    public void enterEmail(String username) {
        enterText(lenderLoginTextbox,username);
        logger.info("Entered username :"+username);
    }

    private void enterText(By locator,String text) {
        Actions actions = new Actions(driver);
        WebElement passwordTxtBox = driver.findElement(locator);
        actions.moveToElement(passwordTxtBox);
        actions.click();
        actions.sendKeys(text);
        actions.build().perform();
    }


}
