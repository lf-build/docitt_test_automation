package net.sigmainfo.lf.automation.portal.pages.borrowerportal.profile;

import net.sigmainfo.lf.automation.common.AbstractTests;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.springframework.test.util.AssertionErrors.assertEquals;
import static org.testng.AssertJUnit.assertTrue;

/**
 * Created by shaishav.s on 22-08-2017.
 */
public class WhosOnLoanPage extends AbstractTests {
    private Logger logger = LoggerFactory.getLogger(WhosOnLoanPage.class);

    public WhosOnLoanPage(WebDriver driver){
        this.driver = driver;
        logger.info("========= WhosOnLoanPage page is loaded===========");
        assertTrue(driver.findElement(By.xpath("//div[@id='SS4']/h3[contains(text(),\""+portalParam.firstName+" who's on the loan\")]")).isDisplayed());
        assertTrue(driver.findElement(By.cssSelector("div[id='SS4'] questioner-question-set[class*='sub-sections'] div:nth-child(2) div[class*=col-md-12] questioner-question[class*='questioner-question-set'] ui-container[class*='ui-container-0'] ui-popover[custom-icon='true']")).isDisplayed());
        Actions action = new Actions(driver);
        action.moveToElement(driver.findElement(By.cssSelector("div[id='SS4'] questioner-question-set[class*='sub-sections'] div:nth-child(2) div[class*=col-md-12] questioner-question[class*='questioner-question-set'] ui-container[class*='ui-container-0'] ui-popover[custom-icon='true']"))).build().perform();
    }
    public WhosOnLoanPage(){}

    WebDriverWait wait = new WebDriverWait(driver,60);

    public static By MarriedButton = By.xpath("//input[@id='Married']");
    public static By SeparatedButton = By.xpath("//input[@id='Separated']");
    public static By UnmarriedButton = By.xpath("//input[@id='Unmarried']");
    public static By AddSpouceButton = By.xpath("//ui-switch[@id='16']//label[normalize-space(.)='Yes']");
    public static By NoSpouceButton = By.xpath("//ui-switch[@id='16']//label[normalize-space(.)='No']");
    public static By NoCoborrowerButton = By.xpath("//ui-switch[@id='17']//label[normalize-space(.)='No']");
    public static By AddCoborrowerButton = By.xpath("//ui-switch[@id='17']//label[normalize-space(.)='Yes']");
    public static By YesDependantButton = By.xpath("//ui-switch[@id='57']//input[@id='true']");
    public static By NoDependantButton = By.xpath("//ui-switch[@id='57']//input[@id='false']");
    public static By NextButton = By.xpath("//button[contains(text(),'Next')]");

    public void selectBoolean(By locator,boolean value)
    {
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("arguments[0].click()", element);
        logger.info("Selected boolean :"+value);
    }

    public void selectButton(By locator,String value){
        if(value.equalsIgnoreCase("Yes")) {
            driver.findElement(locator).click();
        }
        else
        {
            driver.findElement(locator).click();
        }
        logger.info("Clicking on "+ value +" button");
    }

    public void clickNext() throws Exception {
        wait.until(ExpectedConditions.elementToBeClickable(NextButton));
        driver.findElement(NextButton).click();
        logger.info("Clicking on NEXT button");
    }

    public SpouseInformationPage selectApplicants(boolean married, boolean addSpouce, boolean addCoborrower,boolean addDependant) throws Exception {
        Thread.sleep(1500);
        selectBoolean(MarriedButton,married);
        Thread.sleep(1500);
        if(addSpouce) {
            new WebDriverWait(driver, 30).until(ExpectedConditions.elementToBeClickable(AddSpouceButton));
            driver.findElement(AddSpouceButton).click();
            logger.info("Adding Spouce");
        }
        else if(!addSpouce)
        {
            new WebDriverWait(driver, 30).until(ExpectedConditions.elementToBeClickable(NoSpouceButton));
            driver.findElement(NoSpouceButton).click();
            logger.info("Not adding Spouce");
        }
        Thread.sleep(1500);
        if(addCoborrower) {
            new WebDriverWait(driver, 30).until(ExpectedConditions.elementToBeClickable(AddCoborrowerButton));
            driver.findElement(AddCoborrowerButton).click();
            logger.info("Adding Non spouce");
        }
        else if (!addCoborrower)
        {
            new WebDriverWait(driver, 30).until(ExpectedConditions.elementToBeClickable(NoCoborrowerButton));
            driver.findElement(NoCoborrowerButton).click();
            logger.info("Not adding Non spouce");
        }
        Thread.sleep(1500);
        selectBoolean(NoDependantButton,addDependant);
        Thread.sleep(1500);
        clickNext();
        return new SpouseInformationPage(driver);
    }

    public EligibilityPage selectApplicant(boolean married, boolean addSpouce, boolean addCoborrwer,boolean addDependant) throws Exception {
        Thread.sleep(1500);
        selectBoolean(MarriedButton,married);
        Thread.sleep(1500);
        if(addSpouce) {
            new WebDriverWait(driver, 30).until(ExpectedConditions.elementToBeClickable(AddSpouceButton));
            driver.findElement(AddSpouceButton).click();
            logger.info("Adding Spouce");
        }
        else
        {
            new WebDriverWait(driver, 30).until(ExpectedConditions.elementToBeClickable(NoSpouceButton));
            driver.findElement(NoSpouceButton).click();
            logger.info("Not adding Spouce");
        }
        Thread.sleep(1500);
        if(addCoborrwer)
        {
            driver.findElement(AddCoborrowerButton).click();
            logger.info("Adding coborrower");
        }
        else {
            driver.findElement(NoCoborrowerButton).click();
            logger.info("Not adding coborrower");
        }
        Thread.sleep(1500);
        selectBoolean(NoDependantButton,addDependant);
        Thread.sleep(1500);
        clickNext();
        return new EligibilityPage(driver);
    }
}
