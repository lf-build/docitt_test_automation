package net.sigmainfo.lf.automation.portal.pages.borrowerportal.income;

import net.sigmainfo.lf.automation.common.AbstractTests;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Type;

/**
 * Created by shaishav.s on 07-09-2017.
 */
public class BusinessSelfEmploymentPage extends AbstractTests {
    WebDriverWait wait = new WebDriverWait(driver,60);
    WebElement PropertyTypeListItem;

    private Logger logger = LoggerFactory.getLogger(BusinessSelfEmploymentPage.class);

    public BusinessSelfEmploymentPage(WebDriver driver) {
        this.driver = driver;
        logger.info("========= BusinessSelfEmploymentPage is loaded===========");
    }

    public BusinessSelfEmploymentPage() {}

    public static By MonthlyIncomeTextBox = By.xpath("//platform-shell/div/borrower-private-layout/div/micro-app/div/questioner-sections/div[1]/div[3]/div/div/ui-form/right-content-area/div/div/sub-sections/div[7]/questioner-question-set/div[2]/div/div/questioner-question/ui-container/div/ui-input/div/div[1]/input");
    public static By CompanyNameTextBox = By.xpath("//platform-shell/div/borrower-private-layout/div/micro-app/div/questioner-sections/div[1]/div[3]/div/div/ui-form/right-content-area/div/div/sub-sections/div[7]/questioner-question-set/div[3]/div/div/questioner-question/ui-container/div/ui-input/div/div[1]/input");
    public static By TitleTextBox = By.xpath("//platform-shell/div/borrower-private-layout/div/micro-app/div/questioner-sections/div[1]/div[3]/div/div/ui-form/right-content-area/div/div/sub-sections/div[7]/questioner-question-set/div[4]/div/div/questioner-question/ui-container/div/ui-input/div/div[1]/input");
    public static By AddressTextBox = By.xpath("//platform-shell/div/borrower-private-layout/div/micro-app/div/questioner-sections/div[1]/div[3]/div/div/ui-form/right-content-area/div/div/sub-sections/div[7]/questioner-question-set/div[5]/div/div/questioner-question/ui-container/div/ui-business-address/div/div[1]/ui-container/div/ui-input/div/div[1]/input");
    public static By SelectAddressOption = By.xpath("//div[4]/div[4]/span[3]");
    public static By PhoneTextBox = By.xpath("//platform-shell/div/borrower-private-layout/div/micro-app/div/questioner-sections/div[1]/div[3]/div/div/ui-form/right-content-area/div/div/sub-sections/div[7]/questioner-question-set/div[6]/div/div/questioner-question/ui-container/div/ui-input/div/div[1]/input");
    public static By StartDateTextBox = By.xpath("//platform-shell/div/borrower-private-layout/div/micro-app/div/questioner-sections/div[1]/div[3]/div/div/ui-form/right-content-area/div/div/sub-sections/div[7]/questioner-question-set/div[7]/div/div/questioner-question/ui-container/div/ui-input/div/div[1]/input");
    public static By PercentageOwnershipButton = By.xpath("//ui-switch[@id='64']//label[normalize-space(.)='Yes']");
    public static By NoPercentageOwnershipButton = By.xpath("//ui-switch[@id='64']//label[normalize-space(.)='No']");
    //public static By TypeOfCompanyDropdown = By.xpath("//platform-shell/div/borrower-private-layout/div/micro-app/div/questioner-sections/div[1]/div[3]/div/div/ui-form/right-content-area/div/div/sub-sections/div[7]/questioner-question-set/div[9]/div[2]/div/questioner-question/ui-container/div/ui-options/div[1]/div/input");
    //public static By TypeOfCompanyDropdown = By.xpath("//platform-shell/div/borrower-private-layout/div/micro-app/div/questioner-sections/div[1]/div[3]/div/div/ui-form/right-content-area/div/div/sub-sections/div[9]/questioner-question-set/div[5]/div/div[1]/questioner-question/ui-container/div/ui-options/div[1]/div/input");
    public static By TypeOfCompanyDropdown = By.xpath("//platform-shell/div/borrower-private-layout/div/micro-app/div/questioner-sections/div[1]/div[3]/div/div/ui-form/right-content-area/div/div/sub-sections/div[7]/questioner-question-set/div[9]/div[2]/div/questioner-question/ui-container/div/ui-options/div[1]/div/input");
    public static By PartnerShipCompany = By.xpath("//span[contains(text(),'Partnership'");
    public static By ScorpCompany = By.xpath("//span[contains(text(),'S-Corp'");
    public static By LLCCompany = By.xpath("//span[contains(text(),'LLC'");
    public static By ScheduleCompany = By.xpath("//span[contains(text(),'Schedule C'");
    public static By IamDoneButton = By.xpath("//button[@name='next']");

	public MilitaryPage provideSelfEmploymentDetails(String selfEmploymentMonthlyIncome,
			String selfEmploymentCompanyName, String selfEmploymentTitle, String selfEmploymentBusinessAddress,
			String selfEmploymentBusinessPhone, String selfEmploymentBusinessStartDate, boolean percentageOwnership,
			String typeOfCompany) throws Exception {
		enterMonthlyIncome(MonthlyIncomeTextBox,selfEmploymentMonthlyIncome);
		enterCompanyName(CompanyNameTextBox,selfEmploymentCompanyName);
		enterTitle(TitleTextBox,selfEmploymentTitle);
		selectBusinessAddress(AddressTextBox,selfEmploymentBusinessAddress);
		Thread.sleep(2000);
		enterBusinessPhone(PhoneTextBox,selfEmploymentBusinessPhone);
		Thread.sleep(2000);
		enterBusinessStartDate(StartDateTextBox,selfEmploymentBusinessStartDate);
		selectOwnershipInCompany(PercentageOwnershipButton,percentageOwnership);
		scrollDownThePage(driver);
		selectTypeOfCompany(TypeOfCompanyDropdown,typeOfCompany);
		clickButton();
		return new MilitaryPage(driver);
	}

	private void scrollDownThePage(WebDriver driver) {
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
	}

	private void selectTypeOfCompany(By locator, String typeOfCompany) throws InterruptedException {
	    wait.until(ExpectedConditions.elementToBeClickable(TypeOfCompanyDropdown));
		driver.findElement(TypeOfCompanyDropdown).click();
		Thread.sleep(2000);
		for(int i=3;i<=6;i++){
			WebElement companyType = driver.findElement(By.cssSelector("ui-options[name='dobMonth'] div[class*='select-wrapper'] li:nth-child("+i+")"));
			String text = companyType.getText();

			if(text.equalsIgnoreCase(typeOfCompany)){
				companyType.click();
			}
		}
		logger.info("Selected :"+typeOfCompany);
	}

	private void selectOwnershipInCompany(By locator, boolean percentageOwnership) {
		selectBoolean(locator, percentageOwnership);
		
	}

	private void enterBusinessStartDate(By locator, String selfEmploymentBusinessStartDate) {
		enterText(locator,selfEmploymentBusinessStartDate);
		
	}

	private void enterBusinessPhone(By locator, String selfEmploymentBusinessPhone) {
		enterText(locator,selfEmploymentBusinessPhone);
	}

	private void enterTitle(By locator, String selfEmploymentTitle) {
		enterText(locator,selfEmploymentTitle);
		
	}

	private void enterCompanyName(By locator, String selfEmploymentCompanyName) {
		enterText(locator,selfEmploymentCompanyName);
		
	}

	private void enterMonthlyIncome(By locator, String selfEmploymentMonthlyIncome) {
		enterText(locator,selfEmploymentMonthlyIncome);
	}

	private void selectBusinessAddress(By locator,String currentAddress) throws InterruptedException {
        enterText(locator,currentAddress);
        Thread.sleep(3000);
        selectOptionWithText(currentAddress);
        Thread.sleep(3000);
        
    }

    private void selectOptionWithText(String currentAddress) {
        driver.findElement(By.xpath("//div[@class='pac-item']//span[contains(text(),'"+currentAddress+"')]")).click();
        logger.info("Selected address :" + currentAddress);
    }
    
    public void selectBoolean(By locator,boolean value)
    {
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("arguments[0].click()", element);
        logger.info("Selected boolean :"+value);
    }

    public void enterText(By locator,String text){
        Actions actions = new Actions(driver);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        actions.moveToElement(element);
        actions.click();
        actions.sendKeys(text);
        actions.build().perform();
        logger.info("Entering :"+text);
    }

    public void clickButton() throws Exception {
        wait.until(ExpectedConditions.elementToBeClickable(IamDoneButton));
        driver.findElement(IamDoneButton).click();
        logger.info("Clicking on I am done button");
    }
    
    
}

