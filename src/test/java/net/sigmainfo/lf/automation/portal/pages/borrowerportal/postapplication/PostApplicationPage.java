package net.sigmainfo.lf.automation.portal.pages.borrowerportal.postapplication;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

import net.sigmainfo.lf.automation.common.AbstractTests;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import groovyjarjarantlr.debug.GuessingEvent;
import net.sigmainfo.lf.automation.portal.constant.PortalParam;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.profile.ApplicantInformationPage;
import org.testng.Assert;

public class PostApplicationPage extends AbstractTests{

	//WebDriver driver;
	WebDriverWait wait = new WebDriverWait(driver,60);
	private Logger logger = LoggerFactory.getLogger(ApplicantInformationPage.class);

	public static By fileUploadArea=By.xpath("//*[@id='docu-drop']/table/tbody/tr/td");
	//public static By nextBtn=By.xpath(".//*[@id='addDocument']/div[2]/button");

	public static By nextBtn=By.xpath(".//*[@id='form']/post-application-right-content-area//div/div[2]/button");

	public static By TurboTaxBtn= By.xpath(".//*[@id='divTaxReturnsGrid']/assets-plaid-grid/div[2]/div[1]/div");
	public static By HAndRBlockBtn= By.xpath(".//*[@id='divTaxReturnsGrid']/assets-plaid-grid/div[2]/div[2]/div");

	//public static By OnlineID=By.xpath(".//*[@id='username']/div/dv[1]");
	//public static By password=By.xpath(".//*[@id='password']/div/div[1]");
	public static By OnlineID=By.xpath("//label[contains(text(),'Username')]//preceding-sibling::input");
	public static By password=By.xpath("//label[contains(text(),'Password')]//preceding-sibling::input");

	public static By institutionNameTextbox = By.xpath("//ui-input[@id='taxInstitutionName']//input[@id='input']");
	public static By institutionOwnerTextbox = By.xpath("//ui-input[@id='taxInstitutionOwner']//input[@id='input']");
	public static By institutionTypeTextbox = By.xpath("//ui-input[@id='taxInstitutionType']//input[@id='input']");
	public static By saveLink = By.xpath(".//*[@id='liTaxreturnsNew']/li/a[1]");

	public static By paystubInstitutionNameTextbox = By.xpath("//ui-input[@id='w2sInstitutionName']//input[@id='input']");
	public static By paystubInstitutionOwnerTextbox = By.xpath("//ui-input[@id='w2sInstitutionOwner']//input[@id='input']");
	public static By paystubDateTextbox = By.xpath("//ui-input[@id='w2sDate']//input[@id='input']");
	public static By paystubCompanyTextbox = By.xpath("//ui-input[@id='w2sCompany']//input[@id='input']");
	public static By paystubAmountTextbox = By.xpath("//ui-input[@id='w2sTotalAmount']//input[@id='input']");

	public static By w2InstitutionNameTextbox = By.xpath("//div[@id='liNew']//ui-input[@id='w2sInstitutionName']//input[@id='input']");
	public static By w2InstitutionOwnerTextbox = By.xpath("//div[@id='liNew']//ui-input[@id='w2sInstitutionOwner']//input[@id='input']");
	public static By w2DateTextbox = By.xpath("//div[@id='liNew']//ui-input[@id='w2sDate']//input[@id='input']");
	public static By w2CompanyTextbox = By.xpath("//div[@id='liNew']//ui-input[@id='w2sCompany']//input[@id='input']");
	public static By w2AmountTextbox = By.xpath("//div[@id='liNew']//ui-input[@id='w2sTotalAmount']//input[@id='input']");
	public static By saveW2Link = By.xpath("//div[@id='liNew']//li/a[contains(text(),'Save')]");

	public static By alimonyFileUploadButton = By.xpath(".//*[@id='accNewformData']/li[2]/div/div[2]/a[2]");
	public static By alimonyInstitutionNameTextbox = By.xpath("//div[@id='liAssetsPlaidNew']//ui-input[@id='newInstitutionName']//input[@id='input']");
	public static By alimonyOwnerNameTextbox = By.xpath("//div[@id='liAssetsPlaidNew']//ui-input[@id='accountOwner']//input[@id='input']");
	public static By alimonyAccountTextbox = By.xpath("//div[@id='liAssetsPlaidNew']//ui-input[@id='accountMask']//input[@id='input']");
	public static By alimonyAccountTypeTextbox = By.xpath("//div[@id='liAssetsPlaidNew']//ui-input[@id='accountSubType']//input[@id='input']");
	public static By alimonyAccountBalanceTextbox = By.xpath("//div[@id='liAssetsPlaidNew']//ui-input[@id='accTotalBalance']//input[@id='input']");
	public static By alimonySaveLink = By.xpath("//div[@id='liAssetsPlaidNew']//a[contains(text(),'Save')]");


	public static By enterBtn=By.cssSelector("button[action='assets-submit']");
	public static By mostRecenet2YearTax=By.cssSelector("div[class*='post-application-left-navigation'] div:nth-child(10)");
	public static By tomatoQue=By.xpath(".//*[@id='input']");
	public static By queAnswerEnterBtn=By.xpath(".//*[@id='challengeForm']/div/ui-form/div[2]/div[2]/button");
	public static By imAllDoneBtn=By.cssSelector("button[class*='tax-returns-summary-list']");

	public static By formType = By.xpath(".//table/tbody/tr[2]/td[3]");


	//Section-8
	public static By twomostRecentSyncBtn = By.xpath(".//*[@id='accNewformData']/li[2]/div/div[2]/a[1]");
	public static By bankOfAmerica=By.xpath(".//*[@id='divAssetsGrid']/assets-plaid-grid/div[2]/div[1]/div/table/tbody/tr/td/img");
	public static By wellsFargo=By.xpath(".//*[@id='divAssetsGrid']/assets-plaid-grid/div[2]/div[2]/div/table/tbody/tr/td/img");

	public static By OnlineIDInstitution=By.xpath(".//*[@id='credentialsForm']/div[1]/ui-container/div/ui-input/div/div[1]");
	public static By OnlinePasswordInstitution=By.xpath(".//*[@id='credentialsForm']/div[2]/ui-container/div/ui-input/div/div[1]");
	public static By EnterBtnInstitution=By.xpath(".//*[@id='form']/post-application-right-content-area/div//assets-plaid-forms-credentials/div/div[5]/div[2]/button");

	//Section-12
	public static By institutionName=By.xpath(".//*[@id='w2sInstitutionName']/div/div[1]");
	public static By uploadBtn=By.xpath(".//*[@id='newformData']/li[3]/a[2]");
	public static By ownerName=By.xpath(".//*[@id='w2sInstitutionOwner']/div/div[1]");
	public static By date=By.xpath(".//*[@id='w2sDate']/div");
	public static By companyName=By.xpath(".//*[@id='w2sCompany']/div/div[1]");
	public static By totalAmount=By.xpath(".//*[@id='w2sTotalAmount']/div/div[1]");
	public static By saveBtn=By.xpath(".//*[@id='liNew']/li/a[1]");

	public static By syncBtn=By.xpath(".//*[@id='newformData']/li[3]/a[1]");
	public static By paystubNextBtn =By.cssSelector("button[class*='w2s-and-paystubs-summary-list']");

	public static By PaycheXtaxPrepareSerice =By.xpath(".//*[@id='divPayStubsGrid']/assets-grid/div[2]/div[2]/div/table/tbody/tr/td");
	public static By TrinetTaxPrepareSerice =By.xpath(".//*[@id='divPayStubsGrid']/assets-grid/div[2]/div[3]/div/table/tbody/tr/td");
	public static By payStubEnterBtn=By.xpath("//w2s-and-paystubs-container/assets-forms-credentials/div/div[5]/div[2]/button");
	public static By payStubtomatoQue=By.xpath(".//ui-input[@name='challengeAnswer']//input[@id='input']");
	public static By payStubQueEnterBtn=By.xpath(".//*[@id='challengeForm']/div/ui-form/div[2]/div[2]/button");


	//Section-13
	public static By section13NextBtn = By.xpath(".//w2s-and-paystubs-summary-list/div[2]/div/button");


	public static By SupplyaStatementService=By.cssSelector("div[class='left style-scope post-application-left-navigation'] div:nth-child(2)");

	//Two most Recent bank statement
	public static By newInstitutionName=By.xpath(".//*[@id='newInstitutionName']/div//input[@id='input']");
	public static By InstitutionSyncBtn=By.xpath("//ui-form[@id='newformData']/li//a[@title='Sync']");
	public static By InstitutionUploadBtn=By.xpath("//ui-form[@id='newformData']/li//a[@title='Upload']");
	public static By bankStatementUploadBtn = By.xpath(".//*[@id='accNewformData']/li[2]/div/div[2]/a[2]");

	public static By AccountOwnerName=By.xpath("//*[@id='accNewformData']/li[2]/div/div[5]//ui-input[@id='accountOwner']");
	public static By Account=By.xpath(".//*[@id='accNewformData']/li[2]/div/div[6]//ui-input[@id='accountMask']");
	public static By AccountType=By.xpath(".//*[@id='accNewformData']/li[2]/div/div[7]//ui-input[@id='accountSubType']");
	public static By AccountBalance=By.xpath(".//*[@id='accNewformData']/li[2]/div/div[8]//ui-input[@id='accTotalBalance']");
	public static By AccountSaveBtn=By.xpath(".//*[@id='accNewformData']/li[2]/div/div[9]/a[1]");


	//Most Recent last 2 year Personal tax Return
	public static By last2YearNextBtn=By.xpath(".//*[@id='form']/post-application-right-content-area/div/tax-return-container/tax-returns-summary-list/div[2]/div/button");
	public static By AlimonyNextBtn=By.xpath(".//*[@id='form']/post-application-right-content-area//post-assets-plaid-lists-summary/div[2]/div/button");
	public static By nextButton = By.xpath("//button[@name='next']");

	public static By writeExplanationTab = By.xpath("//a[contains(text(),'Write Explanation')]");
	public static By largeDepositNextButton = By.xpath(".//*[@id='manualUploadLargeDeposits']/document-with-write-explaination/div/div[2]/button");


	public PostApplicationPage(WebDriver driver)
	{
		this.driver=driver;

	}

	public static void fileupload() throws AWTException, InterruptedException{
		String userDirectory =  System.getProperty("user.dir");
		userDirectory=userDirectory.replaceAll("/", "\\\\/");

		String filepath= userDirectory+"\\src\\test\\resources\\images\\space_1.jpg";

		setClipboardData(filepath);

		Thread.sleep(3000);

		Robot robot = new Robot();
		// Press CTRL+V
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);

		//Release CTRL+V
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);

		// Press Enter
		robot.keyPress(KeyEvent.VK_ENTER);

		//Release Enter Key
		robot.keyRelease(KeyEvent.VK_ENTER);

	}


	public void clickingonOptions(By element){
		WebDriverWait wait = new WebDriverWait(driver, 40);
		wait.until(ExpectedConditions.elementToBeClickable(element));
		driver.findElement(element).click();
	}

	public DashbordTourPage fillingPostApp() throws AWTException, InterruptedException{

		boolean jQuery=PortalFuncUtils.waitForJQueryProcessing(driver, 50, 5);
		//WebDriverWait wait = new WebDriverWait(driver,40);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		int totalItems=0;
		
		if(jQuery){
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[contains(text(),'1.')]")));
			System.out.println("Post Application Page is sucessully loaded");
			totalItems = Integer.parseInt(js.executeScript("return document.querySelector(\"div[class='left style-scope post-application-left-navigation']\").childElementCount;").toString());
		}
		Thread.sleep(20000);
		for(int i=2;i<totalItems;i++) {
			String conditionName = driver.findElement(By.cssSelector("div[class*='post-application-left-navigation'] div:nth-child(" + i + ")")).getText();
			//conditionName=conditionName.replaceAll("[0-9.]", "").trim();
			conditionName = conditionName.split("\\.")[1].trim();

			if (conditionName.contains("the adjutant")) {
				supplyAStatementOfService(driver);
			} else if (conditionName.contains("Divorce decree")) {
				divorceDecreeOrCourtOrder(driver);
			} else if (conditionName.contains("Certificate of Eligibility")) {
				divorceDecreeOrCourtOrder(driver);
			} else if (conditionName.equals("Certificate of Release or Discharge from Active Duty (DD214 Form)")) {
				certificateofReleaseOrDischarge(driver);
			} else if (conditionName.equals("Verification of Mortgage")) {
				certificateofReleaseOrDischarge(driver);
			} else if (conditionName.contains("Social Security award letter")) {
				copyOfSocialSecurityAward(driver);
			} else if (conditionName.equals("Driver's License or Passport")) {
				driverLicenseOrPassport(driver);
			} else if (conditionName.contains("H06")) {
				H06PropertyInsurance(driver);
			} else if (conditionName.equals("YTD P&L and Balance Sheet (Un-audited)")) {
				YTDPAndLBalanceSheet(driver);
			} else if (conditionName.contains("Two most recent bank statements")) {
				twoMostRecenetBankStatement(conditionName,driver);
			} else if (conditionName.contains("Proof of receipt of Alimony and Child support")) {
				proofofReceipt(conditionName,driver);
			} else if (conditionName.contains("Most recent two years personal tax returns ")) {
				lastTwoYearPersonalTax(conditionName,driver);
			} else if (conditionName.equals("Most recent two years business federal tax return")) {
				personalNBusinessDeFederalTax(conditionName,driver);
			} else if (conditionName.contains("Most recent two years K1")) {
				twoYearsK1(driver);
			} else if (conditionName.equals("Articles of Incorporation to verify percentage of ownership of company")) {
				articlesofIncorporationtoVerify(driver);
			} else if (conditionName.equals("Most recent 60 days of PayStubs")) {
				PayStub(conditionName,driver);
			} else if (conditionName.equals("Most recent 2 years W-2's")) {
				W2(conditionName,driver);
			} else if (conditionName.equals("Purchase and Sale Agreement")) {
				purchaseNSale(driver);
			} else if (conditionName.equals("A copy of final closing statement from sale")) {
				finalClosingStatement(driver);
			} else if (conditionName.contains("Hazard Insurance")) {
				certificateofReleaseOrDischarge(driver);
			} else if(conditionName.equals("Letter of Explanation regarding bank transactions")){
				letterOfExplanation(driver);
			}
			logger.info(conditionName +" is successfully fulfilled.");
			Thread.sleep(4000);

		}
		return new DashbordTourPage(driver);
	}

	private void letterOfExplanation(WebDriver driver) throws InterruptedException {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ui-form[@id='form']//assets-large-deposits-container//large-deposits-list//li[contains(text(),'Date')]")));
			Assert.assertTrue(driver.findElement(By.xpath("//ui-form[@id='form']//assets-large-deposits-container//large-deposits-list//li[contains(text(),'Date')]")).isDisplayed());
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ui-form[@id='form']//assets-large-deposits-container//large-deposits-list//li[contains(text(),'Account#')]")));
			Assert.assertTrue(driver.findElement(By.xpath("//ui-form[@id='form']//assets-large-deposits-container//large-deposits-list//li[contains(text(),'Account#')]")).isDisplayed());
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ui-form[@id='form']//assets-large-deposits-container//large-deposits-list//li[contains(text(),'Category')]")));
			Assert.assertTrue(driver.findElement(By.xpath("//ui-form[@id='form']//assets-large-deposits-container//large-deposits-list//li[contains(text(),'Category')]")).isDisplayed());
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ui-form[@id='form']//assets-large-deposits-container//large-deposits-list//li[contains(text(),'Description')]")));
			Assert.assertTrue(driver.findElement(By.xpath("//ui-form[@id='form']//assets-large-deposits-container//large-deposits-list//li[contains(text(),'Description')]")).isDisplayed());
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ui-form[@id='form']//assets-large-deposits-container//large-deposits-list//li[contains(text(),'Amount')]")));
			Assert.assertTrue(driver.findElement(By.xpath("//ui-form[@id='form']//assets-large-deposits-container//large-deposits-list//li[contains(text(),'Amount')]")).isDisplayed());
			wait.until(ExpectedConditions.elementToBeClickable(writeExplanationTab));
			driver.findElement(writeExplanationTab).click();
			Thread.sleep(3000);
			Assert.assertTrue(driver.findElement(By.xpath("//ui-text-editor[@id='textEditor']")).getText().contains("To Whom it May Concern,"));
			portalFuncUtils.scrollToElementandClick(driver,largeDepositNextButton);
	}


	private void finalClosingStatement(WebDriver driver) throws InterruptedException, AWTException {
		fileUploadAndClickNextBtn(driver);
	}

	private void W2(String conditionName,WebDriver driver) throws InterruptedException, AWTException {

		/*//Clicking sync button
		wait.until(ExpectedConditions.elementToBeClickable(syncBtn));
		driver.findElement(syncBtn).click();


		PortalFuncUtils.scrollPageDown(driver);

		//Clicking on Tax Preparation Service
		wait.until(ExpectedConditions.visibilityOfElementLocated(TrinetTaxPrepareSerice));
		driver.findElement(TrinetTaxPrepareSerice).click();

		//Entering credentials and enter Button
		wait.until(ExpectedConditions.elementToBeClickable(OnlineID));
		eneterText(OnlineID,PortalParam.TaxuserName);
		eneterText(password,PortalParam.Taxpassword);
		wait.until(ExpectedConditions.elementToBeClickable(enterBtn));
		driver.findElement(enterBtn).click();

		wait.until(ExpectedConditions.elementToBeClickable(tomatoQue));
		eneterText(tomatoQue,"tomato");
		wait.until(ExpectedConditions.elementToBeClickable(queAnswerEnterBtn));
		driver.findElement(queAnswerEnterBtn).click();
		 */		
		//Clicking upload button
		wait.until(ExpectedConditions.elementToBeClickable(uploadBtn));
		driver.findElement(uploadBtn).click();
		PortalFuncUtils.scrollToElementandClick(driver,fileUploadArea);
		fileupload();
		Thread.sleep(1500);
		portalFuncUtils.scrollPageUp(driver);
		enterW2Details(conditionName,portalParam.postAppDate,portalParam.postAppAmount);
		wait.until(ExpectedConditions.presenceOfElementLocated(saveW2Link));
		wait.until(ExpectedConditions.elementToBeClickable(saveW2Link));
		driver.findElement(saveW2Link).click();
		String splitString[] = conditionName.split("\\s");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//li[contains(text(),'"+splitString[splitString.length-2]+"')]")));
		wait.until(ExpectedConditions.elementToBeClickable(nextButton));
		driver.findElement(nextButton).click();
	}

	private void enterW2Details(String conditionName, String postAppDate, String postAppAmount) {
		String splitString[] = conditionName.split("\\s");
		wait.until(ExpectedConditions.presenceOfElementLocated(w2InstitutionNameTextbox));
		enterText(w2InstitutionNameTextbox,splitString[splitString.length-2]);
		wait.until(ExpectedConditions.presenceOfElementLocated(w2InstitutionOwnerTextbox));
		enterText(w2InstitutionOwnerTextbox,splitString[splitString.length-4]);
		wait.until(ExpectedConditions.presenceOfElementLocated(w2DateTextbox));
		enterText(w2DateTextbox,postAppDate);
		wait.until(ExpectedConditions.presenceOfElementLocated(w2CompanyTextbox));
		enterText(w2CompanyTextbox,splitString[splitString.length-5]);
		wait.until(ExpectedConditions.presenceOfElementLocated(w2AmountTextbox));
		enterText(w2AmountTextbox,postAppAmount);
	}

	private void purchaseNSale(WebDriver driver) throws InterruptedException, AWTException {
		fileUploadAndClickNextBtn(driver);
	}

	private void PayStub(String conditionName,WebDriver driver) throws InterruptedException, AWTException {
		WebDriverWait wait = new WebDriverWait(driver,60);
		//Clicking sync button
		/*wait.until(ExpectedConditions.elementToBeClickable(syncBtn));
		driver.findElement(syncBtn).click();


		PortalFuncUtils.scrollPageDown(driver);

		//Clicking on Tax Preparation Service
		wait.until(ExpectedConditions.visibilityOfElementLocated(PaycheXtaxPrepareSerice));
		driver.findElement(PaycheXtaxPrepareSerice).click();

		//Entering credentials and enter btn
		wait.until(ExpectedConditions.elementToBeClickable(OnlineID));
		eneterText(OnlineID,PortalParam.TaxuserName);
		eneterText(password,PortalParam.Taxpassword);
		wait.until(ExpectedConditions.elementToBeClickable(enterBtn));
		driver.findElement(enterBtn).click();

		wait.until(ExpectedConditions.elementToBeClickable(tomatoQue));
		eneterText(tomatoQue,"tomato");
		wait.until(ExpectedConditions.elementToBeClickable(queAnswerEnterBtn));
		driver.findElement(queAnswerEnterBtn).click();
		 */		
		//Clicking upload button
		wait.until(ExpectedConditions.elementToBeClickable(uploadBtn));
		driver.findElement(uploadBtn).click();
		PortalFuncUtils.scrollToElementandClick(driver,fileUploadArea);
		fileupload();
		Thread.sleep(1500);
		portalFuncUtils.scrollPageUp(driver);
		enterPayStubDetails(conditionName,portalParam.postAppDate,portalParam.postAppAmount);
		wait.until(ExpectedConditions.presenceOfElementLocated(saveW2Link));
		wait.until(ExpectedConditions.elementToBeClickable(saveW2Link));
		driver.findElement(saveW2Link).click();
		String splitString[] = conditionName.split("\\s");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//li[contains(text(),'"+splitString[splitString.length-1]+"')]")));
		wait.until(ExpectedConditions.elementToBeClickable(nextButton));
		driver.findElement(nextButton).click();

	}

	private void enterPayStubDetails(String conditionName, String postAppDate,  String postAppAmount) throws InterruptedException {
		String splitString[] = conditionName.split("\\s");
		wait.until(ExpectedConditions.presenceOfElementLocated(paystubInstitutionNameTextbox));
		enterText(paystubInstitutionNameTextbox,splitString[splitString.length-1]);
		wait.until(ExpectedConditions.presenceOfElementLocated(paystubInstitutionOwnerTextbox));
		enterText(paystubInstitutionOwnerTextbox,splitString[splitString.length-3]);
		wait.until(ExpectedConditions.presenceOfElementLocated(paystubDateTextbox));
		enterText(paystubDateTextbox,postAppDate);
		wait.until(ExpectedConditions.presenceOfElementLocated(paystubCompanyTextbox));
		enterText(paystubCompanyTextbox,splitString[splitString.length-5]);
		wait.until(ExpectedConditions.presenceOfElementLocated(paystubAmountTextbox));
		enterText(paystubAmountTextbox,postAppAmount);
	}


	private void articlesofIncorporationtoVerify(WebDriver driver) throws InterruptedException, AWTException {
		fileUploadAndClickNextBtn(driver);
	}

	private void twoYearsK1(WebDriver driver) throws InterruptedException, AWTException {
		fileUploadAndClickNextBtn(driver);
	}

	private void personalNBusinessDeFederalTax(String conditionName,WebDriver driver) throws InterruptedException, AWTException {
		WebDriverWait wait = new WebDriverWait(driver,50);
		//Clicking on Sync Button
		/*wait.until(ExpectedConditions.elementToBeClickable(syncBtn));
		driver.findElement(syncBtn).click();


		PortalFuncUtils.scrollToElement(driver, HAndRBlockBtn);

		wait.until(ExpectedConditions.elementToBeClickable(HAndRBlockBtn));
		if(PortalParam.TaxInstitution.contains("TurboTax")){
			//wait.until(ExpectedConditions.elementToBeClickable(TurboTaxBtn));
			driver.findElement(TurboTaxBtn).click();
		}else{
			driver.findElement(HAndRBlockBtn).click();
		}

		//wait.until(ExpectedConditions.visibilityOf(OnlineID));
		Thread.sleep(7000);


		eneterText(OnlineID,PortalParam.TaxuserName);
		eneterText(password,PortalParam.Taxpassword);

		//Clicking Enter Button
		driver.findElement(enterBtn).click();

		wait.until(ExpectedConditions.elementToBeClickable(tomatoQue));
		eneterText(tomatoQue,"tomato");
		wait.until(ExpectedConditions.elementToBeClickable(queAnswerEnterBtn));
		driver.findElement(queAnswerEnterBtn).click();
		 */		
		//Clicking upload button
		wait.until(ExpectedConditions.elementToBeClickable(uploadBtn));
		driver.findElement(uploadBtn).click();
		PortalFuncUtils.scrollToElementandClick(driver,fileUploadArea);
		fileupload();
		enterInstitutionDetails(conditionName);
		wait.until(ExpectedConditions.presenceOfElementLocated(saveLink));
		wait.until(ExpectedConditions.elementToBeClickable(saveLink));
		driver.findElement(saveLink).click();
		String splitString[] = conditionName.split("\\s");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='taxList']//li[contains(text(),'"+splitString[splitString.length-4]+"')]")));
		wait.until(ExpectedConditions.elementToBeClickable(last2YearNextBtn));
		driver.findElement(last2YearNextBtn).click();

	}

	private void lastTwoYearPersonalTax(String conditionName,WebDriver driver) throws InterruptedException, AWTException {
		WebDriverWait wait = new WebDriverWait(driver,50);
		//Clicking on Sync Button
		/*wait.until(ExpectedConditions.elementToBeClickable(syncBtn));
		driver.findElement(syncBtn).click();


		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(HAndRBlockBtn));
		wait.until(ExpectedConditions.elementToBeClickable(HAndRBlockBtn));

		if(PortalParam.TaxInstitution.contains("TurboTax")){
			//wait.until(ExpectedConditions.elementToBeClickable(TurboTaxBtn));
			driver.findElement(TurboTaxBtn).click();
		}else{
			driver.findElement(HAndRBlockBtn).click();
		}

		//wait.until(ExpectedConditions.visibilityOf(OnlineID));
		Thread.sleep(7000);


		eneterText(OnlineID,PortalParam.TaxuserName);
		eneterText(password,PortalParam.Taxpassword);

		//Clicking Enter Button
		driver.findElement(enterBtn).click();

		wait.until(ExpectedConditions.elementToBeClickable(tomatoQue));
		eneterText(tomatoQue,"tomato");
		wait.until(ExpectedConditions.elementToBeClickable(queAnswerEnterBtn));
		driver.findElement(queAnswerEnterBtn).click();
		 */		
		//Clicking upload button

		wait.until(ExpectedConditions.elementToBeClickable(uploadBtn));
		driver.findElement(uploadBtn).click();
		PortalFuncUtils.scrollToElementandClick(driver,fileUploadArea);
		fileupload();
		enterInstitutionDetails(conditionName);
		wait.until(ExpectedConditions.presenceOfElementLocated(saveLink));
		wait.until(ExpectedConditions.elementToBeClickable(saveLink));
		driver.findElement(saveLink).click();
		String splitString[] = conditionName.split("\\s");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='taxList']//li[contains(text(),'"+splitString[splitString.length-4]+"')]")));
		wait.until(ExpectedConditions.elementToBeClickable(last2YearNextBtn));
		driver.findElement(last2YearNextBtn).click();
	}

	public void enterInstitutionDetails(String conditionName) {
		String splitString[] = conditionName.split("\\s");
		wait.until(ExpectedConditions.presenceOfElementLocated(institutionNameTextbox));
		enterText(institutionNameTextbox,splitString[splitString.length-2]);
		wait.until(ExpectedConditions.presenceOfElementLocated(institutionOwnerTextbox));
		enterText(institutionOwnerTextbox,splitString[splitString.length-3]);
		wait.until(ExpectedConditions.presenceOfElementLocated(institutionTypeTextbox));
		enterText(institutionTypeTextbox,splitString[splitString.length-4]);

	}

	private void twoMostRecenetBankStatement(String conditionName,WebDriver driver) throws InterruptedException, AWTException {
		WebDriverWait wait = new WebDriverWait(driver,50);
		//Clicking on Sync Button
		/*wait.until(ExpectedConditions.elementToBeClickable(twomostRecentSyncBtn));
		driver.findElement(twomostRecentSyncBtn).click();

		//Clicking on bank institution
		wait.until(ExpectedConditions.elementToBeClickable(bankOfAmerica));
		driver.findElement(bankOfAmerica).click();

		//Entering OnlineID
		wait.until(ExpectedConditions.visibilityOfElementLocated(OnlineIDInstitution));
		driver.findElement(OnlineIDInstitution).sendKeys("user_good");

		//Entering password
		wait.until(ExpectedConditions.visibilityOfElementLocated(OnlinePasswordInstitution));
		driver.findElement(OnlinePasswordInstitution).sendKeys("pass_good");

		//Clicking Enter Btn
		wait.until(ExpectedConditions.elementToBeClickable(EnterBtnInstitution));
		driver.findElement(EnterBtnInstitution).click();

		//Entering Manual Details
		eneterText(newInstitutionName, PortalParam.PayStubInstitutionName);
		eneterText(ownerName, PortalParam.PayStubOwnerName);
		eneterText(Account, PortalParam.PayStubCompanyName);
		eneterText(AccountType, PortalParam.accountType);
		eneterText(AccountBalance, PortalParam.PayStubTotal);
		 */

		PortalFuncUtils.scrollToElement(driver, bankStatementUploadBtn);
		wait.until(ExpectedConditions.elementToBeClickable(bankStatementUploadBtn));
		driver.findElement(bankStatementUploadBtn).click();
		PortalFuncUtils.scrollToElementandClick(driver, fileUploadArea);
		//FileUpload
		fileupload();
		Thread.sleep(1500);
		portalFuncUtils.scrollPageUp(driver);
		enterAlimonyDetails(conditionName,portalParam.postAppAmount);
		wait.until(ExpectedConditions.presenceOfElementLocated(alimonySaveLink));
		wait.until(ExpectedConditions.elementToBeClickable(alimonySaveLink));
		driver.findElement(alimonySaveLink).click();
		String splitString[] = conditionName.split("\\s");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//li[contains(text(),'"+splitString[0]+"')]")));
		wait.until(ExpectedConditions.elementToBeClickable(nextButton));
		driver.findElement(nextButton).click();

		//Clicking on Save button
		wait.until(ExpectedConditions.elementToBeClickable(AlimonyNextBtn));
		driver.findElement(AlimonyNextBtn).click();
		
		
		
	}

	private void proofofReceipt(String conditionName,WebDriver driver) throws AWTException, InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver,50);
		/*//Clicking on Sync Button
		wait.until(ExpectedConditions.elementToBeClickable(twomostRecentSyncBtn));
		driver.findElement(twomostRecentSyncBtn).click();

		//Clicking on bank institution
		wait.until(ExpectedConditions.elementToBeClickable(wellsFargo));
		driver.findElement(wellsFargo).click();

		//Entering OnlineID
		wait.until(ExpectedConditions.visibilityOfElementLocated(OnlineIDInstitution));
		driver.findElement(OnlineIDInstitution).sendKeys("user_good");

		//Entering password
		wait.until(ExpectedConditions.visibilityOfElementLocated(OnlinePasswordInstitution));
		driver.findElement(OnlinePasswordInstitution).sendKeys("pass_good");

		//Clicking Enter Btn
		wait.until(ExpectedConditions.elementToBeClickable(EnterBtnInstitution));
		driver.findElement(EnterBtnInstitution).click();

		//Entering Manual Details
		eneterText(newInstitutionName, PortalParam.PayStubInstitutionName);
		eneterText(ownerName, PortalParam.PayStubOwnerName);
		eneterText(Account, PortalParam.PayStubCompanyName);
		eneterText(AccountType, PortalParam.accountType);
		eneterText(AccountBalance, PortalParam.PayStubTotal);

		 */

		PortalFuncUtils.scrollToElement(driver, alimonyFileUploadButton);
		wait.until(ExpectedConditions.elementToBeClickable(alimonyFileUploadButton));
		driver.findElement(alimonyFileUploadButton).click();
		PortalFuncUtils.scrollToElementandClick(driver, fileUploadArea);
		//FileUpload
		fileupload();
		Thread.sleep(1500);
		portalFuncUtils.scrollPageUp(driver);
		enterAlimonyDetails(conditionName,portalParam.postAppAmount);
		wait.until(ExpectedConditions.presenceOfElementLocated(alimonySaveLink));
		wait.until(ExpectedConditions.elementToBeClickable(alimonySaveLink));
		driver.findElement(alimonySaveLink).click();
		String splitString[] = conditionName.split("\\s");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//li[contains(text(),'"+splitString[0]+"')]")));
		wait.until(ExpectedConditions.elementToBeClickable(nextButton));
		driver.findElement(nextButton).click();

		//Clicking on Save button
		wait.until(ExpectedConditions.elementToBeClickable(AlimonyNextBtn));
		driver.findElement(AlimonyNextBtn).click();

	}

	private void enterAlimonyDetails(String conditionName, String postAppAmount) {
		String splitString[] = conditionName.split("\\s");
		wait.until(ExpectedConditions.presenceOfElementLocated(alimonyInstitutionNameTextbox));
		enterText(alimonyInstitutionNameTextbox,splitString[0]);
		wait.until(ExpectedConditions.presenceOfElementLocated(alimonyOwnerNameTextbox));
		enterText(alimonyOwnerNameTextbox,splitString[1]);
		wait.until(ExpectedConditions.presenceOfElementLocated(alimonyAccountTextbox));
		enterText(alimonyAccountTextbox,splitString[2]);
		wait.until(ExpectedConditions.presenceOfElementLocated(alimonyAccountTypeTextbox));
		enterText(alimonyAccountTypeTextbox,splitString[3]);
		wait.until(ExpectedConditions.presenceOfElementLocated(alimonyAccountBalanceTextbox));
		enterText(alimonyAccountBalanceTextbox,postAppAmount);
	}

	public static void setClipboardData(String string) {
		StringSelection stringSelection = new StringSelection(string);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
	}

	/*private void payTaxInstituionData(){
		WebDriverWait wait = new WebDriverWait(driver,60);
		//Clicking sync button
		wait.until(ExpectedConditions.elementToBeClickable(syncBtn));
		driver.findElement(syncBtn).click();


		PortalFuncUtils.scrollPageDown(driver);

		//Clicking on Tax Preparation Service
		wait.until(ExpectedConditions.visibilityOfElementLocated(PaycheXtaxPrepareSerice));
		driver.findElement(PaycheXtaxPrepareSerice).click();

		//Entering credentials and enter btn
		wait.until(ExpectedConditions.elementToBeClickable(OnlineID));
		eneterText(OnlineID,PortalParam.TaxuserName);
		eneterText(password,PortalParam.Taxpassword);
		wait.until(ExpectedConditions.elementToBeClickable(enterBtn));
		driver.findElement(enterBtn).click();



	}*/


	public void enterText(By locator,String text){
		Actions actions = new Actions(driver);
		new WebDriverWait(driver,60).until(ExpectedConditions.presenceOfElementLocated(locator));
		WebElement element = driver.findElement(locator);
		actions.moveToElement(element);
		actions.click();
		actions.sendKeys(text);
		actions.build().perform();
	}

	public static void supplyAStatementOfService(WebDriver driver) throws InterruptedException, AWTException{
		fileUploadAndClickNextBtn(driver);	
	}

	public static void divorceDecreeOrCourtOrder(WebDriver driver)throws InterruptedException, AWTException{
		fileUploadAndClickNextBtn(driver);
	}
	public static void certificateofReleaseOrDischarge(WebDriver driver)throws InterruptedException, AWTException{
		skipCondition(driver);
	}
	public static void copyOfSocialSecurityAward(WebDriver driver)throws InterruptedException, AWTException{
		fileUploadAndClickNextBtn(driver);
	}
	public static void driverLicenseOrPassport(WebDriver driver)throws InterruptedException, AWTException{
		fileUploadAndClickNextBtn(driver);
	}
	public static void H06PropertyInsurance(WebDriver driver)throws InterruptedException, AWTException{
		skipCondition(driver);
	}

	private static void skipCondition(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver,50);
		PortalFuncUtils.scrollToElement(driver,nextBtn);
		wait.until(ExpectedConditions.elementToBeClickable(nextBtn));
		driver.findElement(nextBtn).click();
	}

	public static void YTDPAndLBalanceSheet(WebDriver driver) throws InterruptedException, AWTException{
		fileUploadAndClickNextBtn(driver);
	}

	private static void fileUploadAndClickNextBtn(WebDriver driver) throws InterruptedException, AWTException{
		//Clicking on FileUpload Area
		WebDriverWait wait = new WebDriverWait(driver,50);
		wait.until(ExpectedConditions.elementToBeClickable(fileUploadArea));
		driver.findElement(fileUploadArea).click();

		Thread.sleep(2000);

		//FileUpload
		fileupload();

		//Put some Sleep for file uploading
		Thread.sleep(2000);

		//Scroll to Element and click on Element
		PortalFuncUtils.scrollToElementandClick(driver,nextBtn);

		Thread.sleep(6000);

	}

}
