package net.sigmainfo.lf.automation.portal.pages.borrowerportal;

import net.sigmainfo.lf.automation.common.AbstractTests;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by           : Shaishav.s on 08-02-2017.
 * Test class           : VerifyAccessCodePage.java
 * Includes             : 1. Objects on VerifyAccessCodePage
 *                        2. Methods implementation on VerifyAccessCodePage
 */
public class VerifyAccessCodePage extends AbstractTests {

    private Logger logger = LoggerFactory.getLogger(VerifyAccessCodePage.class);
    public VerifyAccessCodePage(WebDriver driver){this.driver = driver;}
    public VerifyAccessCodePage(){}


    public static By accessMyPortalButton = By.xpath("//span[contains(text(),'SIGN UP')]");
    public static By verificationCodeTextBox1 = By.xpath("//input[@name=''][1]");
    public static By enterCodeInstruction = By.xpath("//p[contains(text(),'Enter your 6-digit code below')]");


    public WelcomePage sendVerificationCode(WebDriver driver, String code) {
        int rem;
        int otp = Integer.parseInt(code);
        int a[]= new int[6];
        WebDriverWait wait = new WebDriverWait(driver,30);
        logger.info("Entering 6 digit access code for verification");
        Actions actions = new Actions(driver);

        WebElement codeTxtbox1 = driver.findElement(verificationCodeTextBox1);
        actions.moveToElement(codeTxtbox1);
        actions.click();
        actions.sendKeys(code);
        actions.build().perform();

        driver.findElement(accessMyPortalButton).click();
        return new WelcomePage(this.driver);
    }
}
