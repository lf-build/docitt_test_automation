package net.sigmainfo.lf.automation.portal.pages.borrowerportal.income;

import net.sigmainfo.lf.automation.common.AbstractTests;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.testng.AssertJUnit.assertTrue;

/**
 * Created by shaishav.s on 06-09-2017.
 */
public class IncomeSearchPage extends AbstractTests {
    WebDriverWait wait = new WebDriverWait(driver,60);
    WebElement PropertyTypeListItem;

    private Logger logger = LoggerFactory.getLogger(IncomeSearchPage.class);

    public IncomeSearchPage(WebDriver driver) {
        this.driver = driver;
        logger.info("========= IncomeSearchPage is loaded===========");
        assertTrue(driver.findElement(By.xpath("//div[@id='SS1']/h3[contains(text(),\""+portalParam.firstName+"'s income search\")]")).isDisplayed());
    }

    public IncomeSearchPage() {
    }
    //public static By DoBTextBox = By.xpath("//platform-shell/div/borrower-private-layout/div/micro-app/div/questioner-sections/div[1]/div[3]/div/div/ui-form/right-content-area/div/div/sub-sections/div[1]/questioner-question-set/div[2]/div/div/questioner-question/ui-container/div/ui-input/div/div[1]/input");
    public static By DoBTextBox = By.cssSelector("div[id='SS1'] questioner-question-set[class=\"style-scope sub-sections\"]>div:nth-child(2) input[id='input']");
    public static By SpouseDoBTextBox = By.xpath("//div[starts-with(@id,'sigma_shaishav')]//label[contains(text(),'Date of Birth (MM/DD/YYYY)')]");
    public static By SpouseSsnTextBox = By.xpath("//div[starts-with(@id,'sigma_shaishav')]//label[contains(text(),'Social Security #')]");
    public static By SsnTextbox = By.xpath("//div[@id='SS1']/questioner-question-set/div[3]/div/div/questioner-question/ui-container/div/ui-input/div/div[1]/input");
    public static By FindMyIncomeButton = By.xpath("//button[@name='next']");
    
    public IncomeTypesPage enterIncomeSearchDetails(String dateOfBirth, String ssnNumber) throws Exception {

        enterText(DoBTextBox, dateOfBirth);
    	enterText(SsnTextbox, ssnNumber);
    	clickButton();    	
    	return new IncomeTypesPage(driver);
    }
    
    public void enterText(By locator,String text){
        Actions actions = new Actions(driver);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        actions.moveToElement(element);
        actions.click();
        actions.sendKeys(text);
        actions.build().perform();
        logger.info("Entering :"+text);
    }

    public void clickButton() throws Exception {
        wait.until(ExpectedConditions.elementToBeClickable(FindMyIncomeButton));
        driver.findElement(FindMyIncomeButton).click();
        logger.info("Clicking on Find My Income button");
    }
}
