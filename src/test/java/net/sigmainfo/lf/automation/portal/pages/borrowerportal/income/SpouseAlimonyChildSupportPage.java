package net.sigmainfo.lf.automation.portal.pages.borrowerportal.income;

import net.sigmainfo.lf.automation.common.AbstractTests;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static net.sigmainfo.lf.automation.portal.constant.PortalParam.twoMonthSupport;
import static net.sigmainfo.lf.automation.portal.pages.borrowerportal.income.AlimonyChildSupportPage.twoMonthSupportButton;
import static net.sigmainfo.lf.automation.portal.pages.borrowerportal.income.AlimonyChildSupportPage.willReceiveOrderByCourtButton;
import static org.testng.AssertJUnit.assertTrue;

/**
 * Created by shaishav.s on 26-09-2017.
 */
public class SpouseAlimonyChildSupportPage extends AbstractTests {
    WebDriverWait wait = new WebDriverWait(driver,60);
    WebElement PropertyTypeListItem;

    private Logger logger = LoggerFactory.getLogger(SpouseEmpAddressPage.class);

    public SpouseAlimonyChildSupportPage(WebDriver driver) {
        this.driver = driver;
        logger.info("========= SpouseAlimonyChildSupportPage is loaded===========");
        //assertTrue(driver.findElement(By.xpath("//div[starts-with(@id,'ID-')][11]/h3[contains(text(),\"\"+portalParam.spouceFirstName+\"'s Alimony / Child's support\")]")).isDisplayed());
    }

    public SpouseAlimonyChildSupportPage() {
    }

    public static By SpouseSupportByCourtButton =By.xpath("//div[starts-with(@id,'ID-')][14]//div[2]//questioner-question//ui-switch[@id='36']//label[normalize-space(.)='Yes']");
    public static By SpousewillReceiveOrderByCourtButton =By.xpath("//div[starts-with(@id,'ID-')][14]//div[3]//questioner-question//ui-switch[@id='37']//label[normalize-space(.)='Yes']");
    public static By SpousetwoMonthSupportButton =By.xpath("//div[starts-with(@id,'ID-')][14]//div[4]//questioner-question//ui-switch[@id='38']//label[normalize-space(.)='Yes']");

    public static By SpouseNoSupportByCourtButton =By.xpath("//div[starts-with(@id,'ID-')][14]//div[2]//questioner-question//ui-switch[@id='36']//label[normalize-space(.)='No']");
    public static By SpousewillNotReceiveOrderByCourtButton =By.xpath("//div[starts-with(@id,'ID-')][14]//div[3]//questioner-question//ui-switch[@id='37']//label[normalize-space(.)='No']");
    public static By SpouseNoTwoMonthSupportButton =By.xpath("//div[starts-with(@id,'ID-')][14]//div[4]//questioner-question//ui-switch[@id='38']//label[normalize-space(.)='No']");

    public static By NextButton = By.xpath("//button[@name='next']");

    public SpouseBusinessSelfEmploymentPage provideChildSupportDetails(boolean spouseSupportByCourt, boolean spouseWillReceiveOrderByCourt, boolean spouseTwoMonthSupport) throws Exception {
        if(spouseSupportByCourt)
        {
            selectBoolean(SpouseSupportByCourtButton, spouseSupportByCourt);
        }
        else
        {
            selectBoolean(SpouseNoSupportByCourtButton, spouseSupportByCourt);
        }
        Thread.sleep(3000);
        new WebDriverWait(driver,30).until(ExpectedConditions.elementToBeClickable(SpousewillReceiveOrderByCourtButton));
        if(spouseWillReceiveOrderByCourt)
        {
            selectBoolean(SpousewillReceiveOrderByCourtButton, spouseWillReceiveOrderByCourt);
        }
        else
        {
            selectBoolean(SpousewillNotReceiveOrderByCourtButton, spouseWillReceiveOrderByCourt);
        }
        Thread.sleep(3000);
        new WebDriverWait(driver,30).until(ExpectedConditions.elementToBeClickable(SpouseNoTwoMonthSupportButton));
        if(spouseTwoMonthSupport)
        {
            selectBoolean(SpousetwoMonthSupportButton, twoMonthSupport);
        }
        else
        {
            selectBoolean(SpouseNoTwoMonthSupportButton, twoMonthSupport);
        }
        new WebDriverWait(driver,30).until(ExpectedConditions.elementToBeClickable(NextButton));
        clickNext();
        return new SpouseBusinessSelfEmploymentPage(driver);
    }

    public void selectBoolean(By locator,boolean value)
    {
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("arguments[0].click()", element);
        logger.info("Selected boolean :"+value);
    }

    public void clickNext() throws Exception {
        wait.until(ExpectedConditions.elementToBeClickable(NextButton));
        driver.findElement(NextButton).click();
        logger.info("Clicking on NEXT button");
    }
}
