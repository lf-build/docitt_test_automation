package net.sigmainfo.lf.automation.portal.pages.borrowerportal;

import net.sigmainfo.lf.automation.common.AbstractTests;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by           : Shaishav.s on 08-02-2017.
 * Test class           : TermsOfServicePage.java
 * Includes             : 1. Objects on TermsOfServicePage
 *                        2. Methods implementation on TermsOfServicePage
 */
public class TermsOfServicePage extends AbstractTests {

    private Logger logger = LoggerFactory.getLogger(TermsOfServicePage.class);

    public TermsOfServicePage(WebDriver driver) {
        this.driver = driver;
    }
    public TermsOfServicePage(){}
}
