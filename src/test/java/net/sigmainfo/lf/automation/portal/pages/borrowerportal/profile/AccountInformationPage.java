package net.sigmainfo.lf.automation.portal.pages.borrowerportal.profile;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.testng.Assert;

import static org.testng.Assert.*;

/**
 * Created by shaishav.s on 22-08-2017.
 */

public class AccountInformationPage extends AbstractTests{

    @Autowired
    PortalFuncUtils portalFuncUtils;

    private Logger logger = LoggerFactory.getLogger(AccountInformationPage.class);

    WebDriverWait wait = new WebDriverWait(driver,60);

    public AccountInformationPage(WebDriver driver){
        this.driver = driver;
        assertTrue(driver.findElement(By.xpath("//div[@id='SS1']/h3")).getText().contains("Account Information"),"Account Information page header is not displayed.");
        logger.info("========= AccountInformationPage page is loaded===========");
    }
    public AccountInformationPage(){}

    public static By FirstNameTextbox = By.xpath("//label[contains(text(),'First Name')]");
    public static By MiddleNameTextbox = By.xpath("//label[contains(text(),'Middle Name')]");
    public static By LastNameTextbox = By.xpath("//label[contains(text(),'Last Name')]");
    public static By SuffixTextBox = By.xpath("//label[contains(text(),'Suffix')]");
    public static By PreferredEmailTextbox = By.xpath("//label[contains(text(),'Preferred Email')]");
    public static By PhoneTextbox = By.xpath("//label[contains(text(),'Phone#')]");
    public static By EmailTextbox = By.xpath("(//label/input[@id='Email'])[1]");
    public static By PhoneButton = By.xpath(".//*[@id='SS1']/questioner-question-set/div[7]/div/div/questioner-question/ui-container/div/ui-toggle-button-group/div[1]/label");
    public static By EmailButton = By.xpath(".//*[@id='SS1']/questioner-question-set/div[7]/div/div/questioner-question/ui-container/div/ui-toggle-button-group/div[2]/label");
    public static By TextButton = By.xpath(".//*[@id='SS1']/questioner-question-set/div[7]/div/div/questioner-question/ui-container/div/ui-toggle-button-group/div[3]/label");
    public static By AllButton = By.xpath(".//*[@id='SS1']/questioner-question-set/div[7]/div/div/questioner-question/ui-container/div/ui-toggle-button-group/div[4]/label");
    public static By NextButton = By.xpath("//button[@name='next']");
    public static By NavigationHeaderLabel = By.xpath("//div[contains(text(),'LEFT TO DO')]");
    public static By progressBarLabel = By.xpath(".//*[@id='micro-app-host']/questioner-sections/div[1]/div[1]/left-navigation/div/div[2]/questioner-progress-bar/div/div");
    public static By AccountInformationLabel = By.xpath("//h3[contains(text(),'Account Information')]");

    public void enterFirstName(String firstName)  {

        enterText(FirstNameTextbox,firstName);
        logger.info("Entering first name :"+firstName);
    }

    public void enterMiddleName(String MiddleName)  {
        enterText(MiddleNameTextbox,MiddleName);
        logger.info("Entering middle name :"+MiddleName);
    }

    public void enterLastName(String LastName)  {
        enterText(LastNameTextbox,LastName);
        logger.info("Entering last name :"+LastName);
    }

    public void enterSuffix(String Suffix)  {
        /*driver.findElement(txtSuffix).click();
        driver.findElement(txtSuffix).sendKeys(Suffix);*/
        enterText(SuffixTextBox,Suffix);
        logger.info("Entering suffix :"+Suffix);
    }

    public void enterPreferredEmail(String PreferredEmail)  {
        enterText(PreferredEmailTextbox,PreferredEmail);
        logger.info("Entering PreferredEmail :"+PreferredEmail);
    }

    public void enterPhone(String Phone)  {
        enterText(PhoneTextbox,Phone);
        logger.info("Entering Phone :"+Phone);
    }

    public void selectPreferredMethodOfCommunication(String modeOfComm) throws Exception {
        if(modeOfComm.equalsIgnoreCase("Email"))
        {
            driver.findElement(EmailButton).click();
        }
        else if(modeOfComm.equalsIgnoreCase("Phone"))
        {
            driver.findElement(PhoneButton).click();
        }
        if(modeOfComm.equalsIgnoreCase("Text"))
        {
            driver.findElement(TextButton).click();
        }
        else
        {
            driver.findElement(AllButton).click();
        }
        logger.info("Selecting preferred method of communication");

    }

    public void clickNext() throws Exception {
        wait.until(ExpectedConditions.elementToBeClickable(NextButton));
        driver.findElement(NextButton).click();
        logger.info("Clicking on NEXT button");
    }

    public PurchasePropertyDetailsPage enterAccountInfo(String firstName, String middleName, String lastName, String suffix, String preferredEmail, String phone, String modeOfComm) throws Exception {
        enterFirstName(firstName);
        enterMiddleName(middleName);
        enterLastName(lastName);
        enterSuffix(suffix);
        enterPreferredEmail(preferredEmail);
        enterPhone(phone);
        selectPreferredMethodOfCommunication(modeOfComm);
        clickNext();
        return new PurchasePropertyDetailsPage(driver);
    }

   public void verifyAccountInformationPageUI()
    {
        wait.until(ExpectedConditions.visibilityOfElementLocated(NavigationHeaderLabel));
        assertTrue(driver.findElement(NavigationHeaderLabel).isDisplayed());
        wait.until(ExpectedConditions.visibilityOfElementLocated(progressBarLabel));
        assertTrue(driver.findElement(progressBarLabel).isDisplayed());
        wait.until(ExpectedConditions.visibilityOfElementLocated(AccountInformationLabel));
        assertTrue(driver.findElement(AccountInformationLabel).isDisplayed());
    }

    public void enterText(By locator,String text){
        Actions actions = new Actions(driver);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        actions.moveToElement(element);
        actions.click();
        actions.sendKeys(text);
        actions.build().perform();
    }


    public RefinancePropertyDetailsPage enterAccountInfo(String firstName, String middleName, String lastName, String suffix, String phone, String modeOfCommunication) throws Exception {
        enterFirstName(firstName);
        enterMiddleName(middleName);
        enterLastName(lastName);
        enterSuffix(suffix);
        enterPhone(phone);
        selectPreferredMethodOfCommunication(modeOfCommunication);
        clickNext();
        return new RefinancePropertyDetailsPage(driver);
    }

    public PurchasePropertyDetailsPage enterPurchaseAccountInfo(String firstName, String middleName, String lastName, String suffix, String phone, String modeOfCommunication) throws Exception {
            enterFirstName(firstName);
            enterMiddleName(middleName);
            enterLastName(lastName);
            enterSuffix(suffix);
            enterPhone(phone);
            selectPreferredMethodOfCommunication(modeOfCommunication);
            clickNext();
            return new PurchasePropertyDetailsPage(driver);
        }
    }

