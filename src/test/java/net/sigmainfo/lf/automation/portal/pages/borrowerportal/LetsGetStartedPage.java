package net.sigmainfo.lf.automation.portal.pages.borrowerportal;

import net.sigmainfo.lf.automation.portal.pages.borrowerportal.profile.AccountInformationPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sigmainfo.lf.automation.common.AbstractTests;

public class LetsGetStartedPage extends AbstractTests {
	private Logger logger = LoggerFactory.getLogger(LetsGetStartedPage.class);

	   public LetsGetStartedPage(WebDriver driver){ this.driver = driver;}
	   public LetsGetStartedPage(){}
	   
	   public static By beginSectionButton = By.xpath("//button[contains(text(),'BEGIN SECTION')]");
	   
	   public AccountInformationPage clickBeginSectionButton() {
		   driver.findElement(beginSectionButton).click();
		   return new AccountInformationPage(driver);
	   }
	
}