package net.sigmainfo.lf.automation.portal.pages.borrowerportal.credit;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.declaration.DeclarationSectionCompletePage;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.summary.SummaryPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by shaishav.s on 20-09-2017.
 */
public class CreditCompletedPage extends AbstractTests {
    private Logger logger = LoggerFactory.getLogger(DeclarationSectionCompletePage.class);
    WebDriverWait wait = new WebDriverWait(driver,60);

    public CreditCompletedPage(WebDriver driver){
        this.driver = driver;
        logger.info("========= CreditCompletedPage is loaded===========");
    }
    public CreditCompletedPage(){}

    public static By BeginSectionButton = By.xpath("//ui-button[@id='goNextSection']/button");

    public SummaryPage clickBeginSectionBtn()
    {
        wait.until(ExpectedConditions.elementToBeClickable(BeginSectionButton));
        driver.findElement(BeginSectionButton).click();
        logger.info("Clicking on BEGIN SECTION button");
        return new SummaryPage(driver);
    }
}
