package net.sigmainfo.lf.automation.portal.pages.borrowerportal.summary;

import net.sigmainfo.lf.automation.common.AbstractTests;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by shaishav.s on 20-09-2017.
 */
public class AgreementTermsPage extends AbstractTests {
    private Logger logger = LoggerFactory.getLogger(AgreementTermsPage.class);
    WebDriverWait wait = new WebDriverWait(driver,60);

    AgreementTermsPage(){}

    //public static By CheckBoxesList = By.xpath("//input[@type='checkbox' and starts-with(@id,'C-')]");
    
    public static By CheckBox = By.xpath(".//*[@id='SS2']/questioner-question-set//input[@type='checkbox']");

    public AgreementTermsPage(WebDriver driver){
        logger.info("AgreementTermsPage is loaded");
        this.driver=driver;
    }

    public static By ConsentButton = By.xpath("//button[@name='submit']");

    public SummaryCompletedPage provideConsent() throws Exception {
        /*List<WebElement> checkboxele= driver.findElements(CheckBoxesList);
        for(int i=1;i<checkboxele.size();i++)
        {
            checkboxele.get(i).click();
            Thread.sleep(2500);
        }*/
    	 ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(ConsentButton));
         Thread.sleep(2000);
    	//wait.until(ExpectedConditions.elementToBeClickable(CheckBox));
        driver.findElement(CheckBox).click();
    	clickConsentButton();
        return new SummaryCompletedPage(driver);
    }

    public void clickConsentButton() throws Exception {
        wait.until(ExpectedConditions.elementToBeClickable(ConsentButton));
        driver.findElement(ConsentButton).click();
        logger.info("Clicking on I HAVE READ AND CONSENT button");
    }
}
