package net.sigmainfo.lf.automation.portal.pages.borrowerportal.profile;

import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import net.sigmainfo.lf.automation.common.AbstractTests;

/**
 * Created by           : Prakriti.c
 * Test class           : PurchasePropertyDetailsPage.java
 * Includes             : 1. Objects on PurchasePropertyDetailsPage
 *                        2. Methods implementation on PurchasePropertyDetailsPage
 */

public class CurrentResidenceDetailsPage extends AbstractTests {

    PortalFuncUtils portalFuncUtils;

	private Logger logger = LoggerFactory.getLogger(CurrentResidenceDetailsPage.class);
	
	public CurrentResidenceDetailsPage(WebDriver driver){
        this.driver = driver;
        logger.info("========= CurrentResidenceDetailsPage is loaded===========");

	}
    public CurrentResidenceDetailsPage(){}

    WebDriverWait wait = new WebDriverWait(driver,60);
    
    public static By SameResidenceAsSpouceButton = By.xpath("//ui-switch[@id='37']//label[normalize-space(.)='Yes']");
    public static By DifferentResidenceAsSpouceButton = By.xpath("//ui-switch[@id='37']//label[normalize-space(.)='No']");
    public static By OwnButton = By.xpath("//div[@id='SS10']/questioner-question-set/div[2]/div/div/questioner-question/ui-container/div/ui-single-toggle/div[1]/label");
    public static By RentButton = By.xpath("//div[@id='SS10']/questioner-question-set/div[2]/div/div/questioner-question/ui-container/div/ui-single-toggle/div[2]/label");
    public static By OtherButton = By.xpath("//div[@id='SS10']/questioner-question-set/div[2]/div/div/questioner-question/ui-container/div/ui-single-toggle/div[3]/label");
    public static By StartLivingDateTextBox = By.xpath("//div[@id='SS10']/questioner-question-set/div[4]/div/div/questioner-question/ui-container/div/ui-input/div/div[1]/input");
    public static By CurrentAddressTextBox = By.xpath("//div[@id='SS10']/questioner-question-set/div[3]/div/div/questioner-question/ui-container/div/ui-business-address/div/div[1]/ui-container/div/ui-input/div/div[1]/input");
    public static By SameAsMailingAddressCheckbox = By.id("C-41");
    public static By PlanningToSaleButton = By.xpath("//ui-switch[@id='44']//label[normalize-space(.)='Yes']");
    public static By NotPlanningToSaleButton = By.xpath("//ui-switch[@id='44']//label[normalize-space(.)='No']");
    public static By NextButton = By.xpath("//button[contains(text(),'Next')]");

    public void enterText(By locator,String text){
        Actions actions = new Actions(driver);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        actions.moveToElement(element);
        actions.click();
        actions.sendKeys(text);
        actions.build().perform();
        logger.info("Entering :"+text);
    }

    public void clickNext() throws Exception {
        wait.until(ExpectedConditions.elementToBeClickable(NextButton));
        driver.findElement(NextButton).click();
        logger.info("Clicking on NEXT button");
    }

    public void selectBoolean(By locator,boolean value)
    {
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("arguments[0].click()", element);
        logger.info("Selected boolean :"+value);
    }

    public void selectButton(By locator,String value){
        wait.until(ExpectedConditions.elementToBeClickable(locator));
        driver.findElement(locator).click();
        logger.info("Clicking on "+ value +" button");
    }


    public RealEstatePage enterCurrentResidenceDetails(boolean sameResidence, String residenceStatus, String currentAddress, String stayingSince, boolean planningToSale) throws Exception {
        if(portalParam.addSpouce) {
            selectResidenceWithSpouce(sameResidence);
        }
            selectResidenceStatus(residenceStatus);
            selectCurrentResidenceAddress(currentAddress);
            Thread.sleep(3000);
            enterStayingSince(stayingSince);
            Thread.sleep(3000);
            enterIfPlanningToSale(planningToSale);
            portalFuncUtils.scrollUntil(NextButton,driver);
            clickNext();
            return new RealEstatePage(driver);
        }

    private void scrollDownThePage(WebDriver driver) {
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("window.scrollBy(0,250)", "");
    }

    private void enterIfPlanningToSale(boolean planningToSale) {
        if(planningToSale)
        {
            selectButton(PlanningToSaleButton,"Planning to sale");
        }
        else
        {
            selectButton(NotPlanningToSaleButton,"Not planning to sale");
        }
    }

    private void enterStayingSince(String stayingSince) {
        enterText(StartLivingDateTextBox,stayingSince);
    }

    private void selectCurrentResidenceAddress(String currentAddress) {
        enterText(CurrentAddressTextBox,currentAddress);
        selectOptionWithText(currentAddress);
    }

    private void selectOptionWithText(String currentAddress) {
        driver.findElement(By.xpath("//div[@class='pac-item']//span[contains(text(),'"+currentAddress+"')]")).click();
        logger.info("Selected address :" + currentAddress);
    }

    private void selectResidenceStatus(String residenceStatus) {
        if (residenceStatus.equalsIgnoreCase("Own")) {
            selectButton(OwnButton, "Own");
        }
        else if(residenceStatus.equalsIgnoreCase("Rent"))
        {
            selectButton(RentButton, "Rent");
        }
        else if(residenceStatus.equalsIgnoreCase("Other"))
        {
            selectButton(OtherButton, "Other");
        }

    }

    public void selectResidenceWithSpouce(boolean sameResidence){
        if(sameResidence) {
            selectButton(SameResidenceAsSpouceButton, "Residence same as spouce");
        }
        else
        {
            selectButton(DifferentResidenceAsSpouceButton,"Residence not same as spouce");
        }
    }


}
