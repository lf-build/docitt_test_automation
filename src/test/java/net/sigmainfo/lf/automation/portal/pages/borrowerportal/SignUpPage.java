package net.sigmainfo.lf.automation.portal.pages.borrowerportal;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;
import net.sigmainfo.lf.automation.portal.function.StringEncrypter;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.income.IncomeSearchPage;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.NoSuchElementException;

import static java.lang.Thread.sleep;
import static org.testng.Assert.assertEquals;

/**
 * Created by           : Shaishav.s on 10-04-2017.
 * Test class           : SignUpPage.java
 * Includes             : 1. Objects on SignUpPage
 *                        2. Methods implementation on SignUpPage
 */
@Component
public class SignUpPage extends AbstractTests {

    /*@Autowired
    PortalFuncUtils portalFuncUtils;
*/
    private Logger logger = LoggerFactory.getLogger(SignUpPage.class);
    public SignUpPage(WebDriver driver){
        this.driver = driver;
        logger.info("========= SIGN UP page is loaded===========");
    }
    public SignUpPage(){}

    public static By emailTextBox = By.name("email");
    public static By passwordTextBox = By.name("password");
    public static By confirmpasswordTextBox = By.name("confirmPassword");
    public static By signupButton = By.id("acceptButton");
    public static By termsofserviceLink = By.linkText("Terms of Service.");
    public static By errorMsgTxt = By.xpath("");
    public static By loginButton = By.xpath(".//*[@id='micro-app-host']/account-creation-sign-up/div[2]/a");

    public String getPageTitle() {
        String title = driver.getTitle();
        return title;
    }

    public boolean verifyPageTitle() {
        String pageTitle = "";
        return getPageTitle().contains(pageTitle);
    }

    public void enterEmail(String username)  {
        logger.info("Entering Email");
            Actions actions = new Actions(driver);
            WebElement emailTxtBox = driver.findElement(emailTextBox);
            /*if((emailTxtBox.isDisplayed()) && (emailTxtBox.isEnabled())) {
                emailTxtBox.click();
                emailTxtBox.sendKeys(username);
            }*/
            actions.moveToElement(emailTxtBox);
            actions.click();
            actions.sendKeys(username);
            actions.build().perform();
        }


    public void enterPassword(String password) throws StringEncrypter.EncryptionException {
        logger.info("Entering Password");
        Actions actions = new Actions(driver);
        WebElement passwordTxtBox = driver.findElement(passwordTextBox);
        actions.moveToElement(passwordTxtBox);
        actions.click();
        actions.sendKeys(StringEncrypter.createNewEncrypter().decrypt(password));
        actions.build().perform();
        }

    public void enterConfirmPassword(String password) throws StringEncrypter.EncryptionException {
        logger.info("Entering Confirm Password");
        Actions actions = new Actions(driver);
        WebElement confirmpasswordTxtBox = driver.findElement(confirmpasswordTextBox);
        actions.moveToElement(confirmpasswordTxtBox);
        actions.click();
        actions.sendKeys(StringEncrypter.createNewEncrypter().decrypt(password));
        actions.build().perform();
    }
    public VerifyYourAccountPage clickSignUpAndCheckEmail() {
        logger.info("Clicking SIGN UP button");
        WebElement signupBtn = driver.findElement(signupButton);
        if((signupBtn.isDisplayed()) || (signupBtn.isEnabled()))
            signupBtn.click();
        else
            new Exception("SIGN UP button is not found");
        return new VerifyYourAccountPage(driver);
    }
    
    public VerifyYourAccountPage clickSignUp() {
        logger.info("Clicking SIGN UP button");
        WebElement signupBtn = driver.findElement(signupButton);
        if((signupBtn.isDisplayed()) || (signupBtn.isEnabled()))
            signupBtn.click();
        else
            new Exception("SIGN UP button is not found");
        return new VerifyYourAccountPage(driver);
    }
    
    public VerifyMobileNumberPage clickSignUpAndEnterMobile() {
        logger.info("Clicking SIGN UP button");
        WebElement signupBtn = driver.findElement(signupButton);
        if((signupBtn.isDisplayed()) || (signupBtn.isEnabled()))
            signupBtn.click();
        else
            new Exception("SIGN UP button is not found");
        return new VerifyMobileNumberPage(driver);
    }
    
    public WelcomePage clickSignUpAndEnterDocittHome() {
        WebDriverWait wait = new WebDriverWait(driver,60);
        logger.info("Clicking SIGN UP button");
        wait.until(ExpectedConditions.presenceOfElementLocated(signupButton));
        wait.until(ExpectedConditions.elementToBeClickable(signupButton));
        WebElement signupBtn = driver.findElement(signupButton);
        if((!signupBtn.isDisplayed()) || (signupBtn.isEnabled()))
            signupBtn.click();
        else
            new Exception("SIGN UP button is not found");
        return new WelcomePage(driver);
    }

    public void clickLogin() throws NoSuchElementException{
        logger.info("Clicking LOGIN button");
        WebElement loginBtn = driver.findElement(loginButton);
        if(loginBtn.isDisplayed())
            loginBtn.click();
    }
    public void clickTermsofserviceLink(){
        logger.info("Clicking Terms of Service link");
        WebElement termsofserviceLnk = driver.findElement(termsofserviceLink);
        if(termsofserviceLnk.isDisplayed())
            termsofserviceLnk.click();
    }
    public String getErrorMessage(){
        logger.info("Reading error message");
        String strErrorMsg = null;
        WebElement errorMsg = driver.findElement(errorMsgTxt);
        if(errorMsg.isDisplayed()&&errorMsg.isEnabled())
            strErrorMsg = errorMsg.getText();
        return strErrorMsg;
    }

    public LoginPage clickOnSignInButton(WebDriver driver) throws Exception {
        driver.findElement(loginButton).click();
        logger.info("Clicking on Sign In button.");
        return (new LoginPage(driver));
    }

    public WelcomePage signUp(String username,String password) throws InterruptedException, StringEncrypter.EncryptionException {
        WebDriverWait wait = new WebDriverWait(driver,60);
        String mainTab=null;
        wait.until(ExpectedConditions.elementToBeClickable(By.name("email")));
        enterEmail(username);
        enterPassword(password);
        enterConfirmPassword(password);

        if(portalParam.isEmailVerification && portalParam.isMobileVerification)
        {
            logger.info("EmailVerification:"+portalParam.isEmailVerification +" MobileVerification:"+portalParam.isMobileVerification);
            VerifyYourAccountPage verifyYourAccountPage = clickSignUp();
            wait.until(ExpectedConditions.urlMatches("http://docitt."+ getOnlyStrings(System.getProperty("envParam"))+".lendfoundry.com:9005/#/signup-success"));
            assertEquals(driver.getCurrentUrl(),"http://docitt."+ getOnlyStrings(System.getProperty("envParam"))+".lendfoundry.com:9005/#/signup-success");
            mainTab = driver.getWindowHandle();
            VerifyMobileNumberPage verifyMobileNumberPage = readDocittEmail(driver,mainTab);
            wait.until(ExpectedConditions.visibilityOfElementLocated(verifyMobileNumberPage.mobileNumberTextBox));
            VerifyAccessCodePage verifyAccessCodePage = verifyMobileNumberPage.sendMobileNumber(driver,"9999999999");
            sleep(2000);
            //wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//class[contains(text(),'A 6-digit code has been sent to']")));
            wait.until(ExpectedConditions.visibilityOfElementLocated(verifyAccessCodePage.enterCodeInstruction));
            WelcomePage welcomePage = verifyAccessCodePage.sendVerificationCode(driver,"123456");
        }
        if(portalParam.isEmailVerification && !portalParam.isMobileVerification)
        {
            logger.info("EmailVerification:"+portalParam.isEmailVerification +" MobileVerification:"+portalParam.isMobileVerification);
            VerifyYourAccountPage verifyYourAccountPage = clickSignUpAndCheckEmail();
            wait.until(ExpectedConditions.urlMatches("http://docitt."+ getOnlyStrings(System.getProperty("envParam"))+".lendfoundry.com:9005/#/signup-success"));
            assertEquals(driver.getCurrentUrl(),"http://docitt."+ getOnlyStrings(System.getProperty("envParam"))+".lendfoundry.com:9005/#/signup-success");
            mainTab = driver.getWindowHandle();
            WelcomePage welcomePage = readDocittEmailAndLogin(driver,mainTab);
        }

        if(!portalParam.isEmailVerification && portalParam.isMobileVerification)
        {
            logger.info("EmailVerification:"+portalParam.isEmailVerification +" MobileVerification:"+portalParam.isMobileVerification);
            VerifyMobileNumberPage verifyMobileNumberPage = clickSignUpAndEnterMobile();
            wait.until(ExpectedConditions.urlContains("http://docitt."+ getOnlyStrings(System.getProperty("envParam"))+".lendfoundry.com:9005/#/accountVerification/refid"));
            wait.until(ExpectedConditions.elementToBeClickable(verifyMobileNumberPage.sendCodeButton));
            VerifyAccessCodePage verifyAccessCodePage = verifyMobileNumberPage.sendMobileNumber(driver,"9999999999");
            sleep(2000);
            wait.until(ExpectedConditions.visibilityOfElementLocated(verifyAccessCodePage.accessMyPortalButton));
            WelcomePage welcomePage = verifyAccessCodePage.sendVerificationCode(driver,"123456");

        }
        if(!portalParam.isEmailVerification && !portalParam.isMobileVerification)
        {
            logger.info("EmailVerification:"+portalParam.isEmailVerification +" MobileVerification:"+portalParam.isMobileVerification);
            WelcomePage welcomePage = clickSignUpAndEnterDocittHome();
        }
        wait.until(ExpectedConditions.urlContains("http://docitt."+ getOnlyStrings(System.getProperty("envParam"))+".lendfoundry.com:9005/#/home"));
        assertEquals(driver.getCurrentUrl(), "http://docitt."+ getOnlyStrings(System.getProperty("envParam"))+".lendfoundry.com:9005/#/home");
        logger.info("Home page is displayed");
        return new WelcomePage(driver);
    }
}
