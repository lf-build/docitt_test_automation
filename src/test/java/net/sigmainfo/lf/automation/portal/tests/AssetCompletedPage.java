package net.sigmainfo.lf.automation.portal.tests;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.assets.AssetsPage;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.income.IncomeSearchPage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by shaishav.s on 06-09-2017.
 */
public class AssetCompletedPage extends AbstractTests {
    private Logger logger = LoggerFactory.getLogger(AssetCompletedPage.class);
    
    WebDriverWait wait = new WebDriverWait(driver,60);

    public AssetCompletedPage(WebDriver driver){
        this.driver = driver;
        logger.info("========= AssetCompletedPage is loaded===========");
    }
    public AssetCompletedPage(){}

    public static By BeginSectionButton = By.xpath("//ui-button[@id='goNextSection']/button");
    
    public IncomeSearchPage clickBeginSectionBtn()
    {
    	wait.until(ExpectedConditions.elementToBeClickable(BeginSectionButton));
        driver.findElement(BeginSectionButton).click();
        logger.info("Clicking on BEGIN SECTION button");
        return new IncomeSearchPage(driver);
    }
}
