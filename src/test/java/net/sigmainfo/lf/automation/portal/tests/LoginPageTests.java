package net.sigmainfo.lf.automation.portal.tests;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.common.CaptureScreenshot;
import net.sigmainfo.lf.automation.common.TestResults;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.*;
import org.json.JSONException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Listeners;

import java.io.IOException;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

/**
 * Created by           : Shaishav.s on 20-02-2017.
 * Test class           : LoginPageTests.java
 * Includes             : Test cases on Borrower Login Page
 */
@Listeners(CaptureScreenshot.class)
public class LoginPageTests extends AbstractTests {

    private Logger logger = LoggerFactory.getLogger(LoginPageTests.class);

    public static String funcMod="Docitt_Portal";
    // public WebDriver driver;

    @Autowired
    TestResults testResults;

    @Autowired
    LoginPage loginPage;

    @Autowired
    WelcomePage welcomePage;

    @Autowired
    SignUpPage signUpPage;

    @Autowired
    PasswordChangedPage passwordChangedPage;

    public LoginPageTests() {}

    @AfterClass(alwaysRun=true)
    public void endCasereport() throws IOException, JSONException {

        String funcModule = "Docitt_Portal";
        String className = org.apache.commons.lang.StringUtils.substringAfterLast(this.getClass().getName(), ".");
        String description = "Verify  "+ org.apache.commons.lang.StringUtils.substringAfterLast(this.getClass().getName(), ".");
        generateReport(className, description,funcModule);
    }

    /**
     * Description          : verifySuccessfulSignIn
     * Includes             : 1. Borrower signs up
     *                        2. Verify that he is able to sign up on portal successfully
     *                        3. After signup, he is able to login successfully
     */

    @Test(priority=1,description = "", groups = {"PortalTests","LoginPageTests","verifySuccessfulSignIn"})
    public void SuccessfulSignIn() throws Exception {
        String sTestID = "verifySuccessfulSignIn";
        String result = "Failed";
        WebDriverWait wait = new WebDriverWait(driver,30);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            signUpPage = new SignUpPage(driver);
            wait.until(ExpectedConditions.elementToBeClickable(signUpPage.loginButton));
            LoginPage loginPage= signUpPage.clickOnSignInButton(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(loginPage.signInButton));
            loginPage.enterEmail(portalParam.getUsername());
            loginPage.enterPassword(portalParam.getPassword());
            WelcomePage welcomePage = loginPage.clickLogIn();
            //wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//button[contains(text(),'GET STARTED')]")));
            WebElement getStartedBtn = driver.findElement(welcomePage.beginHereButton);
            if(getStartedBtn.isDisplayed())
            {
                logger.info("BEGIN HERE button is displayed");
                assertTrue(getStartedBtn.isDisplayed());
            }
            else
            {
                logger.info("BEGIN HERE button is not displayed");
                assertFalse(getStartedBtn.isDisplayed());
            }
            assertEquals(driver.getCurrentUrl().contains("http://docitt."+ getOnlyStrings(System.getProperty("envParam"))+".lendfoundry.com:9005/#/home"),true);
            result="Passed";
        }
        catch(Exception e) {
            e.printStackTrace();
            logger.info("******************" + sTestID +  "  failed. *****\n" +e);
        }

        finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
            if(result.equalsIgnoreCase("Failed"))
                Assert.fail();
        }
    }

    /**
     * Description          : verifyIncorrectUsername
     * Includes             : 1. Borrower signs up with incorrect username
     *                        2. Verify that appropriate error message is shown
     */

    @Test(priority=2,description = "", groups = {"PortalTests","LoginPageTests","verifyIncorrectUsername"})
    public void IncorrectUserNameTest() throws Exception {
        String sTestID = "verifyIncorrectUsername";
        String result = "Failed";
        String errorMessage = "Email address is not valid.";
        WebDriverWait wait = new WebDriverWait(driver,30);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            signUpPage = new SignUpPage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(signUpPage.loginButton));
            LoginPage loginPage= signUpPage.clickOnSignInButton(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(loginPage.signInButton));
            loginPage.enterEmail(portalParam.getUsername());
            loginPage.enterPassword(portalParam.getPassword());
            WelcomePage welcomePage = loginPage.clickLogIn();
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(),'"+errorMessage+"')]")));
            WebElement errorMsg = driver.findElement(By.xpath("//div[contains(text(),'"+errorMessage+"')]"));
            if(errorMsg.isDisplayed())
            {
                logger.info("Error message \""+ errorMessage + "\"displayed as expected.");
                assertTrue(errorMsg.isDisplayed());
            }
            else
            {
                logger.info("Error message \""+ errorMessage + "\"displayed as expected.");
                assertFalse(errorMsg.isDisplayed());
            }
            assertEquals(driver.getCurrentUrl().contains("http://docitt."+ getOnlyStrings(System.getProperty("envParam"))+".lendfoundry.com:9005/#/login"),true);
            result="Passed";
        }
        catch(Exception e) {
            e.printStackTrace();
            logger.info("******************" + sTestID +  "  failed. *****\n" +e);
        }

        finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
            if(result.equalsIgnoreCase("Failed"))
                Assert.fail();
        }
    }

    /**
     * Description          : verifyIncorrectPassword
     * Includes             : 1. Borrower signs up with incorrect password
     *                        2. Verify that appropriate error message is shown
     */

    @Test(priority=3,description = "", groups = {"PortalTests","LoginPageTests","verifyIncorrectPassword"})
    public void IncorrectPasswordTest() throws Exception {
        String sTestID = "verifyIncorrectPassword";
        String result = "Failed";
        String errorMessage = "Invalid Username or Password";
        WebDriverWait wait = new WebDriverWait(driver,30);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            signUpPage = new SignUpPage(driver);
            wait.until(ExpectedConditions.elementToBeClickable(signUpPage.loginButton));
            LoginPage loginPage= signUpPage.clickOnSignInButton(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(loginPage.signInButton));
            loginPage.enterEmail(portalParam.getUsername());
            loginPage.enterPassword(portalParam.getPassword());
            WelcomePage welcomePage = loginPage.clickLogIn();
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//li[contains(text(),'"+ errorMessage +"')]")));
            WebElement errorMsg = driver.findElement(By.xpath("//li[contains(text(),'"+errorMessage+"')]"));
            if(errorMsg.isDisplayed())
            {
                logger.info("Error message \""+ errorMessage + "\"displayed as expected.");
                assertTrue(errorMsg.isDisplayed());
            }
            else
            {
                logger.info("Error message \""+ errorMessage + "\"displayed as expected.");
                assertFalse(errorMsg.isDisplayed());
            }
            assertEquals(driver.getCurrentUrl().contains("http://docitt."+ getOnlyStrings(System.getProperty("envParam"))+".lendfoundry.com:9005/#/login"),true);
            result="Passed";
        }
        catch(Exception e) {
            e.printStackTrace();
            logger.info("******************" + sTestID +  "  failed. *****\n" +e);
        }

        finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
            if(result.equalsIgnoreCase("Failed"))
                Assert.fail();
        }
    }

    /**
     * Description          : verifyForgetPasswordFlow
     * Includes             : 1. Borrower tries to reset the password
     *                        2. User resets the password
     *                        3. He is able to login successfully with new password
     */

    @Test(priority=4,description = "", groups = {"PortalTests","LoginPageTests","verifyForgetPasswordFlow"})
    public void ForgetPasswordTest() throws Exception {
        String sTestID = "verifyForgetPasswordFlow";
        String result = "Failed";
        String errorMessage = "Not Found";
        WebDriverWait wait = new WebDriverWait(driver,30);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            signUpPage = new SignUpPage(driver);
            wait.until(ExpectedConditions.elementToBeClickable(signUpPage.loginButton));
            LoginPage loginPage= signUpPage.clickOnSignInButton(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(loginPage.forgetPasswordLink));
            ForgotPasswordPage forgotPasswordPage = loginPage.clickOnForgetPasswordLink();
            //wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='input']")));
            forgotPasswordPage.enterEmail(driver,portalParam.getUsername());
            CheckYourEmailPage checkYourEmailPage = forgotPasswordPage.clickSendEmail();
            assertEquals(driver.getCurrentUrl(),"http://docitt."+ getOnlyStrings(System.getProperty("envParam"))+".lendfoundry.com:9005/#/password-instruction");
            String mainTab = driver.getWindowHandle();
            ResetPasswordPage resetPasswordPage = readResetPasswordEmail(driver,mainTab);
            resetPasswordPage.enterEmail(driver,portalParam.getUsername());
            resetPasswordPage.enterNewPassword(driver,portalParam.getPassword());
            resetPasswordPage.confirmNewPassword(driver,portalParam.getPassword());
            PasswordChangedPage passwordChangedPage = resetPasswordPage.clickResetButton(driver);
            LoginPage loginPage1 = passwordChangedPage.clickOnAccessMyPortal(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(loginPage.forgetPasswordLink));
            loginPage1.enterEmail(portalParam.getUsername());
            loginPage1.enterPassword(portalParam.getPassword());
            WelcomePage welcomePage = loginPage1.clickLogIn();
            WebElement getStartedBtn = driver.findElement(welcomePage.beginHereButton);
            if(getStartedBtn.isDisplayed())
            {
                logger.info("BEGIN HERE button is displayed");
                assertTrue(getStartedBtn.isDisplayed());
            }
            else
            {
                logger.info("BEGIN HERE button is not displayed");
                assertFalse(getStartedBtn.isDisplayed());
            }
            assertEquals(driver.getCurrentUrl().contains("http://docitt."+ getOnlyStrings(System.getProperty("envParam"))+".lendfoundry.com:9005/#/home"),true);
            deleteGmailEmails(driver,mainTab);
            result="Passed";
        }
        catch(Exception e) {
            e.printStackTrace();
            logger.info("******************" + sTestID +  "  failed. *****\n" +e);
        }

        finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
            if(result.equalsIgnoreCase("Failed"))
                Assert.fail();
        }
    }

}
