package net.sigmainfo.lf.automation.portal.pages.borrowerportal.profile;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.summary.SummaryPage;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import java.util.List;

/**
 * Created by shaishav.s on 06-09-2017.
 */
public class RealEstatePage extends AbstractTests {
    private Logger logger = LoggerFactory.getLogger(RealEstatePage.class);

    WebDriverWait wait = new WebDriverWait(driver,60);
    WebElement PropertyTypeListItem;

    public RealEstatePage(){}
    public RealEstatePage(WebDriver driver){
        this.driver = driver;
        logger.info("========= RealEstatePage is loaded===========");
    }



    public static By OwnAdditionalPropertyButton = By.xpath("//ui-switch[@id='69']//label[normalize-space(.)='Yes']");
    public static By PropertyAddressTextBox = By.xpath("//div[@id='SS12']/questioner-question-set/div[4]/div/div/questioner-question/ui-container/div/ui-business-address/div/div[1]/ui-container/div/ui-input/div/div[1]/input");
    public static By PropertyValueTextbox = By.xpath("//div[@id='SS12']/questioner-question-set/div[6]/div/div/questioner-question/ui-container/div/ui-input/div/div[1]/input");
    public static By RentalIncomeTextbox = By.xpath("//div[@id='SS12']/questioner-question-set/div[7]/div/div/questioner-question/ui-container/div/ui-input/div/div[1]/input");
    public static By AdditionalExpenseTextbox = By.xpath("//div[@id='SS12']/questioner-question-set/div[8]/div[2]/div/questioner-question/ui-container/div/ui-input/div/div[1]/input");
    public static By NetRentalIncomeLabel = By.xpath("//alloy-format[@class='style-scope ui-calculator']");
    public static By PropertyTypeDropDown = By.xpath("//div[@id='SS12']/questioner-question-set/div[5]/div/div[1]/questioner-question/ui-container/div/ui-options/div[1]/div/input");
    public static By propertyList = By.cssSelector("ui-options[name='dobMonth'] div[class*='select-wrapper'] li");
    public static By SingleFamilyPropertyType = By.xpath("//span[contains(text(),'Single Family']");
    public static By CondoPropertyType = By.xpath("//platform-shell/div/borrower-private-layout/div/micro-app/div/questioner-sections/div[1]/div[3]/div/div/ui-form/right-content-area/div/div/sub-sections/div[11]/questioner-question-set/div[5]/div/div[1]/questioner-question/ui-container/div/ui-options/div[1]/div/ul/li[3]/span");
    public static By UnitsPropertyType = By.xpath("//span[contains(text(),'2-4 Units']");
    public static By MixedUsePropertyType = By.xpath("//span[contains(text(),'Other']");
    public static By OtherPropertyType = By.xpath("//span[contains(text(),'Condo']");
    public static By PropertyStatusDropDown = By.xpath("//div[@id='SS12']/questioner-question-set/div[5]/div/div[2]/questioner-question/ui-container/div/ui-options/div[1]/div/input");
    public static By SoldPropertyStatus = By.xpath("//platform-shell/div/borrower-private-layout/div/micro-app/div/questioner-sections/div[1]/div[3]/div/div/ui-form/right-content-area/div/div/sub-sections/div[11]/questioner-question-set/div[5]/div/div[2]/questioner-question/ui-container/div/ui-options/div[1]/div/ul/li[3]/span");
    public static By RetainedPropertyStatus = By.xpath("//platform-shell/div/borrower-private-layout/div/micro-app/div/questioner-sections/div[1]/div[3]/div/div/ui-form/right-content-area/div/div/sub-sections/div[11]/questioner-question-set/div[5]/div/div[2]/questioner-question/ui-container/div/ui-options/div[1]/div/ul/li[2]/span");
    public static By PendingSalePropertyStatus = By.xpath("//platform-shell/div/borrower-private-layout/div/micro-app/div/questioner-sections/div[1]/div[3]/div/div/ui-form/right-content-area/div/div/sub-sections/div[11]/questioner-question-set/div[5]/div/div[2]/questioner-question/ui-container/div/ui-options/div[1]/div/ul/li[4]/span");
    public static By SubmitButton = By.xpath("//button[contains(text(),'Submit')]");

    public ProfileCompletedPage enterRealEstateDetails(boolean additionalProperty, String propertyAddress, String propertyType, String propertyStatus, String propertyValue, String rentalIncome, String expenses) throws Exception {
        int netRentalIncome = (Integer.parseInt(portalParam.rentalIncome)-Integer.parseInt(portalParam.expenses));
        selectAdditionalProperty(additionalProperty);
        enterPropertyAddress(propertyAddress);
        scrollDown(driver);
        Thread.sleep(2000);
        selectPropertyType(PropertyTypeDropDown,propertyType);
        selectPropertyStatus(PropertyStatusDropDown,propertyStatus);
        enterPropertyValue(PropertyValueTextbox,propertyValue);
        enterMonthlyRentalIncome(RentalIncomeTextbox,rentalIncome);
        enterAdditionalExpense(AdditionalExpenseTextbox,expenses);
        wait.until(ExpectedConditions.visibilityOfElementLocated(NetRentalIncomeLabel)).getText().contains(String.valueOf(netRentalIncome));
//        Assert.assertEquals("$"+netRentalIncome,Integer.parseInt(driver.findElement(NetRentalIncomeLabel).getText().replaceAll("$", "")),"Net rental income does not match");
        clickSubmit();
        return new ProfileCompletedPage(driver);
    }

    private void selectPropertyStatus(By propertyStatusDropDown, String propertyStatus) throws InterruptedException {
        driver.findElement(propertyStatusDropDown).click();
        Thread.sleep(2000);
        List<WebElement> propertyTypeList= driver.findElements(propertyList);

        for(WebElement s:propertyTypeList){
            String s2=s.getText();
            if(s2.equals(propertyStatus)){
                s.click();
                break;
            }
        }
    }

    private void selectPropertyType(By propertyTypeDropDown, String propertyType) throws InterruptedException {
        driver.findElement(propertyTypeDropDown).click();
        Thread.sleep(2000);
        List<WebElement> propertyTypeList= driver.findElements(propertyList);

        for(WebElement s:propertyTypeList){
            String s2=s.getText();
            if(s2.equals(propertyType)){
                s.click();
                break;
            }
        }

    }

    /*private void selectPropertyStatus(By propertyStatusDropDown, String propertyStatus) throws InterruptedException {
        driver.findElement(propertyStatusDropDown).click();
        Thread.sleep(2000);
        for(int i=3;i<=7;i++){
            WebElement companyType = driver.findElement(By.cssSelector("ui-options[name='dobMonth'] div[class*='select-wrapper'] li:nth-child("+i+")"));
            String text = companyType.getText();

            if(text.equalsIgnoreCase(propertyStatus)){
                companyType.click();
            }
        }
        logger.info("Selected :"+propertyStatus);
    }*/

    private void scrollDown(WebDriver driver) {
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("window.scrollBy(0,250)", "");
    }

    private void enterAdditionalExpense(By additionalExpenseTextbox, String expenses) {
    	driver.findElement(additionalExpenseTextbox).sendKeys(expenses);
    	logger.info("Entering :"+expenses);
    }

    private void enterMonthlyRentalIncome(By rentalIncomeTextbox, String rentalIncome) {
        driver.findElement(rentalIncomeTextbox).sendKeys(rentalIncome);
        logger.info("Entering :"+rentalIncome);
    }

    private void enterPropertyValue(By PropertyValueTextbox,String propertyValue) {
        enterText(PropertyValueTextbox,propertyValue);
    }


    public void selectFromDropdown(By locator,String propertyType) throws InterruptedException {
        /*Actions actions = new Actions(driver);
		driver.findElement(locator).click();
		Thread.sleep(3000);
        if(value.equalsIgnoreCase("Single Family"))
        {
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(SingleFamilyPropertyType)));
            PropertyTypeListItem = driver.findElement(SingleFamilyPropertyType);
        }
        else if(value.equalsIgnoreCase("Condo")){
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(CondoPropertyType)));
            PropertyTypeListItem = driver.findElement(CondoPropertyType);
        }
        else if(value.equalsIgnoreCase("2-4 Units")){
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(UnitsPropertyType)));
            PropertyTypeListItem = driver.findElement(UnitsPropertyType);
        }
        else if(value.equalsIgnoreCase("Mixed Use")){
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(MixedUsePropertyType)));
            PropertyTypeListItem = driver.findElement(MixedUsePropertyType);
        }
        else if(value.equalsIgnoreCase("Other")){
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(OtherPropertyType)));
            PropertyTypeListItem = driver.findElement(OtherPropertyType);
        }
        else if(value.equalsIgnoreCase("Sold")){
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(SoldPropertyStatus)));
            PropertyTypeListItem = driver.findElement(SoldPropertyStatus);
        }
        else if(value.equalsIgnoreCase("Retained")){
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(RetainedPropertyStatus)));
            PropertyTypeListItem = driver.findElement(RetainedPropertyStatus);
        }
        else if(value.equalsIgnoreCase("Pending Sale")){
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(PendingSalePropertyStatus)));
            PropertyTypeListItem = driver.findElement(PendingSalePropertyStatus);
        }
        actions.moveToElement(PropertyTypeListItem);
        actions.click();
        actions.build().perform();
        logger.info("Selected from dropdown :"+value);*/
        driver.findElement(locator).click();
        Thread.sleep(2000);
        for(int i=3;i<=7;i++){
            WebElement companyType = driver.findElement(By.cssSelector("ui-options[name='dobMonth'] div[class*='select-wrapper'] li:nth-child("+i+")"));
            String text = companyType.getText();

            if(text.contains(propertyType)){
                companyType.click();
            }
        }
        logger.info("Selected :"+propertyType);
    }

    private void enterPropertyAddress(String propertyAddress) {
        enterText(PropertyAddressTextBox,propertyAddress);
        selectOptionWithText(propertyAddress);
    }

    private void selectOptionWithText(String propertyAddress) {
            driver.findElement(By.xpath("//div[@class='pac-item']//span[contains(text(),'"+propertyAddress+"')]")).click();
            logger.info("Selected address :" + propertyAddress);
        }


    public void enterText(By locator,String text){
        Actions actions = new Actions(driver);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        actions.moveToElement(element);
        actions.click();
        actions.sendKeys(text);
        actions.build().perform();
        logger.info("Entering:"+text);
    }

    public void clickSubmit() throws Exception {
        wait.until(ExpectedConditions.elementToBeClickable(SubmitButton));
        driver.findElement(SubmitButton).click();
        logger.info("Clicking on SUBMIT button");
    }

    private void selectAdditionalProperty(boolean additionalProperty) {
        selectButton(OwnAdditionalPropertyButton,"Additional property");

    }

   public void selectButton(By locator,String value){
        wait.until(ExpectedConditions.elementToBeClickable(locator));
        driver.findElement(locator).click();
        logger.info("Clicking on "+ value +" button");
    }
}
