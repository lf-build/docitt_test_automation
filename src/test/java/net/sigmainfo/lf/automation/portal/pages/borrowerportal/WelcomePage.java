package net.sigmainfo.lf.automation.portal.pages.borrowerportal;

import net.sigmainfo.lf.automation.common.AbstractTests;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import static java.lang.Thread.sleep;

/**
 * Created by           : Shaishav.s on 08-02-2017.
 * Test class           : WelcomePage.java
 * Includes             : 1. Objects on WelcomePage
 *                        2. Methods implementation on WelcomePage
 */
@Component
public class WelcomePage extends AbstractTests {
    private Logger logger = LoggerFactory.getLogger(WelcomePage.class);

   public WelcomePage(WebDriver driver){ this.driver = driver;}
    public WelcomePage(){}

    public static By beginHereButton = By.xpath("//button[contains(text(),'BEGIN HERE')]");
    public static By purchaseButton = By.xpath("//span[contains(text(),'Purchase')]");
    public static By refinanceButton = By.xpath("//span[contains(text(),'Refinance')]");
    
    public WelcomeVideoPage clickPurposeButton(String purposeText) {
    	if(purposeText.equalsIgnoreCase("Purchase"))
    	{
    		driver.findElement(purchaseButton).click();
    	}
    	else if(purposeText.equalsIgnoreCase("Refinance"))
    	{
    		driver.findElement(refinanceButton).click();
    	}
    	return new WelcomeVideoPage(driver);
    }
    
}
