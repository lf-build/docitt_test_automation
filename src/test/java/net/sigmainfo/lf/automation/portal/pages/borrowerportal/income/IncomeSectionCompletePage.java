package net.sigmainfo.lf.automation.portal.pages.borrowerportal.income;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.assets.AssetsPage;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.declaration.DeclarationPage;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.profile.ProfileCompletedPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by shaishav.s on 07-09-2017.
 */
public class IncomeSectionCompletePage extends AbstractTests {
    private Logger logger = LoggerFactory.getLogger(IncomeSectionCompletePage.class);
    WebDriverWait wait = new WebDriverWait(driver,60);

    public IncomeSectionCompletePage(WebDriver driver){
        this.driver = driver;
        logger.info("========= IncomeSectionCompletePage is loaded===========");
    }
    public IncomeSectionCompletePage(){}

    public static By BeginSectionButton = By.xpath("//ui-button[@id='goNextSection']/button");

    public DeclarationPage clickBeginSectionBtn()
    {
        wait.until(ExpectedConditions.elementToBeClickable(BeginSectionButton));
        driver.findElement(BeginSectionButton).click();
        logger.info("Clicking on BEGIN SECTION button");
        return new DeclarationPage(driver);
    }
}
