package net.sigmainfo.lf.automation.portal.pages.borrowerportal;

import net.sigmainfo.lf.automation.common.AbstractTests;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Created by           : Shaishav.s on 27-02-2017.
 * Test class           : CheckYourEmailPage.java
 * Includes             : 1. Objects on CheckYourEmailPage
 *                        2. Methods implementation on CheckYourEmailPage
 */
@Component
public class CheckYourEmailPage extends AbstractTests{
    private Logger logger = LoggerFactory.getLogger(CheckYourEmailPage.class);
    public CheckYourEmailPage(WebDriver driver) {
        this.driver = driver;
    }
    public CheckYourEmailPage(){}

}
