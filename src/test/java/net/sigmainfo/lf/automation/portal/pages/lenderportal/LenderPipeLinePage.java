package net.sigmainfo.lf.automation.portal.pages.lenderportal;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.constant.PortalParam;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by shaishav.s on 03-11-2017.
 */
public class LenderPipeLinePage extends AbstractTests {

    private Logger logger = LoggerFactory.getLogger(LenderPipeLinePage.class);
    WebDriverWait wait = new WebDriverWait(driver,60);
    static String tomorrowDate = new SimpleDateFormat("yyyy-MM-dd").format((new Date()).getTime() + 86400000);

    public LenderPipeLinePage(WebDriver driver) {

        this.driver = driver;
        logger.info("=========== LenderPipeLinePage is loaded============");
    }
    public LenderPipeLinePage(){}

    public static By inviteBtn=By.cssSelector("lender-pipeline-invite[class*='lender-pipeline-list-options']>a");
    public static By firstNameTextbox = By.xpath(".//ui-input[@name='firstName']//input[@id='input']");
    public static By lastNameTextbox = By.xpath(".//ui-input[@name='lastName']//input[@id='input']");
    public static By borrowerBtn=By.xpath(".//*[@id='uiContainerGroup']/ul/div[2]/div[1]/button");
    public static By affinityPartnerBtn=By.xpath(".//*[@id='uiContainerGroup']/ul/div[2]/div[2]/button");
    public static By emailTextbox=By.xpath(".//ui-input[@name='email']//input[@id='input']");
    public static By phoneTextbox=By.xpath(".//ui-input[@name='phone']//input[@id='input']");
    public static By checkBox=By.xpath(".//*[@name='checkbox']//input[@id='checkbox']");
    public static By sendInviteBtn=By.xpath(".//*[@id='sendinvite']");
    public static By profileDropdown = By.xpath("//div[@id='micro-app-host']/header-view/nav/profile-summary/lender-context-menu-list/context-menu-list/div/a/i");
    public static By logoutLink = By.xpath("//lender-context-menu-list[@class='style-scope profile-summary']//a[contains(text(),'Log out')]");

    public static By calendarTab = By.xpath("//span[contains(text(),'Calendar')]");
    public static By tomorrowDateCell = By.xpath("//td[@id='"+tomorrowDate+"']");
    public static By summaryTab = By.xpath("//span[contains(text(),'Summary')]");
    public static By documentsTab = By.xpath("//span[contains(text(),'Documents')]");
    public static By conditionsTab = By.xpath("//span[contains(text(),'Conditions')]");
    public static By communicationTab = By.xpath("//span[contains(text(),'Communication')]");
    public static By activityHistoryTab = By.xpath("//span[contains(text(),'Activity History')]");
    public static By reviewStatus = By.xpath("//a[@href='javascipt:void(0);']/div[@role='progressbar']/span[contains(text(),'Review')]");
    public static By loanIdLabel = By.xpath("//span[contains(text(),'Loan #:')]");

    public static By titleTextbox=By.xpath("//input[@placeholder='Title']");
    public static By locationTextbox=By.xpath("//input[@placeholder='Location']");
    public static By startTimeTextbox=By.xpath(".//*[@id='start-input']/div/div[1]//input");
    public static By endTimeTextbox=By.xpath(".//*[@id='end-input']/div/div[1]//input");
    public static By descriptionTextbox = By.xpath("//textarea[@placeholder='Description']");
    public static By inviteBorrowerName = By.xpath("//ul[@class='style-scope users-list']//span[contains(text(),'Borrower')]");
    public static By inviteUserTextbox = By.xpath("//input[@placeholder='Type name or email']");
    public static By startTimeSelect=By.xpath(".//*[@id='start']/div[1]/div");
    public static By endTimeSelect=By.xpath(".//*[@id='end']/div[1]/div");
    public static By startTimeValues=By.cssSelector("ui-options[id='start'] ul[id*='select-options-'] li:nth-child(3)");
    public static By endTimeValues=By.cssSelector("ui-options[id='end'] ul[id*='select-options-'] li:nth-child(3)");
    public static By triggerEventButton = By.xpath("//button[contains(text(),'SAVE')]");
    public static By description=By.xpath(".//*[@id='uiContainerGroup']//textarea");
    public static By priority=By.xpath(".//*[@id='calendarControl']//ui-input-checkbox/div/input");
    public static By userListAddBtn=By.xpath(".//*[@id='userLists']/div/div[1]/span");
    public static By addNewConditionButton = By.xpath("//span[contains(text(),'Add New Condition')]");
    public static By conditionsLabel = By.xpath("//h4[contains(text(),'Condition(s)')]");
    public static By recipientListDropdown = By.xpath("//ui-container[@id='recipientList']//input[@id='input']");
    public static By createCustomConditionLabel = By.xpath("//a[contains(text(),'Create Custom Condition')]");
    public static By conditionLabel = By.xpath("//div/h4[contains(text(),'Conditions')]");
    public static By legalCategory = By.xpath("//ui-container-group[@id='createCustomCondition']/ui-container/div/ui-single-toggle/div[1]/label");
    public static By personalCategory = By.xpath("//ui-container-group[@id='createCustomCondition']/ui-container/div/ui-single-toggle/div[2]/label");
    public static By propertyCategory = By.xpath("//ui-container-group[@id='createCustomCondition']/ui-container/div/ui-single-toggle/div[3]/label");
    public static By qualCategory = By.xpath("//ui-container-group[@id='createCustomCondition']/ui-container/div/ui-single-toggle/div[4]/label");
    public static By transactionCategory = By.xpath("//label/input[@id='transaction']");
    public static By categoryTitleTextbox = By.xpath("//ui-input[@name='title']//input[@id='input']");
    public static By categoryDescriptionTextArea = By.xpath("//ui-text-area[@name='description']//textarea");
    public static By conditionSaveButton = By.xpath("//div[@id='customCondition']//button/span[contains(text(),'SAVE')]");
    public static By selectTemplateDropdown = By.xpath("//ui-container[@floating-label='Select template']//input[@id='input']");
    public static By previewCustomCondition = By.xpath("//ui-form[@id='form']/div[4]//h4[contains(text(),'Preview')]");
    public  static By sendConditionButton = By.xpath("//ui-custom-button[@id='send']/button/span[contains(text(),'SEND')]");
    public static By giftCondition = By.xpath("//li[@id='letter-giftor-gifted']/div");
    public static By purchaseAndSaleCondition = By.xpath("//li[@id='agreenment-purchase-sale']/div");
    public static By mortgageVerificationCondition = By.xpath("//li[@id='verification-mortgage']/div");
    public static By hazardInsuranceCondition = By.xpath("//li[@id='hazard-insurance']/div");
    public static By rentVerificationCondition = By.xpath("//li[@id='verification-rent']/div");


    public void inviteBorrower(String borrowerEmail) throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver,50);
        wait.until(ExpectedConditions.visibilityOfElementLocated(inviteBtn));
        driver.findElement(inviteBtn).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(sendInviteBtn));
        Thread.sleep(1500);
        wait.until(ExpectedConditions.presenceOfElementLocated(firstNameTextbox));
        enterText(firstNameTextbox,portalParam.firstName);
        wait.until(ExpectedConditions.presenceOfElementLocated(lastNameTextbox));
        enterText(lastNameTextbox,portalParam.lastName);
        if(PortalParam.inviteUserType.equalsIgnoreCase("Borrower")){
            driver.findElement(borrowerBtn).click();
        }else{
            driver.findElement(affinityPartnerBtn).click();
        }
        wait.until(ExpectedConditions.presenceOfElementLocated(emailTextbox));
        enterText(emailTextbox,portalParam.borrowerEmail);
        wait.until(ExpectedConditions.presenceOfElementLocated(phoneTextbox));
        enterText(phoneTextbox,portalParam.phone);

       /* if(PortalParam.getInviteBoth().equalsIgnoreCase("No")){
            driver.findElement(checkBox).click();
        }*/
        wait.until(ExpectedConditions.elementToBeClickable(sendInviteBtn));
        driver.findElement(sendInviteBtn).click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(sendInviteBtn));
    }

    public void enterText(By locator,String text)  {
        Actions actions = new Actions(driver);
        new WebDriverWait(driver,30).until(ExpectedConditions.visibilityOfElementLocated(locator));
        WebElement passwordTxtBox = driver.findElement(locator);
        actions.moveToElement(passwordTxtBox);
        actions.click();
        actions.sendKeys(text);
        actions.build().perform();
        logger.info("Entered :"+text);
    }

    public LenderLoginPage logoutLender(String lenderUserLogin) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(profileDropdown));
        wait.until(ExpectedConditions.elementToBeClickable(profileDropdown));
        driver.findElement(profileDropdown).click();
        logger.info("Clicking profile dropdown");
        wait.until(ExpectedConditions.elementToBeClickable(logoutLink));
        driver.findElement(logoutLink).click();
        logger.info("Clicking logout link");
        return new LenderLoginPage(driver);
    }

    public LenderLoginPage createEventAndCondition(String loanid) throws InterruptedException {
        for(int i=1;i<=20;i++){
            String loanID=driver.findElement(By.cssSelector("div[class*='lender-pipeline-list-view'] active-loan-overview[class*='style-scope lender-pipeline-list-view']:nth-child("+i+") span[class*='loanid']")).getText();
            if(loanID.equals("ID: "+loanid+"")){
                PortalFuncUtils.scrollToElement(driver, By.cssSelector("div[class*='lender-pipeline-list-view'] active-loan-overview[class*='style-scope lender-pipeline-list-view']:nth-child("+i+") span[class*='loanid']"));
                driver.findElement(By.cssSelector("div[class*='lender-pipeline-list-view'] active-loan-overview[class*='style-scope lender-pipeline-list-view']:nth-child("+i+") div[class*='loandet-btn']")).click();
                break;
            }
        }
        wait.until(ExpectedConditions.visibilityOfElementLocated(loanIdLabel));
        wait.until(ExpectedConditions.presenceOfElementLocated(calendarTab));
        wait.until(ExpectedConditions.elementToBeClickable(calendarTab));
        selectTabs("Calendar");
        wait.until(ExpectedConditions.presenceOfElementLocated(tomorrowDateCell));
        wait.until(ExpectedConditions.elementToBeClickable(tomorrowDateCell));
        portalFuncUtils.scrollToElementandClick(driver,tomorrowDateCell);
        wait.until(ExpectedConditions.visibilityOfElementLocated(inviteBorrowerName));
        wait.until(ExpectedConditions.presenceOfElementLocated(titleTextbox));
        wait.until(ExpectedConditions.elementToBeClickable(titleTextbox));
        driver.findElement(titleTextbox).sendKeys("Title");
        wait.until(ExpectedConditions.presenceOfElementLocated(locationTextbox));
        wait.until(ExpectedConditions.elementToBeClickable(locationTextbox));
        driver.findElement(locationTextbox).sendKeys("Location");
        wait.until(ExpectedConditions.presenceOfElementLocated(startTimeTextbox));
        wait.until(ExpectedConditions.elementToBeClickable(startTimeTextbox));
        driver.findElement(startTimeTextbox).sendKeys("11:15");
        wait.until(ExpectedConditions.presenceOfElementLocated(endTimeTextbox));
        wait.until(ExpectedConditions.elementToBeClickable(endTimeTextbox));
        driver.findElement(endTimeTextbox).sendKeys("11:50");
        driver.findElement(descriptionTextbox).sendKeys("Description");
        if(driver.findElements(inviteBorrowerName).size() < 1)
        {
            driver.findElement(inviteUserTextbox).sendKeys(portalParam.borrowerEmail);
            driver.findElement(userListAddBtn).click();
        }
        wait.until(ExpectedConditions.elementToBeClickable(triggerEventButton));
        driver.findElement(triggerEventButton).click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(inviteUserTextbox));
        createCustomCondition();
        logoutLender(portalParam.borrowerEmail);
        return new LenderLoginPage(driver);
    }

    private void createCustomCondition() throws InterruptedException {
        portalFuncUtils.scrollOnTopOfThePage(driver);
        wait.until(ExpectedConditions.presenceOfElementLocated(conditionsTab));
        wait.until(ExpectedConditions.elementToBeClickable(conditionsTab));
        driver.findElement(conditionsTab).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(addNewConditionButton));
        wait.until(ExpectedConditions.elementToBeClickable(addNewConditionButton));
        driver.findElement(addNewConditionButton).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(conditionsLabel));
        selectRecipient(portalParam.firstName);
        selectConditionType("VA Streamline");
        selectCondition("Gift Letter");
        selectCustomCondition();
    }

    private void selectCustomCondition() throws InterruptedException {
        portalFuncUtils.scrollToElementandClick(driver,createCustomConditionLabel);
        portalFuncUtils.scrollToElement(driver,categoryDescriptionTextArea);
        Thread.sleep(3000);
        wait.until(ExpectedConditions.visibilityOfElementLocated(qualCategory));
        wait.until(ExpectedConditions.elementToBeClickable(qualCategory));
        driver.findElement(qualCategory).click();
        driver.findElement(categoryTitleTextbox).sendKeys("EC");
        driver.findElement(categoryDescriptionTextArea).sendKeys("EC");
        portalFuncUtils.scrollToElementandClick(driver,conditionSaveButton);
        driver.findElement(conditionSaveButton).click();
        portalFuncUtils.scrollToElement(driver,previewCustomCondition);
        driver.findElement(sendConditionButton).click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(sendConditionButton));
    }

    private void selectCondition(String condition) throws InterruptedException {
        Thread.sleep(2000);
        for(int i=1;i<=15;i++){
            WebElement incomeType = driver.findElement(By.cssSelector("tab-pane[id='tab-new-condition'] ul[id='filtered-request-list'] li:nth-child("+i+")"));
            Thread.sleep(2500);
            if(condition.contains("Gift"))
                incomeType.findElement(giftCondition).click();
            else if(condition.contains("Mortgage"))
                incomeType.findElement(mortgageVerificationCondition).click();
            else if(condition.contains("Hazard"))
                incomeType.findElement(hazardInsuranceCondition).click();
            else if(condition.contains("Rent"))
                incomeType.findElement(rentVerificationCondition).click();
            else if(condition.contains("Purchase and Sales"))
                incomeType.findElement(purchaseAndSaleCondition).click();
            break;
        }
        logger.info("Selected condition:"+condition);
    }

    private void selectConditionType(String conditionType) throws InterruptedException {
        Thread.sleep(2000);
        portalFuncUtils.scrollToElementandClick(driver,selectTemplateDropdown);
        Thread.sleep(2000);
        for(int i=2;i<=7;i++){
            WebElement incomeType = driver.findElement(By.cssSelector("ui-container[floating-label='Select template'] div[class*='ui-options'] ul[id*='select-options'] li:nth-child("+i+")"));
            Thread.sleep(2500);
            incomeType.findElement(By.xpath("//span[contains(text(),'"+conditionType+"')]")).click();
            break;
        }
        logger.info("Selected conditionType:"+conditionType);
    }

    private void selectRecipient(String firstName) throws InterruptedException {
        portalFuncUtils.scrollPageDown(driver);
        wait.until(ExpectedConditions.elementToBeClickable(recipientListDropdown));
        driver.findElement(recipientListDropdown).click();
        Thread.sleep(2000);
        for(int i=2;i<=3;i++){
            WebElement incomeType = driver.findElement(By.cssSelector("ui-container[id='recipientList'] div[class*='select-wrapper'] ul[id*='select-options'] li:nth-child("+i+")"));
            Thread.sleep(2500);
            incomeType.findElement(By.xpath("//ul[starts-with(@id,'select-options')]//span[contains(text(),'"+firstName+"')]")).click();
            break;
        }
        logger.info("Selected :"+firstName);
    }

    private void selectTabs(String tabName) {
        for(int i=1;i<=7;i++){
            String Tabname=driver.findElement(By.cssSelector("ul[class*='lender-application-tabs']>li:nth-child("+i+") span[class*=tab-title]")).getText();

            if(Tabname.equalsIgnoreCase(tabName)){
                driver.findElement(By.cssSelector("ul[class*='lender-application-tabs']>li:nth-child("+i+")")).click();
                break;
            }

        }
    }
}
