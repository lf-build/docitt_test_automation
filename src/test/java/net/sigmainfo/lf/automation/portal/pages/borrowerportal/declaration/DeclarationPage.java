package net.sigmainfo.lf.automation.portal.pages.borrowerportal.declaration;

import java.util.List;
import java.util.concurrent.TimeUnit;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.summary.SummaryPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sigmainfo.lf.automation.portal.constant.PortalParam;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.profile.ApplicantInformationPage;


public class DeclarationPage extends AbstractTests {

    private Logger logger = LoggerFactory.getLogger(DeclarationPage.class);
    WebDriverWait wait = new WebDriverWait(driver,60);

    public DeclarationPage(){}

    public static By outStandingJudgementYesBtn = By.cssSelector("div[id='SS1'] questioner-question-set[class*='sub-sections'] div:nth-child(3) div[class*=yes-no]>div:nth-of-type(1)");
    public static By outStandingJudgementNoBtn = By.cssSelector("div[id='SS1'] questioner-question-set[class*='sub-sections'] div:nth-child(3) div[class*=yes-no]>div:nth-of-type(2)");
    public static By outStandingJudgementText=By.xpath("//*[@id='SS1']/questioner-question-set/div[4]/div/div/questioner-question/ui-container/div/ui-text-area/div/textarea");

    public static By declareBankruptYesBtn= By.cssSelector("div[id='SS1'] questioner-question-set[class*='sub-sections'] div:nth-child(5) div[class*=yes-no]>div:nth-of-type(1)");
    public static By declareBankruptNoBtn= By.cssSelector("div[id='SS1'] questioner-question-set[class*='sub-sections'] div:nth-child(5) div[class*=yes-no]>div:nth-of-type(2)");
    public static By declareBankruptText=By.xpath(".//*[@id='SS1']/questioner-question-set/div[6]/div/div/questioner-question/ui-container/div/ui-text-area/div/textarea");


    public static By propertyForeclosedYesBtn=By.cssSelector("div[id='SS1'] questioner-question-set[class*='sub-sections'] div:nth-child(7) div[class*=yes-no]>div:nth-of-type(1)");
    public static By propertyForeclosedNoBtn=By.cssSelector("div[id='SS1'] questioner-question-set[class*='sub-sections'] div:nth-child(7) div[class*=yes-no]>div:nth-of-type(2)");
    public static By propertyForeclosedText=By.xpath(".//*[@id='SS1']/questioner-question-set/div[8]/div/div/questioner-question/ui-container/div/ui-text-area/div/textarea");

    public static By lawsuitYesBtn=By.cssSelector("div[id='SS1'] questioner-question-set[class*='sub-sections'] div:nth-child(9) div[class*=yes-no]>div:nth-of-type(1)");
    public static By lawsuitNoBtn=By.cssSelector("div[id='SS1'] questioner-question-set[class*='sub-sections'] div:nth-child(9) div[class*=yes-no]>div:nth-of-type(2)");
    public static By lawsuitText=By.xpath(".//*[@id='SS1']/questioner-question-set/div[10]/div/div/questioner-question/ui-container/div/ui-text-area/div/textarea");

    public static By obligatedOnAnyLoanYesBtn=By.cssSelector("div[id='SS1'] questioner-question-set[class*='sub-sections'] div:nth-child(11) div[class*=yes-no]>div:nth-of-type(1)");
    public static By obligatedOnAnyLoanNoBtn=By.cssSelector("div[id='SS1'] questioner-question-set[class*='sub-sections'] div:nth-child(11) div[class*=yes-no]>div:nth-of-type(2)");
    public static By obligatedOnAnyLoanText=By.xpath(" .//*[@id='SS1']/questioner-question-set/div[12]/div/div/questioner-question/ui-container/div/ui-text-area/div/textarea");


    public static By federaldebtYesBtn=By.cssSelector("div[id='SS1'] questioner-question-set[class*='sub-sections'] div:nth-child(13) div[class*=yes-no]>div:nth-of-type(1)");
    public static By federaldebtNoBtn=By.cssSelector("div[id='SS1'] questioner-question-set[class*='sub-sections'] div:nth-child(13) div[class*=yes-no]>div:nth-of-type(2)");
    public static By federaldebtText=By.xpath(" .//*[@id='SS1']/questioner-question-set/div[14]/div/div/questioner-question/ui-container/div/ui-text-area/div/textarea");

    public static By obligatedToPayYesBtn=By.cssSelector("div[id='SS1'] questioner-question-set[class*='sub-sections'] div:nth-child(15) div[class*=yes-no]>div:nth-of-type(1)");
    public static By obligatedToPayNoBtn=By.cssSelector("div[id='SS1'] questioner-question-set[class*='sub-sections'] div:nth-child(15) div[class*=yes-no]>div:nth-of-type(2)");
    public static By obligatedToPayText=By.xpath(" .//*[@id='SS1']/questioner-question-set/div[16]/div/div/questioner-question/ui-container/div/ui-text-area/div/textarea");


    public static By downPaymentBorrowedYesBtn=By.cssSelector("div[id='SS1'] questioner-question-set[class*='sub-sections'] div:nth-child(17) div[class*=yes-no]>div:nth-of-type(1)");
    public static By downPaymentBorrowedNoBtn=By.cssSelector("div[id='SS1'] questioner-question-set[class*='sub-sections'] div:nth-child(17) div[class*=yes-no]>div:nth-of-type(2)");
    public static By downPaymentBorrowedText=By.xpath(" .//*[@id='SS1']/questioner-question-set/div[18]/div/div/questioner-question/ui-container/div/ui-text-area/div/textarea");


    public static By endorserYesBtn=By.cssSelector("div[id='SS1'] questioner-question-set[class*='sub-sections'] div:nth-child(19) div[class*=yes-no]>div:nth-of-type(1)");
    public static By endorserNoBtn=By.cssSelector("div[id='SS1'] questioner-question-set[class*='sub-sections'] div:nth-child(19) div[class*=yes-no]>div:nth-of-type(2)");
    public static By endorserText=By.xpath(" .//*[@id='SS1']/questioner-question-set/div[20]/div/div/questioner-question/ui-container/div/ui-text-area/div/textarea");


    public static By  UScitizenYesBtn=By.cssSelector("div[id='SS1'] questioner-question-set[class*='sub-sections'] div:nth-child(21) div[class*=yes-no]>div:nth-of-type(1)");
    public static By  UScitizenNoBtn=By.cssSelector("div[id='SS1'] questioner-question-set[class*='sub-sections'] div:nth-child(21) div[class*=yes-no]>div:nth-of-type(2)");
    public static By  permanentYesBtn=By.cssSelector("div[id='SS1'] questioner-question-set[class*='sub-sections'] div:nth-child(22) div[class*=yes-no]>div:nth-of-type(1)");
    public static By  permanentNoBtn=By.cssSelector("div[id='SS1'] questioner-question-set[class*='sub-sections'] div:nth-child(22) div[class*=yes-no]>div:nth-of-type(2)");

    public static By  occupyPropertyYesBtn=By.cssSelector("div[id='SS1'] questioner-question-set[class*='sub-sections'] div:nth-child(24) div[class*=yes-no]>div:nth-of-type(1)");
    public static By occupyPropertyNoBtn=By.cssSelector("div[id='SS1'] questioner-question-set[class*='sub-sections'] div:nth-child(24) div[class*=yes-no]>div:nth-of-type(2)");
    public static By  ownershipInterestINPropertyYesBtn=By.cssSelector("div[id='SS1'] questioner-question-set[class*='sub-sections'] div:nth-child(26) div[class*=yes-no]>div:nth-of-type(1)");
    public static By ownershipInterestINPropertyNoBtn=By.cssSelector("div[id='SS1'] questioner-question-set[class*='sub-sections'] div:nth-child(26) div[class*=yes-no]>div:nth-of-type(2)");

    //public static By CheckBoxBtn =By.cssSelector("ui-check-box[class*='questioner-question'] input[type='checkbox']");
    //public static By CheckBoxBtn =By.cssSelector("ui-check-box[class*='questioner-question'] div[class*='ui-check-box']>input[type='checkbox']");
    public static By CheckBoxBtn  = By.xpath(".//*[@id='C-43']");

    public static By Ethnicity = By.xpath(".//*[@id='SS1']/questioner-question-set/div[31]/div/div[2]/questioner-question/ui-container/div/ui-options/div[1]/div/input");
    public static By HispanicOrLatino = By.xpath(".//*[@id='SS1']/questioner-question-set/div[31]/div/div[2]/questioner-question/ui-container/div/ui-options//li[2]/span");
    public static By NotHispanicOrLatino = By.xpath(".//*[@id='SS1']/questioner-question-set/div[31]/div/div[2]/questioner-question/ui-container/div/ui-options//li[3]/span");

    public static By Race = By.xpath(".//*[@id='SS1']/questioner-question-set/div[32]/div/div[2]/questioner-question/ui-container/div/ui-options/div[1]/div/input");
    public static By Asian = By.cssSelector("questioner-question-set[class='style-scope sub-sections'] div:nth-child(32) ui-options[name='dobMonth'] li:nth-child(4)");
    public static By raceList = By.xpath(".//*[@id='SS1']/questioner-question-set/div[32]/div/div[2]/questioner-question/ui-container/div/ui-options/div[1]/div//li");

    public static By MaleBtn=By.xpath(".//*[@id='SS1']/questioner-question-set/div[33]/div/div[2]/questioner-question/ui-container/div/ui-single-toggle/div[2]/label");
    public static By femaleBtn=By.xpath(".//*[@id='SS1']/questioner-question-set/div[33]/div/div[2]/questioner-question/ui-container/div/ui-single-toggle/div[1]/label");

    public static By ethnicityCheckbox = By.xpath("//div[@id='SS1']//input[@id='C-65']");
    public static By raceCheckbox = By.xpath("//div[@id='SS1']//input[@id='C-70']");
    public static By sexCheckbox = By.xpath("//div[@id='SS1']//input[@id='C-70']");



    //public static By submintbtn = By.xpath(".//*[@id='form']/right-content-area/div/div/div/div[3]/button[1]");
    public static By SubmitButton = By.xpath("//button[@name='next']");



    public DeclarationPage(WebDriver driver) {
        this.driver = driver;
        System.out.println("Driver is :"+driver);
        logger.info("========= Declaration Page is loaded===========");
    }


    public void selectBtn(By btn){
        WebDriverWait wait = new WebDriverWait(driver,60);
        wait.until(ExpectedConditions.elementToBeClickable(btn));
        driver.findElement(btn).click();
    }

    public void eneterText(By locator,String text){
        Actions actions = new Actions(driver);
        new WebDriverWait(driver,60).until(ExpectedConditions.visibilityOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        element.clear();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        actions.moveToElement(element);
        actions.click();
        actions.sendKeys(text);
        actions.build().perform();
    }


    public SpouseDeclarationPage fillDeclarationInfo() throws InterruptedException {

        //driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);

        System.out.println(PortalFuncUtils.waitForJQueryProcessing(driver,60,2));

        //a Question
        if(PortalParam.declarationQueA.equals("Yes")){
            selectBtn(outStandingJudgementYesBtn);
            eneterText(outStandingJudgementText,PortalParam.DeclarationQuestionAText);
            logger.info("Question A : Entering text:"+PortalParam.DeclarationQuestionAText);
        }else{
            selectBtn(outStandingJudgementNoBtn);
            logger.info("Question A : "+PortalParam.declarationQueA);
        }


        //b Question
        if(PortalParam.declarationQueB.equals("Yes")){
            selectBtn(declareBankruptYesBtn);
            eneterText(declareBankruptText,PortalParam.DeclarationQuestionBText);
            logger.info("Question B : Entering text:"+PortalParam.DeclarationQuestionBText);
        }else{
            selectBtn(declareBankruptNoBtn);
            logger.info("Question B : "+PortalParam.declarationQueB);
        }


        //c Question
        if(PortalParam.declarationQueC.equals("Yes")){
            selectBtn(propertyForeclosedYesBtn);
            eneterText(propertyForeclosedText,PortalParam.DeclarationQuestionCText);
            logger.info("Question C : Entering text:"+PortalParam.DeclarationQuestionCText);
        }else{
            selectBtn(propertyForeclosedNoBtn);
            logger.info("Question C : "+PortalParam.declarationQueC);
        }

        PortalFuncUtils.scrollPageDown(driver);

        //d Question
        if(PortalParam.declarationQueD.equals("Yes")){
            selectBtn(lawsuitYesBtn);
            eneterText(lawsuitText,PortalParam.DeclarationQuestionDText);
            logger.info("Question D : Entering text:"+PortalParam.DeclarationQuestionCText);
        }else{
            selectBtn(lawsuitNoBtn);
            logger.info("Question D : "+PortalParam.declarationQueD);
        }


        //e Question
        if(PortalParam.declarationQueE.equals("Yes")){
            selectBtn(obligatedOnAnyLoanYesBtn);
            eneterText(obligatedOnAnyLoanText,PortalParam.DeclarationQuestionEText);
            logger.info("Question E : Entering text:"+PortalParam.DeclarationQuestionEText);
        }else{
            selectBtn(obligatedOnAnyLoanNoBtn);
            logger.info("Question E : "+PortalParam.declarationQueE);
        }

        PortalFuncUtils.scrollPageDown(driver);

        //f Question
        if(PortalParam.declarationQueF.equals("Yes")){
            selectBtn(federaldebtYesBtn);
            eneterText(federaldebtText,PortalParam.DeclarationQuestionFText);
            logger.info("Question F : Entering text:"+PortalParam.DeclarationQuestionFText);
        }else{
            selectBtn(federaldebtNoBtn);
            logger.info("Question F : "+PortalParam.declarationQueF);
        }


        //g Question
        if(PortalParam.declarationQueG.equals("Yes")){
            selectBtn(obligatedToPayYesBtn);
            eneterText(obligatedToPayText,PortalParam.DeclarationQuestionGText);
            logger.info("Question C : Entering text:"+PortalParam.DeclarationQuestionGText);
        }else{
            selectBtn(obligatedToPayNoBtn);
            logger.info("Question G : "+PortalParam.declarationQueG);
        }

        PortalFuncUtils.scrollPageDown(driver);

        //h Question
        if(PortalParam.declarationQueH.equals("Yes")){
            selectBtn(downPaymentBorrowedYesBtn);
            eneterText(downPaymentBorrowedText,PortalParam.DeclarationQuestionHText);
            logger.info("Question H : Entering text:"+PortalParam.DeclarationQuestionHText);
        }else{
            selectBtn(downPaymentBorrowedNoBtn);
            logger.info("Question H : "+PortalParam.declarationQueH);
        }

        PortalFuncUtils.scrollPageDown(driver);
        //i Question
        if(PortalParam.declarationQueI.equals("Yes")){
            selectBtn(endorserYesBtn);
            eneterText(endorserText,PortalParam.DeclarationQuestionIText);
            logger.info("Question I : Entering text:"+PortalParam.DeclarationQuestionIText);
        }else{
            selectBtn(endorserNoBtn);
            logger.info("Question I : "+PortalParam.declarationQueI);
        }


        //j Question
        if(PortalParam.declarationQueJ.equals("Yes")){
            selectBtn(UScitizenYesBtn);
            logger.info("Question J:"+PortalParam.declarationQueJ);
        }else{
            selectBtn(UScitizenNoBtn);
            logger.info("Question J:"+PortalParam.declarationQueJ);
        }


        //k Question
        if(PortalParam.declarationQueK.equals("Yes")){
            selectBtn(permanentYesBtn);
            logger.info("Question K:"+PortalParam.declarationQueK);
        }else{
            selectBtn(permanentNoBtn);
            logger.info("Question K:"+PortalParam.declarationQueK);
        }


        //l Question
        if(PortalParam.declarationQueL.equals("Yes")){
            selectBtn(occupyPropertyYesBtn);
            logger.info("Question L:"+PortalParam.declarationQueL);
            //Select m Question
            if(PortalParam.declarationQueL.equals("Yes")){
                selectBtn(ownershipInterestINPropertyYesBtn);
            }else{
                selectBtn(ownershipInterestINPropertyNoBtn);
            }

        }else{
            selectBtn(occupyPropertyNoBtn);
            logger.info("Question L:"+PortalParam.declarationQueL);
        }

        /*PortalFuncUtils.scrollPageDown(driver);
        //Borrower CheckBox
        driver.findElement(CheckBoxBtn).click();

        //Select Ethnicity
        driver.findElement(Ethnicity).click();

        new WebDriverWait(driver,40).until(ExpectedConditions.visibilityOfElementLocated(HispanicOrLatino));

        if(PortalParam.DeclarationEthnicity.equals("Hispanic or Latino")){
            driver.findElement(HispanicOrLatino).click();
        }else{
            driver.findElement(NotHispanicOrLatino).click();
        }
        logger.info("Selected ethnicity:"+PortalParam.DeclarationEthnicity);

        PortalFuncUtils.scrollPageDown(driver);
        Thread.sleep(1500);
        //Select Race
        new WebDriverWait(driver,60).until(ExpectedConditions.visibilityOfElementLocated(Race));
        driver.findElement(Race).click();
        for(int i=2;i<=6;i++){
            WebElement raceType = driver.findElement(By.xpath("/*//*[@id='SS1']/questioner-question-set/div[32]/div/div[2]/questioner-question/ui-container/div/ui-options/div[1]/div//li["+i+"]"));
            String text = raceType.findElement(By.xpath("//span[contains(text(),'"+portalParam.DeclarationRace+"')]")).getText();
            if(text.contains(portalParam.DeclarationRace)){
                raceType.findElement(By.xpath("//span[contains(text(),'"+portalParam.DeclarationRace+"')]")).click();
                break;
            }

        }
        logger.info("Selected :"+portalParam.DeclarationRace);


        *//*driver.findElement(Race).click();
        new WebDriverWait(driver,60).until(ExpectedConditions.visibilityOfElementLocated(Asian));
        List<WebElement> RacelistElements= driver.findElements(raceList);

        for(int i=1; i <= RacelistElements.size(); i++)
        {
            WebElement raceElement = driver.findElement(By.xpath(".*//*//**//*[@id='SS1']/questioner-question-set/div[32]/div/div[2]/questioner-question/ui-container/div/ui-options/div[1]/div//li[" + i + "]"));
            System.out.println(raceElement.getText());
            if(raceElement.getText().equalsIgnoreCase("Asian"))
            {
                raceElement.click();
            }

        }*//*
        *//*for(WebElement s:Racelist){
            String s2=s.getText();
            if(s2.equals(PortalParam.DeclarationRace)){
                s.click();
                break;
            }
        }*//*
        *//*driver.findElement(Asian).click();*//*
        logger.info("Selected race:"+PortalParam.DeclarationRace);

        //Sex
        if(PortalParam.DeclarationSex.equals("Male")){
            driver.findElement(MaleBtn).click();
        }else{
            driver.findElement(femaleBtn).click();
        }
        logger.info("Selected sex:"+PortalParam.DeclarationSex);
        PortalFuncUtils.scrollUntil(SubmitButton,driver);
        wait.until(ExpectedConditions.elementToBeClickable(ethnicityCheckbox)).click();
        driver.findElement(ethnicityCheckbox).click();
        wait.until(ExpectedConditions.elementToBeClickable(raceCheckbox)).click();
        driver.findElement(raceCheckbox).click();
        wait.until(ExpectedConditions.elementToBeClickable(sexCheckbox)).click();
        driver.findElement(sexCheckbox).click();*/

        PortalFuncUtils.scrollUntil(SubmitButton,driver);

        //click on submit button
        wait.until(ExpectedConditions.elementToBeClickable(SubmitButton));
        driver.findElement(SubmitButton).click();
        logger.info("Clicking on SUBMIT button");

        return new SpouseDeclarationPage(driver);
    }

}
