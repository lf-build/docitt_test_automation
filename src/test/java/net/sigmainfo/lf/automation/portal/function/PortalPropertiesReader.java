package net.sigmainfo.lf.automation.portal.function;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by           : Shaishav.s on 08-02-2017.
 * Test class           : PortalPropertiesReader.java
 * Description          : Reads portal.properties key values declared
 * Includes             : 1. Declaration of keys declared in property file
 *                        2. Getter and setter methods
 */
@Component
public class PortalPropertiesReader {

    @Value(value = "${baseweburl}")
    private String baseweburl;
    @Value(value = "${lenderUserLogin}")
    private String lenderUserLogin;
    @Value(value = "${lenderurl}")
    private String lenderurl;
    @Value(value = "${Apbaseweburl}")
    private String apbaseweburl;
    @Value(value = "${docittwebuserid}")
    private String docittwebuserid;
    @Value(value = "${docittwebuserpassword}")
    private String docittwebuserpassword;
    @Value(value = "${plaid_username}")
    private String plaid_username;
    @Value(value = "${plaid_password}")
    private String plaid_password;
    @Value(value = "${plaid_pin}")
    private String plaid_pin;
    @Value(value = "${custom_report_location}")
    private String custom_report_location;
    @Value(value = "${browser}")
    private String browser;
    @Value(value = "${mongoHost}")
    private String mongoHost;
    @Value(value = "${mongoPort}")
    private String mongoPort;
    @Value(value="${upload_file_location}")
    private String upload_file_loaction;
    @Value(value = "${mongoDbUser}")
    private String mongoDbUser;
    @Value(value = "${mongoDbPwd}")
    private String mongoDbPwd;
    @Value(value = "${mongoDb}")
    private String mongoDb;
    @Value(value = "${gmailId}")
    private String gmailId;
    @Value(value = "${gmailPassword}")
    private String gmailPassword;
    @Value(value = "${emailVerification}")
    private boolean emailVerification;
    @Value(value = "${mobileVerification}")
    private boolean mobileVerification;

    public String getEncyptedPassword() {
        return encyptedPassword;
    }

    public void setEncyptedPassword(String encyptedPassword) {
        this.encyptedPassword = encyptedPassword;
    }

    @Value(value = "${encyptedPassword}")
    private String encyptedPassword;

    public String getBorrowerurl() {
        return borrowerurl;
    }

    public void setBorrowerurl(String borrowerurl) {
        this.borrowerurl = borrowerurl;
    }

    @Value(value = "${borrowerurl}")
    private String borrowerurl;


    public String getLenderUserLogin() {
        return lenderUserLogin;
    }

    public void setLenderUserLogin(String lenderUserLogin) {
        this.lenderUserLogin = lenderUserLogin;
    }

    public String getApbaseweburl() {
        return apbaseweburl;
    }

    public void setApbaseweburl(String apbaseweburl) {
        this.apbaseweburl = apbaseweburl;
    }

    public String getLenderurl() {
        return lenderurl;
    }

    public void setLenderurl(String lenderurl) {
        this.lenderurl = lenderurl;
    }


    public String getPlaid_username() {
		return plaid_username;
	}

	public void setPlaid_username(String plaid_username) {
		this.plaid_username = plaid_username;
	}

	public String getPlaid_password() {
		return plaid_password;
	}

	public void setPlaid_password(String plaid_password) {
		this.plaid_password = plaid_password;
	}

	public String getPlaid_pin() {
		return plaid_pin;
	}

	public void setPlaid_pin(String plaid_pin) {
		this.plaid_pin = plaid_pin;
	}

	public String getBaseweburl() {
        return baseweburl;
    }

    public void setBaseweburl(String baseweburl) {
        this.baseweburl = baseweburl;
    }

    public String getDocittwebuserid() {
        return docittwebuserid;
    }

    public void setDocittwebuserid(String docittwebuserid) {
        this.docittwebuserid = docittwebuserid;
    }

    public String getDocittwebuserpassword() {
        return docittwebuserpassword;
    }

    public void setDocittwebuserpassword(String docittwebuserpassword) {
        this.docittwebuserpassword = docittwebuserpassword;
    }

    public String getCustom_report_location() {
        return custom_report_location;
    }

    public void setCustom_report_location(String custom_report_location) {
        this.custom_report_location = custom_report_location;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }



    public String getUpload_file_loaction() {
        return upload_file_loaction;
    }

    public void setUpload_file_loaction(String upload_file_loaction) {
        this.upload_file_loaction = upload_file_loaction;
    }



    public String getmongoHost() {
        return mongoHost;
    }

    public void setmongoHost(String mongoHost) {
        this.mongoHost = mongoHost;
    }

    public String getmongoPort() {
        return mongoPort;
    }

    public void setmongoPort(String mongoPort) {
        this.mongoPort = mongoPort;
    }

    public String getMongoDbUser() {
        return mongoDbUser;
    }

    public void setMongoDbUser(String mongoDbUser) {
        this.mongoDbUser = mongoDbUser;
    }

    public String getMongoDbPwd() {
        return mongoDbPwd;
    }

    public void setMongoDbPwd(String mongoDbPwd) {
        this.mongoDbPwd = mongoDbPwd;
    }

    public String getMongoDb() {
        return mongoDb;
    }

    public void setMongoDb(String mongoDb) {
        this.mongoDb = mongoDb;
    }



    public String getGmailId() {
        return gmailId;
    }

    public boolean isEmailVerification() {
		return emailVerification;
	}

	public void setEmailVerification(boolean emailVerification) {
		this.emailVerification = emailVerification;
	}

	public boolean isMobileVerification() {
		return mobileVerification;
	}

	public void setMobileVerification(boolean mobileVerification) {
		this.mobileVerification = mobileVerification;
	}

	public void setGmailId(String gmailId) {
        this.gmailId = gmailId;
    }

    public String getGmailPassword() {
        return gmailPassword;
    }

    public void setGmailPassword(String gmailPassword) {
        this.gmailPassword = gmailPassword;
    }


}
