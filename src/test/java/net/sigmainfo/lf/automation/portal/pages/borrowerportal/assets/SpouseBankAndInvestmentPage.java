package net.sigmainfo.lf.automation.portal.pages.borrowerportal.assets;

import net.sigmainfo.lf.automation.common.AbstractTests;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by shaishav.s on 26-09-2017.
 */
public class SpouseBankAndInvestmentPage extends AbstractTests {
    private Logger logger = LoggerFactory.getLogger(SpouseBankAndInvestmentPage.class);
    WebDriverWait wait = new WebDriverWait(driver,60);

    SpouseBankAndInvestmentPage(){}

    public SpouseBankAndInvestmentPage(WebDriver driver){
        this.driver = driver;
        logger.info("========= SpouseBankAndInvestmentPage is loaded===========");
    }

}
