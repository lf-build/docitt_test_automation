package net.sigmainfo.lf.automation.portal.pages.borrowerportal;

import net.sigmainfo.lf.automation.common.AbstractTests;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Created by           : Shaishav.s on 10-04-2017.
 * Test class           : ResetPasswordPage.java
 * Includes             : 1. Objects on ResetPasswordPage
 *                        2. Methods implementation on ResetPasswordPage
 */
@Component
public class ResetPasswordPage extends AbstractTests {

    private Logger logger = LoggerFactory.getLogger(ResetPasswordPage.class);

    public ResetPasswordPage(WebDriver driver) {
        this.driver = driver;
    }
    public ResetPasswordPage(){}

    public static By emailTextBox = By.name("email");
    public static By newPasswordTextBox = By.name("password");
    public static By confirmPasswordTextBox = By.name("confirmPassword");
    public static By resetButton = By.xpath("//span[contains(text(),' RESET')]");
    public static By cancelLink = By.xpath("//a[contains(text(),'Cancel')]");

    public String getPageTitle() {
        String title = driver.getTitle();
        return title;
    }

    public boolean verifyPageTitle() {
        String pageTitle = "";
        return getPageTitle().contains(pageTitle);
    }

    public void enterEmail(WebDriver driver, String username)  {

        logger.info("Entering Email:"+username);
        Actions actions = new Actions(this.driver);
        new WebDriverWait(this.driver,30).until(ExpectedConditions.visibilityOfElementLocated(emailTextBox));
        WebElement emailTxtBox = this.driver.findElement(emailTextBox);
        actions.moveToElement(emailTxtBox);
        actions.click();
        actions.sendKeys(username);
        actions.build().perform();
    }

    public void enterNewPassword(WebDriver driver, String password)  {

        logger.info("Entering New Password:"+password);
        Actions actions = new Actions(this.driver);
        new WebDriverWait(this.driver,30).until(ExpectedConditions.visibilityOfElementLocated(newPasswordTextBox));
        WebElement confirmNewPswdTxtBox = this.driver.findElement(newPasswordTextBox);
        actions.moveToElement(confirmNewPswdTxtBox);
        actions.click();
        actions.sendKeys(password);
        actions.build().perform();
    }

    public void confirmNewPassword(WebDriver driver, String password)  {

        logger.info("Entering Confirm New Password:"+password);
        Actions actions = new Actions(this.driver);
        new WebDriverWait(this.driver,30).until(ExpectedConditions.visibilityOfElementLocated(confirmPasswordTextBox));
        WebElement newPswdTxtBox = this.driver.findElement(confirmPasswordTextBox);
        actions.moveToElement(newPswdTxtBox);
        actions.click();
        actions.sendKeys(password);
        actions.build().perform();
    }

    public PasswordChangedPage clickResetButton(WebDriver driver)
    {
        logger.info("Clicking RESET button");
        WebElement resetBtn = this.driver.findElement(resetButton);
        if((resetBtn.isDisplayed()) || (resetBtn.isEnabled()))
            resetBtn.click();
        else
            new Exception("Login button is not found");
        return new PasswordChangedPage(this.driver);
    }
}
