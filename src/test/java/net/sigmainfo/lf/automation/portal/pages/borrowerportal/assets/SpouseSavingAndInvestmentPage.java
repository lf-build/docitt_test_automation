package net.sigmainfo.lf.automation.portal.pages.borrowerportal.assets;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.income.BusinessSelfEmploymentPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.testng.AssertJUnit.assertTrue;

/**
 * Created by shaishav.s on 27-09-2017.
 */
public class SpouseSavingAndInvestmentPage extends AbstractTests {
    WebDriverWait wait = new WebDriverWait(driver,60);
    WebElement PropertyTypeListItem;

    private Logger logger = LoggerFactory.getLogger(SpouseSavingAndInvestmentPage.class);

    public SpouseSavingAndInvestmentPage(WebDriver driver) {
        this.driver = driver;
        assertTrue(portalParam.spouceFirstName+"'s Savings & Investments is not displayed",driver.findElement(By.xpath("//div[starts-with(@id,'sigma_shaishav')]//assets-plaid-scene-title/h3")).getText().contains(portalParam.spouceFirstName+"'s Savings & Investments"));
        logger.info("========= SpouseSavingAndInvestmentPage is loaded===========");
    }

    public SpouseSavingAndInvestmentPage() {}

    public static By institutionNameField=By.cssSelector("ui-input[name='institutionName'] input[id='input']");
    public static By accountTypeField=By.cssSelector("ui-input[name='accountType'] input[id='input']");
    public static By currentBalanceField=By.cssSelector("ui-input[name='currentBalance'] input[id='input']");
    public static By accountNumberField=By.cssSelector("ui-input[name='accountNumber'] input[id='input']");
    public static By accountHolderField=By.cssSelector("ui-input[name='accountHolder'] input[id='input']");
    public static By enterBtn=By.cssSelector(".docitt-btn.next.style-scope.borrower-assets-forms-add-institution");

    @SuppressWarnings("static-access")
    public void fillingSavingandInvenstmentForm(String institutionName,String accountType,String currentBalance,String accountNumber,String accountName){
        enterValueinField(institutionNameField,institutionName);
        enterValueinField(accountTypeField,accountType);
        enterValueinField(currentBalanceField,currentBalance);
        enterValueinField(accountNumberField,accountNumber);
        enterValueinField(accountHolderField,accountName);
        clickBtn(enterBtn);

    }

    public void enterValueinField(By element,String value)  {
        try {

            Actions actions = new Actions(driver);
            new WebDriverWait(driver,30).until(ExpectedConditions.visibilityOfElementLocated(element));
            WebElement emailTxtBox = driver.findElement(element);
            actions.moveToElement(emailTxtBox);
            actions.click();
            actions.sendKeys(value);
            actions.build().perform();
            logger.info("Entering :"+ value);
        } catch (Exception e) {
            logger.error("Unable to enter "+ value + " value");
        }
    }

    public void clickBtn(By element){
        try {
            new WebDriverWait(driver,30).until(ExpectedConditions.visibilityOfElementLocated(element));
            driver.findElement(element).click();
            logger.info("Clicking ENTER button");
        } catch (Exception e) {
            logger.error("Unable to Click "+ element);
        }
    }

    public BankAndInvestmentPage enterManualAssetsDetails(String institutionName,String accountType,String currentBalance,String accountNumber,String accountName)
    {
        fillingSavingandInvenstmentForm(institutionName,accountType,currentBalance,accountNumber,accountName);
        return new BankAndInvestmentPage(driver);
    }

}
