package net.sigmainfo.lf.automation.portal.pages.borrowerportal.declaration;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.pages.borrowerportal.credit.CreditScorePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by shaishav.s on 07-09-2017.
 */
public class DeclarationSectionCompletePage extends AbstractTests {
    private Logger logger = LoggerFactory.getLogger(DeclarationSectionCompletePage.class);
    WebDriverWait wait = new WebDriverWait(driver,60);

    public DeclarationSectionCompletePage(WebDriver driver){
        this.driver = driver;
        logger.info("========= DeclarationSectionCompletePage is loaded===========");
    }
    public DeclarationSectionCompletePage(){}

    public static By BeginSectionButton = By.xpath("//ui-button[@id='goNextSection']/button");

    public CreditScorePage clickBeginSectionBtn()
    {
        wait.until(ExpectedConditions.elementToBeClickable(BeginSectionButton));
        driver.findElement(BeginSectionButton).click();
        logger.info("Clicking on BEGIN SECTION button");
        return new CreditScorePage(driver);
    }
}
