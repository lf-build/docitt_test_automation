package net.sigmainfo.lf.automation.portal.pages.borrowerportal;

import net.sigmainfo.lf.automation.common.AbstractTests;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by           : Shaishav.s on 27-02-2017.
 * Test class           : AccessMyPortalPage.java
 * Includes             : 1. Objects on AccessMyPortalPage
 *                        2. Methods implementation on AccessMyPortalPage
 */
public class AccessMyPortalPage extends AbstractTests {

    private Logger logger = LoggerFactory.getLogger(AccessMyPortalPage.class);

    public AccessMyPortalPage(WebDriver driver){this.driver = driver;}
    public AccessMyPortalPage(){}

    public static By accessMyPortalButton = By.xpath("//span[contains(text(),'Access my portal')]");

    public LoginPage clickAccessMyPortal() {
        logger.info("Clicking Access my portal button");
        WebElement accessMyPortalBtn = driver.findElement(accessMyPortalButton);
        if((accessMyPortalBtn.isDisplayed()) || (accessMyPortalBtn.isEnabled()))
            accessMyPortalBtn.click();
        else
            new Exception("Access my portal button is not found");
        return new LoginPage(driver);
    }
}
