package net.sigmainfo.lf.automation.portal.pages.borrowerportal.income;

import net.sigmainfo.lf.automation.common.AbstractTests;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static net.sigmainfo.lf.automation.portal.constant.PortalParam.percentageOwnership;
import static net.sigmainfo.lf.automation.portal.constant.PortalParam.typeOfCompany;
import static net.sigmainfo.lf.automation.portal.pages.borrowerportal.income.BusinessSelfEmploymentPage.PercentageOwnershipButton;

/**
 * Created by shaishav.s on 26-09-2017.
 */
public class SpouseBusinessSelfEmploymentPage extends AbstractTests {
    WebDriverWait wait = new WebDriverWait(driver,120);
    WebElement PropertyTypeListItem;

    private Logger logger = LoggerFactory.getLogger(SpouseBusinessSelfEmploymentPage.class);

    public SpouseBusinessSelfEmploymentPage(WebDriver driver) {
        this.driver = driver;
        logger.info("========= SpouseBusinessSelfEmploymentPage is loaded===========");
    }

    public SpouseBusinessSelfEmploymentPage() {}

    public static By SpouseMonthlyIncomeTextBox = By.xpath("//div[starts-with(@id,'ID-')][15]/questioner-question-set/div[2]//questioner-question//input[@id='input']");
    public static By SpouseCompanyNameTextBox = By.xpath("//div[starts-with(@id,'ID-')][15]/questioner-question-set/div[3]//questioner-question//input[@id='input']");
    public static By SpouseTitleTextBox = By.xpath("//div[starts-with(@id,'ID-')][15]/questioner-question-set/div[4]//questioner-question//input[@id='input']");
    public static By SpouseCompanyAddressTextBox = By.xpath("//div[starts-with(@id,'ID-')][15]/questioner-question-set/div[5]//questioner-question//label[contains(text(),'Company Address')]");
    public static By SpouseCompanyPhoneNumberTextBox = By.xpath("//div[starts-with(@id,'ID-')][15]/questioner-question-set/div[6]//questioner-question//label[contains(text(),'Phone #')]");
    public static By SpouseBusinessStartDateTextBox = By.xpath("//div[starts-with(@id,'ID-')][15]/questioner-question-set/div[7]//questioner-question//label[contains(text(),'Start Date')]");
    public static By SpousePercentOwnershipButton = By.xpath("//div[starts-with(@id,'ID-')][15]//ui-switch[@id='64']//label[normalize-space(.)='Yes']");
    public static By SpouseNoPercentOwnershipButton = By.xpath("//div[starts-with(@id,'ID-')][15]//ui-switch[@id='64']//label[normalize-space(.)='No']");
    public static By TypeOfCompanyDropdown = By.xpath("//div[starts-with(@id,'ID-')][15]//div[9]/div[2]//questioner-question/ui-container//ui-options[@name='dobMonth']");

    public static By NextButton = By.xpath("//button[@name='next']");

    public SpouseMilitaryPage provideSpouseBusinessDetails(String spouseBusinessMonthlyIncome, String spouseBusinessCompanyName, String spouseBusinessTitle, String spouseBusinessAddress, String spouseBusinessPhone, String spouseBusinessStartDate, boolean spousePercentOwnership, String spouseTypeOfCompany) throws Exception {
        enterText(SpouseMonthlyIncomeTextBox,spouseBusinessMonthlyIncome);
        enterText(SpouseCompanyNameTextBox,spouseBusinessCompanyName);
        enterText(SpouseTitleTextBox,spouseBusinessTitle);
        Thread.sleep(500);
        selectBusinessAddress(SpouseCompanyAddressTextBox,spouseBusinessAddress);
        Thread.sleep(500);
        enterText(SpouseCompanyPhoneNumberTextBox,spouseBusinessPhone);
        enterText(SpouseBusinessStartDateTextBox,spouseBusinessStartDate);
        scrollDownThePage(driver);
        selectOwnershipInCompany(SpousePercentOwnershipButton,spousePercentOwnership);
        selectTypeOfCompany(TypeOfCompanyDropdown,spouseTypeOfCompany);
        clickButton();
        return new SpouseMilitaryPage(driver);
    }

    private void selectBusinessAddress(By locator, String spouseBusinessAddress) throws InterruptedException {
            enterText(locator,spouseBusinessAddress);
            selectOptionWithText(spouseBusinessAddress);
            Thread.sleep(3000);
        }
    private void selectOwnershipInCompany(By percentageOwnershipButton, boolean percentageOwnership) {
        selectBoolean(percentageOwnershipButton, percentageOwnership);
    }

    private void scrollDownThePage(WebDriver driver) {
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("window.scrollBy(0,250)", "");
    }

    private void selectTypeOfCompany(By locator, String typeOfCompany) throws InterruptedException {
        wait.until(ExpectedConditions.elementToBeClickable(TypeOfCompanyDropdown));
        driver.findElement(TypeOfCompanyDropdown).click();
        Thread.sleep(2000);
        for(int i=3;i<=6;i++){
            WebElement companyType = driver.findElement(By.cssSelector("div[id^='ID-']:nth-child(19) ui-options[name='dobMonth'] div[class*='select-wrapper'] li:nth-child("+i+")"));
            String text = companyType.getText();

            if(text.equalsIgnoreCase(typeOfCompany)){
                companyType.click();
            }
        }
        logger.info("Selected :"+typeOfCompany);
    }

    private void selectOptionWithText(String currentAddress) throws InterruptedException {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='pac-item']//span[contains(text(),'"+currentAddress+"')]")));
        driver.findElement(By.xpath("//div[@class='pac-item']//span[contains(text(),'"+currentAddress+"')]")).click();
        logger.info("Selected address :" + currentAddress);
    }

    public void selectBoolean(By locator,boolean value)
    {
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("arguments[0].click()", element);
        logger.info("Selected boolean :"+value);
    }

    public void enterText(By locator,String text){
        Actions actions = new Actions(driver);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        actions.moveToElement(element);
        actions.click();
        actions.sendKeys(text);
        actions.build().perform();
        logger.info("Entering :"+text);
    }

    public void clickButton() throws Exception {
        wait.until(ExpectedConditions.elementToBeClickable(NextButton));
        driver.findElement(NextButton).click();
        logger.info("Clicking on I am done button");
    }

}
