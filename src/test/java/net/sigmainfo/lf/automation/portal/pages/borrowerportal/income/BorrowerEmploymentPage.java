package net.sigmainfo.lf.automation.portal.pages.borrowerportal.income;

import net.sigmainfo.lf.automation.common.AbstractTests;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by shaishav.s on 06-09-2017.
 */
public class BorrowerEmploymentPage extends AbstractTests {

    WebDriverWait wait = new WebDriverWait(driver,60);
    WebElement PropertyTypeListItem;

    private Logger logger = LoggerFactory.getLogger(BorrowerEmploymentPage.class);

    public BorrowerEmploymentPage(WebDriver driver) {
        this.driver = driver;
        logger.info("========= BorrowerEmploymentPage is loaded===========");
    }

    public BorrowerEmploymentPage() {}

    public static By EmployerTextBox = By.xpath("//label[contains(text(),'Current Employer')]");

    public static By TitleTextBox = By.xpath("//platform-shell/div/borrower-private-layout/div/micro-app/div/questioner-sections/div[1]/div[3]/div/div/ui-form/right-content-area/div/div/sub-sections/div[3]/questioner-question-set/div[3]/div/div/questioner-question/ui-container/div/ui-input/div/div[1]/input");

    public static By EmpStartDateTextBox = By.xpath("//platform-shell/div/borrower-private-layout/div/micro-app/div/questioner-sections/div[1]/div[3]/div/div/ui-form/right-content-area/div/div/sub-sections/div[3]/questioner-question-set/div[4]/div/div/questioner-question/ui-container/div/ui-input/div/div[1]/input");

    public static By InlineYearTextBox = By.xpath("//label[contains(text(),'Years')]");

    public static By InlineMonthTextBox = By.xpath("//label[contains(text(),'Months')]");

    public static By StartDateTextBox = By.xpath("//div[@id='ID-42f643cb-1ffb-2e04-dc7b-0658c80eb61b']/questioner-question-set/div[4]/div/div/questioner-question/ui-container/div/ui-input/div/div[1]/input");
    public static By MonthlySalaryTextBox = By.xpath("//platform-shell/div/borrower-private-layout/div/micro-app/div/questioner-sections/div[1]/div[3]/div/div/ui-form/right-content-area/div/div/sub-sections/div[3]/questioner-question-set/div[7]/div/div/questioner-question/ui-container/div/ui-input/div/div[1]/input");

    public static By BonusButton = By.xpath("//platform-shell/div/borrower-private-layout/div/micro-app/div/questioner-sections/div[1]/div[3]/div/div/ui-form/right-content-area/div/div/sub-sections/div[3]/questioner-question-set/div[8]/div[3]/div/questioner-question/ui-container/div/ui-toggle-button-group/div[1]/label");
    public static By BonusTextbox = By.xpath("//platform-shell/div/borrower-private-layout/div/micro-app/div/questioner-sections/div[1]/div[3]/div/div/ui-form/right-content-area/div/div/sub-sections/div[3]/questioner-question-set/div[9]/div/div/questioner-question/ui-container/div/ui-input/div/div[1]/input");
    public static By NextButton = By.xpath("//button[@name='next']");

    
    public BorrowerEmpAddressPage enterBorrowerEmploymentDetails(String currentEmployer, String employmentTitle, String employmentStartDate, String inThisLineYear, String inThisLineMonth, String monthlyBaseSalary, String bonusAmount) {
    	enterText(EmployerTextBox, currentEmployer);
    	enterText(TitleTextBox, employmentTitle);
    	enterText(EmpStartDateTextBox, employmentStartDate);
        enterText(InlineYearTextBox, inThisLineYear);
    	enterText(InlineMonthTextBox, inThisLineMonth);
    	enterText(MonthlySalaryTextBox, monthlyBaseSalary);
    	selectButton(BonusButton, "Bonus");
    	enterText(BonusTextbox, bonusAmount);
        scrollDownThePage(driver);
    	selectButton(NextButton, "Next");
    	return new BorrowerEmpAddressPage(driver);
    }

    private void scrollDownThePage(WebDriver driver) {
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("window.scrollBy(0,250)", "");
    }
    
    public void enterText(By locator,String text){
        Actions actions = new Actions(driver);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        actions.moveToElement(element);
        actions.click();
        actions.sendKeys(text);
        actions.build().perform();
        logger.info("Entering :"+text);
    }
    public void selectButton(By locator,String value){
        wait.until(ExpectedConditions.elementToBeClickable(locator));
        driver.findElement(locator).click();
        logger.info("Clicking on "+ value +" button");
    }
}
