package net.sigmainfo.lf.automation.api.dataset;

import org.springframework.stereotype.Component;

/**
 * Created by           : Shaishav.s on 11-04-2017.
 * Test class           : verifyDocument.java
 * Description          : Contains members making a complete verifyDocument class within automation
 * Includes             : 1. Declares members within the class
 *                        2. Contains getter and setter methods
 */
@Component
public class verifyDocument {

    private String notes;
    private questions questions;

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public net.sigmainfo.lf.automation.api.dataset.questions getQuestions() {
        return questions;
    }

    public void setQuestions(net.sigmainfo.lf.automation.api.dataset.questions questions) {
        this.questions = questions;
    }
}
