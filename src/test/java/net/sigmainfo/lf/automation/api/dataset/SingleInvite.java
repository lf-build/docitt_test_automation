package net.sigmainfo.lf.automation.api.dataset;

import org.springframework.stereotype.Component;

/**
 * Created by           : Shaishav.s on 29-03-2017.
 * Test class           : SingleInvite.java
 * Description          : Contains members making a complete SingleInvite class within automation
 * Includes             : 1. Declares members within the class
 *                        2. Contains getter and setter methods
 */
@Component
public class SingleInvite {
    private String UserId;

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getUser() {
        return User;
    }

    public void setUser(String user) {
        User = user;
    }

    public String getRole() {
        return Role;
    }

    public void setRole(String role) {
        Role = role;
    }

    private String User;
    private String Role;

    public boolean isLenderTeam() {
        return lenderTeam;
    }

    public void setLenderTeam(boolean lenderTeam) {
        this.lenderTeam = lenderTeam;
    }

    private boolean lenderTeam;


}
