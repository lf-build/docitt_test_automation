package net.sigmainfo.lf.automation.api.dataset;

import org.springframework.stereotype.Component;

/**
 * Created by           : Shaishav.s on 11-04-2017.
 * Test class           : questions.java
 * Description          : Contains members making a complete questions class within automation
 * Includes             : 1. Declares members within the class
 *                        2. Contains getter and setter methods
 */
@Component
public class questions {
    private boolean altered;
    private boolean name;
    private boolean address;
    private boolean birthdate;
    private boolean issueCountry;
    private boolean photo;
    private boolean notExpired;
    private boolean misspelled;

    public boolean isAltered() {
        return altered;
    }

    public void setAltered(boolean altered) {
        this.altered = altered;
    }

    public boolean isName() {
        return name;
    }

    public void setName(boolean name) {
        this.name = name;
    }

    public boolean isAddress() {
        return address;
    }

    public void setAddress(boolean address) {
        this.address = address;
    }

    public boolean isBirthdate() {
        return birthdate;
    }

    public void setBirthdate(boolean birthdate) {
        this.birthdate = birthdate;
    }

    public boolean isIssueCountry() {
        return issueCountry;
    }

    public void setIssueCountry(boolean issueCountry) {
        this.issueCountry = issueCountry;
    }

    public boolean isPhoto() {
        return photo;
    }

    public void setPhoto(boolean photo) {
        this.photo = photo;
    }

    public boolean isNotExpired() {
        return notExpired;
    }

    public void setNotExpired(boolean notExpired) {
        this.notExpired = notExpired;
    }

    public boolean isMisspelled() {
        return misspelled;
    }

    public void setMisspelled(boolean misspelled) {
        this.misspelled = misspelled;
    }
}
