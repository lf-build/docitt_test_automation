package net.sigmainfo.lf.automation.api.tests;

import com.jayway.restassured.response.Response;
import net.sigmainfo.lf.automation.api.constant.ApiParam;
import net.sigmainfo.lf.automation.api.function.ApiPropertiesReader;
import net.sigmainfo.lf.automation.api.function.QuestionnaireUtils;
import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.common.TestResults;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import java.net.URLDecoder;

import static org.testng.Assert.assertEquals;

/**
 * Created by shaishav.s on 02-05-2017.
 */
public class ReminderApiTests extends AbstractTests {

    @Autowired
    TestResults testResults;

    @Autowired
    QuestionnaireUtils qUtils;

    @Autowired
    ApiPropertiesReader apiPropertiesReader;

    @Autowired
    ApiParam apiParam;

    public static String funcMod="Docitt_Api";

    @AfterClass(alwaysRun = true)
    private void endCasereport() throws Exception {


        String funcModule = "Docitt_Api";
        String className = org.apache.commons.lang.StringUtils.substringAfterLast(this.getClass().getName(), ".");
        String description = "Verify  " + org.apache.commons.lang.StringUtils.substringAfterLast(this.getClass().getName(), ".");
        generateReport(className, description,funcMod);
    }

    /**
     * Description          : createEvent
     * Includes             : 1. Create an event for given applicationId
     */

    @Test(priority=1,description = "", groups = {"docittapitests","reminderTests","remindersFlowTest"})
    public void VerifyAddReminderTest() throws Exception{
        String sTestID = "remindersFlowTest";
        String result = "Failed";
        int totalReminders=0;
        int totalUnreadReminders=0;
        String collection="reminders";
        String id= null;
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            String urlEndPoint = restUrl + ":"+ apiParam.getPort() + apiParam.getRequestType() + "/" + apiParam.getEntityId();
            // Get all reminders --> count
            Response getAllReminderResponse = qUtils.getUnreadReminderCountResponse(conType, urlEndPoint, apiParam.requestType,null);
            assertEquals(getAllReminderResponse.getStatusCode(),SUCCESS_WITH_CONTENT);
            totalReminders = qUtils.getEventsForEntityId(apiParam.getEntityId(),collection);
            assertEquals(totalReminders,new JSONArray(getAllReminderResponse.prettyPrint().toString()).length());
            logger.info("####################### Total Reminders count: "+totalReminders);

            // Get unread reminders count
            urlEndPoint = restUrl + ":"+ apiParam.getPort() + apiParam.getRequestType() + "/" + apiParam.getEntityId() +"/unread/count";
            Response getUnreadReminderCountResponse = qUtils.getUnreadReminderCountResponse(conType, urlEndPoint, apiParam.requestType,null);
            assertEquals(getUnreadReminderCountResponse.getStatusCode(),SUCCESS_WITH_CONTENT);
            totalUnreadReminders = new JSONArray(getUnreadReminderCountResponse.prettyPrint().toString()).length();

            // Create a reminder
            Response createReminderResp = qUtils.getCreateReminderResponse(conType, urlEndPoint, apiParam.requestType,null);
            assertEquals(createReminderResp.getStatusCode(),SUCCESS_WITH_NO_CONTENT);
            assertEquals(qUtils.getEventsForEntityId(apiParam.getEntityId(),collection),totalReminders+1);
            id = qUtils.getIdOfLastInsertedRecord(apiParam.getEntityId(),collection);
            logger.info("Document inserted with id :"+id);
            // Get all unread reminders --> count increased by 1
            Response getReminderResponse = qUtils.getAllRemindersResponse(conType, urlEndPoint, apiParam.requestType,null);
            assertEquals(getReminderResponse.getStatusCode(),SUCCESS_WITH_CONTENT);
            // Get unread reminders count
            urlEndPoint = restUrl + ":"+ apiParam.getPort() + apiParam.getRequestType() + "/" + apiParam.getEntityId() +"/unread/count";
            Response getUnreadReminderCountResponse1 = qUtils.getUnreadReminderCountResponse(conType, urlEndPoint, apiParam.requestType,null);
            assertEquals(getUnreadReminderCountResponse1.getStatusCode(),SUCCESS_WITH_CONTENT);
            assertEquals(new JSONArray(getUnreadReminderCountResponse1.prettyPrint().toString()).length(),totalUnreadReminders+1);

            // Get unread reminders by date
            urlEndPoint = restUrl + ":"+ apiParam.getPort() + apiParam.getRequestType() + "/" + apiParam.getEntityId() +"unread/date";
            Response getUnreadReminderByDateResponse = qUtils.getReminderByDateResponse(conType, urlEndPoint, apiParam.requestType,null);
            assertEquals(getUnreadReminderByDateResponse.getStatusCode(),SUCCESS_WITH_CONTENT);
            // Mark reminder as read

            urlEndPoint = restUrl + ":"+ apiParam.getPort() + apiParam.getRequestType() + "/" + apiParam.getEntityId() +"/markasread";
            Response getMarkAsReadResponse = qUtils.getMarkAsReadResponse(conType, urlEndPoint, apiParam.requestType,id);
            assertEquals(getMarkAsReadResponse.getStatusCode(),SUCCESS_WITH_CONTENT);

            // Get unread reminders count
            urlEndPoint = restUrl + ":"+ apiParam.getPort() + apiParam.getRequestType() + "/" + apiParam.getEntityId() +"/unread/count";
            Response getUnreadReminderCountResponse2 = qUtils.getUnreadReminderCountResponse(conType, urlEndPoint, apiParam.requestType,null);
            assertEquals(getUnreadReminderCountResponse2.getStatusCode(),SUCCESS_WITH_CONTENT);
            assertEquals(new JSONArray(getUnreadReminderCountResponse2.prettyPrint().toString()).length(),totalUnreadReminders);

            // Get all reminders by date

            urlEndPoint = restUrl + ":"+ apiParam.getPort() + apiParam.getRequestType() + "/" + apiParam.getEntityId() +"/date";
            Response getUnreadReminderByDateResponse2 = qUtils.getReminderByDateResponse(conType, urlEndPoint, apiParam.requestType,null);
            assertEquals(getUnreadReminderByDateResponse2.getStatusCode(),SUCCESS_WITH_CONTENT);

            // Get all reminders

            urlEndPoint = restUrl + ":"+ apiParam.getPort() + apiParam.getRequestType() + "/" + apiParam.getEntityId() +"/";
            Response getAllResponse = qUtils.getAllReminderResponse(conType, urlEndPoint, apiParam.requestType,null);
            assertEquals(getAllResponse.getStatusCode(),SUCCESS_WITH_CONTENT);

            totalReminders = new JSONArray(getAllReminderResponse.prettyPrint().toString()).length();
            //


        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }
}
