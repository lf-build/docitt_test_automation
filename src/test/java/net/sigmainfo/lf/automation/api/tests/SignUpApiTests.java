package net.sigmainfo.lf.automation.api.tests;

import com.jayway.restassured.response.Response;
import net.sigmainfo.lf.automation.api.constant.ApiParam;
import net.sigmainfo.lf.automation.api.function.APIFuncUtils;
import net.sigmainfo.lf.automation.api.function.ApiPropertiesReader;
import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.common.TestResults;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.testng.Assert.*;

/**
 * Created by shaishav.s on 08-02-2017.
 */
public class SignUpApiTests extends AbstractTests {

    @Autowired
    TestResults testResults;

    @Autowired
    APIFuncUtils apiFuncUtils;

    @Autowired
    ApiParam apiParam;

    @Autowired
    ApiPropertiesReader apiPropertiesReader;

    public static String funcMod="Docitt_Api";

    @AfterClass(alwaysRun = true)
    private void endCasereport() throws IOException, JSONException {

        String funcModule = "Docitt_Api";
        String className = org.apache.commons.lang.StringUtils.substringAfterLast(this.getClass().getName(), ".");
        String description = "Verify  " + org.apache.commons.lang.StringUtils.substringAfterLast(this.getClass().getName(), ".");
        generateReport(className, description,funcMod);
    }

    /**
     * Description          : checkuseravailability
     * Includes             : 1. verifies whether user already exists
     */

    @Test(description = "", groups = {"docittapitests","checkuseravailability","signupapis"})
    public void checkuseravailabilityTest() throws Exception{
        String sTestID = "checkuseravailability";
        String result = "Failed";
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            String urlEndPoint = restUrl + ":"+ apiParam.getPort() + apiParam.getRequestType();
            Response postResponse = apiFuncUtils.postRestRequest(conType, urlEndPoint, apiParam.requestType);
            assertEquals(postResponse.getStatusCode(), 200);
            assertEquals(new JSONObject(postResponse.prettyPrint()).get("username"),apiParam.username);
            assertEquals(new JSONObject(postResponse.prettyPrint()).get("available"),true);
            postResponse.prettyPrint();
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }

    /**
     * Description          : incorrectToken
     * Includes             : 1. verifies response when using incorrect token
     */

    @Test(description = "", groups = {"docittapitests","incorrectToken","signupapis"})
    public void incorrectTokenTest() throws Exception{
        String sTestID = "incorrectToken";
        String result = "Failed";
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            String urlEndPoint = restUrl + ":"+ apiParam.getPort() + apiParam.getRequestType();
            Response postResponse = apiFuncUtils.postRestRequest(conType, urlEndPoint, apiParam.requestType);
            assertEquals(postResponse.getStatusCode(), 401);
            postResponse.prettyPrint();
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }

    /**
     * Description          : blankusername
     * Includes             : 1. verifies response when using blankusername
     */

    @Test(description = "", groups = {"docittapitests","blankusername","signupapis"})
    public void blankUserNameTest() throws Exception{
        String sTestID = "blankusername";
        String result = "Failed";
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            String urlEndPoint = restUrl + ":"+ apiParam.getPort() + apiParam.getRequestType();
            Response postResponse = apiFuncUtils.postRestRequest(conType, urlEndPoint, apiParam.requestType);
            assertEquals(postResponse.getStatusCode(), 400);
            postResponse.prettyPrint();
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }

    /**
     * Description          : incorrectusername
     * Includes             : 1. verifies response when using incorrectusername
     */

    @Test(description = "", groups = {"docittapitests","incorrectusername","signupapis"})
    public void incorrectusernameTest() throws Exception{
        String sTestID = "incorrectusername";
        String result = "Failed";
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            String urlEndPoint = restUrl + ":"+ apiParam.getPort() + apiParam.getRequestType();
            Response postResponse = apiFuncUtils.postRestRequest(conType, urlEndPoint, apiParam.requestType);
            assertEquals(postResponse.getStatusCode(), 400);
            postResponse.prettyPrint();
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }

    /**
     * Description          : checkuserunavailability
     * Includes             : 1. verifies response user is already registered
     */

    @Test(description = "", groups = {"docittapitests","checkuserunavailability","signupapis"})
    public void checkuserunavailabilityTest() throws Exception{
        String sTestID = "checkuserunavailability";
        String result = "Failed";
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            String urlEndPoint = restUrl + ":"+ apiParam.getPort() + apiParam.getRequestType();
            Response postResponse = apiFuncUtils.postRestRequest(conType, urlEndPoint, apiParam.requestType);
            assertEquals(postResponse.getStatusCode(), 200);
            assertEquals(new JSONObject(postResponse.prettyPrint()).get("username"),apiParam.username);
            assertEquals(new JSONObject(postResponse.prettyPrint()).get("available"),false);
            postResponse.prettyPrint();
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }

    /**
     * Description          : borrowersignupflow
     * Includes             : 1. verifies complete borrowersignupflow
     */

    @Test(description = "", groups = {"docittapitests","borrowersignupflow","signupapis"})
    public void borrowersignupflowTest() throws Exception{
        String sTestID = "borrowersignupflow";
        String result = "Failed";
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            // BORROWER SIGN UP REQUEST
            String urlEndPoint = restUrl + ":"+ apiParam.getPort() + "/";
            Response borrowerSignUpResp = apiFuncUtils.putRestRequest(conType, urlEndPoint, apiParam.requestType);
            apiFuncUtils.verifyBorrowerSignUpPutResponse(borrowerSignUpResp);
            // VERIFYING THE TOKEN GENERATED
            apiParam.requestType = "verify-token";
            urlEndPoint = restUrl + ":"+ apiParam.getPort() + "/" + apiParam.requestType;
            String refID = new JSONObject(borrowerSignUpResp.prettyPrint()).get("id").toString() ;
            String emailVerificationToken = new JSONObject(borrowerSignUpResp.prettyPrint()).get("emailVerificationToken").toString() ;
            Response verifyTokenResponse = apiFuncUtils.putVerifyTokenRequest(conType, urlEndPoint, apiParam.requestType,refID,emailVerificationToken);
            apiFuncUtils.verifyTokenResponse(verifyTokenResponse,refID);
            // SENDING OTP
            apiParam.requestType = "sendotp";
            urlEndPoint = restUrl + ":5066/9999999999/91/6/" + apiParam.requestType;
            Response getOtpResponse = apiFuncUtils.triggerGetRequest(conType, urlEndPoint);
            apiFuncUtils.verifygetOtpResponse(getOtpResponse);
            // VERIFY OTP
            apiParam.requestType = "verify";
            urlEndPoint = restUrl + ":5066/9999999999/91/123456/" + apiParam.requestType;
            Response getVerifyOtpResponse = apiFuncUtils.triggerGetRequest(conType, urlEndPoint);
            apiFuncUtils.verifygetOtpVerification(getVerifyOtpResponse);

            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }

    /**
     * Description          : noroleborrowersignupflow
     * Includes             : 1. verifies borrowersignupflow when no role is specified
     */

    @Test(description = "", groups = {"docittapitests","noroleborrowersignupflow","signupapis"})
    public void noroleborrowersignupflowTest() throws Exception{
        String sTestID = "noroleborrowersignupflow";
        String result = "Failed";
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            // BORROWER SIGN UP REQUEST
            String urlEndPoint = restUrl + ":"+ apiParam.getPort() + "/";
            Response borrowerSignUpResp = apiFuncUtils.putRestRequest(conType, urlEndPoint, apiParam.requestType);
            assertEquals(borrowerSignUpResp.getStatusCode(),400);
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }

    /**
     * Description          : Incorrectroleborrowersignupflow
     * Includes             : 1. verifies borrowersignupflow when inappropriate role is specified
     */

    @Test(description = "", groups = {"docittapitests","Incorrectroleborrowersignupflow","signupapis"})
    public void IncorrectroleborrowersignupflowTest() throws Exception{
        String sTestID = "Incorrectroleborrowersignupflow";
        String result = "Failed";
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            // BORROWER SIGN UP REQUEST
            String urlEndPoint = restUrl + ":"+ apiParam.getPort() + "/";
            Response borrowerSignUpResp = apiFuncUtils.putRestRequest(conType, urlEndPoint, apiParam.requestType);
            assertEquals(borrowerSignUpResp.getStatusCode(),400);
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }
}
