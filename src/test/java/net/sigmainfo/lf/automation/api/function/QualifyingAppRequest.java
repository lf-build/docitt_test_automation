package net.sigmainfo.lf.automation.api.function;

import org.springframework.stereotype.Component;

/**
 * Created by           : Shaishav.s on 23-02-2017.
 * Test class           : QualifyingAppRequest.java
 * Description          : QualifyingAppRequest
 * Includes             : 1. Declaration of members which makes this request
 *                        2. Getter and setter methods
 */
@Component
public class QualifyingAppRequest {


    private int SectionId;
    private String QuestionId;
    private String Answer;

    public int getSectionId() {
        return SectionId;
    }

    public void setSectionId(int sectionId) {
        SectionId = sectionId;
    }

    public String getQuestionId() {
        return QuestionId;
    }

    public void setQuestionId(String questionId) {
        QuestionId = questionId;
    }

    public String getAnswer() {
        return Answer;
    }

    public void setAnswer(String answer) {
        Answer = answer;
    }

    public String getSubSectionName() {
        return subSectionName;
    }

    public void setSubSectionName(String subSectionName) {
        this.subSectionName = subSectionName;
    }

    private String subSectionName;



}
