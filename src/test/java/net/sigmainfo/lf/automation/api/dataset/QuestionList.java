package net.sigmainfo.lf.automation.api.dataset;

import java.util.ArrayList;

/**
 * Created by           : Shaishav.s on 09-03-2017.
 * Test class           : QuestionList.java
 * Description          : Contains members making a complete QuestionList class within automation
 * Includes             : 1. Declares members within the class
 *                        2. Contains getter and setter methods
 */
public class QuestionList
{
    private String questionId;

    public String getQuestionId() { return this.questionId; }

    public void setQuestionId(String questionId) { this.questionId = questionId; }

    private String questionText;

    public String getQuestionText() { return this.questionText; }

    public void setQuestionText(String questionText) { this.questionText = questionText; }

    private ArrayList<AvailableAns> availableAns;

    public ArrayList<AvailableAns> getAvailableAns() { return this.availableAns; }

    public void setAvailableAns(ArrayList<AvailableAns> availableAns) { this.availableAns = availableAns; }

    private ArrayList<String> availableAnsList;

    public ArrayList<String> getAvailableAnsList() { return this.availableAnsList; }

    public void setAvailableAnsList(ArrayList<String> availableAnsList) { this.availableAnsList = availableAnsList; }

    private int seqNo;

    public int getSeqNo() { return this.seqNo; }

    public void setSeqNo(int seqNo) { this.seqNo = seqNo; }

    private String questionType;

    public String getQuestionType() { return this.questionType; }

    public void setQuestionType(String questionType) { this.questionType = questionType; }

    private boolean required;

    public boolean getRequired() { return this.required; }

    public void setRequired(boolean required) { this.required = required; }

    private String answer;

    public String getAnswer() { return this.answer; }

    public void setAnswer(String answer) { this.answer = answer; }

    private ArrayList<ChildQuestion> childQuestions;

    public ArrayList<ChildQuestion> getChildQuestions() { return this.childQuestions; }

    public void setChildQuestions(ArrayList<ChildQuestion> childQuestions) { this.childQuestions = childQuestions; }

    private String questionICON;

    public String getQuestionICON() { return this.questionICON; }

    public void setQuestionICON(String questionICON) { this.questionICON = questionICON; }

    private String templateName;

    public String getTemplateName() { return this.templateName; }

    public void setTemplateName(String templateName) { this.templateName = templateName; }

    private String templateVersion;

    public String getTemplateVersion() { return this.templateVersion; }

    public void setTemplateVersion(String templateVersion) { this.templateVersion = templateVersion; }

    private boolean isParentQuestion;

    public boolean getIsParentQuestion() { return this.isParentQuestion; }

    public void setIsParentQuestion(boolean isParentQuestion) { this.isParentQuestion = isParentQuestion; }

    private String width;

    public String getWidth() { return this.width; }

    public void setWidth(String width) { this.width = width; }

    private String helpText;

    public String getHelpText() { return this.helpText; }

    public void setHelpText(String helpText) { this.helpText = helpText; }

    private String reg;

    public String getReg() { return this.reg; }

    public void setReg(String reg) { this.reg = reg; }
}
