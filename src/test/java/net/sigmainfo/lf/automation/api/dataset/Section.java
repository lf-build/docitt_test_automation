package net.sigmainfo.lf.automation.api.dataset;

import java.util.ArrayList;

/**
 * Created by           : Shaishav.s on 09-03-2017.
 * Test class           : Section.java
 * Description          : Contains members making a complete Section class within automation
 * Includes             : 1. Declares members within the class
 *                        2. Contains getter and setter methods
 */
public class Section
{
    private int sectionId;

    public int getSectionId() { return this.sectionId; }

    public void setSectionId(int sectionId) { this.sectionId = sectionId; }

    private String sectionName;

    public String getSectionName() { return this.sectionName; }

    public void setSectionName(String sectionName) { this.sectionName = sectionName; }

    private ArrayList<SubSection> subSections;

    public ArrayList<SubSection> getSubSections() { return this.subSections; }

    public void setSubSections(ArrayList<SubSection> subSections) { this.subSections = subSections; }

    private int seqNo;

    public int getSeqNo() { return this.seqNo; }

    public void setSeqNo(int seqNo) { this.seqNo = seqNo; }

    private String bgClass;

    public String getBgClass() { return this.bgClass; }

    public void setBgClass(String bgClass) { this.bgClass = bgClass; }
}
