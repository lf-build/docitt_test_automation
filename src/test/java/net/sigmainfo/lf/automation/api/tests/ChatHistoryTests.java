package net.sigmainfo.lf.automation.api.tests;

import com.jayway.restassured.response.Response;
import net.sigmainfo.lf.automation.api.constant.ApiParam;
import net.sigmainfo.lf.automation.api.function.ApiPropertiesReader;
import net.sigmainfo.lf.automation.api.function.QuestionnaireUtils;
import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.common.TestResults;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

/**
 * Created by shaishav.s on 04-04-2017.
 */
public class ChatHistoryTests extends AbstractTests {

    @Autowired
    TestResults testResults;

    @Autowired
    QuestionnaireUtils qUtils;

    @Autowired
    ApiPropertiesReader apiPropertiesReader;

    @Autowired
    ApiParam apiParam;

    public static String funcMod="Docitt_Api";

    @AfterClass(alwaysRun = true)
    private void endCasereport() throws Exception {


        String funcModule = "Docitt_Api";
        String className = org.apache.commons.lang.StringUtils.substringAfterLast(this.getClass().getName(), ".");
        String description = "Verify  " + org.apache.commons.lang.StringUtils.substringAfterLast(this.getClass().getName(), ".");
        generateReport(className, description,funcMod);
    }

    /**
     * Description          : getChatHistory
     * Includes             : 1. Fetch chat history for a given user
     */

    @Test(priority=1,description = "", groups = {"docittapitests","chatHistoryTests","getChatHistory"})
    public void VerifygetChatHistoryTest() throws Exception{
        String sTestID = "getChatHistory";
        String result = "Failed";
        String toUser="1234";
        String fromUser="user5";
        int index=0;
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            String urlEndPoint = restUrl + ":"+ apiParam.getPort() +"/" + toUser + "/" + fromUser + "/" + index + apiParam.getRequestType();
            // Get Chat History
            Response getChatHistoryResp = qUtils.getChatHistoryResposne(conType, urlEndPoint, apiParam.requestType);
            assertEquals(getChatHistoryResp.getStatusCode(),200);
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }
}
