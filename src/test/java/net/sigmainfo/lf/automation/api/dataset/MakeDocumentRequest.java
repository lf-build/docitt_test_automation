package net.sigmainfo.lf.automation.api.dataset;

import org.springframework.stereotype.Component;

/**
 * Created by           : Shaishav.s on 10-04-2017.
 * Test class           : MakeDocumentRequest.java
 * Description          : Contains members making a complete MakeDocumentRequest class within automation
 * Includes             : 1. Declares members within the class
 *                        2. Contains getter and setter methods
 */
@Component
public class MakeDocumentRequest {
    private String priority;
    private data data;
    private String dueDate;


    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public net.sigmainfo.lf.automation.api.dataset.data getData() {
        return data;
    }

    public void setData(net.sigmainfo.lf.automation.api.dataset.data data) {
        this.data = data;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }


}
