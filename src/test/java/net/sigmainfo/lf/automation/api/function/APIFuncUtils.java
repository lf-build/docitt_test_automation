package net.sigmainfo.lf.automation.api.function;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.internal.mapping.Jackson2Mapper;
import com.jayway.restassured.mapper.ObjectMapper;
import com.jayway.restassured.mapper.factory.Jackson2ObjectMapperFactory;
import com.jayway.restassured.response.Header;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import net.sigmainfo.lf.automation.api.constant.ApiParam;
import net.sigmainfo.lf.automation.common.AbstractTests;
import org.apache.commons.lang.RandomStringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static com.jayway.restassured.RestAssured.given;
import static org.testng.Assert.assertEquals;

/**
 * Created by           : Shaishav.s on 20-02-2017.
 * Test class           : APIFuncUtils.java
 * Description          : Contains reusable methods used while API automation
 * Includes             : 1. Get request implementation
 *                        2. Post request implementation
 *                        3. Response verification methods
 */
@Component
public class APIFuncUtils extends AbstractTests{
    //private static Logger logger = LoggerFactory.getLogger(APIFuncUtils.class);

    @Autowired
    ApiParam apiParam;

    @Autowired
    CheckUserAvailableRequest checkUserAvailableRequest;

    @Autowired
    BorrowerSignUpRequest borrowerSignUpRequest;

    @Autowired
    ApiPropertiesReader apiPropertiesReader;

    @Autowired
    VerifyTokenRequest verifyTokenRequest;


    public Response postRestRequest(String conType, String endPoint, String reqType) {
        RequestSpecification requestSpec = createRequestBody(conType,endPoint,reqType,null,null);
        logger.info("=======================================================================================");
        logger.info("Triggering a post request to :" + endPoint );
        logger.info("=======================================================================================");

        Header header = new Header("Authorization","Bearer "+ apiPropertiesReader.getBearertoken());
        Response response =given()
                .header(header)
                .contentType(conType)
                .spec(requestSpec)
                .log().all()
                .when()
                .post(endPoint);


        logger.info("============== Response Code: "+response.getStatusCode()+"=============================");
        return response;
    }

    private RequestSpecification createRequestBody(String conType,String endPoint,String reqType,String refId,String verificationToken) {

        RequestSpecBuilder builder = new RequestSpecBuilder();
        ObjectMapper objectMapper = null;

        if (conType.equalsIgnoreCase("application/json")) {
            objectMapper = new Jackson2Mapper(new Jackson2ObjectMapperFactory() {

                public com.fasterxml.jackson.databind.ObjectMapper create(
                        Class arg0, String arg1) {
                    com.fasterxml.jackson.databind.ObjectMapper objectMapper = new com.fasterxml.jackson.databind.ObjectMapper();
                    objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
                    return objectMapper;
                }

            });

        }
        builder.setContentType(conType);


        if (reqType.equalsIgnoreCase("/check")) {

            logger.info("Constructing request body for checking availability of the user  in " + conType + " format.");

            checkUserAvailableRequest.setUsername(apiParam.username);

            setBody(builder, objectMapper, checkUserAvailableRequest, conType);
        }
        else if(reqType.equalsIgnoreCase("/borrowersignup")){
            logger.info("Constructing request body for checking availability of the user  in " + conType + " format.");
            borrowerSignUpRequest.setName(apiParam.name);
            borrowerSignUpRequest.setEmail(apiParam.email);
            borrowerSignUpRequest.setUsername(apiParam.username);
            List<String> roles = new ArrayList();
            roles.add(apiParam.roles);
            borrowerSignUpRequest.setRoles(roles);
            borrowerSignUpRequest.setPassword(apiParam.password);
            borrowerSignUpRequest.setPasswordSalt(apiParam.passwordSalt);
            borrowerSignUpRequest.setUsertype(apiParam.userType);
            borrowerSignUpRequest.setInvitationReferenceId(RandomStringUtils.randomAlphanumeric(6).toLowerCase());
            setBody(builder, objectMapper, borrowerSignUpRequest, conType);
        }
        else if(reqType.equalsIgnoreCase("verify-token")){
            logger.info("Constructing request body for checking availability of the user  in " + conType + " format.");
            verifyTokenRequest.setRefid(refId);
            verifyTokenRequest.setToken(verificationToken);
            setBody(builder, objectMapper, verifyTokenRequest, conType);
        }
        return builder.build();
    }

    private static void setBody(RequestSpecBuilder builder,
                                ObjectMapper objectMapper, Object body, String conType) {

        if (conType.contains("json")) {
            builder.setBody(body, objectMapper);
        } else if (conType.contains("xml")) {
            builder.setBody(body);
        }
    }

    public Response putRestRequest(String conType, String urlEndPoint, String requestType) {
        RequestSpecification requestSpec = createRequestBody(conType,urlEndPoint,requestType,null,null);
        logger.info("=======================================================================================");
        logger.info("Triggering a put request to :" + urlEndPoint );
        logger.info("=======================================================================================");

        Header header = new Header("Authorization","Bearer "+ apiPropertiesReader.getBearertoken());
        Response response =given()
                .header(header)
                .contentType(conType)
                .spec(requestSpec)
                .log().all()
                .when()
                .put(urlEndPoint);


        logger.info("============== Response Code: "+response.getStatusCode()+"=============================");
        return response;
    }

    public void verifyBorrowerSignUpPutResponse(Response postResponse) {
        assertEquals(postResponse.getStatusCode(), 200);
        assertEquals(new JSONObject(postResponse.prettyPrint()).get("name"),apiParam.name);
        assertEquals(new JSONObject(postResponse.prettyPrint()).get("email"),apiParam.email);
        assertEquals(new JSONObject(postResponse.prettyPrint()).get("username"),apiParam.username);
        assertEquals(new JSONObject(postResponse.prettyPrint()).get("password"),apiParam.password);
        assertEquals(new JSONObject(postResponse.prettyPrint()).get("passwordSalt"),apiParam.passwordSalt);
       }

    public Response putVerifyTokenRequest(String conType, String urlEndPoint, String requestType,String refId, String verificationToken) {
        RequestSpecification requestSpec = createRequestBody(conType,urlEndPoint,requestType,refId,verificationToken);
        logger.info("=======================================================================================");
        logger.info("Triggering a post request to :" + urlEndPoint );
        logger.info("=======================================================================================");

        Header header = new Header("Authorization","Bearer "+ apiPropertiesReader.getBearertoken());
        Response response =given()
                .header(header)
                .contentType(conType)
                .spec(requestSpec)
                .log().all()
                .when()
                .put(urlEndPoint);


        logger.info("============== Response Code: "+response.getStatusCode()+"=============================");
        return response;
    }

    public void verifyTokenResponse(Response verifyTokenResponse, String refID) {
        assertEquals(verifyTokenResponse.getStatusCode(), 200);
        assertEquals(new JSONObject(verifyTokenResponse.prettyPrint()).get("refid"),refID);
        assertEquals(new JSONObject(verifyTokenResponse.prettyPrint()).get("verified"),true);
    }

    public Response triggerGetRequest(String conType, String urlEndPoint) {

        logger.info("=======================================================================================");
        logger.info("Triggering a get request to :" + urlEndPoint );
        logger.info("=======================================================================================");

        Header header = new Header("Authorization","Bearer "+ apiPropertiesReader.getBearertoken());
        Response response =given()
                .header(header)
                .contentType(conType)
                .log().all()
                .when()
                .get(urlEndPoint);


        logger.info("============== Response Code: "+response.getStatusCode()+"=============================");
        return response;
    }

    public void verifygetOtpResponse(Response getOtpResponse) {
        assertEquals(getOtpResponse.getStatusCode(), 200);
        assertEquals(new JSONObject(getOtpResponse.prettyPrint()).get("status"),"Success");
        assertEquals(new JSONObject(getOtpResponse.prettyPrint()).get("success"),true);
        /*assertEquals(new JSONObject(getOtpResponse.prettyPrint()).get("error_Code"),null);
        assertEquals(new JSONObject(getOtpResponse.prettyPrint()).get("errors"),null);*/
        assertEquals(new JSONObject(getOtpResponse.prettyPrint()).get("message").toString().contains(" +91 999-999-9999"),true);

    }

    public void verifygetOtpVerification(Response getVerifyOtpResponse) {
        assertEquals(getVerifyOtpResponse.getStatusCode(), 200);
        assertEquals(new JSONObject(getVerifyOtpResponse.prettyPrint()).get("status"),"Success");
        assertEquals(new JSONObject(getVerifyOtpResponse.prettyPrint()).get("success"),false);
        /*assertEquals(new JSONObject(getVerifyOtpResponse.prettyPrint()).get("error_Code"),null);
        assertEquals(new JSONObject(getVerifyOtpResponse.prettyPrint()).get("errors"),null);*/
        assertEquals(new JSONObject(getVerifyOtpResponse.prettyPrint()).get("message"),"Verification code is incorrect");
    }
}
