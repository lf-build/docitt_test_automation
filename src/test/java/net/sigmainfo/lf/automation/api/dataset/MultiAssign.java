package net.sigmainfo.lf.automation.api.dataset;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
/**
 * Created by           : Shaishav.s on 04-04-2017.
 * Test class           : MultiAssign.java
 * Description          : Contains members making a complete MultiAssign class within automation
 * Includes             : 1. Declares members within the class
 *                        2. Contains getter and setter methods
 */
@Component
public class MultiAssign {


    public JSONObject getSingleInvite() {
        return singleInvite;
    }

    public void setSingleInvite(JSONObject singleInvite) {
        this.singleInvite = singleInvite;
    }

    private JSONObject singleInvite;
}
