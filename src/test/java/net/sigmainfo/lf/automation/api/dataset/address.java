package net.sigmainfo.lf.automation.api.dataset;

/**
 * Created by           : Shaishav.s on 10-04-2017.
 * Test class           : address.java
 * Description          : Contains members making a complete address class within automation
 * Includes             : 1. Declares members within the class
 *                        2. Contains getter and setter methods
 */
import org.springframework.stereotype.Component;

@Component
public class address {
    private String city;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
