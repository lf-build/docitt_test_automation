package net.sigmainfo.lf.automation.api.dataset;

import java.util.ArrayList;

/**
 * Created by           : Shaishav.s on 09-03-2017.
 * Test class           : SubSection.java
 * Description          : Contains members making a complete SubSection class within automation
 * Includes             : 1. Declares members within the class
 *                        2. Contains getter and setter methods
 */
public class SubSection
{
    private String subSectionName;

    public String getSubSectionName() { return this.subSectionName; }

    public void setSubSectionName(String subSectionName) { this.subSectionName = subSectionName; }

    private ArrayList<QuestionSection> questionSections;

    public ArrayList<QuestionSection> getQuestionSections() { return this.questionSections; }

    public void setQuestionSections(ArrayList<QuestionSection> questionSections) { this.questionSections = questionSections; }

    private int seqNo;

    public int getSeqNo() { return this.seqNo; }

    public void setSeqNo(int seqNo) { this.seqNo = seqNo; }

    private String bgClass;

    public String getBgClass() { return this.bgClass; }

    public void setBgClass(String bgClass) { this.bgClass = bgClass; }
}
