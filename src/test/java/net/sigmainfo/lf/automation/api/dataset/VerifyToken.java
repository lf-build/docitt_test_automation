package net.sigmainfo.lf.automation.api.dataset;

import org.springframework.stereotype.Component;

/**
 * Created by           : Shaishav.s on 04-04-2017.
 * Test class           : VerifyToken.java
 * Description          : Contains members making a complete VerifyToken class within automation
 * Includes             : 1. Declares members within the class
 *                        2. Contains getter and setter methods
 */
@Component
public class VerifyToken {
    private String InviteRefId;

    public String getInviteRefId() {
        return InviteRefId;
    }

    public void setInviteRefId(String inviteRefId) {
        InviteRefId = inviteRefId;
    }

    public String getInviteEmail() {
        return InviteEmail;
    }

    public void setInviteEmail(String inviteEmail) {
        InviteEmail = inviteEmail;
    }

    private String InviteEmail;
}
