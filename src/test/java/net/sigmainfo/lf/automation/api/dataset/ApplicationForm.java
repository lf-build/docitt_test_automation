package net.sigmainfo.lf.automation.api.dataset;

import java.util.ArrayList;

/**
 * Created by           : Shaishav.s on 09-03-2017.
 * Test class           : ApplicationForm.java
 * Description          : Contains members making a complete ApplicationForm class within automation
 * Includes             : 1. Declares members within the class
 *                        2. Contains getter and setter methods
 */
public class ApplicationForm
{
    private String formId;

    public String getFormId() { return this.formId; }

    public void setFormId(String formId) { this.formId = formId; }

    private String formName;

    public String getFormName() { return this.formName; }

    public void setFormName(String formName) { this.formName = formName; }

    private ArrayList<Section> sections;

    public ArrayList<Section> getSections() { return this.sections; }

    public void setSections(ArrayList<Section> sections) { this.sections = sections; }

    private String id;

    public String getId() { return this.id; }

    public void setId(String id) { this.id = id; }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    private String userName;
}
