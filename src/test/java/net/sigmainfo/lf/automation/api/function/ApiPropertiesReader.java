package net.sigmainfo.lf.automation.api.function;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by           : Shaishav.s on 08-02-2017.
 * Test class           : ApiPropertiesReader.java
 * Description          : Reads api.properties key values declared
 * Includes             : 1. Declaration of keys declared in property file
 *                        2. Getter and setter methods
 */
@Component
public class ApiPropertiesReader {

    @Value(value = "${baseresturl}")
    private String baseresturl;

    public String getBearertoken() {
        return bearertoken;
    }

    public void setBearertoken(String bearertoken) {
        this.bearertoken = bearertoken;
    }

    @Value(value = "${bearertoken}")
    private String bearertoken;

    public String getBaseresturl() {
        return baseresturl;
    }

    public void setBaseresturl(String baseresturl) {
        this.baseresturl = baseresturl;
    }



    public String getCustom_report_location() {
        return custom_report_location;
    }

    public void setCustom_report_location(String custom_report_location) {
        this.custom_report_location = custom_report_location;
    }

    @Value(value = "${custom_report_location}")
    private String custom_report_location;

    @Value(value = "${MongoHost}")
    private String MongoHost;

    public String getMongoHost() {
        return MongoHost;
    }

    public void setMongoHost(String mongoHost) {
        MongoHost = mongoHost;
    }

    public int getMongoPort() {
        return MongoPort;
    }

    public void setMongoPort(int mongoPort) {
        MongoPort = mongoPort;
    }

    @Value(value = "${MongoPort}")
    private int MongoPort;

    public String getBeareruser() {
        return beareruser;
    }

    public void setBeareruser(String beareruser) {
        this.beareruser = beareruser;
    }

    @Value(value = "${beareruser}")
    private String beareruser;

    @Value(value="${approveDocument}")
    private String approveDocument;

    @Value(value="${rejectDocument}")
    private String rejectDocument;

    public String getApproveDocument() {
        return approveDocument;
    }

    public void setApproveDocument(String approveDocument) {
        this.approveDocument = approveDocument;
    }

    public String getRejectDocument() {
        return rejectDocument;
    }

    public void setRejectDocument(String rejectDocument) {
        this.rejectDocument = rejectDocument;
    }
}
