package net.sigmainfo.lf.automation.api.dataset;

import org.springframework.stereotype.Component;

/**
 * Created by           : Shaishav.s on 28-03-2017.
 * Test class           : UpdateInvite.java
 * Description          : Contains members making a complete UpdateInvite class within automation
 * Includes             : 1. Declares members within the class
 *                        2. Contains getter and setter methods
 */
@Component
public class UpdateInvite {
    private String InviteRefId;
    private String UserId;
    private String UserName;

    public String getInviteRefId() {
        return InviteRefId;
    }

    public void setInviteRefId(String inviteRefId) {
        InviteRefId = inviteRefId;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }




}
