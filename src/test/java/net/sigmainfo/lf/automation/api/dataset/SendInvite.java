package net.sigmainfo.lf.automation.api.dataset;

import org.springframework.stereotype.Component;

/**
 * Created by           : Shaishav.s on 28-03-2017.
 * Test class           : SendInvite.java
 * Description          : Contains members making a complete SendInvite class within automation
 * Includes             : 1. Declares members within the class
 *                        2. Contains getter and setter methods
 */
@Component
public class SendInvite {

    private String InvitedBy;
    private String InviteEmail;
    private String InviteName;
    private String InviteMobileNumber;
    private String role;
    private boolean lenderTeam;

    public boolean isLenderTeam() {
        return lenderTeam;
    }

    public void setLenderTeam(boolean lenderTeam) {
        this.lenderTeam = lenderTeam;
    }



    public String getInvitedBy() {
        return InvitedBy;
    }

    public void setInvitedBy(String invitedBy) {
        InvitedBy = invitedBy;
    }

    public String getInviteEmail() {
        return InviteEmail;
    }

    public void setInviteEmail(String inviteEmail) {
        InviteEmail = inviteEmail;
    }

    public String getInviteName() {
        return InviteName;
    }

    public void setInviteName(String inviteName) {
        InviteName = inviteName;
    }

    public String getInviteMobileNumber() {
        return InviteMobileNumber;
    }

    public void setInviteMobileNumber(String inviteMobileNumber) {
        InviteMobileNumber = inviteMobileNumber;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
