package net.sigmainfo.lf.automation.api.function;

/**
 * Created by           : Shaishav.s on 24-02-2017.
 * Test class           : VerifyTokenRequest.java
 * Description          : VerifyTokenRequest
 * Includes             : 1. Declaration of members which makes this request
 *                        2. Getter and setter methods
 */
import org.springframework.stereotype.Component;

@Component
public class VerifyTokenRequest {
    public  String refid;

    public String getRefid() {
        return refid;
    }

    public void setRefid(String refid) {
        this.refid = refid;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String token;
}
