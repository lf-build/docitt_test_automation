package net.sigmainfo.lf.automation.api.dataset;

import org.springframework.stereotype.Component;

/**
 * Created by shaishav.s on 27-04-2017.
 */
@Component
public class eventDate {

        private String eventDate;

    public String getEventDate() { return this.eventDate; }

    public void setEventDate(String eventDate) { this.eventDate = eventDate; }
}

