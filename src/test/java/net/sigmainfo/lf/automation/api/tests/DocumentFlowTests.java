package net.sigmainfo.lf.automation.api.tests;

import com.jayway.restassured.response.Response;
import net.sigmainfo.lf.automation.api.constant.ApiParam;
import net.sigmainfo.lf.automation.api.function.ApiPropertiesReader;
import net.sigmainfo.lf.automation.api.function.QuestionnaireUtils;
import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.common.TestResults;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

/**
 * Created by shaishav.s on 10-04-2017.
 */
public class DocumentFlowTests extends AbstractTests {

    @Autowired
    TestResults testResults;

    @Autowired
    QuestionnaireUtils qUtils;

    @Autowired
    ApiPropertiesReader apiPropertiesReader;

    @Autowired
    ApiParam apiParam;

    public static String funcMod="Docitt_Api";

    @AfterClass(alwaysRun = true)
    private void endCasereport() throws Exception {


        String funcModule = "Docitt_Api";
        String className = org.apache.commons.lang.StringUtils.substringAfterLast(this.getClass().getName(), ".");
        String description = "Verify  " + org.apache.commons.lang.StringUtils.substringAfterLast(this.getClass().getName(), ".");
        generateReport(className, description,funcMod);
    }

    /**
     * Description          : documentApprovalFlow
     * Includes             : 1. Document approval flow which includes :
     *                          a. Get request list --> Borrower portal
     *                          b. Create a document request --> Back Office
     *                          c. Upload a document --> Borrower portal
     *                          d. Download a document --> Back Office
     *                          e. Approve document --> Back Office
     *                          f. Get Document List --> Borrower Portal + Back Office
     *                          g. Verify status of the request after successive steps in mongodb
     */

    @Test(priority=1,description = "", groups = {"docittapitests","DocumentFlowTests","documentApprovalFlow"})
    public void VerifydocumentApprovalFlowTest() throws Exception{
        String sTestID = "documentApprovalFlow";
        String result = "Failed";
        int requestsize=0;
        boolean isApprove=true;
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            String urlEndPoint = restUrl + ":"+ apiParam.getPort() + apiParam.getRequestType() + "/"+ apiParam.username;
            // Get Request List --> From Borrower
            Response getRequestListResp = qUtils.getRequestListResposne(conType, urlEndPoint, apiParam.requestType);
            assertEquals(getRequestListResp.getStatusCode(),200);
            requestsize = new JSONArray(getRequestListResp.prettyPrint().toString()).length();
            logger.info("requestsize :" +requestsize);
            // Create Request from back office --> From Backoffice
            Response getMakeRequestResp = qUtils.postMakeRequestResponse(conType, urlEndPoint, apiParam.requestType,apiParam.typeOfRequest);
            assertEquals(getMakeRequestResp.getStatusCode(),204);
            // Get Request List --> From Borrower
            Response getRequestListResp1 = qUtils.getRequestListResposne(conType, urlEndPoint, apiParam.requestType);
            assertEquals(getRequestListResp1.getStatusCode(),200);
            int reqSize = new JSONArray (getRequestListResp1.prettyPrint().toString()).length();
            String requestId = new JSONArray (getRequestListResp1.prettyPrint().toString()).getJSONObject(reqSize-1).getString("id").toString();
            assertEquals(new JSONArray (getRequestListResp1.prettyPrint().toString()).getJSONObject(reqSize-1).getString("status").toString(),"Pending");
            //upload the document --> From Borrower
            urlEndPoint = restUrl + ":5050"+ apiParam.getRequestType() + "/"+ apiParam.username;
            Response uploadDocResp = qUtils.postCreateUploadRequest(conType, urlEndPoint, apiParam.requestType,requestId,apiParam.typeOfRequest);
            assertEquals(uploadDocResp.getStatusCode(),200);
            assertEquals(qUtils.getDocumentRequestStatusFromMongo(requestId).toString(),"Submitted");
            String fileId = new JSONObject(uploadDocResp.prettyPrint().toString()).getString("id");
            logger.info("FileId: "+ fileId);
            // Download the document --> From Backoffice
            urlEndPoint = urlEndPoint + "/" + fileId + "/download";
            Response downloadFileResp = qUtils.getDownloadFileResponse(conType, urlEndPoint, apiParam.requestType);
            assertEquals(downloadFileResp.getStatusCode(),200);
            // Approve the document --> From Backoffice
            urlEndPoint = restUrl + ":5023"+ apiParam.getRequestType() + "/"+ apiParam.username +"/" + requestId + "/verify";
            Response verifyDocumentResp = qUtils.postVerifyDocumentRequest(conType, urlEndPoint, apiParam.requestType,isApprove);
            assertEquals(verifyDocumentResp.getStatusCode(),204);
            assertEquals(qUtils.getDocumentRequestStatusFromMongo(requestId).toString(),"Completed");
            // Get Documents List
            urlEndPoint = restUrl + ":5023"+ apiParam.getRequestType() + "/"+ apiParam.username;
            Response getDocumentListResp = qUtils.getDocumentListResponse(conType, urlEndPoint, apiParam.requestType);
            assertEquals(getDocumentListResp.getStatusCode(),200);
            assertEquals(new JSONArray(getDocumentListResp.prettyPrint().toString()).length(),requestsize+1);
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }

    /**
     * Description          : documentApprovalFlow
     * Includes             : 1. Document approval flow which includes :
     *                          a. Get request list --> Borrower portal
     *                          b. Create a document request --> Back Office
     *                          c. Upload a document --> Borrower portal
     *                          d. Download a document --> Back Office
     *                          e. Reject document with comments--> Back Office
     *                          f. Get Document List --> Borrower Portal + Back Office
     *                          g. Verify status of the request after successive steps in mongodb
     */

    @Test(priority=1,description = "", groups = {"docittapitests","DocumentFlowTests","documentRejectionFlow"})
    public void VerifydocumentRejectionFlowTest() throws Exception{
        String sTestID = "documentRejectionFlow";
        String result = "Failed";
        int requestsize=0;
        boolean isApprove=false;
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            String urlEndPoint = restUrl + ":"+ apiParam.getPort() + apiParam.getRequestType() + "/"+ apiParam.username;
            // Get Request List --> From Borrower
            Response getRequestListResp = qUtils.getRequestListResposne(conType, urlEndPoint, apiParam.requestType);
            assertEquals(getRequestListResp.getStatusCode(),200);
            requestsize = new JSONArray(getRequestListResp.prettyPrint().toString()).length();
            logger.info("requestsize :" +requestsize);
            // Create Request from back office --> From Backoffice
            Response getMakeRequestResp = qUtils.postMakeRequestResponse(conType, urlEndPoint, apiParam.requestType,apiParam.typeOfRequest);
            assertEquals(getMakeRequestResp.getStatusCode(),204);
            // Get Request List --> From Borrower
            Response getRequestListResp1 = qUtils.getRequestListResposne(conType, urlEndPoint, apiParam.requestType);
            assertEquals(getRequestListResp1.getStatusCode(),200);
            int reqSize = new JSONArray (getRequestListResp1.prettyPrint().toString()).length();
            String requestId = new JSONArray (getRequestListResp1.prettyPrint().toString()).getJSONObject(reqSize-1).getString("id").toString();
            assertEquals(new JSONArray (getRequestListResp1.prettyPrint().toString()).getJSONObject(reqSize-1).getString("status").toString(),"Pending");
            //upload the document --> From Borrower
            urlEndPoint = restUrl + ":5050"+ apiParam.getRequestType() + "/"+ apiParam.username;
            Response uploadDocResp = qUtils.postCreateUploadRequest(conType, urlEndPoint, apiParam.requestType,requestId,apiParam.typeOfRequest);
            assertEquals(uploadDocResp.getStatusCode(),200);
            assertEquals(qUtils.getDocumentRequestStatusFromMongo(requestId).toString(),"Submitted");
            String fileId = new JSONObject(uploadDocResp.prettyPrint().toString()).getString("id");
            logger.info("FileId: "+ fileId);
            // Download the document --> From Backoffice
            urlEndPoint = urlEndPoint + "/" + fileId + "/download";
            Response downloadFileResp = qUtils.getDownloadFileResponse(conType, urlEndPoint, apiParam.requestType);
            assertEquals(downloadFileResp.getStatusCode(),200);
            // Approve the document --> From Backoffice
            urlEndPoint = restUrl + ":5023"+ apiParam.getRequestType() + "/"+ apiParam.username +"/" + requestId + "/verify";
            Response verifyDocumentResp = qUtils.postVerifyDocumentRequest(conType, urlEndPoint, apiParam.requestType,isApprove);
            assertEquals(verifyDocumentResp.getStatusCode(),204);
            assertEquals(qUtils.getDocumentRequestStatusFromMongo(requestId).toString(),"Rejected");
            // Get Documents List
            urlEndPoint = restUrl + ":5023"+ apiParam.getRequestType() + "/"+ apiParam.username;
            Response getDocumentListResp = qUtils.getDocumentListResponse(conType, urlEndPoint, apiParam.requestType);
            assertEquals(getDocumentListResp.getStatusCode(),200);
            assertEquals(new JSONArray(getDocumentListResp.prettyPrint().toString()).length(),requestsize+1);
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }
}
