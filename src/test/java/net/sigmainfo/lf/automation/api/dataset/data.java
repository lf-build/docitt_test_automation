package net.sigmainfo.lf.automation.api.dataset;

import org.springframework.stereotype.Component;

/**
 * Created by           : Shaishav.s on 10-04-2017.
 * Test class           : data.java
 * Description          : Contains members making a complete data class within automation
 * Includes             : 1. Declares members within the class
 *                        2. Contains getter and setter methods
 */
@Component
public class data {
    private String applicant;
    private address address;

    public String getApplicant() {
        return applicant;
    }

    public void setApplicant(String applicant) {
        this.applicant = applicant;
    }

    public net.sigmainfo.lf.automation.api.dataset.address getAddress() {
        return address;
    }

    public void setAddress(net.sigmainfo.lf.automation.api.dataset.address address) {
        this.address = address;
    }
}
