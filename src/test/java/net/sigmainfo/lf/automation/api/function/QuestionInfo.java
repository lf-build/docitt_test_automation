package net.sigmainfo.lf.automation.api.function;


import org.springframework.stereotype.Component;
/**
 * Created by           : Shaishav.s on 09-03-2017.
 * Test class           : QuestionInfo.java
 * Description          : QuestionInfo
 * Includes             : 1. Declaration of members which makes this request
 *                        2. Getter and setter methods
 */
@Component
public class QuestionInfo {
    private int SeqNo;
    private Boolean Required;
    private String AvailableAns;
    private String QuestionType;

    public int getSeqNo() {
        return SeqNo;
    }

    public void setSeqNo(int SeqNo) {
        this.SeqNo = SeqNo;
    }

    public Boolean getRequired() {
        return Required;
    }

    public void setRequired(Boolean Required) {
        this.Required = Required;
    }

    public String getAvailableAns() {
        return AvailableAns;
    }

    public void setAvailableAns(String AvailableAns) {
        this.AvailableAns = AvailableAns;
    }

    public String getQuestionType() {
        return QuestionType;
    }

    public void setQuestionType(String QuestionType) {
        this.QuestionType = QuestionType;
    }

    public String getChildQuestions() {
        return ChildQuestions;
    }

    public void setChildQuestions(String ChildQuestions) {
        this.ChildQuestions = ChildQuestions;
    }

    public String getQuestionId() {
        return QuestionId;
    }

    public void setQuestionId(String QuestionId) {
        this.QuestionId = QuestionId;
    }

    public String getQuestionText() {
        return QuestionText;
    }

    public void setQuestionText(String QuestionText) {
        this.QuestionText = QuestionText;
    }

    public String getAnswer() {
        return Answer;
    }

    public void setAnswer(String Answer) {
        this.Answer = Answer;
    }

    private String ChildQuestions;
    private String QuestionId;
    private String QuestionText;
    private String Answer;


}
