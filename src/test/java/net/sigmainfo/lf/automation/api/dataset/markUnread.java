package net.sigmainfo.lf.automation.api.dataset;

import org.springframework.stereotype.Component;

import java.util.ArrayList;

/**
 * Created by shaishav.s on 03-05-2017.
 */
@Component
public class markUnread {

    private ArrayList<String> ids;

    public ArrayList<String> getIds() { return this.ids; }

    public void setIds(ArrayList<String> ids) { this.ids = ids; }
}
