package net.sigmainfo.lf.automation.api.tests;

import com.jayway.restassured.response.Response;
import net.sigmainfo.lf.automation.api.constant.ApiParam;
import net.sigmainfo.lf.automation.api.function.ApiPropertiesReader;
import net.sigmainfo.lf.automation.api.function.QuestionnaireUtils;
import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.common.TestResults;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.testng.Assert.assertEquals;

/**
 * Created by shaishav.s on 26-04-2017.
 */
public class EventApiTests extends AbstractTests{

    @Autowired
    TestResults testResults;

    @Autowired
    QuestionnaireUtils qUtils;

    @Autowired
    ApiPropertiesReader apiPropertiesReader;

    @Autowired
    ApiParam apiParam;

    public static String funcMod="Docitt_Api";

    @AfterClass(alwaysRun = true)
    private void endCasereport() throws Exception {


        String funcModule = "Docitt_Api";
        String className = org.apache.commons.lang.StringUtils.substringAfterLast(this.getClass().getName(), ".");
        String description = "Verify  " + org.apache.commons.lang.StringUtils.substringAfterLast(this.getClass().getName(), ".");
        generateReport(className, description,funcMod);
    }

    /**
     * Description          : createEvent
     * Includes             : 1. Create an event for given applicationId
     */

    @Test(priority=1,description = "", groups = {"docittapitests","eventTests","createEventTest"})
    public void VerifycreateEventTest() throws Exception{
        String sTestID = "createEventTest";
        String result = "Failed";
        int totalEvents=0;
        String collection="eventmanagement";
        String id= null;
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            String urlEndPoint = restUrl + ":"+ apiParam.getPort() + apiParam.getRequestType() + "/" + apiParam.getEntityId();
            totalEvents = qUtils.getEventsForEntityId(apiParam.getEntityId(),collection);
            // Create an event
            Response createEventResp = qUtils.getCreateEventResponse(conType, urlEndPoint, apiParam.requestType,null);
            assertEquals(createEventResp.getStatusCode(),SUCCESS_WITH_NO_CONTENT);
            assertEquals(qUtils.getEventsForEntityId(apiParam.getEntityId(),collection),totalEvents+1);
            id = qUtils.getIdOfLastInsertedRecord(apiParam.getEntityId(),collection);
            logger.info("Document inserted with id :"+id);
            // Send an event as an attachment
            urlEndPoint = restUrl + ":"+ apiParam.getPort() + apiParam.getRequestType() + "/" + apiParam.getEntityId() + "/" + URLDecoder.decode(apiParam.email,"UTF-8") + "/sendtocalendar";
            Response sendEventResponse = qUtils.getCreateEventResponse(conType, urlEndPoint, apiParam.requestType,id);
            assertEquals(sendEventResponse.getStatusCode(),SUCCESS_WITH_CONTENT);
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }

    /**
     * Description          : createEventwithIncorrectEntityId
     * Includes             : 1. Create an event for given applicationId but it contains only special characters
     */

    @Test(priority=1,description = "", groups = {"docittapitests","eventTests","createEventTestWithIncorrectId"})
    public void VerifycreateEventIncorrectEntityIdTest() throws Exception{
        String sTestID = "createEventTestWithIncorrectId";
        String result = "Failed";
        int totalEvents=0;
        String eventId=null;
        String collection="eventmanagement";
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            String urlEndPoint = restUrl + ":"+ apiParam.getPort() + apiParam.getRequestType() + "/" + apiParam.getEntityId();
            totalEvents = qUtils.getEventsForEntityId(apiParam.getEntityId(),collection);
            // Create an event
            Response createEventResp = qUtils.getCreateEventResponse(conType, urlEndPoint, apiParam.requestType,null);
            assertEquals(createEventResp.getStatusCode(),BAD_REQUEST);
            assertEquals(qUtils.getEventsForEntityId(apiParam.getEntityId(),collection),totalEvents);
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }

    @Test(priority=1,description = "", groups = {"docittapitests","eventTests","getEventByDateFlow"})
    public void VerifyEventByDateTest() throws Exception{
        String sTestID = "getEventByDateFlow";
        String result = "Failed";
        int totalEvents=0;
        int totalEventsToday=0;
        String collection="eventmanagement";
        String id= null;
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            String urlEndPoint = null;
            totalEvents = qUtils.getEventsForEntityId(apiParam.getEntityId(),collection);
            // Verify event by date
            urlEndPoint = restUrl + ":"+ apiParam.getPort() + apiParam.getRequestType() + "/" + apiParam.getEntityId() + "/date";
            Response eventByDateResponse = qUtils.getCreateEventResponse(conType, urlEndPoint, apiParam.requestType,id);
            totalEventsToday = new JSONArray(eventByDateResponse.prettyPrint().toString()).length();
            // Create an event
            urlEndPoint = restUrl + ":"+ apiParam.getPort() + apiParam.getRequestType() + "/" + apiParam.getEntityId();
            Response createEventResp = qUtils.getCreateEventResponse(conType, urlEndPoint, apiParam.requestType,null);
            assertEquals(createEventResp.getStatusCode(), SUCCESS_WITH_NO_CONTENT);
            id = qUtils.getIdOfLastInsertedRecord(apiParam.getEntityId(),collection);
            logger.info("Document inserted with id :"+id);
            // Send an event as an attachment
            urlEndPoint = restUrl + ":"+ apiParam.getPort() + apiParam.getRequestType() + "/" + apiParam.getEntityId() + "/" + URLDecoder.decode(apiParam.email,"UTF-8") + "/sendtocalendar";
            Response sendEventResponse = qUtils.getCreateEventResponse(conType, urlEndPoint, apiParam.requestType,id);
            assertEquals(sendEventResponse.getStatusCode(),SUCCESS_WITH_CONTENT);
            // Verify event by date
            urlEndPoint = restUrl + ":"+ apiParam.getPort() + apiParam.getRequestType() + "/" + apiParam.getEntityId() + "/date";
            Response eventByDateResponse1 = qUtils.getCreateEventResponse(conType, urlEndPoint, apiParam.requestType,id);
            assertEquals(eventByDateResponse1.getStatusCode(),SUCCESS_WITH_CONTENT);
            assertEquals(new JSONArray(eventByDateResponse1.prettyPrint().toString()).length(),totalEventsToday + 1);
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }

    @Test(priority=1,description = "", groups = {"docittapitests","eventTests","getEventByMonthFlow"})
    public void VerifyEventByMonthTest() throws Exception{
        String sTestID = "getEventByMonthFlow";
        String result = "Failed";
        int totalEvents=0;
        int totalEventsThisMonth=0;
        String collection="eventmanagement";
        String id= null;
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            String urlEndPoint = null;
            // Verify event by month
            urlEndPoint = restUrl + ":"+ apiParam.getPort() + apiParam.getRequestType() + "/" + apiParam.getEntityId() + "/" + new SimpleDateFormat("MM/yyyy").format(new Date()).toString() +"/month";
            Response eventByMonthResponse = qUtils.getEventByMonthResponse(conType, urlEndPoint, apiParam.requestType,id);
            totalEventsThisMonth = new JSONArray(eventByMonthResponse.prettyPrint().toString()).length();
            // Create an event
            urlEndPoint = restUrl + ":"+ apiParam.getPort() + apiParam.getRequestType() + "/" + apiParam.getEntityId();
            Response createEventResp = qUtils.getCreateEventResponse(conType, urlEndPoint, apiParam.requestType,null);
            assertEquals(createEventResp.getStatusCode(), SUCCESS_WITH_NO_CONTENT);
            id = qUtils.getIdOfLastInsertedRecord(apiParam.getEntityId(),collection);
            logger.info("Document inserted with id :"+id);
            // Send an event as an attachment
            urlEndPoint = restUrl + ":"+ apiParam.getPort() + apiParam.getRequestType() + "/" + apiParam.getEntityId() + "/" + URLDecoder.decode(apiParam.email,"UTF-8") + "/sendtocalendar";
            Response sendEventResponse = qUtils.getCreateEventResponse(conType, urlEndPoint, apiParam.requestType,id);
            assertEquals(sendEventResponse.getStatusCode(),SUCCESS_WITH_CONTENT);
            // Verify event by month
            urlEndPoint = restUrl + ":"+ apiParam.getPort() + apiParam.getRequestType() + "/" + apiParam.getEntityId() + "/" + new SimpleDateFormat("MM/yyyy").format(new Date()).toString() +"/month";
            Response eventByMonthResponse1 = qUtils.getEventByMonthResponse(conType, urlEndPoint, apiParam.requestType,id);
            assertEquals(eventByMonthResponse1.getStatusCode(),SUCCESS_WITH_CONTENT);
            assertEquals(new JSONArray(eventByMonthResponse1.prettyPrint().toString()).length(),totalEventsThisMonth + 1);
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }
}
