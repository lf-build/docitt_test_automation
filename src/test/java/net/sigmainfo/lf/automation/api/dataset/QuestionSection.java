package net.sigmainfo.lf.automation.api.dataset;

import java.util.ArrayList;

/**
 * Created by           : Shaishav.s on 09-03-2017.
 * Test class           : QuestionSection.java
 * Description          : Contains members making a complete QuestionSection class within automation
 * Includes             : 1. Declares members within the class
 *                        2. Contains getter and setter methods
 */
public class QuestionSection
{
    private String questionSectionName;

    public String getQuestionSectionName() { return this.questionSectionName; }

    public void setQuestionSectionName(String questionSectionName) { this.questionSectionName = questionSectionName; }

    private ArrayList<QuestionList> questionList;

    public ArrayList<QuestionList> getQuestionList() { return this.questionList; }

    public void setQuestionList(ArrayList<QuestionList> questionList) { this.questionList = questionList; }

    private boolean lrui;

    public boolean getLrui() { return this.lrui; }

    public void setLrui(boolean lrui) { this.lrui = lrui; }

    private int seqNo;

    public int getSeqNo() { return this.seqNo; }

    public void setSeqNo(int seqNo) { this.seqNo = seqNo; }
}
