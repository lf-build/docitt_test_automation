package net.sigmainfo.lf.automation.api.dataset;

/**
 * Created by           : Shaishav.s on 09-03-2017.
 * Test class           : QuestionnaireResponse.java
 * Description          : Contains members making a complete QuestionnaireResponse class within automation
 * Includes             : 1. Declares members within the class
 *                        2. Contains getter and setter methods
 */
public class QuestionnaireResponse {
    private String temporaryApplicationNumber;

    public String getTemporaryApplicationNumber() { return this.temporaryApplicationNumber; }

    public void setTemporaryApplicationNumber(String temporaryApplicationNumber) { this.temporaryApplicationNumber = temporaryApplicationNumber; }

    private String applicantId;

    public String getApplicantId() { return this.applicantId; }

    public void setApplicantId(String applicantId) { this.applicantId = applicantId; }

    private ApplicationForm applicationForm;

    public ApplicationForm getApplicationForm() { return this.applicationForm; }

    public void setApplicationForm(ApplicationForm applicationForm) { this.applicationForm = applicationForm; }

    private int lastAccessedSection;

    public int getLastAccessedSection() { return this.lastAccessedSection; }

    public void setLastAccessedSection(int lastAccessedSection) { this.lastAccessedSection = lastAccessedSection; }

    private String lastAccessedSubSection;

    public String getLastAccessedSubSection() { return this.lastAccessedSubSection; }

    public void setLastAccessedSubSection(String lastAccessedSubSection) { this.lastAccessedSubSection = lastAccessedSubSection; }

    private String applicationNumber;

    public String getApplicationNumber() { return this.applicationNumber; }

    public void setApplicationNumber(String applicationNumber) { this.applicationNumber = applicationNumber; }

    private String status;

    public String getStatus() { return this.status; }

    public void setStatus(String status) { this.status = status; }

    private int lastAccessedSectionSeqNo;

    public int getLastAccessedSectionSeqNo() { return this.lastAccessedSectionSeqNo; }

    public void setLastAccessedSectionSeqNo(int lastAccessedSectionSeqNo) { this.lastAccessedSectionSeqNo = lastAccessedSectionSeqNo; }

    private int lastAccessedSubSectionSeqNo;

    public int getLastAccessedSubSectionSeqNo() { return this.lastAccessedSubSectionSeqNo; }

    public void setLastAccessedSubSectionSeqNo(int lastAccessedSubSectionSeqNo) { this.lastAccessedSubSectionSeqNo = lastAccessedSubSectionSeqNo; }

    private String id;

    public String getId() { return this.id; }

    public void setId(String id) { this.id = id; }
}
