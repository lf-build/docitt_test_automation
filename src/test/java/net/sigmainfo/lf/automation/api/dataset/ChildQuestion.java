package net.sigmainfo.lf.automation.api.dataset;

/**
 * Created by           : Shaishav.s on 09-03-2017.
 * Test class           : ChildQuestion.java
 * Description          : Contains members making a complete ChildQuestion class within automation
 * Includes             : 1. Declares members within the class
 *                        2. Contains getter and setter methods
 */
public class ChildQuestion
{
    private String answer;

    public String getAnswer() { return this.answer; }

    public void setAnswer(String answer) { this.answer = answer; }

    private String questionId;

    public String getQuestionId() { return this.questionId; }

    public void setQuestionId(String questionId) { this.questionId = questionId; }
}
