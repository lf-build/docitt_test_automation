package net.sigmainfo.lf.automation.api.function;

import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by           : Shaishav.s on 08-02-2017.
 * Test class           : BorrowerSignUpRequest.java
 * Description          : BorrowerSignUpRequest
 * Includes             : 1. Declaration of members which makes this request
 *                        2. Getter and setter methods
 */
@Component
public class BorrowerSignUpRequest {

        String Name;
        String Email;
        String Username;
        String Password;
        String PasswordSalt;
        String Usertype;

    public String getInvitationReferenceId() {
        return invitationReferenceId;
    }

    public void setInvitationReferenceId(String invitationReferenceId) {
        this.invitationReferenceId = invitationReferenceId;
    }

    String invitationReferenceId;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getPasswordSalt() {
        return PasswordSalt;
    }

    public void setPasswordSalt(String passwordSalt) {
        PasswordSalt = passwordSalt;
    }

    public String getUsertype() {
        return Usertype;
    }

    public void setUsertype(String usertype) {
        Usertype = usertype;
    }

    public List<String> getRoles() {
        return Roles;
    }

    public void setRoles(List<String> roles) {
        Roles = roles;
    }

    List<String> Roles;

    }

