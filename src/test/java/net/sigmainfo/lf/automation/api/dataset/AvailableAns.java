package net.sigmainfo.lf.automation.api.dataset;

/**
 * Created by           : Shaishav.s on 09-03-2017.
 * Test class           : AvailableAns.java
 * Description          : Contains members making a complete AvailableAns class within automation
 * Includes             : 1. Declares members within the class
 *                        2. Contains getter and setter methods
 */
public class AvailableAns
{
    private String answer;

    public String getAnswer() { return this.answer; }

    public void setAnswer(String answer) { this.answer = answer; }

    private String icon;

    public String getIcon() { return this.icon; }

    public void setIcon(String icon) { this.icon = icon; }

    private int seqNo;

    public int getSeqNo() { return this.seqNo; }

    public void setSeqNo(int seqNo) { this.seqNo = seqNo; }

    private String label;

    public String getLabel() { return this.label; }

    public void setLabel(String label) { this.label = label; }

    private String value;

    public String getValue() { return this.value; }

    public void setValue(String value) { this.value = value; }
}
