package net.sigmainfo.lf.automation.api.function;

import com.google.gson.Gson;
import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.response.Header;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import com.jayway.restassured.internal.mapping.Jackson2Mapper;
import com.jayway.restassured.mapper.ObjectMapper;
import com.jayway.restassured.mapper.factory.Jackson2ObjectMapperFactory;
import com.mongodb.*;
import net.sigmainfo.lf.automation.api.constant.ApiParam;
import net.sigmainfo.lf.automation.api.dataset.*;
import net.sigmainfo.lf.automation.common.AbstractTests;
import org.apache.commons.lang.RandomStringUtils;
import org.bson.types.ObjectId;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.testng.Assert;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static com.jayway.restassured.RestAssured.given;
import static org.testng.Assert.*;

/**
 * Created by           : Shaishav.s on 08-03-2017.
 * Test class           : QuestionnaireUtils.java
 * Description          : QuestionnaireUtils
 * Includes             : 1. Get requests for different api testcases
 *                        2. Post requests for different api testcases
 *                        3. Reading key values from mongodb
 *                        4.
 */
@Component
public class QuestionnaireUtils extends AbstractTests {

    @Autowired
    ApiPropertiesReader apiPropertiesReader;

    @Autowired
    QuestionInfo qInfo;

    @Autowired
    QualifyingAppRequest qappRequest;

    @Autowired
    ApiParam apiParam;

    @Autowired
    SendInvite sendInviteReq;

    @Autowired
    UpdateInvite updateInviteReq;

    @Autowired
    SingleInvite singleInviteReq;

    @Autowired
    VerifyToken verifyTokenReq;

    @Autowired
    MakeDocumentRequest makeDocumentRequest;

    @Autowired
    data dataRequest;

    @Autowired
    address addressRequest;

    @Autowired
    metadata metadataReq;

    @Autowired
    verifyDocument verifyDocumentRequest;

    @Autowired
    questions questions;

    @Autowired
    createEvent createEventRequest;

    @Autowired
    sendEventAsAttachment sendEventRequest;

    @Autowired
    eventDate eventDateRequest;

    @Autowired
    createReminder createReminderRequest;

    @Autowired
    unreadReminderByDate unreadReminderByDateRequest;

    @Autowired
    markUnread markUnreadRequest;

    public Response postCreateFormRequest(String conType, String urlEndPoint, String requestType) {
        requestType = "/createformtemplate";
        String formName = getFormName();
        String url = urlEndPoint + requestType + "/" + formName+"/Full";
        String requestBody = org.apache.commons.lang.StringUtils.join(new String[]{
                "{\n" +
                        "    \"FormId\" : \"\",\n" +
                        "    \"FormName\" : \"\",\n" +
                        "\t\"Sections\" :[\n" +
                        "     {\"SectionName\": \"Qualifying Details\", \"SectionId\":1, \"SeqNo\": \"1\", \"SubSections\": [\n" +
                        "          {\"SubSectionName\": \"Choose One Option\", \"SeqNo\": \"1\", \"QuestionSections\": [\n" +
                        "               {\"QuestionSectionName\": \"Purchase\", \"LRUI\": true, \"SeqNo\": \"1\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"1\", \"QuestionText\": \"Is this a Purchase or Refinance?\", \"QuestionType\":\"SelectIcon\", \"SeqNo\":\"1\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[\"Purchase\",\"Refinance\",\"Equity Line Of Credit\"],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[{\"Answer\":\"Purchase\", \"Icon\":\"Purchase\", \"SeqNo\": 1, \"Value\":\"Purchase\", \"Label\":\"Purchase\"},{\"Answer\":\"Refinance\", \"Icon\":\"Refinance\", \"SeqNo\": 2, \"Value\":\"Refinance\", \"Label\":\"Refinance\"},{\"Answer\":\"Equity Line Of Credit\", \"Icon\":\"Equity Line Of Credit\", \"SeqNo\": 3, \"Value\":\"Equity Line Of Credit\", \"Label\":\"Equity Line Of Credit\"}], \"Width\":\"40\",\"HelpText\":\"This is purchase\",\"Reg\":\"RegEx\",\"Answer\": \"\", \"Required\":\"false\"  }]},\n" +
                        "               {\"QuestionSectionName\": \"Realtor\", \"LRUI\": true, \"SeqNo\": \"2\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"2\", \"QuestionText\": \"Are you working with a realtor already?\", \"QuestionType\":\"SelectBool\", \"SeqNo\":\"2\",\"IsParentQuestion\": \"FALSE\",\"AvailableAnsList\":[\"TRUE\",\"FALSE\"],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[{\"Answer\":\"TRUE\", \"Icon\":\"TRUE\", \"SeqNo\": 1, \"Value\":\"TRUE\", \"Label\":\"TRUE\"},{\"Answer\":\"FALSE\", \"Icon\":\"FALSE\", \"SeqNo\": 2, \"Value\":\"FALSE\", \"Label\":\"FALSE\"}], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"3\", \"QuestionText\": \"How will you use this property?\", \"QuestionType\":\"Select\", \"SeqNo\":\"3\",\"IsParentQuestion\": \"FALSE\",\"AvailableAnsList\":[\"Primary\",\"Vacation/Secondary\",\"Investment\"],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[{\"Answer\":\"Primary\", \"Icon\":\"Primary\", \"SeqNo\": 1, \"Value\":\"Primary\", \"Label\":\"Primary\"},{\"Answer\":\"Vacation/Secondary\", \"Icon\":\"Vacation/Secondary\", \"SeqNo\": 2, \"Value\":\"Vacation/Secondary\", \"Label\":\"Vacation/Secondary\"},{\"Answer\":\"Investment\", \"Icon\":\"Investment\", \"SeqNo\": 3, \"Value\":\"Investment\", \"Label\":\"Investment\"}], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]},\n" +
                        "\n" +
                        "               {\"QuestionSectionName\": \"Co-Borrower 1\", \"LRUI\": true, \"SeqNo\": \"3\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"5\", \"QuestionText\": \"Will you have a co-borrower?\", \"QuestionType\":\"SelectBool\", \"SeqNo\":\"5\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[\"TRUE\",\"FALSE\"],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[{\"Answer\":\"TRUE\", \"Icon\":\"TRUE\", \"SeqNo\": 1, \"Value\":\"TRUE\", \"Label\":\"TRUE\"},{\"Answer\":\"FALSE\", \"Icon\":\"FALSE\", \"SeqNo\": 2, \"Value\":\"FALSE\", \"Label\":\"FALSE\"}], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"6\", \"QuestionText\": \"Are you a first time home buyer?\", \"QuestionType\":\"SelectBool\", \"SeqNo\":\"6\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[\"TRUE\",\"FALSE\"],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[{\"Answer\":\"TRUE\", \"Icon\":\"TRUE\", \"SeqNo\": 1, \"Value\":\"TRUE\", \"Label\":\"TRUE\"},{\"Answer\":\"FALSE\", \"Icon\":\"FALSE\", \"SeqNo\": 2, \"Value\":\"FALSE\", \"Label\":\"FALSE\"}], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]},\n" +
                        "               {\"QuestionSectionName\": \"Co-Borrower 2\", \"LRUI\": true, \"SeqNo\": \"4\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"7\", \"QuestionText\": \"Are you self-employed?\", \"QuestionType\":\"SelectBool\", \"SeqNo\":\"7\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[\"TRUE\",\"FALSE\"],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[{\"Answer\":\"TRUE\", \"Icon\":\"TRUE\", \"SeqNo\": 1, \"Value\":\"TRUE\", \"Label\":\"TRUE\"},{\"Answer\":\"FALSE\", \"Icon\":\"FALSE\", \"SeqNo\": 2, \"Value\":\"FALSE\", \"Label\":\"FALSE\"}], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"8\", \"QuestionText\": \"Are you a veteran?\", \"QuestionType\":\"SelectBool\", \"SeqNo\":\"8\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[\"TRUE\",\"FALSE\"],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[{\"Answer\":\"TRUE\", \"Icon\":\"TRUE\", \"SeqNo\": 1, \"Value\":\"TRUE\", \"Label\":\"TRUE\"},{\"Answer\":\"FALSE\", \"Icon\":\"FALSE\", \"SeqNo\": 2, \"Value\":\"FALSE\", \"Label\":\"FALSE\"}], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]},\n" +
                        "\n" +
                        "               {\"QuestionSectionName\": \"AddCoBorrower\", \"LRUI\": true, \"SeqNo\": \"3\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"9\", \"QuestionText\": \"Add Co-Borrower Information\", \"QuestionType\":\"Link\", \"SeqNo\":\"9\",\"IsParentQuestion\": \"FALSE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]\n" +
                        "\t\t\t\t}\n" +
                        "\t\t\t]\n" +
                        "\t\t}\n" +
                        "\t]\n" +
                        "},\n" +
                        "\n" +
                        "\t\t\t\t\n" +
                        "     {\"SectionName\": \"Personal Info Details\", \"SectionId\":2, \"SeqNo\": \"2\", \"SubSections\": [\n" +
                        "          {\"SubSectionName\": \"Personal Detail\", \"SeqNo\": \"1\", \"QuestionSections\": [\n" +
                        "               {\"QuestionSectionName\": \"Name\", \"LRUI\": true, \"SeqNo\": \"1\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"10\", \"QuestionText\": \"First Name\", \"QuestionType\":\"Text\", \"SeqNo\":\"10\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"11\", \"QuestionText\": \"Middle Name\", \"QuestionType\":\"Text\", \"SeqNo\":\"11\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"12\", \"QuestionText\": \"Last Name\", \"QuestionType\":\"Text\", \"SeqNo\":\"12\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]},\n" +
                        "\n" +
                        "               {\"QuestionSectionName\": \"Suffix\", \"LRUI\": true, \"SeqNo\": \"2\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"13\", \"QuestionText\": \"Suffix\", \"QuestionType\":\"Text\", \"SeqNo\":\"13\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[\"Mr\",\"Mrs\"],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[{\"Answer\":\"Mr\", \"Icon\":\"Mr\", \"SeqNo\": 1, \"Value\":\"Mr\", \"Label\":\"Mr\"},{\"Answer\":\"Mrs\", \"Icon\":\"Mrs\", \"SeqNo\": 2, \"Value\":\"Mrs\", \"Label\":\"Mrs\"}], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"14\", \"QuestionText\": \"Date of Birth\", \"QuestionType\":\"Date\", \"SeqNo\":\"14\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]},\n" +
                        "\n" +
                        "               {\"QuestionSectionName\": \"YearsinSchool\", \"LRUI\": true, \"SeqNo\": \"3\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"15\", \"QuestionText\": \"Years in School\", \"QuestionType\":\"TextNum2\", \"SeqNo\":\"15\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"16\", \"QuestionText\": \"Marital Status\", \"QuestionType\":\"Select\", \"SeqNo\":\"16\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[\"Single\",\"Married\",\"Separated\"],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[{\"Answer\":\"Single\", \"Icon\":\"Single\", \"SeqNo\": 1, \"Value\":\"Single\", \"Label\":\"Single\"},{\"Answer\":\"Married\", \"Icon\":\"Married\", \"SeqNo\": 2, \"Value\":\"Married\", \"Label\":\"Married\"},{\"Answer\":\"Separated\", \"Icon\":\"Separated\", \"SeqNo\": 3, \"Value\":\"Separated\", \"Label\":\"Separated\"}], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]},\n" +
                        "\n" +
                        "               {\"QuestionSectionName\": \"AddCoBorrower\", \"LRUI\": true, \"SeqNo\": \"4\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"9\", \"QuestionText\": \"Add Co-Borrower Information\", \"QuestionType\":\"Link\", \"SeqNo\":\"9\",\"IsParentQuestion\": \"FALSE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]\n" +
                        "\t\t\t\t}\n" +
                        "\t\t\t]\n" +
                        "\t\t}\n" +
                        "\t]\n" +
                        "},\n" +
                        "\n" +
                        "     {\"SectionName\": \"Contact Info\", \"SectionId\":3, \"SeqNo\": \"3\", \"SubSections\": [\n" +
                        "          {\"SubSectionName\": \"Contact Info\", \"SeqNo\": \"1\", \"QuestionSections\": [\n" +
                        "               {\"QuestionSectionName\": \"Email\", \"LRUI\": true, \"SeqNo\": \"1\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"17\", \"QuestionText\": \"Email\", \"QuestionType\":\"TextEmail\", \"SeqNo\":\"17\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"18\", \"QuestionText\": \"Home Phone Number\", \"QuestionType\":\"TextPhone\", \"SeqNo\":\"18\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"19\", \"QuestionText\": \"Cell Phone Number\", \"QuestionType\":\"TextPhone\", \"SeqNo\":\"19\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]},\n" +
                        "\n" +
                        "               {\"QuestionSectionName\": \"PreferredWay\", \"LRUI\": true, \"SeqNo\": \"2\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"20\", \"QuestionText\": \"Preferred Way to Contact you\", \"QuestionType\":\"Select\", \"SeqNo\":\"20\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[\"Text\",\"Email\",\"Phone Call\"],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[{\"Answer\":\"Text\", \"Icon\":\"Text\", \"SeqNo\": 1, \"Value\":\"Text\", \"Label\":\"Text\"},{\"Answer\":\"Email\", \"Icon\":\"Email\", \"SeqNo\": 2, \"Value\":\"Email\", \"Label\":\"Email\"},{\"Answer\":\"Phone Call\", \"Icon\":\"Phone Call\", \"SeqNo\": 3, \"Value\":\"Phone Call\", \"Label\":\"Phone Call\"}], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]},\n" +
                        "\n" +
                        "\n" +
                        "               {\"QuestionSectionName\": \"AddCoBorrower\", \"LRUI\": true, \"SeqNo\": \"3\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"9\", \"QuestionText\": \"Add Co-Borrower Information\", \"QuestionType\":\"Link\", \"SeqNo\":\"9\",\"IsParentQuestion\": \"FALSE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]\n" +
                        "\t\t\t\t}\n" +
                        "\t\t\t]\n" +
                        "\t\t}\n" +
                        "\t]\n" +
                        "},\n" +
                        " \n" +
                        "\n" +
                        " {\"SectionName\": \"Dependents\", \"SectionId\":4, \"SeqNo\": \"4\", \"SubSections\": [\n" +
                        "          {\"SubSectionName\": \"Dependents\", \"SeqNo\": \"1\", \"QuestionSections\": [\n" +
                        "               {\"QuestionSectionName\": \"Dependents\", \"LRUI\": true, \"SeqNo\": \"1\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"22\", \"QuestionText\": \"Do you have any dependents?\", \"QuestionType\":\"Select\", \"SeqNo\":\"22\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[\"TRUE\",\"FALSE\"],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[{\"Answer\":\"TRUE\", \"Icon\":\"TRUE\", \"SeqNo\": 1, \"Value\":\"TRUE\", \"Label\":\"TRUE\"},{\"Answer\":\"FALSE\", \"Icon\":\"FALSE\", \"SeqNo\": 2, \"Value\":\"FALSE\", \"Label\":\"FALSE\"}], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]},\n" +
                        "\n" +
                        "               {\"QuestionSectionName\": \"AddCoBorrower\", \"LRUI\": true, \"SeqNo\": \"2\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"9\", \"QuestionText\": \"Add Co-Borrower Information\", \"QuestionType\":\"Link\", \"SeqNo\":\"9\",\"IsParentQuestion\": \"FALSE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]\n" +
                        "\t\t\t\t}\n" +
                        "\t\t\t]\n" +
                        "\t\t}\n" +
                        "\t]\n" +
                        "},\n" +
                        "\n" +
                        "     \n" +
                        "\t {\"SectionName\": \"Residence History\", \"SectionId\":5, \"SeqNo\": \"5\", \"SubSections\": [\n" +
                        "          {\"SubSectionName\": \"Residency Info\", \"SeqNo\": \"1\", \"QuestionSections\": [\n" +
                        "               {\"QuestionSectionName\": \"ResidenceInfo\", \"LRUI\": true, \"SeqNo\": \"1\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"23\", \"QuestionText\": \"What is your current address?\", \"QuestionType\":\"Address\", \"SeqNo\":\"23\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]\n" +
                        "\t\t\t\t}\n" +
                        "\t\t\t]\n" +
                        "\t\t},\n" +
                        "\n" +
                        "          {\"SubSectionName\": \"Stay History\", \"SeqNo\": \"2\", \"QuestionSections\": [\n" +
                        "               {\"QuestionSectionName\": \"StayHistory\", \"LRUI\": true, \"SeqNo\": \"2\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"24\", \"QuestionText\": \"How long have you lived at this address?\", \"QuestionType\":\"Label\", \"SeqNo\":\"24\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]},\n" +
                        "\n" +
                        "               {\"QuestionSectionName\": \"StayHistoryLabel\", \"LRUI\": true, \"SeqNo\": \"3\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"1025\", \"QuestionText\": \"From\", \"QuestionType\":\"Label\", \"SeqNo\":\"1025\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"1026\", \"QuestionText\": \"To\", \"QuestionType\":\"Label\", \"SeqNo\":\"1026\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]},\n" +
                        "\n" +
                        "               {\"QuestionSectionName\": \"StayHistory2\", \"LRUI\": true, \"SeqNo\": \"4\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"25\", \"QuestionText\": \"From\", \"QuestionType\":\"Date\", \"SeqNo\":\"25\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"26\", \"QuestionText\": \"To\", \"QuestionType\":\"Date\", \"SeqNo\":\"26\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]},\n" +
                        "\n" +
                        "               {\"QuestionSectionName\": \"Rent\", \"LRUI\": true, \"SeqNo\": \"5\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"27\", \"QuestionText\": \"Did you own or rent?\", \"QuestionType\":\"Select\", \"SeqNo\":\"27\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[\"Rented\",\"Owned\"],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[{\"Answer\":\"Rented\", \"Icon\":\"Rented\", \"SeqNo\": 1, \"Value\":\"Rented\", \"Label\":\"Rented\"},{\"Answer\":\"Owned\", \"Icon\":\"Owned\", \"SeqNo\": 2, \"Value\":\"Owned\", \"Label\":\"Owned\"}], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]},\n" +
                        "\n" +
                        "               {\"QuestionSectionName\": \"MailingAddressVerification\", \"LRUI\": true, \"SeqNo\": \"6\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"28\", \"QuestionText\": \"Is your mailing address the same as your current address?\", \"QuestionType\":\"SelectBool\", \"SeqNo\":\"28\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[\"TRUE\",\"FALSE\"],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[{\"Answer\":\"TRUE\", \"Icon\":\"TRUE\", \"SeqNo\": 1, \"Value\":\"TRUE\", \"Label\":\"TRUE\"},{\"Answer\":\"FALSE\", \"Icon\":\"FALSE\", \"SeqNo\": 2, \"Value\":\"FALSE\", \"Label\":\"FALSE\"}], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]},\n" +
                        "\n" +
                        "               {\"QuestionSectionName\": \"AddCoBorrower\", \"LRUI\": true, \"SeqNo\": \"7\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"9\", \"QuestionText\": \"Add Co-Borrower Information\", \"QuestionType\":\"Link\", \"SeqNo\":\"9\",\"IsParentQuestion\": \"FALSE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]\n" +
                        "\t\t\t\t}\n" +
                        "\t\t\t]\n" +
                        "\t\t}\n" +
                        "\t]\n" +
                        "},\n" +
                        "     \n" +
                        "\t \n" +
                        "\t {\"SectionName\": \"Property Information\", \"SectionId\":6, \"SeqNo\": \"6\", \"SubSections\": [\n" +
                        "          {\"SubSectionName\": \"Property Information\", \"SeqNo\": \"1\", \"QuestionSections\": [\n" +
                        "               {\"QuestionSectionName\": \"Property Queries\", \"LRUI\": true, \"SeqNo\": \"1\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"29\", \"QuestionText\": \"Do you plan to sell your current home?\", \"QuestionType\":\"SelectBool\", \"SeqNo\":\"29\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[\"TRUE\",\"FALSE\"],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[{\"Answer\":\"TRUE\", \"Icon\":\"TRUE\", \"SeqNo\": 1, \"Value\":\"TRUE\", \"Label\":\"TRUE\"},{\"Answer\":\"FALSE\", \"Icon\":\"FALSE\", \"SeqNo\": 2, \"Value\":\"FALSE\", \"Label\":\"FALSE\"}], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"30\", \"QuestionText\": \"Are you currently in contract to purchase a new property?\", \"QuestionType\":\"SelectBool\", \"SeqNo\":\"30\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[\"TRUE\",\"FALSE\"],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[{\"Answer\":\"TRUE\", \"Icon\":\"TRUE\", \"SeqNo\": 1, \"Value\":\"TRUE\", \"Label\":\"TRUE\"},{\"Answer\":\"FALSE\", \"Icon\":\"FALSE\", \"SeqNo\": 2, \"Value\":\"FALSE\", \"Label\":\"FALSE\"}], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"31\", \"QuestionText\": \"Target Home Price\", \"QuestionType\":\"TextCurr\", \"SeqNo\":\"31\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"32\", \"QuestionText\": \"Subject Property TBD\", \"QuestionType\":\"Text\", \"SeqNo\":\"32\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[\"TRUE\",\"FALSE\"],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[{\"Answer\":\"TRUE\", \"Icon\":\"TRUE\", \"SeqNo\": 1, \"Value\":\"TRUE\", \"Label\":\"TRUE\"},{\"Answer\":\"FALSE\", \"Icon\":\"FALSE\", \"SeqNo\": 2, \"Value\":\"FALSE\", \"Label\":\"FALSE\"}], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]},\n" +
                        "\n" +
                        "               {\"QuestionSectionName\": \"NewHome\", \"LRUI\": true, \"SeqNo\": \"2\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"33\", \"QuestionText\": \"Location of new home\", \"QuestionType\":\"Address\", \"SeqNo\":\"33\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]},\n" +
                        "\n" +
                        "               {\"QuestionSectionName\": \"TitleHeldByInfo\", \"LRUI\": true, \"SeqNo\": \"3\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"34\", \"QuestionText\": \"[The Title Will Be Held By:]\", \"QuestionType\":\"Text\", \"SeqNo\":\"34\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]},\n" +
                        "\n" +
                        "               {\"QuestionSectionName\": \"TitleHowInfo\", \"LRUI\": true, \"SeqNo\": \"4\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"35\", \"QuestionText\": \"[How Will the Title be Held?]\", \"QuestionType\":\"Text\", \"SeqNo\":\"35\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]},\n" +
                        "\n" +
                        "               {\"QuestionSectionName\": \"TitleHeldByInfo\", \"LRUI\": true, \"SeqNo\": \"5\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"36\", \"QuestionText\": \"[How Will the Estate be Held?]\", \"QuestionType\":\"Text\", \"SeqNo\":\"36\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]},\n" +
                        "\n" +
                        "               {\"QuestionSectionName\": \"IncludePropertyTaxes\", \"LRUI\": true, \"SeqNo\": \"6\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"37\", \"QuestionText\": \"Would you like to include your Property Taxes and Home Owners' Insurance obligations into your monthly payment(impound account)\", \"QuestionType\":\"SelectBool\", \"SeqNo\":\"37\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[\"TRUE\",\"FALSE\"],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[{\"Answer\":\"TRUE\", \"Icon\":\"TRUE\", \"SeqNo\": 1, \"Value\":\"TRUE\", \"Label\":\"TRUE\"},{\"Answer\":\"FALSE\", \"Icon\":\"FALSE\", \"SeqNo\": 2, \"Value\":\"FALSE\", \"Label\":\"FALSE\"}], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]},\n" +
                        "\n" +
                        "               {\"QuestionSectionName\": \"AddCoBorrower\", \"LRUI\": true, \"SeqNo\": \"7\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"9\", \"QuestionText\": \"Add Co-Borrower Information\", \"QuestionType\":\"Link\", \"SeqNo\":\"9\",\"IsParentQuestion\": \"FALSE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]\n" +
                        "\t\t\t\t}\n" +
                        "\t\t\t]\n" +
                        "\t\t}\n" +
                        "\t]\n" +
                        "},\n" +
                        "\n" +
                        "     {\"SectionName\": \"Down Payments\", \"SectionId\":7, \"SeqNo\": \"7\", \"SubSections\": [\n" +
                        "          {\"SubSectionName\": \"Down Payments\", \"SeqNo\": \"1\", \"QuestionSections\": [\n" +
                        "               {\"QuestionSectionName\": \"DownPayment\", \"LRUI\": true, \"SeqNo\": \"1\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"38\", \"QuestionText\": \"$Dollar\", \"QuestionType\":\"TextCurr\", \"SeqNo\":\"38\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"39\", \"QuestionText\": \"Percentage\", \"QuestionType\":\"TextPer\", \"SeqNo\":\"39\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"40\", \"QuestionText\": \"Source of Down Payment\", \"QuestionType\":\"Text\", \"SeqNo\":\"40\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]},\n" +
                        "\n" +
                        "               {\"QuestionSectionName\": \"AddCoborrower\", \"LRUI\": true, \"SeqNo\": \"2\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"41\", \"QuestionText\": \"Add Co-Borrower Information:\", \"QuestionType\":\"Link\", \"SeqNo\":\"41\",\"IsParentQuestion\": \"FALSE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]\n" +
                        "\t\t\t\t}\n" +
                        "\t\t\t]\n" +
                        "\t\t}\n" +
                        "\t]\n" +
                        "},\n" +
                        "\n" +
                        "     {\"SectionName\": \"Employment History\", \"SectionId\":8, \"SeqNo\": \"8\", \"SubSections\": [\n" +
                        "          {\"SubSectionName\": \"EmploymentHistory\", \"SeqNo\": \"1\", \"QuestionSections\": [\n" +
                        "               {\"QuestionSectionName\": \"EmploymentHistory1\", \"LRUI\": true, \"SeqNo\": \"1\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"42\", \"QuestionText\": \"Are you retired?\", \"QuestionType\":\"SelectBool\", \"SeqNo\":\"42\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[\"TRUE\",\"FALSE\"],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[{\"Answer\":\"TRUE\", \"Icon\":\"TRUE\", \"SeqNo\": 1, \"Value\":\"TRUE\", \"Label\":\"TRUE\"},{\"Answer\":\"FALSE\", \"Icon\":\"FALSE\", \"SeqNo\": 2, \"Value\":\"FALSE\", \"Label\":\"FALSE\"}], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]},\n" +
                        "\n" +
                        "               {\"QuestionSectionName\": \"EmploymentHistory2\", \"LRUI\": true, \"SeqNo\": \"2\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"43\", \"QuestionText\": \"Company Name\", \"QuestionType\":\"Text\", \"SeqNo\":\"43\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]},\n" +
                        "\n" +
                        "               {\"QuestionSectionName\": \"EmploymentHistory3\", \"LRUI\": true, \"SeqNo\": \"3\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"44\", \"QuestionText\": \"Job Title\", \"QuestionType\":\"Text\", \"SeqNo\":\"44\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]},\n" +
                        "\n" +
                        "               {\"QuestionSectionName\": \"EmploymentHistory4\", \"LRUI\": true, \"SeqNo\": \"4\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"45\", \"QuestionText\": \"Company Address\", \"QuestionType\":\"Address\", \"SeqNo\":\"45\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]},\n" +
                        "\n" +
                        "               {\"QuestionSectionName\": \"EmploymentHistory5\", \"LRUI\": true, \"SeqNo\": \"5\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"46\", \"QuestionText\": \"Work Phone Number\", \"QuestionType\":\"TextPhone\", \"SeqNo\":\"46\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[\"WorkPhone\",\"Extension #\"],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[{\"Answer\":\"WorkPhone\", \"Icon\":\"WorkPhone\", \"SeqNo\": 1, \"Value\":\"WorkPhone\", \"Label\":\"WorkPhone\"},{\"Answer\":\"Extension #\", \"Icon\":\"Extension #\", \"SeqNo\": 2, \"Value\":\"Extension #\", \"Label\":\"Extension #\"}], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"47\", \"QuestionText\": \"Work Phone Ext\", \"QuestionType\":\"TextPhone\", \"SeqNo\":\"47\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[\"WorkPhone\",\"Extension #\"],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[{\"Answer\":\"WorkPhone\", \"Icon\":\"WorkPhone\", \"SeqNo\": 1, \"Value\":\"WorkPhone\", \"Label\":\"WorkPhone\"},{\"Answer\":\"Extension #\", \"Icon\":\"Extension #\", \"SeqNo\": 2, \"Value\":\"Extension #\", \"Label\":\"Extension #\"}], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]},\n" +
                        "\n" +
                        "               {\"QuestionSectionName\": \"EmploymentHistory6\", \"LRUI\": true, \"SeqNo\": \"6\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"48\", \"QuestionText\": \"Start Date\", \"QuestionType\":\"Date\", \"SeqNo\":\"48\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[\"StartDate\",\"CurrentDate\"],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[{\"Answer\":\"StartDate\", \"Icon\":\"StartDate\", \"SeqNo\": 1, \"Value\":\"StartDate\", \"Label\":\"StartDate\"},{\"Answer\":\"CurrentDate\", \"Icon\":\"CurrentDate\", \"SeqNo\": 2, \"Value\":\"CurrentDate\", \"Label\":\"CurrentDate\"}], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"49\", \"QuestionText\": \"Present\", \"QuestionType\":\"Date\", \"SeqNo\":\"49\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[\"StartDate\",\"CurrentDate\"],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[{\"Answer\":\"StartDate\", \"Icon\":\"StartDate\", \"SeqNo\": 1, \"Value\":\"StartDate\", \"Label\":\"StartDate\"},{\"Answer\":\"CurrentDate\", \"Icon\":\"CurrentDate\", \"SeqNo\": 2, \"Value\":\"CurrentDate\", \"Label\":\"CurrentDate\"}], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]},\n" +
                        "\n" +
                        "               {\"QuestionSectionName\": \"EmploymentHistory7\", \"LRUI\": true, \"SeqNo\": \"7\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"50\", \"QuestionText\": \"Years in this Field\", \"QuestionType\":\"TextYear\", \"SeqNo\":\"50\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"51\", \"QuestionText\": \"Connect securely to your Payroll provider.\", \"QuestionType\":\"SelectBool\", \"SeqNo\":\"51\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[\"TRUE\",\"FALSE\"],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[{\"Answer\":\"TRUE\", \"Icon\":\"TRUE\", \"SeqNo\": 1, \"Value\":\"TRUE\", \"Label\":\"TRUE\"},{\"Answer\":\"FALSE\", \"Icon\":\"FALSE\", \"SeqNo\": 2, \"Value\":\"FALSE\", \"Label\":\"FALSE\"}], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]\n" +
                        "\t\t\t\t}\n" +
                        "\t\t\t]\n" +
                        "\t\t}\n" +
                        "\t]\n" +
                        "},\n" +
                        "\n" +
                        "     {\"SectionName\": \"Monthly Income\", \"SectionId\":9, \"SeqNo\": \"9\", \"SubSections\": [\n" +
                        "          {\"SubSectionName\": \"Monthly Income\", \"SeqNo\": \"1\", \"QuestionSections\": [\n" +
                        "               {\"QuestionSectionName\": \"Monthly Income\", \"LRUI\": false, \"SeqNo\": \"1\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"52\", \"QuestionText\": \"Basic Income\", \"QuestionType\":\"TextCurr\", \"SeqNo\":\"52\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"53\", \"QuestionText\": \"Overtime\", \"QuestionType\":\"TextNum\", \"SeqNo\":\"53\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"54\", \"QuestionText\": \"Bonus\", \"QuestionType\":\"TextCurr\", \"SeqNo\":\"54\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]},\n" +
                        "\n" +
                        "               {\"QuestionSectionName\": \"Monthly Income1\", \"LRUI\": false, \"SeqNo\": \"2\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"55\", \"QuestionText\": \"Commissions\", \"QuestionType\":\"TextCurr\", \"SeqNo\":\"55\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"56\", \"QuestionText\": \"List Other Income\", \"QuestionType\":\"TextCurr\", \"SeqNo\":\"56\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"57\", \"QuestionText\": \"Interest/Divided\", \"QuestionType\":\"TextCurr\", \"SeqNo\":\"57\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]},\n" +
                        "\n" +
                        "               {\"QuestionSectionName\": \"Monthly Income2\", \"LRUI\": false, \"SeqNo\": \"3\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"58\", \"QuestionText\": \"Net Rental Income\", \"QuestionType\":\"TextCurr\", \"SeqNo\":\"58\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"59\", \"QuestionText\": \"Social Security\", \"QuestionType\":\"TextSSN\", \"SeqNo\":\"59\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"60\", \"QuestionText\": \"Other\", \"QuestionType\":\"TextCurr\", \"SeqNo\":\"60\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"61\", \"QuestionText\": \"Total\", \"QuestionType\":\"Label\", \"SeqNo\":\"61\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]\n" +
                        "\t\t\t\t}\n" +
                        "\t\t\t]\n" +
                        "\t\t}\n" +
                        "\t]\n" +
                        "},\n" +
                        "\n" +
                        "\t{\"SectionName\": \"Monthly Housing Expenses\", \"SectionId\":10, \"SeqNo\": \"10\", \"SubSections\": [\n" +
                        "          {\"SubSectionName\": \"Monthly Housing Expense\", \"SeqNo\": \"1\", \"QuestionSections\": [\n" +
                        "               {\"QuestionSectionName\": \"MonthlyExpense\", \"LRUI\": false, \"SeqNo\": \"1\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"62\", \"QuestionText\": \"Rent\", \"QuestionType\":\"TextCurr\", \"SeqNo\":\"62\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"63\", \"QuestionText\": \"First Mortgage (P&I)\", \"QuestionType\":\"TextCurr\", \"SeqNo\":\"63\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"64\", \"QuestionText\": \"Other Finances (P&I)\", \"QuestionType\":\"TextCurr\", \"SeqNo\":\"64\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]},\n" +
                        "\n" +
                        "               {\"QuestionSectionName\": \"MonthlyExpense1\", \"LRUI\": false, \"SeqNo\": \"2\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"65\", \"QuestionText\": \"Hazard Insurance\", \"QuestionType\":\"TextCurr\", \"SeqNo\":\"65\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"66\", \"QuestionText\": \"Real Estate Taxes\", \"QuestionType\":\"TextCurr\", \"SeqNo\":\"66\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"67\", \"QuestionText\": \"Mortgage Insurance\", \"QuestionType\":\"TextCurr\", \"SeqNo\":\"67\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]},\n" +
                        "\n" +
                        "               {\"QuestionSectionName\": \"MonthlyExpense2\", \"LRUI\": false, \"SeqNo\": \"3\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"68\", \"QuestionText\": \"Homeowner Association Dues\", \"QuestionType\":\"TextCurr\", \"SeqNo\":\"68\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"69\", \"QuestionText\": \"Other\", \"QuestionType\":\"TextCurr\", \"SeqNo\":\"69\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"70\", \"QuestionText\": \"Total\", \"QuestionType\":\"TextCurr\", \"SeqNo\":\"70\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]\n" +
                        "\t\t\t\t}\n" +
                        "\t\t\t]\n" +
                        "\t\t}\n" +
                        "\t]\n" +
                        "},\n" +
                        "\n" +
                        "     {\"SectionName\": \"Accounts (//Could be 1 to many)\", \"SectionId\":11, \"SeqNo\": \"11\", \"SubSections\": [\n" +
                        "          {\"SubSectionName\": \"Accounts\", \"SeqNo\": \"1\", \"QuestionSections\": [\n" +
                        "               {\"QuestionSectionName\": \"Bank Info\", \"LRUI\": false, \"SeqNo\": \"1\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"71\", \"QuestionText\": \"Name of Bank, S&L or Credit Union\", \"QuestionType\":\"Text\", \"SeqNo\":\"71\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"72\", \"QuestionText\": \"Account Number\", \"QuestionType\":\"Text\", \"SeqNo\":\"72\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"73\", \"QuestionText\": \"Account Balance\", \"QuestionType\":\"TextCurr\", \"SeqNo\":\"73\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]},\n" +
                        "\n" +
                        "               {\"QuestionSectionName\": \"BankType\", \"LRUI\": false, \"SeqNo\": \"2\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"75\", \"QuestionText\": \"Account Type\", \"QuestionType\":\"Select\", \"SeqNo\":\"75\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[\"Saving\",\"Checking\"],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[{\"Answer\":\"Saving\", \"Icon\":\"Saving\", \"SeqNo\": 1, \"Value\":\"Saving\", \"Label\":\"Saving\"},{\"Answer\":\"Checking\", \"Icon\":\"Checking\", \"SeqNo\": 2, \"Value\":\"Checking\", \"Label\":\"Checking\"}], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"76\", \"QuestionText\": \"Connect securely to your Bank.\", \"QuestionType\":\"Button\", \"SeqNo\":\"76\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]\n" +
                        "\t\t\t\t}\n" +
                        "\t\t\t]\n" +
                        "\t\t}\n" +
                        "\t]\n" +
                        "},\n" +
                        "\n" +
                        "     {\"SectionName\": \"Investments (//Could be 1 to many)\", \"SectionId\":12, \"SeqNo\": \"12\", \"SubSections\": [\n" +
                        "          {\"SubSectionName\": \"Investments\", \"SeqNo\": \"1\", \"QuestionSections\": [\n" +
                        "               {\"QuestionSectionName\": \"InvestmentInfo\", \"LRUI\": false, \"SeqNo\": \"1\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"77\", \"QuestionText\": \"First Name\", \"QuestionType\":\"Text\", \"SeqNo\":\"77\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]},\n" +
                        "\n" +
                        "               {\"QuestionSectionName\": \"InvestmentInfo1\", \"LRUI\": false, \"SeqNo\": \"2\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"78\", \"QuestionText\": \"Account Number\", \"QuestionType\":\"Text\", \"SeqNo\":\"78\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"79\", \"QuestionText\": \"Value\", \"QuestionType\":\"TextCurr\", \"SeqNo\":\"79\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"80\", \"QuestionText\": \"Connect securely to your accounts.\", \"QuestionType\":\"Button\", \"SeqNo\":\"80\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]\n" +
                        "\t\t\t\t}\n" +
                        "\t\t\t]\n" +
                        "\t\t}\n" +
                        "\t]\n" +
                        "},\n" +
                        "\n" +
                        "     \n" +
                        "\t {\"SectionName\": \"Liabilities: Please disclose any of the obligation or liabilities.\", \"SectionId\":13, \"SeqNo\": \"13\", \"SubSections\": [\n" +
                        "          {\"SubSectionName\": \"Liabilities\", \"SeqNo\": \"1\", \"QuestionSections\": [\n" +
                        "               {\"QuestionSectionName\": \"Liabilities\", \"LRUI\": false, \"SeqNo\": \"1\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"81\", \"QuestionText\": \"Name of Account\", \"QuestionType\":\"Text\", \"SeqNo\":\"81\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"82\", \"QuestionText\": \"Monthly Payment\", \"QuestionType\":\"TextCurr\", \"SeqNo\":\"82\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]},\n" +
                        "\n" +
                        "               {\"QuestionSectionName\": \"AccountBalance\", \"LRUI\": false, \"SeqNo\": \"2\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"84\", \"QuestionText\": \"Account Balance\", \"QuestionType\":\"TextCurr\", \"SeqNo\":\"84\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"85\", \"QuestionText\": \"Please Explain for Borrower\", \"QuestionType\":\"Text\", \"SeqNo\":\"85\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]\n" +
                        "\t\t\t\t}\n" +
                        "\t\t\t]\n" +
                        "\t\t}\n" +
                        "\t]\n" +
                        "},\n" +
                        "\n" +
                        "\t{\"SectionName\": \"Credit\", \"SectionId\":14, \"SeqNo\": \"14\", \"SubSections\": [\n" +
                        "          {\"SubSectionName\": \"Credit\", \"SeqNo\": \"1\", \"QuestionSections\": [\n" +
                        "               {\"QuestionSectionName\": \"Credit\", \"LRUI\": false, \"SeqNo\": \"1\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"86\", \"QuestionText\": \"Next we have to securely review your credit online. To do that, we need to get a little more information from you\", \"QuestionType\":\"Statement\", \"SeqNo\":\"86\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"87\", \"QuestionText\": \"Please, provide your birthday\", \"QuestionType\":\"Date\", \"SeqNo\":\"87\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"88\", \"QuestionText\": \"Please, enter your Social Security number\", \"QuestionType\":\"TextSSN\", \"SeqNo\":\"88\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[\"XXX XX XXXX\"],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[{\"Answer\":\"XXX XX XXXX\", \"Icon\":\"XXX XX XXXX\", \"SeqNo\": 1, \"Value\":\"XXX XX XXXX\", \"Label\":\"XXX XX XXXX\"}], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"89\", \"QuestionText\": \"Is this going to effect my credit score\", \"QuestionType\":\"TextBool\", \"SeqNo\":\"89\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"90\", \"QuestionText\": \"Please, enter your phone number\", \"QuestionType\":\"TextPhone\", \"SeqNo\":\"90\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"91\", \"QuestionText\": \"You are authorizing Docitt to access your credit report for the purpose of a mortgage inquiry\", \"QuestionType\":\"Statement\", \"SeqNo\":\"91\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]\n" +
                        "\t\t\t\t}\n" +
                        "\t\t\t]\n" +
                        "\t\t}\n" +
                        "\t]\n" +
                        "},\n" +
                        " \n" +
                        "\t{\"SectionName\": \"Government Declaration\", \"SectionId\":15, \"SeqNo\": \"15\", \"SubSections\": [\n" +
                        "          {\"SubSectionName\": \"Declaration\", \"SeqNo\": \"1\", \"QuestionSections\": [\n" +
                        "               {\"QuestionSectionName\": \"Declaration\", \"LRUI\": false, \"SeqNo\": \"1\", \"QuestionList\": [\n" +
                        "                    { \"QuestionId\":\"92\", \"QuestionText\": \"Alterations, improvements, repairs\", \"QuestionType\":\"SelectBool\", \"SeqNo\":\"92\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[\"TRUE\",\"FALSE\"],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[{\"Answer\":\"TRUE\", \"Icon\":\"TRUE\", \"SeqNo\": 1, \"Value\":\"TRUE\", \"Label\":\"TRUE\"},{\"Answer\":\"FALSE\", \"Icon\":\"FALSE\", \"SeqNo\": 2, \"Value\":\"FALSE\", \"Label\":\"FALSE\"}], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"93\", \"QuestionText\": \"Are there any outstanding judgments against you?\", \"QuestionType\":\"SelectBool\", \"SeqNo\":\"93\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[\"TRUE\",\"FALSE\"],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[{\"Answer\":\"TRUE\", \"Icon\":\"TRUE\", \"SeqNo\": 1, \"Value\":\"TRUE\", \"Label\":\"TRUE\"},{\"Answer\":\"FALSE\", \"Icon\":\"FALSE\", \"SeqNo\": 2, \"Value\":\"FALSE\", \"Label\":\"FALSE\"}], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"94\", \"QuestionText\": \"Land (if acquired separately)\", \"QuestionType\":\"SelectBool\", \"SeqNo\":\"94\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[\"TRUE\",\"FALSE\"],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[{\"Answer\":\"TRUE\", \"Icon\":\"TRUE\", \"SeqNo\": 1, \"Value\":\"TRUE\", \"Label\":\"TRUE\"},{\"Answer\":\"FALSE\", \"Icon\":\"FALSE\", \"SeqNo\": 2, \"Value\":\"FALSE\", \"Label\":\"FALSE\"}], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"95\", \"QuestionText\": \"Have you been declared bankrupt within the past 7 years?\", \"QuestionType\":\"SelectBool\", \"SeqNo\":\"95\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[\"TRUE\",\"FALSE\"],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[{\"Answer\":\"TRUE\", \"Icon\":\"TRUE\", \"SeqNo\": 1, \"Value\":\"TRUE\", \"Label\":\"TRUE\"},{\"Answer\":\"FALSE\", \"Icon\":\"FALSE\", \"SeqNo\": 2, \"Value\":\"FALSE\", \"Label\":\"FALSE\"}], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"96\", \"QuestionText\": \"Refinance (incl. debts to be paid off)\", \"QuestionType\":\"SelectBool\", \"SeqNo\":\"96\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[\"TRUE\",\"FALSE\"],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[{\"Answer\":\"TRUE\", \"Icon\":\"TRUE\", \"SeqNo\": 1, \"Value\":\"TRUE\", \"Label\":\"TRUE\"},{\"Answer\":\"FALSE\", \"Icon\":\"FALSE\", \"SeqNo\": 2, \"Value\":\"FALSE\", \"Label\":\"FALSE\"}], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"97\", \"QuestionText\": \"Have you had property foreclosed upon or given title or deed in lieu thereof in the last 7 years?\", \"QuestionType\":\"SelectBool\", \"SeqNo\":\"97\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[\"TRUE\",\"FALSE\"],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[{\"Answer\":\"TRUE\", \"Icon\":\"TRUE\", \"SeqNo\": 1, \"Value\":\"TRUE\", \"Label\":\"TRUE\"},{\"Answer\":\"FALSE\", \"Icon\":\"FALSE\", \"SeqNo\": 2, \"Value\":\"FALSE\", \"Label\":\"FALSE\"}], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"98\", \"QuestionText\": \"Estimated prepaid items\", \"QuestionType\":\"Text\", \"SeqNo\":\"98\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"99\", \"QuestionText\": \"Are you a party to a lawsuit?\", \"QuestionType\":\"SelectBool\", \"SeqNo\":\"99\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[\"TRUE\",\"FALSE\"],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[{\"Answer\":\"TRUE\", \"Icon\":\"TRUE\", \"SeqNo\": 1, \"Value\":\"TRUE\", \"Label\":\"TRUE\"},{\"Answer\":\"FALSE\", \"Icon\":\"FALSE\", \"SeqNo\": 2, \"Value\":\"FALSE\", \"Label\":\"FALSE\"}], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"100\", \"QuestionText\": \"Estimated closing costs\", \"QuestionType\":\"TextCurr\", \"SeqNo\":\"100\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"101\", \"QuestionText\": \"PMI, MIP, Funding Fee\", \"QuestionType\":\"TextCurr\", \"SeqNo\":\"101\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"102\", \"QuestionText\": \"Discount (if Borrower will pay)\", \"QuestionType\":\"TextCurr\", \"SeqNo\":\"102\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"103\", \"QuestionText\": \"i. Total costs (add items a through h)\", \"QuestionType\":\"TextCurr\", \"SeqNo\":\"103\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"105\", \"QuestionText\": \"Are you presently delinquent or in default on any Federal debt or any other loan, mortgage, financial obligation, bond, or loan guarantee?\", \"QuestionType\":\"SelectBool\", \"SeqNo\":\"105\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[\"TRUE\",\"FALSE\"],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[{\"Answer\":\"TRUE\", \"Icon\":\"TRUE\", \"SeqNo\": 1, \"Value\":\"TRUE\", \"Label\":\"TRUE\"},{\"Answer\":\"FALSE\", \"Icon\":\"FALSE\", \"SeqNo\": 2, \"Value\":\"FALSE\", \"Label\":\"FALSE\"}], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"106\", \"QuestionText\": \"Borrower’s closing costs paid by seller\", \"QuestionType\":\"TextCurr\", \"SeqNo\":\"106\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"107\", \"QuestionText\": \"Are you obligated to pay alimony, child support, or separate maintenance?\", \"QuestionType\":\"SelectBool\", \"SeqNo\":\"107\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[\"TRUE\",\"FALSE\"],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[{\"Answer\":\"TRUE\", \"Icon\":\"TRUE\", \"SeqNo\": 1, \"Value\":\"TRUE\", \"Label\":\"TRUE\"},{\"Answer\":\"FALSE\", \"Icon\":\"FALSE\", \"SeqNo\": 2, \"Value\":\"FALSE\", \"Label\":\"FALSE\"}], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"108\", \"QuestionText\": \"Is any part of the down payment borrowed?\", \"QuestionType\":\"SelectBool\", \"SeqNo\":\"108\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[\"TRUE\",\"FALSE\"],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[{\"Answer\":\"TRUE\", \"Icon\":\"TRUE\", \"SeqNo\": 1, \"Value\":\"TRUE\", \"Label\":\"TRUE\"},{\"Answer\":\"FALSE\", \"Icon\":\"FALSE\", \"SeqNo\": 2, \"Value\":\"FALSE\", \"Label\":\"FALSE\"}], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"109\", \"QuestionText\": \"Other Credits (explain)\", \"QuestionType\":\"Text\", \"SeqNo\":\"109\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"110\", \"QuestionText\": \"Are you a co-maker or endorser on a note?\", \"QuestionType\":\"SelectBool\", \"SeqNo\":\"110\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[\"TRUE\",\"FALSE\"],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[{\"Answer\":\"TRUE\", \"Icon\":\"TRUE\", \"SeqNo\": 1, \"Value\":\"TRUE\", \"Label\":\"TRUE\"},{\"Answer\":\"FALSE\", \"Icon\":\"FALSE\", \"SeqNo\": 2, \"Value\":\"FALSE\", \"Label\":\"FALSE\"}], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"111\", \"QuestionText\": \"Loan amount (exclude PMI, MIP, -------------------------------------------------------\", \"QuestionType\":\"TextCurr\", \"SeqNo\":\"111\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[\"TRUE\",\"FALSE\"],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[{\"Answer\":\"TRUE\", \"Icon\":\"TRUE\", \"SeqNo\": 1, \"Value\":\"TRUE\", \"Label\":\"TRUE\"},{\"Answer\":\"FALSE\", \"Icon\":\"FALSE\", \"SeqNo\": 2, \"Value\":\"FALSE\", \"Label\":\"FALSE\"}], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"112\", \"QuestionText\": \"Funding Fee financed)\", \"QuestionType\":\"TextCurr\", \"SeqNo\":\"112\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"113\", \"QuestionText\": \"Are you a U.S. citizen?\", \"QuestionType\":\"SelectBool\", \"SeqNo\":\"113\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[\"TRUE\",\"FALSE\"],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[{\"Answer\":\"TRUE\", \"Icon\":\"TRUE\", \"SeqNo\": 1, \"Value\":\"TRUE\", \"Label\":\"TRUE\"},{\"Answer\":\"FALSE\", \"Icon\":\"FALSE\", \"SeqNo\": 2, \"Value\":\"FALSE\", \"Label\":\"FALSE\"}], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"114\", \"QuestionText\": \"PMI, MIP, Funding Fee financed \", \"QuestionType\":\"SelectBool\", \"SeqNo\":\"114\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[\"TRUE\",\"FALSE\"],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[{\"Answer\":\"TRUE\", \"Icon\":\"TRUE\", \"SeqNo\": 1, \"Value\":\"TRUE\", \"Label\":\"TRUE\"},{\"Answer\":\"FALSE\", \"Icon\":\"FALSE\", \"SeqNo\": 2, \"Value\":\"FALSE\", \"Label\":\"FALSE\"}], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"115\", \"QuestionText\": \"Are you a permanent resident alien?\", \"QuestionType\":\"SelectBool\", \"SeqNo\":\"115\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[\"TRUE\",\"FALSE\"],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[{\"Answer\":\"TRUE\", \"Icon\":\"TRUE\", \"SeqNo\": 1, \"Value\":\"TRUE\", \"Label\":\"TRUE\"},{\"Answer\":\"FALSE\", \"Icon\":\"FALSE\", \"SeqNo\": 2, \"Value\":\"FALSE\", \"Label\":\"FALSE\"}], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"116\", \"QuestionText\": \"Loan amount\", \"QuestionType\":\"SelectBool\", \"SeqNo\":\"116\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[\"TRUE\",\"FALSE\"],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[{\"Answer\":\"TRUE\", \"Icon\":\"TRUE\", \"SeqNo\": 1, \"Value\":\"TRUE\", \"Label\":\"TRUE\"},{\"Answer\":\"FALSE\", \"Icon\":\"FALSE\", \"SeqNo\": 2, \"Value\":\"FALSE\", \"Label\":\"FALSE\"}], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"117\", \"QuestionText\": \"Do you intend to occupy the property as your primary residence?\", \"QuestionType\":\"SelectBool\", \"SeqNo\":\"117\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[\"TRUE\",\"FALSE\"],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[{\"Answer\":\"TRUE\", \"Icon\":\"TRUE\", \"SeqNo\": 1, \"Value\":\"TRUE\", \"Label\":\"TRUE\"},{\"Answer\":\"FALSE\", \"Icon\":\"FALSE\", \"SeqNo\": 2, \"Value\":\"FALSE\", \"Label\":\"FALSE\"}], \"ChildQuestions\":[{\"Answer\":\"TRUE\",\"QuestionId\":\"119\"},{\"Answer\":\"TRUE\",\"QuestionId\":\"120\"},{\"Answer\":\"TRUE\",\"QuestionId\":\"121\"},{\"Answer\":\"TRUE\",\"QuestionId\":\"122\"}], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"118\", \"QuestionText\": \"If Yes, ” complete question below.\", \"QuestionType\":\"label\", \"SeqNo\":\"118\",\"IsParentQuestion\": \"TRUE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"119\", \"QuestionText\": \"p. Cash from/to Borrower\", \"QuestionType\":\"Text\", \"SeqNo\":\"119\",\"IsParentQuestion\": \"FALSE\",\"AvailableAnsList\":[],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"120\", \"QuestionText\": \"m. Have you had an ownership interest in a property in the last three years?\", \"QuestionType\":\"SelectBool\", \"SeqNo\":\"120\",\"IsParentQuestion\": \"FALSE\",\"AvailableAnsList\":[\"TRUE\",\"FALSE\"],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[{\"Answer\":\"TRUE\", \"Icon\":\"TRUE\", \"SeqNo\": 1, \"Value\":\"TRUE\", \"Label\":\"TRUE\"},{\"Answer\":\"FALSE\", \"Icon\":\"FALSE\", \"SeqNo\": 2, \"Value\":\"FALSE\", \"Label\":\"FALSE\"}], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"121\", \"QuestionText\": \"(1) What type of property did you own—principal residence (PR), second home (SH), or investment property (IP)?\", \"QuestionType\":\"Select\", \"SeqNo\":\"121\",\"IsParentQuestion\": \"FALSE\",\"AvailableAnsList\":[\"Primary\",\"Secondary Home\",\"Investment Property\"],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[{\"Answer\":\"Primary\", \"Icon\":\"Primary\", \"SeqNo\": 1, \"Value\":\"Primary\", \"Label\":\"Primary\"},{\"Answer\":\"Secondary Home\", \"Icon\":\"Secondary Home\", \"SeqNo\": 2, \"Value\":\"Secondary Home\", \"Label\":\"Secondary Home\"},{\"Answer\":\"Investment Property\", \"Icon\":\"Investment Property\", \"SeqNo\": 3, \"Value\":\"Investment Property\", \"Label\":\"Investment Property\"}], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  },\n" +
                        "                    { \"QuestionId\":\"122\", \"QuestionText\": \"(2) How did you hold title to the home— by yourself (S), jointly with your spouse (SP), or jointly with another person (O)?\", \"QuestionType\":\"Select\", \"SeqNo\":\"122\",\"IsParentQuestion\": \"FALSE\",\"AvailableAnsList\":[\"Yourself\",\"Spouse\",\"Another Person\"],\"TemplateName\":\"\", \"TemplateVersion\":\"\", \"AvailableAns\":[{\"Answer\":\"Yourself\", \"Icon\":\"Yourself\", \"SeqNo\": 1, \"Value\":\"Yourself\", \"Label\":\"Yourself\"},{\"Answer\":\"Spouse\", \"Icon\":\"Spouse\", \"SeqNo\": 2, \"Value\":\"Spouse\", \"Label\":\"Spouse\"},{\"Answer\":\"Another Person\", \"Icon\":\"Another Person\", \"SeqNo\": 3, \"Value\":\"Another Person\", \"Label\":\"Another Person\"}], \"Width\":\"\",\"HelpText\":\"\",\"Reg\":\"\",\"Answer\": \"\", \"Required\":\"false\"  }]\n" +
                        "\t\t\t\t}\n" +
                        "\t\t\t]\n" +
                        "\t\t}\n" +
                        "\t]\n" +
                        "}      \n" +
                        "]\n" +
                        "}"
        });

        Header header = new Header("Authorization", "Bearer " + apiPropertiesReader.getBearertoken());
        Response response = given()
                .header(header)
                .contentType("application/json")
                .body(requestBody)
                .log().all()
                .when()
                .post(url);

        requestBody = null;
        logger.info("============== Response Code: " + response.getStatusCode() + "=============================");
        logger.info("============== Description: " + response.getStatusLine() + "=============================");
        return response;
    }

    private String getFormName() {
        DateFormat dateFormat = new SimpleDateFormat("ddMMmmss");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public String getJsonString(Response response, String formId) {
        JSONObject jsonObj = new JSONObject(response.prettyPrint().toString());
        return jsonObj.get("formId").toString();
    }

    public Response postGetStartedRequest(String conType, String urlEndPoint, String requestType, String formId) {
        requestType = "/getStarted/Full" ;
        String url = urlEndPoint + requestType ;

        logger.info("=======================================================================================");
        logger.info("Triggering a get started request to :" + url);
        logger.info("=======================================================================================");


        Header header = new Header("Authorization", "Bearer " + apiPropertiesReader.getBearertoken());
        Response response = given()
                .header(header)
                .contentType(conType)
                .log().all()
                .when()
                .post(url);

        logger.info("============== Response Code: " + response.getStatusCode() + "=============================");
        return response;
    }

    public Response getQualifyingApp(String conType, String urlEndPoint, String requestType, String tempAppId) {
        requestType = "/qualifyingApplication/";
        String url = urlEndPoint + requestType;

        Header header = new Header("Authorization", "Bearer " + apiPropertiesReader.getBearertoken());
        Response response = given()
                .header(header)
                .contentType(conType)
                .when()
                .get(url);

        logger.info("============== Response Code: " + response.getStatusCode() + "=============================");
        return response;
    }

    public Response updateQualifyingApp(String conType, String urlEndPoint, String requestType, String tempAppId) {
        requestType = "/qualifyingapplication/update";
        String url = urlEndPoint + requestType;

        RequestSpecification requestSpec = createRequestBody(conType, requestType);
        logger.info("=======================================================================================");
        logger.info("Triggering a update qualifying request to :" + url);
        logger.info("=======================================================================================");

        Header header = new Header("Authorization", "Bearer " + apiPropertiesReader.getBearertoken());
        Response response = given()

                .header(header)
                .spec(requestSpec)
                .contentType(conType)
                .log().all()
                .when()
                .put(url);

        logger.info("============== Response Code: " + response.getStatusCode() + "=============================");
        return response;
    }

    private RequestSpecification createRequestBody(String conType, String requestType) {

        RequestSpecBuilder builder = new RequestSpecBuilder();
        ObjectMapper objectMapper = null;

        if (conType.equalsIgnoreCase("application/json")) {
            objectMapper = new Jackson2Mapper(new Jackson2ObjectMapperFactory() {

                public com.fasterxml.jackson.databind.ObjectMapper create(
                        Class arg0, String arg1) {
                    com.fasterxml.jackson.databind.ObjectMapper objectMapper = new com.fasterxml.jackson.databind.ObjectMapper();
                    //objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
                    return objectMapper;
                }

            });

        }

        qappRequest.setQuestionId(apiParam.getQuestionId());
        qappRequest.setAnswer(apiParam.getAnswer());
        qappRequest.setSectionId(apiParam.getSectionId());
        qappRequest.setSubSectionName(apiParam.getSubSectionName());


        setBody(builder, objectMapper, qappRequest, conType);

        return builder.build();

    }

    private static void setBody(RequestSpecBuilder builder,
                                ObjectMapper objectMapper, Object body, String conType) {

        if (conType.contains("json")) {
            builder.setBody(body, objectMapper);
        } else if (conType.contains("xml")) {
            builder.setBody(body);
        }
    }

    public void verifyQualifyingAppWithAnswser(String s) {
        QuestionnaireResponse qResp = new Gson().fromJson(s, QuestionnaireResponse.class);
        String answer = qResp.getApplicationForm().getSections().get(0).getSubSections().get(0).getQuestionSections().get(1).getQuestionList().get(0).getAnswer();
        logger.info("Answer in qualifyingApplication:" + answer);
        assertEquals(answer, apiParam.getAnswer());

    }

    public void verifyQualifyingAppJson(String s) {
        QuestionnaireResponse qResp = new Gson().fromJson(s, QuestionnaireResponse.class);
        ApplicationForm appForm = qResp.getApplicationForm();
        JSONObject qRespObj = new JSONObject(s);
        logger.info("========= Verifying Root Response=========");
        assertEquals(qRespObj.has("applicationForm"), true);
        assertNotNull(qRespObj.has("temporaryApplicationNumber"));
        assertEquals(qRespObj.has("applicantId"), true);
        assertEquals(qRespObj.has("lastAccessedSection"), true);
        assertEquals(qRespObj.has("lastAccessedSubSection"), true);
        assertEquals(qRespObj.has("applicationNumber"), true);
        assertEquals(qRespObj.has("status"), true);
        assertEquals(qRespObj.has("lastAccessedSectionSeqNo"), true);
        assertEquals(qRespObj.has("lastAccessedSubSectionSeqNo"), true);
        assertNotNull(qRespObj.has("userName"));
        assertEquals(qRespObj.has("id"), true);

        logger.info("========= Verifying ApplicationForm =========");
        //ApplicationForm appForm = qResp.getApplicationForm();
        assertEquals(appForm.getSections().size(), 15);
        assertNotNull(appForm.getFormId());
        assertNotNull(appForm.getFormName());
        assertNotNull(appForm.getId());
        int section = 0;
        for (section = 0; section < (appForm.getSections().size()); section++) {
            logger.info("========= Verifying section: " + section + " =========");
            assertNotNull(appForm.getSections().get(section).getSectionId());
            assertNotNull(appForm.getSections().get(section).getSectionName());
            assertNotNull(appForm.getSections().get(section).getSeqNo());
            assertNull(appForm.getSections().get(section).getBgClass());
            int subsection;
            for (subsection = 0; subsection < (appForm.getSections().get(section).getSubSections().size()); subsection++) {
                logger.info("========= Verifying section: " + section + " : subsection :" + subsection + " =========");
                assertNotNull(appForm.getSections().get(section).getSubSections().get(subsection).getSubSectionName());
                assertNotNull(appForm.getSections().get(section).getSubSections().get(subsection).getSeqNo());
                assertNull(appForm.getSections().get(section).getSubSections().get(subsection).getBgClass());
                int questionSections;
                for (questionSections = 0; questionSections < (appForm.getSections().get(section).getSubSections().get(subsection).getQuestionSections().size()); questionSections++) {
                    logger.info("========= Verifying section: " + section + " : subsection :" + subsection + " questionSections:" + questionSections + " =========");
                    if (appForm.getSections().get(section).getSubSections().get(subsection).getQuestionSections().get(questionSections).getQuestionSectionName() != null) {
                        assertNotNull(appForm.getSections().get(section).getSubSections().get(subsection).getQuestionSections().get(questionSections).getQuestionSectionName());
                        assertNotNull(appForm.getSections().get(section).getSubSections().get(subsection).getQuestionSections().get(questionSections).getSeqNo());
                        assertNotNull(appForm.getSections().get(section).getSubSections().get(subsection).getQuestionSections().get(questionSections).getLrui());
                        int questionList;
                        for (questionList = 0; questionList < (appForm.getSections().get(section).getSubSections().get(subsection).getQuestionSections().get(questionSections).getQuestionList().size()); questionList++) {
                            logger.info("========= Verifying section: " + section + " : subsection :" + subsection + " questionSections:" + questionSections + "questionList :" + questionList + " =========");
                            assertNotNull(appForm.getSections().get(section).getSubSections().get(subsection).getQuestionSections().get(questionSections).getQuestionList().get(questionList).getQuestionId());
                            assertNotNull(appForm.getSections().get(section).getSubSections().get(subsection).getQuestionSections().get(questionSections).getQuestionList().get(questionList).getQuestionText());
                            assertNotNull(appForm.getSections().get(section).getSubSections().get(subsection).getQuestionSections().get(questionSections).getQuestionList().get(questionList).getAvailableAns());
                            assertNotNull(appForm.getSections().get(section).getSubSections().get(subsection).getQuestionSections().get(questionSections).getQuestionList().get(questionList).getSeqNo());
                            assertNotNull(appForm.getSections().get(section).getSubSections().get(subsection).getQuestionSections().get(questionSections).getQuestionList().get(questionList).getQuestionType());
                            assertNotNull(appForm.getSections().get(section).getSubSections().get(subsection).getQuestionSections().get(questionSections).getQuestionList().get(questionList).getRequired());
                            assertNotNull(appForm.getSections().get(section).getSubSections().get(subsection).getQuestionSections().get(questionSections).getQuestionList().get(questionList).getAnswer());
                            assertNull(appForm.getSections().get(section).getSubSections().get(subsection).getQuestionSections().get(questionSections).getQuestionList().get(questionList).getQuestionICON());
                            assertNotNull(appForm.getSections().get(section).getSubSections().get(subsection).getQuestionSections().get(questionSections).getQuestionList().get(questionList).getTemplateName());
                            assertNotNull(appForm.getSections().get(section).getSubSections().get(subsection).getQuestionSections().get(questionSections).getQuestionList().get(questionList).getIsParentQuestion());
                            assertNotNull(appForm.getSections().get(section).getSubSections().get(subsection).getQuestionSections().get(questionSections).getQuestionList().get(questionList).getTemplateVersion());
                            assertNotNull(appForm.getSections().get(section).getSubSections().get(subsection).getQuestionSections().get(questionSections).getQuestionList().get(questionList).getWidth());
                            assertNotNull(appForm.getSections().get(section).getSubSections().get(subsection).getQuestionSections().get(questionSections).getQuestionList().get(questionList).getHelpText());
                            assertNotNull(appForm.getSections().get(section).getSubSections().get(subsection).getQuestionSections().get(questionSections).getQuestionList().get(questionList).getReg());


                        }
                    } else
                        break;

                }


            }

        }
    }

    public void verifyQualifyingAppAnswser(String s) {
        JSONObject jsonObject = new JSONObject(s);
        //String appFormString = jsonObject.get("applicationForm").toString();
        JSONObject appFormObj = new JSONObject(jsonObject.get("applicationForm").toString());
        JSONArray jsonArray = appFormObj.getJSONArray("sections");
        JSONObject sections = jsonArray.getJSONObject((apiParam.getSectionId() - 1));
        System.out.println("===============Section=============");
        System.out.println(sections.toString());

        JSONObject sectionObj = new JSONObject(sections.toString());
        JSONArray subsectionArr = sectionObj.getJSONArray("subSections");
        int subsection;
        for (subsection = 0; subsection < subsectionArr.length(); subsection++) {
            JSONObject subsectionsObj = new JSONObject(subsectionArr.get(subsection).toString());
            if (subsectionsObj.get("subSectionName").toString().equalsIgnoreCase(apiParam.getSubSectionName())) {
                JSONArray qSectionArr = subsectionsObj.getJSONArray("questionSections");
                int qSection;
                for (qSection = 0; qSection < qSectionArr.length(); qSection++) {
                    JSONObject qSectionsObj = new JSONObject(qSectionArr.get(qSection).toString());
                    if (qSectionsObj.get("questionSectionName").toString().equalsIgnoreCase(apiParam.getQuestionSectionName())) {
                        JSONArray qListArr = qSectionsObj.getJSONArray("questionList");
                        int qList;
                        for (qList = 0; qList < qListArr.length(); qList++) {
                            JSONObject qListObj = new JSONObject(qListArr.get(qList).toString());
                            if (qListObj.get("questionId").toString().equalsIgnoreCase(apiParam.getQuestionId())) {
                                logger.info("Actual answer : " + qListObj.get("answer") + ", expected answer :" + apiParam.getAnswer());
                                Assert.assertEquals(qListObj.get("answer"), apiParam.getAnswer());
                                return;
                            }
                        }
                    }
                }


            }
        }


    }

    public String showErrorIfFailed(Response createTemplateResponse) {
        JSONObject respObj = new JSONObject(createTemplateResponse.prettyPrint().toString());
        if(respObj.getString("message").equals(null))
            return respObj.get("Message").toString();
        else
            return respObj.get("message").toString();


    }

    public Response getTeamAssignmentResposne(String conType, String urlEndPoint, String requestType, int teamNum) {
        requestType = "/" + teamNum + "/assignments";
        String url = urlEndPoint + requestType;

        logger.info("=======================================================================================");
        logger.info("Triggering get team assignment request to :" + url);
        logger.info("=======================================================================================");

        Header header = new Header("Authorization", "Bearer " + apiPropertiesReader.getBearertoken());
        Response response = given()
                .header(header)
                .contentType(conType)
                .log().all()
                .when()
                .get(url);

        logger.info("============== Response Code: " + response.getStatusCode() + "=============================");
        return response;
    }

    public Response postSendInviteRequest(String conType, String urlEndPoint, String requestType, int teamNum) {
        requestType = "/" + teamNum + "/invite/send";
        String url = urlEndPoint + requestType;

        RequestSpecification requestSpec = createTeamAssignmentRequestBody(conType, requestType, null, null,true);
        logger.info("=======================================================================================");
        logger.info("Triggering a send invitation request to :" + url);
        logger.info("=======================================================================================");

        Header header = new Header("Authorization", "Bearer " + apiPropertiesReader.getBearertoken());
        Response response = given()

                .header(header)
                .spec(requestSpec)
                .contentType(conType)
                .log().all()
                .when()
                .post(url);

        logger.info("============== Response Code: " + response.getStatusCode() + "=============================");
        return response;
    }

    private RequestSpecification createTeamAssignmentRequestBody(String conType, String requestType, String token, String id,boolean isCorrectEmail) {
        RequestSpecBuilder builder = new RequestSpecBuilder();
        ObjectMapper objectMapper = null;

        if (conType.equalsIgnoreCase("application/json")) {
            objectMapper = new Jackson2Mapper(new Jackson2ObjectMapperFactory() {

                public com.fasterxml.jackson.databind.ObjectMapper create(
                        Class arg0, String arg1) {
                    com.fasterxml.jackson.databind.ObjectMapper objectMapper = new com.fasterxml.jackson.databind.ObjectMapper();
                    //objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
                    return objectMapper;
                }

            });

        }

        if (requestType.contains("invite/send")) {
            sendInviteReq.setInvitedBy(apiParam.getInvitedBy());
            sendInviteReq.setInviteEmail(apiParam.getInviteEmail());
            sendInviteReq.setInviteName(apiParam.getInviteName());
            sendInviteReq.setInviteMobileNumber(apiParam.getInviteMobileNumber());
            sendInviteReq.setRole(apiParam.getRoles());
            sendInviteReq.setLenderTeam(apiParam.isLenderTeam());

            setBody(builder, objectMapper, sendInviteReq, conType);
        }

        if (requestType.contains("/invite/update")) {
            updateInviteReq.setInviteRefId(token);
            updateInviteReq.setUserId(id);
            if(isCorrectEmail) {
                updateInviteReq.setUserName(apiParam.getInviteEmail());
            }
            else
            {
                updateInviteReq.setUserName("abcd"+apiParam.getInviteEmail());
            }

            setBody(builder, objectMapper, updateInviteReq, conType);
        }

        if (requestType.contains("/multiassign")) {
            JSONArray ja = new JSONArray();

            SingleInvite s1 = new SingleInvite();
            s1.setUserId("123");
            s1.setUser("Sh");
            s1.setRole("Borrower");
            s1.setLenderTeam(false);

            SingleInvite s2 = new SingleInvite();
            s2.setUserId("1234");
            s2.setUser("Sha");
            s2.setRole("Borrower");
            s2.setLenderTeam(true);

            SingleInvite s3 = new SingleInvite();
            s3.setUserId("1231");
            s3.setUser("Shai");
            s3.setRole("Borrower");
            s3.setLenderTeam(false);

            ja.put(s1);
            ja.put(s2);
            ja.put(s3);

            setBody(builder, objectMapper, ja, conType);
        }

        if (requestType.contains("/assign")) {
            singleInviteReq.setUserId(id);
            singleInviteReq.setUser(apiParam.getInviteName());
            singleInviteReq.setRole(apiParam.getRoles());
            singleInviteReq.setLenderTeam(apiParam.isLenderTeam());

            setBody(builder, objectMapper, singleInviteReq, conType);
        }

        return builder.build();
    }

    public Response postTokenVerificationRequest(String conType, String urlEndPoint, String requestType, String invitationToken) {
        requestType = "/invite/verify";
        String url = urlEndPoint + requestType;

        RequestSpecification requestSpec = createVerifyTokenRequestBody(conType, requestType, invitationToken, apiParam.getInviteEmail());
        logger.info("=======================================================================================");
        logger.info("Triggering get verify invitation token request to :" + url);
        logger.info("=======================================================================================");

        Header header = new Header("Authorization", "Bearer " + apiPropertiesReader.getBearertoken());
        Response response = given()
                .header(header)
                .spec(requestSpec)
                .contentType(conType)
                .log().all()
                .when()
                .post(url);

        logger.info("============== Response Code: " + response.getStatusCode() + "=============================");
        return response;
    }

    private RequestSpecification createVerifyTokenRequestBody(String conType, String requestType, String invitationToken, String inviteEmail) {
        RequestSpecBuilder builder = new RequestSpecBuilder();
        ObjectMapper objectMapper = null;

        if (conType.equalsIgnoreCase("application/json")) {
            objectMapper = new Jackson2Mapper(new Jackson2ObjectMapperFactory() {

                public com.fasterxml.jackson.databind.ObjectMapper create(
                        Class arg0, String arg1) {
                    com.fasterxml.jackson.databind.ObjectMapper objectMapper = new com.fasterxml.jackson.databind.ObjectMapper();
                    //objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
                    return objectMapper;
                }

            });

        }

        verifyTokenReq.setInviteRefId(invitationToken);
        verifyTokenReq.setInviteEmail(inviteEmail);

        setBody(builder, objectMapper, verifyTokenReq, conType);
        return builder.build();
    }

    public Response postUpdateInviteRequest(String conType, String urlEndPoint, String requestType, String invitationToken, String userId,boolean isCorrectEmail) {
        requestType = "/invite/update";
        String url = urlEndPoint + requestType;

        RequestSpecification requestSpec = createTeamAssignmentRequestBody(conType, requestType, invitationToken, userId,isCorrectEmail);
        logger.info("=======================================================================================");
        logger.info("Triggering a update invitation request to :" + url);
        logger.info("=======================================================================================");

        Header header = new Header("Authorization", "Bearer " + apiPropertiesReader.getBearertoken());
        Response response = given()

                .header(header)
                .spec(requestSpec)
                .contentType(conType)
                .log().all()
                .when()
                .post(url);

        logger.info("============== Response Code: " + response.getStatusCode() + "=============================");
        return response;
    }

    public Response getInviteInfoRequest(String conType, String urlEndPoint, String requestType, String invitationToken, String userId) {
        requestType = "/invite/" + invitationToken + "/get";
        String url = urlEndPoint + requestType;

        logger.info("=======================================================================================");
        logger.info("Triggering get invite invitation token request to :" + url);
        logger.info("=======================================================================================");

        Header header = new Header("Authorization", "Bearer " + apiPropertiesReader.getBearertoken());
        Response response = given()
                .header(header)
                .log().all()
                .when()
                .get(url);

        logger.info("============== Response Code: " + response.getStatusCode() + "=============================");
        return response;
    }

    public Response deleteMemberRequest(String conType, String urlEndPoint, String requestType, int teamNum) {
        requestType = "/" + teamNum + "/disabled/" + apiParam.getInviteEmail();
        String url = urlEndPoint + requestType;

        logger.info("=======================================================================================");
        logger.info("Triggering disable member request to :" + url);
        logger.info("=======================================================================================");

        Header header = new Header("Authorization", "Bearer " + apiPropertiesReader.getBearertoken());
        Response response = given()
                .header(header)
                .log().all()
                .when()
                .delete(url);

        logger.info("============== Response Code: " + response.getStatusCode() + "=============================");
        return response;
    }

    public Response postSingleInviteRequest(String conType, String urlEndPoint, String requestType, int teamNum) {

        requestType = "/" + teamNum + requestType;

        String url = urlEndPoint + requestType;

        RequestSpecification requestSpec = createTeamAssignmentRequestBody(conType, requestType, null, apiParam.getName(),true);
        logger.info("=======================================================================================");
        logger.info("Triggering a single/multi invitation request to :" + url);
        logger.info("=======================================================================================");

        Header header = new Header("Authorization", "Bearer " + apiPropertiesReader.getBearertoken());
        Response response = given()

                .header(header)
                .spec(requestSpec)
                .contentType(conType)
                .log().all()
                .when()
                .post(url);

        logger.info("============== Response Code: " + response.getStatusCode() + "=============================");
        return response;
    }

    public Response getChatHistoryResposne(String conType, String urlEndPoint, String requestType) {

        logger.info("=======================================================================================");
        logger.info("Triggering get chat history  request to :" + urlEndPoint);
        logger.info("=======================================================================================");

        Header header = new Header("Authorization", "Bearer " + apiPropertiesReader.getBearertoken());
        Response response = given()
                .header(header)
                .contentType(conType)
                .log().all()
                .when()
                .get(urlEndPoint);

        logger.info("============== Response Code: " + response.getStatusCode() + "=============================");
        return response;

    }

    public Response postGetAppStatusRequest(String conType, String urlEndPoint, String requestType) {

        urlEndPoint = urlEndPoint + "/status/";

        logger.info("=======================================================================================");
        logger.info("Triggering get application status request to :" + urlEndPoint);
        logger.info("=======================================================================================");

        Header header = new Header("Authorization", "Bearer " + apiPropertiesReader.getBearertoken());
        Response response = given()
                .header(header)
                .contentType(conType)
                .log().all()
                .when()
                .get(urlEndPoint);

        logger.info("============== Response Code: " + response.getStatusCode() + "=============================");
        return response;
    }

    public Response getRequestListResposne(String conType, String urlEndPoint, String requestType) {

        //urlEndPoint = urlEndPoint + requestType + "/" + apiParam.username;

        logger.info("=======================================================================================");
        logger.info("Triggering get request list request to :" + urlEndPoint);
        logger.info("=======================================================================================");

        Header header = new Header("Authorization", "Bearer " + apiPropertiesReader.getBearertoken());
        Response response = given()
                .header(header)
                .contentType(conType)
                .log().all()
                .when()
                .get(urlEndPoint);

        logger.info("============== Response Code: " + response.getStatusCode() + "=============================");
        return response;
    }

    public Response postMakeRequestResponse(String conType, String urlEndPoint, String requestType,String typeOfRequest) {
        urlEndPoint = urlEndPoint + "/" +typeOfRequest +"/" ;

        logger.info("=======================================================================================");
        logger.info("Triggering create document request to :" + urlEndPoint);
        logger.info("=======================================================================================");

        RequestSpecification requestSpec = createMakeRequestBody(conType, requestType,urlEndPoint);

        Header header = new Header("Authorization", "Bearer " + apiPropertiesReader.getBearertoken());
        Response response = given()
                .header(header)
                .spec(requestSpec)
                .contentType(conType)
                .log().all()
                .when()
                .post(urlEndPoint);

        logger.info("============== Response Code: " + response.getStatusCode() + "=============================");
        return response;
    }

    private RequestSpecification createMakeRequestBody(String conType, String requestType,String url) {

        RequestSpecBuilder builder = new RequestSpecBuilder();
        ObjectMapper objectMapper = null;

        if (conType.equalsIgnoreCase("application/json")) {
            objectMapper = new Jackson2Mapper(new Jackson2ObjectMapperFactory() {

                public com.fasterxml.jackson.databind.ObjectMapper create(
                        Class arg0, String arg1) {
                    com.fasterxml.jackson.databind.ObjectMapper objectMapper = new com.fasterxml.jackson.databind.ObjectMapper();
                    //objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
                    return objectMapper;
                }

            });

        }
        if ((url.contains("proof-id-dl") || (url.contains("proof-id-pp")))) {
            makeDocumentRequest.setPriority(apiParam.priority);
            addressRequest.setCity(apiParam.city);
            dataRequest.setApplicant(apiParam.username);
            dataRequest.setAddress(addressRequest);
            makeDocumentRequest.setData(dataRequest);
            makeDocumentRequest.setDueDate(new SimpleDateFormat("yyyy-MM-dd").format(new Date()).toString());

            setBody(builder, objectMapper, makeDocumentRequest, conType);
        }

        return builder.build();
    }

    public Response postCreateUploadRequest(String conType, String urlEndPoint, String requestType,String requestId,String typeOfRequest) throws IOException {

        logger.info("=======================================================================================");
        logger.info("Triggering upload document request to :" + urlEndPoint);
        logger.info("=======================================================================================");

        String metadataStr = getMetadataString(requestId,typeOfRequest);

        Header header = new Header("Authorization", "Bearer " + apiPropertiesReader.getBearertoken());
        Response response = given()
                .header(header)
                .multiPart("file", new File(apiParam.custom_report_location + "header.txt"))
                .formParam("metadata",metadataStr)
                .log().all()
                .when()
                .post(urlEndPoint);

        logger.info("============== Response Code: " + response.getStatusCode() + "=============================");
        return response;
    }

    private InputStream getResourceFromClasspath(String resourceName) throws IOException {


        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(resourceName);

        if (inputStream == null) {
            throw new FileNotFoundException("resource '" + resourceName  + "' not found in the classpath");
        }

        return inputStream;
    }

    public String getMetadataString(String reqId,String reqType) {

        metadataReq.setFileSize("2048");
        metadataReq.setRequestId(reqId);
        metadataReq.setActionType(reqType);
        metadataReq.setCreatedBy("Shaishav Shah");
        metadataReq.setCreatedOn("2017-04-11");

        Gson gson = new Gson();
        String metadataString = gson.toJson(metadataReq);
        return metadataString;
    }

    public String getDocumentRequestStatusFromMongo(String reqId) throws Exception {
        logger.info("Querying mongoDb for status of the document");
        logger.info("===========================================");
        MongoClient mongoClient = null;

        mongoClient = new MongoClient(apiParam.MongoHost, apiParam.MongoPort);
        DB db = mongoClient.getDB("action-center");

        DBCollection collection = db.getCollection("action-center");
        DBObject query = new BasicDBObject("_id", new ObjectId(reqId));

        if (collection.find(query).count() != 1) {
            throw new Exception("No or Multiple documents found with requestId: " + reqId);
        }
        DBObject queryResult = collection.findOne(query);

        return queryResult.get("Status").toString();
    }

    public Response getDownloadFileResponse(String conType, String urlEndPoint, String requestType) {
        logger.info("=======================================================================================");
        logger.info("Triggering download document request to :" + urlEndPoint);
        logger.info("=======================================================================================");

        Header header = new Header("Authorization", "Bearer " + apiPropertiesReader.getBearertoken());
        Response response = given()
                .header(header)
                .contentType(conType)
                .log().all()
                .when()
                .get(urlEndPoint);

        logger.info("============== Response Code: " + response.getStatusCode() + "=============================");
        return response;
    }

    public Response postVerifyDocumentRequest(String conType, String urlEndPoint, String requestType,boolean isApprove) {
        String approvalSpec=null;
        logger.info("=======================================================================================");
        logger.info("Triggering verify document request to :" + urlEndPoint);
        logger.info("=======================================================================================");

        //RequestSpecification requestSpec = createVerifyDocumentRequestBody(conType, requestType,urlEndPoint);
        if(isApprove)
        {
            approvalSpec=apiPropertiesReader.getApproveDocument();
        }
        else
        {
            approvalSpec=apiPropertiesReader.getRejectDocument();
        }

        Header header = new Header("Authorization", "Bearer " + apiPropertiesReader.getBearertoken());
        Response response = given()
                .header(header)
                //.spec(requestSpec)
                .contentType(conType)
                .body(approvalSpec)
                .log().all()
                .when()
                .post(urlEndPoint);

        logger.info("============== Response Code: " + response.getStatusCode() + "=============================");
        return response;
    }

    private RequestSpecification createVerifyDocumentRequestBody(String conType, String requestType, String urlEndPoint) {

        RequestSpecBuilder builder = new RequestSpecBuilder();
        ObjectMapper objectMapper = null;

        if (conType.equalsIgnoreCase("application/json")) {
            objectMapper = new Jackson2Mapper(new Jackson2ObjectMapperFactory() {

                public com.fasterxml.jackson.databind.ObjectMapper create(
                        Class arg0, String arg1) {
                    com.fasterxml.jackson.databind.ObjectMapper objectMapper = new com.fasterxml.jackson.databind.ObjectMapper();
                    //objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
                    return objectMapper;
                }

            });

        }
        verifyDocumentRequest.setNotes("approved");

        questions.setAltered(true);
        questions.setName(true);
        questions.setAddress(true);
        questions.setBirthdate(true);
        questions.setIssueCountry(true);
        questions.setPhoto(true);
        questions.setNotExpired(true);
        questions.setMisspelled(true);

        verifyDocumentRequest.setQuestions(questions);
        setBody(builder, objectMapper, verifyDocumentRequest, conType);
        return builder.build();
    }

    public Response getDocumentListResponse(String conType, String urlEndPoint, String requestType) {

        logger.info("=======================================================================================");
        logger.info("Triggering get document list request to :" + urlEndPoint);
        logger.info("=======================================================================================");

        Header header = new Header("Authorization", "Bearer " + apiPropertiesReader.getBearertoken());
        Response response = given()
                .header(header)
                .contentType(conType)
                .log().all()
                .when()
                .get(urlEndPoint);

        logger.info("============== Response Code: " + response.getStatusCode() + "=============================");
        return response;
    }

    public Response getCreateEventResponse(String conType, String urlEndPoint, String requestType,String id) {

        logger.info("=======================================================================================");
        logger.info("Triggering create event request to :" + urlEndPoint);
        logger.info("=======================================================================================");

        RequestSpecification requestSpec = createEventRequestBody(conType, requestType,urlEndPoint,id);

        Header header = new Header("Authorization", "Bearer " + apiPropertiesReader.getBearertoken());
        Response response = given()
                .header(header)
                .spec(requestSpec)
                .contentType(conType)
                .log().all()
                .when()
                .urlEncodingEnabled(false)
                .post(urlEndPoint);

        logger.info("============== Response Code: " + response.getStatusCode() + "=============================");
        return response;
    }

    public Response getEventByMonthResponse(String conType, String urlEndPoint, String requestType,String id) {

        logger.info("=======================================================================================");
        logger.info("Triggering get events by month request to :" + urlEndPoint);
        logger.info("=======================================================================================");

        Header header = new Header("Authorization", "Bearer " + apiPropertiesReader.getBearertoken());
        Response response = given()
                .header(header)
                .contentType(conType)
                .log().all()
                .when()
                .urlEncodingEnabled(false)
                .get(urlEndPoint);

        logger.info("============== Response Code: " + response.getStatusCode() + "=============================");
        return response;
    }

    private RequestSpecification createEventRequestBody(String conType, String requestType, String urlEndPoint,String id) {
        RequestSpecBuilder builder = new RequestSpecBuilder();
        ObjectMapper objectMapper = null;

        if (conType.equalsIgnoreCase("application/json")) {
            objectMapper = new Jackson2Mapper(new Jackson2ObjectMapperFactory() {

                public com.fasterxml.jackson.databind.ObjectMapper create(
                        Class arg0, String arg1) {
                    com.fasterxml.jackson.databind.ObjectMapper objectMapper = new com.fasterxml.jackson.databind.ObjectMapper();
                    //objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
                    return objectMapper;
                }

            });

        }

        if(urlEndPoint.contains("sendtocalendar"))
        {
            ArrayList<String> eventIds = new ArrayList<String>();
            eventIds.add(0,id);
            sendEventRequest.setEventIds(eventIds);
            setBody(builder, objectMapper, sendEventRequest, conType);
        }
        else if(urlEndPoint.contains("date"))
        {
            eventDateRequest.setEventDate(new SimpleDateFormat("yyyy-MM-dd").format(new Date()).toString());
            setBody(builder, objectMapper, eventDateRequest, conType);
        }
        else {
            createEventRequest.setEntityId(apiParam.getEntityId());
            createEventRequest.setEntityType(apiParam.getRequestType().replace("/", ""));
            createEventRequest.setEventDate(new SimpleDateFormat("yyyy-MM-dd").format(new Date()).toString());
            createEventRequest.setEventDescription(RandomStringUtils.randomAlphanumeric(17).toUpperCase());
            createEventRequest.setPriority(apiParam.getPriority());
            createEventRequest.setEventTitle(RandomStringUtils.randomAlphanumeric(6).toUpperCase());
            createEventRequest.setCreatedBy(apiParam.getEmail());
            setBody(builder, objectMapper, createEventRequest, conType);
        }

        return builder.build();
    }

    public int getEventsForEntityId(String entityId,String collection) {

        logger.info("Querying mongoDb for assignment added during questionnaire test collection");
        logger.info("==================");
        MongoClient mongoClient = null;

        mongoClient = new MongoClient(apiParam.MongoHost, apiParam.MongoPort);
        DB db = mongoClient.getDB(collection);
        //auth = db.authenticate(portalPropertiesReader.getMongoDbUser(), portalPropertiesReader.getMongoDbPwd().toCharArray());
        DBCollection c1 = db.getCollection(collection);
        BasicDBObject b1 = new BasicDBObject();
        b1.append("EntityId", apiParam.entityId);
        return c1.find(b1).count();
    }

    public String getIdOfLastInsertedRecord(String entityId,String collection) {

        logger.info("Querying mongoDb to get id of last inserted record");
        logger.info("==================");
        MongoClient mongoClient = null;

        mongoClient = new MongoClient(apiParam.MongoHost, apiParam.MongoPort);
        DB db = mongoClient.getDB(collection);
        DBCollection c1 = db.getCollection(collection);
        BasicDBObject findObject = new BasicDBObject().append("EntityId",entityId);
        BasicDBObject sortObject = new BasicDBObject().append("_id", -1);

        DBCursor cur = c1.find(findObject).sort(sortObject).limit(1);
        DBObject obj = cur.one();
        return obj.get("_id").toString();
    }


    public Response getCreateReminderResponse(String conType, String urlEndPoint, String requestType, String id) {
        logger.info("=======================================================================================");
        logger.info("Triggering add reminder request to :" + urlEndPoint);
        logger.info("=======================================================================================");

        RequestSpecification requestSpec = createReminderRequestBody(conType, requestType,urlEndPoint,id);

        Header header = new Header("Authorization", "Bearer " + apiPropertiesReader.getBearertoken());
        Response response = given()
                .header(header)
                .spec(requestSpec)
                .contentType(conType)
                .log().all()
                .when()
                .urlEncodingEnabled(false)
                .post(urlEndPoint);

        logger.info("============== Response Code: " + response.getStatusCode() + "=============================");
        return response;
    }

    private RequestSpecification createReminderRequestBody(String conType, String requestType, String urlEndPoint, String id) {
        RequestSpecBuilder builder = new RequestSpecBuilder();
        ObjectMapper objectMapper = null;

        if (conType.equalsIgnoreCase("application/json")) {
            objectMapper = new Jackson2Mapper(new Jackson2ObjectMapperFactory() {

                public com.fasterxml.jackson.databind.ObjectMapper create(
                        Class arg0, String arg1) {
                    com.fasterxml.jackson.databind.ObjectMapper objectMapper = new com.fasterxml.jackson.databind.ObjectMapper();
                    //objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
                    return objectMapper;
                }

            });

        }

        if(urlEndPoint.contains("date"))
        {
            unreadReminderByDateRequest.setDate(new SimpleDateFormat("yyyy-MM-dd").format(new Date()).toString());
            setBody(builder, objectMapper, unreadReminderByDateRequest, conType);
        }
        else if(urlEndPoint.contains("markasread"))
        {
            ArrayList<String> ids = new ArrayList<String>();
            ids.add(id);
            markUnreadRequest.setIds(ids);
            setBody(builder, objectMapper, markUnreadRequest, conType);
        }
        else {
            createReminderRequest.setDescription(apiParam.getEventDesc());
            setBody(builder, objectMapper, createReminderRequest, conType);
        }
        return builder.build();
    }

    public Response getAllRemindersResponse(String conType, String urlEndPoint, String requestType, Object o) {
        logger.info("=======================================================================================");
        logger.info("Triggering get all reminder request to :" + urlEndPoint);
        logger.info("=======================================================================================");

        Header header = new Header("Authorization", "Bearer " + apiPropertiesReader.getBearertoken());
        Response response = given()
                .header(header)
                .contentType(conType)
                .log().all()
                .when()
                .urlEncodingEnabled(false)
                .get(urlEndPoint);

        logger.info("============== Response Code: " + response.getStatusCode() + "=============================");
        return response;
    }

    public Response getUnreadReminderCountResponse(String conType, String urlEndPoint, String requestType, String id) {
        logger.info("=======================================================================================");
        logger.info("Triggering get all unread reminder request to :" + urlEndPoint);
        logger.info("=======================================================================================");

        Header header = new Header("Authorization", "Bearer " + apiPropertiesReader.getBearertoken());
        Response response = given()
                .header(header)
                .contentType(conType)
                .log().all()
                .when()
                .urlEncodingEnabled(false)
                .get(urlEndPoint);

        logger.info("============== Response Code: " + response.getStatusCode() + "=============================");
        return response;
    }

    public Response getReminderByDateResponse(String conType, String urlEndPoint, String requestType, String id) {
        logger.info("=======================================================================================");
        logger.info("Triggering get unread reminder by date request to :" + urlEndPoint);
        logger.info("=======================================================================================");

        RequestSpecification requestSpec = createReminderRequestBody(conType, requestType,urlEndPoint,id);

        Header header = new Header("Authorization", "Bearer " + apiPropertiesReader.getBearertoken());
        Response response = given()
                .header(header)
                .spec(requestSpec)
                .contentType(conType)
                .log().all()
                .when()
                .urlEncodingEnabled(false)
                .post(urlEndPoint);

        logger.info("============== Response Code: " + response.getStatusCode() + "=============================");
        return response;
    }

    public Response getMarkAsReadResponse(String conType, String urlEndPoint, String requestType, String id) {
        logger.info("=======================================================================================");
        logger.info("Triggering mark reminder as read request to :" + urlEndPoint);
        logger.info("=======================================================================================");

        RequestSpecification requestSpec = createReminderRequestBody(conType, requestType,urlEndPoint,id);

        Header header = new Header("Authorization", "Bearer " + apiPropertiesReader.getBearertoken());
        Response response = given()
                .header(header)
                .spec(requestSpec)
                .contentType(conType)
                .log().all()
                .when()
                .urlEncodingEnabled(false)
                .post(urlEndPoint);

        logger.info("============== Response Code: " + response.getStatusCode() + "=============================");
        return response;
    }


    public Response getAllReminderResponse(String conType, String urlEndPoint, String requestType, String id) {
        logger.info("=======================================================================================");
        logger.info("Triggering get all reminders request to :" + urlEndPoint);
        logger.info("=======================================================================================");

        Header header = new Header("Authorization", "Bearer " + apiPropertiesReader.getBearertoken());
        Response response = given()
                .header(header)
                .contentType(conType)
                .log().all()
                .when()
                .urlEncodingEnabled(false)
                .get(urlEndPoint);

        logger.info("============== Response Code: " + response.getStatusCode() + "=============================");
        return response;
    }
}
