package net.sigmainfo.lf.automation.api.dataset;

import org.springframework.stereotype.Component;

import java.util.ArrayList;

/**
 * Created by shaishav.s on 27-04-2017.
 */
@Component
public class sendEventAsAttachment {

        private ArrayList<String> eventIds;

        public ArrayList<String> getEventIds() { return this.eventIds; }

    public void setEventIds(ArrayList<String> eventIds) { this.eventIds = eventIds; }

}
