package net.sigmainfo.lf.automation.api.tests;

import com.jayway.restassured.response.Response;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import net.sigmainfo.lf.automation.api.function.APIFuncUtils;
import net.sigmainfo.lf.automation.api.function.ApiPropertiesReader;
import net.sigmainfo.lf.automation.common.TestResults;
import net.sigmainfo.lf.automation.api.constant.ApiParam;
import net.sigmainfo.lf.automation.api.function.QuestionnaireUtils;
import net.sigmainfo.lf.automation.common.AbstractTests;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.testng.Assert.assertEquals;

/**
 * Created by shaishav.s on 08-03-2017.
 */
public class QuestionnaireApiTests  extends AbstractTests {

    @Autowired
    TestResults testResults;

    @Autowired
    APIFuncUtils apiFuncUtils;

    @Autowired
    ApiParam apiParam;

    @Autowired
    ApiPropertiesReader apiPropertiesReader;

    @Autowired
    QuestionnaireUtils qUtils;

    public static String funcMod="Docitt_Api";

    @AfterClass(alwaysRun = true)
    private void endCasereport() throws IOException, JSONException {

        //deleteAddedQuestionnaire();
        String funcModule = "Docitt_Api";
        String className = org.apache.commons.lang.StringUtils.substringAfterLast(this.getClass().getName(), ".");
        String description = "Verify  " + org.apache.commons.lang.StringUtils.substringAfterLast(this.getClass().getName(), ".");
        generateReport(className, description,funcMod);
    }

    /*private void deleteAddedQuestionnaire() {
        logger.info("Querying mongoDb for assignment added during questionnaire test collection");
        logger.info("==================");
        MongoClient mongoClient = null;

        mongoClient = new MongoClient(apiParam.MongoHost, apiParam.MongoPort);
        DB db = mongoClient.getDB("questionnnaires");
        //auth = db.authenticate(portalPropertiesReader.getMongoDbUser(), portalPropertiesReader.getMongoDbPwd().toCharArray());
        DBCollection collection = db.getCollection("questionnnaire");
        BasicDBObject b1 = new BasicDBObject();
        b1.append("UserName", apiParam.beareruser);
        collection.remove(b1);

        logger.info("Questionnaire for "+apiParam.beareruser+" has been deleted.");
    }*/

    /**
     * Description          : coborrowerSectionQuestionTest
     * Includes             : 1. Questionnaire flow  :
     *                          a. Create questionnaire
     *                          b. Answer the question in coborrowerSection
     *                          c. Verify the questionnaire in mongodb that answer is updated
     */


    @Test(priority=1,description = "", groups = {"docittapitests","questionnaireApiTests","coborrowerSectionQuestionTest"})
    public void VerifyCoborrowerSectionQuestion() throws Exception{
        String sTestID = "coborrowerSectionQuestionTest";
        String result = "Failed";
        String formId=null;
        String tempAppId=null;
        int SUCCESS_CODE=200;
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            String urlEndPoint = restUrl + ":"+ apiParam.getPort();
            Response createTemplateResponse = qUtils.postCreateFormRequest(conType, urlEndPoint, apiParam.requestType);
            if(createTemplateResponse.getStatusCode() != 200) {
                qUtils.showErrorIfFailed(createTemplateResponse);
            }
            assertEquals(createTemplateResponse.getStatusCode(), 200);
            formId = qUtils.getJsonString(createTemplateResponse,"formId");
            logger.info("FormId extracted is :"+formId);
            Response addQuestionnaireResponse = qUtils.postGetStartedRequest(conType, urlEndPoint, apiParam.requestType,formId);
            assertEquals(addQuestionnaireResponse.getStatusCode(), 200);
            tempAppId = new JSONObject(addQuestionnaireResponse.prettyPrint().toString()).get("temporaryApplicationNumber").toString();
            /*Response qualifyingAppResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(qualifyingAppResponse.getStatusCode(), 200);*/
            Response updateQualifyingAppResponse = qUtils.updateQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(updateQualifyingAppResponse.prettyPrint().toString(),"true");
            Response getUpdatedResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(getUpdatedResponse.getStatusCode(), 200);
            qUtils.verifyQualifyingAppAnswser(getUpdatedResponse.prettyPrint());
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }

    /**
     * Description          : getApplicationStatusTest
     * Includes             : 1. Questionnaire flow  :
     *                          a. Create questionnaire
     *                          b. Answer the question in
     *                          c. Verify the status in the mongodb
     */

    @Test(priority=2,description = "", groups = {"docittapitests","questionnaireApiTests","getApplicationStatusTest"})
    public void VerifyGetApplicationStatusTest() throws Exception{
        String sTestID = "getApplicationStatusTest";
        String result = "Failed";
        String formId=null;
        String tempAppId=null;
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            String urlEndPoint = restUrl + ":"+ apiParam.getPort();
            Response getAppStatusResp = qUtils.postGetAppStatusRequest(conType, urlEndPoint, apiParam.requestType);
            assertEquals(getAppStatusResp.getStatusCode(),200);
            assertEquals(new JSONObject(getAppStatusResp.prettyPrint().toString()).getString("status"),"Open");
            assertEquals(new JSONObject(getAppStatusResp.prettyPrint().toString()).getString("userName"),apiParam.beareruser);
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }

    /**
     * Description          : verifyQualifyingAppJsonTest
     * Includes             : 1. Questionnaire flow  :
     *                          a. Create questionnaire
     *                          b. Answer the question in coborrowerSection
     *                          c. Verify the questionnaire in mongodb that answer is updated
     *                          d. Verify json schema
     */

    @Test(description = "", groups = {"docittapitests","questionnaireApiTests","verifyQualifyingAppJsonTest"})
    public void VerifyQualifyingAppJson() throws Exception{
        String sTestID = "verifyQualifyingAppJsonTest";
        String result = "Failed";
        String formId=null;
        String tempAppId=null;
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            String urlEndPoint = restUrl + ":"+ apiParam.getPort();
            Response createTemplateResponse = qUtils.postCreateFormRequest(conType, urlEndPoint, apiParam.requestType);
            if(createTemplateResponse.getStatusCode() != 200) {
                qUtils.showErrorIfFailed(createTemplateResponse);
            }
            assertEquals(createTemplateResponse.getStatusCode(), 200);
            formId = qUtils.getJsonString(createTemplateResponse,"formId");
            logger.info("FormId extracted is :"+formId);
            Response addQuestionnaireResponse = qUtils.postGetStartedRequest(conType, urlEndPoint, apiParam.requestType,formId);
            assertEquals(addQuestionnaireResponse.getStatusCode(), 200);
            tempAppId = addQuestionnaireResponse.prettyPrint().toString();
            Response qualifyingAppResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(qualifyingAppResponse.getStatusCode(), 200);
            Response updateQualifyingAppResponse = qUtils.updateQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(updateQualifyingAppResponse.prettyPrint().toString(),"true");
            Response getUpdatedResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(getUpdatedResponse.getStatusCode(), 200);
            qUtils.verifyQualifyingAppJson(getUpdatedResponse.prettyPrint());
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }

    /**
     * Description          : purchaseSectionQuestionTest
     * Includes             : 1. Questionnaire flow  :
     *                          a. Create questionnaire
     *                          b. Answer the question in purchaseSection
     *                          c. Verify the questionnaire in mongodb that answer is updated
     *                          d. Verify answer
     */

    @Test(description = "", groups = {"docittapitests","questionnaireApiTests","purchaseSectionQuestionTest"})
    public void VerifyPurchaseSectionQuestion() throws Exception{
        String sTestID = "purchaseSectionQuestionTest";
        String result = "Failed";
        String formId=null;
        String tempAppId=null;
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            String urlEndPoint = restUrl + ":"+ apiParam.getPort();
            Response createTemplateResponse = qUtils.postCreateFormRequest(conType, urlEndPoint, apiParam.requestType);
            if(createTemplateResponse.getStatusCode() != 200) {
                qUtils.showErrorIfFailed(createTemplateResponse);
            }
            assertEquals(createTemplateResponse.getStatusCode(), 200);
            formId = qUtils.getJsonString(createTemplateResponse,"formId");
            logger.info("FormId extracted is :"+formId);
            Response addQuestionnaireResponse = qUtils.postGetStartedRequest(conType, urlEndPoint, apiParam.requestType,formId);
            assertEquals(addQuestionnaireResponse.getStatusCode(), 200);
            tempAppId = addQuestionnaireResponse.prettyPrint().toString();
            Response qualifyingAppResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(qualifyingAppResponse.getStatusCode(), 200);
            Response updateQualifyingAppResponse = qUtils.updateQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(updateQualifyingAppResponse.prettyPrint().toString(),"true");
            Response getUpdatedResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(getUpdatedResponse.getStatusCode(), 200);
            qUtils.verifyQualifyingAppAnswser(getUpdatedResponse.prettyPrint());
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }

    /**
     * Description          : nameSectionQuestionTest
     * Includes             : 1. Questionnaire flow  :
     *                          a. Create questionnaire
     *                          b. Answer the question in nameSection
     *                          c. Verify the questionnaire in mongodb that answer is updated
     *                          d. Verify answer
     */

    @Test(description = "", groups = {"docittapitests","questionnaireApiTests","nameSectionQuestionTest"})
    public void VerifyNameSectionQuestion() throws Exception{
        String sTestID = "nameSectionQuestionTest";
        String result = "Failed";
        String formId=null;
        String tempAppId=null;
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            String urlEndPoint = restUrl + ":"+ apiParam.getPort();
            Response createTemplateResponse = qUtils.postCreateFormRequest(conType, urlEndPoint, apiParam.requestType);
            if(createTemplateResponse.getStatusCode() != 200) {
                qUtils.showErrorIfFailed(createTemplateResponse);
            }
            assertEquals(createTemplateResponse.getStatusCode(), 200);
            formId = qUtils.getJsonString(createTemplateResponse,"formId");
            logger.info("FormId extracted is :"+formId);
            Response addQuestionnaireResponse = qUtils.postGetStartedRequest(conType, urlEndPoint, apiParam.requestType,formId);
            assertEquals(addQuestionnaireResponse.getStatusCode(), 200);
            tempAppId = addQuestionnaireResponse.prettyPrint().toString();
            Response qualifyingAppResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(qualifyingAppResponse.getStatusCode(), 200);
            Response updateQualifyingAppResponse = qUtils.updateQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(updateQualifyingAppResponse.prettyPrint().toString(),"true");
            Response getUpdatedResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(getUpdatedResponse.getStatusCode(), 200);
            qUtils.verifyQualifyingAppAnswser(getUpdatedResponse.prettyPrint());
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }

    /**
     * Description          : suffixSectionQuestionTest
     * Includes             : 1. Questionnaire flow  :
     *                          a. Create questionnaire
     *                          b. Answer the question in suffixSection
     *                          c. Verify the questionnaire in mongodb that answer is updated
     *                          d. Verify answer
     */

    @Test(description = "", groups = {"docittapitests","questionnaireApiTests","suffixSectionQuestionTest"})
    public void VerifySuffixSectionQuestion() throws Exception{
        String sTestID = "suffixSectionQuestionTest";
        String result = "Failed";
        String formId=null;
        String tempAppId=null;
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            String urlEndPoint = restUrl + ":"+ apiParam.getPort();
            Response createTemplateResponse = qUtils.postCreateFormRequest(conType, urlEndPoint, apiParam.requestType);
            if(createTemplateResponse.getStatusCode() != 200) {
                qUtils.showErrorIfFailed(createTemplateResponse);
            }
            assertEquals(createTemplateResponse.getStatusCode(), 200);
            formId = qUtils.getJsonString(createTemplateResponse,"formId");
            logger.info("FormId extracted is :"+formId);
            Response addQuestionnaireResponse = qUtils.postGetStartedRequest(conType, urlEndPoint, apiParam.requestType,formId);
            assertEquals(addQuestionnaireResponse.getStatusCode(), 200);
            tempAppId = addQuestionnaireResponse.prettyPrint().toString();
            Response qualifyingAppResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(qualifyingAppResponse.getStatusCode(), 200);
            Response updateQualifyingAppResponse = qUtils.updateQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(updateQualifyingAppResponse.prettyPrint().toString(),"true");
            Response getUpdatedResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(getUpdatedResponse.getStatusCode(), 200);
            qUtils.verifyQualifyingAppAnswser(getUpdatedResponse.prettyPrint());
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }

    /**
     * Description          : yearsinSchoolSectionQuestionTest
     * Includes             : 1. Questionnaire flow  :
     *                          a. Create questionnaire
     *                          b. Answer the question in yearsinSchoolSection
     *                          c. Verify the questionnaire in mongodb that answer is updated
     *                          d. Verify answer
     */

    @Test(description = "", groups = {"docittapitests","questionnaireApiTests","yearsinSchoolSectionQuestionTest"})
    public void VerifyYearsInSchoolSectionQuestion() throws Exception{
        String sTestID = "yearsinSchoolSectionQuestionTest";
        String result = "Failed";
        String formId=null;
        String tempAppId=null;
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            String urlEndPoint = restUrl + ":"+ apiParam.getPort();
            Response createTemplateResponse = qUtils.postCreateFormRequest(conType, urlEndPoint, apiParam.requestType);
            if(createTemplateResponse.getStatusCode() != 200) {
                qUtils.showErrorIfFailed(createTemplateResponse);
            }
            assertEquals(createTemplateResponse.getStatusCode(), 200);
            formId = qUtils.getJsonString(createTemplateResponse,"formId");
            logger.info("FormId extracted is :"+formId);
            Response addQuestionnaireResponse = qUtils.postGetStartedRequest(conType, urlEndPoint, apiParam.requestType,formId);
            assertEquals(addQuestionnaireResponse.getStatusCode(), 200);
            tempAppId = addQuestionnaireResponse.prettyPrint().toString();
            Response qualifyingAppResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(qualifyingAppResponse.getStatusCode(), 200);
            Response updateQualifyingAppResponse = qUtils.updateQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(updateQualifyingAppResponse.prettyPrint().toString(),"true");
            Response getUpdatedResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(getUpdatedResponse.getStatusCode(), 200);
            qUtils.verifyQualifyingAppAnswser(getUpdatedResponse.prettyPrint());
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }

    /**
     * Description          : emailSectionQuestionTest
     * Includes             : 1. Questionnaire flow  :
     *                          a. Create questionnaire
     *                          b. Answer the question in email Section
     *                          c. Verify the questionnaire in mongodb that answer is updated
     *                          d. Verify answer
     */

    @Test(description = "", groups = {"docittapitests","questionnaireApiTests","emailSectionQuestionTest"})
    public void VerifyEmailSectionQuestion() throws Exception{
        String sTestID = "emailSectionQuestionTest";
        String result = "Failed";
        String formId=null;
        String tempAppId=null;
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            String urlEndPoint = restUrl + ":"+ apiParam.getPort();
            Response createTemplateResponse = qUtils.postCreateFormRequest(conType, urlEndPoint, apiParam.requestType);
            if(createTemplateResponse.getStatusCode() != 200) {
                qUtils.showErrorIfFailed(createTemplateResponse);
            }
            assertEquals(createTemplateResponse.getStatusCode(), 200);
            formId = qUtils.getJsonString(createTemplateResponse,"formId");
            logger.info("FormId extracted is :"+formId);
            Response addQuestionnaireResponse = qUtils.postGetStartedRequest(conType, urlEndPoint, apiParam.requestType,formId);
            assertEquals(addQuestionnaireResponse.getStatusCode(), 200);
            tempAppId = addQuestionnaireResponse.prettyPrint().toString();
            Response qualifyingAppResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(qualifyingAppResponse.getStatusCode(), 200);
            Response updateQualifyingAppResponse = qUtils.updateQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(updateQualifyingAppResponse.prettyPrint().toString(),"true");
            Response getUpdatedResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(getUpdatedResponse.getStatusCode(), 200);
            qUtils.verifyQualifyingAppAnswser(getUpdatedResponse.prettyPrint());
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }

    /**
     * Description          : preferredWaySectionQuestionTest
     * Includes             : 1. Questionnaire flow  :
     *                          a. Create questionnaire
     *                          b. Answer the question in preferredWay Section
     *                          c. Verify the questionnaire in mongodb that answer is updated
     *                          d. Verify answer
     */

    @Test(description = "", groups = {"docittapitests","questionnaireApiTests","preferredWaySectionQuestionTest"})
    public void VerifyPreferredWaySectionQuestion() throws Exception{
        String sTestID = "preferredWaySectionQuestionTest";
        String result = "Failed";
        String formId=null;
        String tempAppId=null;
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            String urlEndPoint = restUrl + ":"+ apiParam.getPort();
            Response createTemplateResponse = qUtils.postCreateFormRequest(conType, urlEndPoint, apiParam.requestType);
            if(createTemplateResponse.getStatusCode() != 200) {
                qUtils.showErrorIfFailed(createTemplateResponse);
            }
            assertEquals(createTemplateResponse.getStatusCode(), 200);
            formId = qUtils.getJsonString(createTemplateResponse,"formId");
            logger.info("FormId extracted is :"+formId);
            Response addQuestionnaireResponse = qUtils.postGetStartedRequest(conType, urlEndPoint, apiParam.requestType,formId);
            assertEquals(addQuestionnaireResponse.getStatusCode(), 200);
            tempAppId = addQuestionnaireResponse.prettyPrint().toString();
            Response qualifyingAppResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(qualifyingAppResponse.getStatusCode(), 200);
            Response updateQualifyingAppResponse = qUtils.updateQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(updateQualifyingAppResponse.prettyPrint().toString(),"true");
            Response getUpdatedResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(getUpdatedResponse.getStatusCode(), 200);
            qUtils.verifyQualifyingAppAnswser(getUpdatedResponse.prettyPrint());
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }

    /**
     * Description          : dependentsSectionQuestionTest
     * Includes             : 1. Questionnaire flow  :
     *                          a. Create questionnaire
     *                          b. Answer the question in dependents Section
     *                          c. Verify the questionnaire in mongodb that answer is updated
     *                          d. Verify answer
     */

    @Test(description = "", groups = {"docittapitests","questionnaireApiTests","dependentsSectionQuestionTest"})
    public void VerifyDependantsSectionQuestion() throws Exception{
        String sTestID = "dependentsSectionQuestionTest";
        String result = "Failed";
        String formId=null;
        String tempAppId=null;
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            String urlEndPoint = restUrl + ":"+ apiParam.getPort();
            Response createTemplateResponse = qUtils.postCreateFormRequest(conType, urlEndPoint, apiParam.requestType);
            if(createTemplateResponse.getStatusCode() != 200) {
                qUtils.showErrorIfFailed(createTemplateResponse);
            }
            assertEquals(createTemplateResponse.getStatusCode(), 200);
            formId = qUtils.getJsonString(createTemplateResponse,"formId");
            logger.info("FormId extracted is :"+formId);
            Response addQuestionnaireResponse = qUtils.postGetStartedRequest(conType, urlEndPoint, apiParam.requestType,formId);
            assertEquals(addQuestionnaireResponse.getStatusCode(), 200);
            tempAppId = addQuestionnaireResponse.prettyPrint().toString();
            Response qualifyingAppResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(qualifyingAppResponse.getStatusCode(), 200);
            Response updateQualifyingAppResponse = qUtils.updateQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(updateQualifyingAppResponse.prettyPrint().toString(),"true");
            Response getUpdatedResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(getUpdatedResponse.getStatusCode(), 200);
            qUtils.verifyQualifyingAppAnswser(getUpdatedResponse.prettyPrint());
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }

    /**
     * Description          : residenceInfoSectionQuestionTest
     * Includes             : 1. Questionnaire flow  :
     *                          a. Create questionnaire
     *                          b. Answer the question in residenceInfo Section
     *                          c. Verify the questionnaire in mongodb that answer is updated
     *                          d. Verify answer
     */

    @Test(description = "", groups = {"docittapitests","questionnaireApiTests","residenceInfoSectionQuestionTest"})
    public void VerifyResidenceInfoSectionQuestion() throws Exception{
        String sTestID = "residenceInfoSectionQuestionTest";
        String result = "Failed";
        String formId=null;
        String tempAppId=null;
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            String urlEndPoint = restUrl + ":"+ apiParam.getPort();
            Response createTemplateResponse = qUtils.postCreateFormRequest(conType, urlEndPoint, apiParam.requestType);
            if(createTemplateResponse.getStatusCode() != 200) {
                qUtils.showErrorIfFailed(createTemplateResponse);
            }
            assertEquals(createTemplateResponse.getStatusCode(), 200);
            formId = qUtils.getJsonString(createTemplateResponse,"formId");
            logger.info("FormId extracted is :"+formId);
            Response addQuestionnaireResponse = qUtils.postGetStartedRequest(conType, urlEndPoint, apiParam.requestType,formId);
            assertEquals(addQuestionnaireResponse.getStatusCode(), 200);
            tempAppId = addQuestionnaireResponse.prettyPrint().toString();
            Response qualifyingAppResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(qualifyingAppResponse.getStatusCode(), 200);
            Response updateQualifyingAppResponse = qUtils.updateQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(updateQualifyingAppResponse.prettyPrint().toString(),"true");
            Response getUpdatedResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(getUpdatedResponse.getStatusCode(), 200);
            qUtils.verifyQualifyingAppAnswser(getUpdatedResponse.prettyPrint());
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }

    /**
     * Description          : stayHistorySectionQuestionTest
     * Includes             : 1. Questionnaire flow  :
     *                          a. Create questionnaire
     *                          b. Answer the question in stayHistory Section
     *                          c. Verify the questionnaire in mongodb that answer is updated
     *                          d. Verify answer
     */

    @Test(description = "", groups = {"docittapitests","questionnaireApiTests","stayHistorySectionQuestionTest"})
    public void VerifyStayHistorySectionQuestion() throws Exception{
        String sTestID = "stayHistorySectionQuestionTest";
        String result = "Failed";
        String formId=null;
        String tempAppId=null;
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            String urlEndPoint = restUrl + ":"+ apiParam.getPort();
            Response createTemplateResponse = qUtils.postCreateFormRequest(conType, urlEndPoint, apiParam.requestType);
            if(createTemplateResponse.getStatusCode() != 200) {
                qUtils.showErrorIfFailed(createTemplateResponse);
            }
            assertEquals(createTemplateResponse.getStatusCode(), 200);
            formId = qUtils.getJsonString(createTemplateResponse,"formId");
            logger.info("FormId extracted is :"+formId);
            Response addQuestionnaireResponse = qUtils.postGetStartedRequest(conType, urlEndPoint, apiParam.requestType,formId);
            assertEquals(addQuestionnaireResponse.getStatusCode(), 200);
            tempAppId = addQuestionnaireResponse.prettyPrint().toString();
            Response qualifyingAppResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(qualifyingAppResponse.getStatusCode(), 200);
            Response updateQualifyingAppResponse = qUtils.updateQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(updateQualifyingAppResponse.prettyPrint().toString(),"true");
            Response getUpdatedResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(getUpdatedResponse.getStatusCode(), 200);
            qUtils.verifyQualifyingAppAnswser(getUpdatedResponse.prettyPrint());
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }

    /**
     * Description          : mailingAddressVerificationSectionQuestionTest
     * Includes             : 1. Questionnaire flow  :
     *                          a. Create questionnaire
     *                          b. Answer the question in mailingAddressVerification Section
     *                          c. Verify the questionnaire in mongodb that answer is updated
     *                          d. Verify answer
     */

    @Test(description = "", groups = {"docittapitests","questionnaireApiTests","mailingAddressVerificationSectionQuestionTest"})
    public void VerifyMailingAddressSectionQuestion() throws Exception{
        String sTestID = "mailingAddressVerificationSectionQuestionTest";
        String result = "Failed";
        String formId=null;
        String tempAppId=null;
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            String urlEndPoint = restUrl + ":"+ apiParam.getPort();
            Response createTemplateResponse = qUtils.postCreateFormRequest(conType, urlEndPoint, apiParam.requestType);
            if(createTemplateResponse.getStatusCode() != 200) {
                qUtils.showErrorIfFailed(createTemplateResponse);
            }
            assertEquals(createTemplateResponse.getStatusCode(), 200);
            formId = qUtils.getJsonString(createTemplateResponse,"formId");
            logger.info("FormId extracted is :"+formId);
            Response addQuestionnaireResponse = qUtils.postGetStartedRequest(conType, urlEndPoint, apiParam.requestType,formId);
            assertEquals(addQuestionnaireResponse.getStatusCode(), 200);
            tempAppId = addQuestionnaireResponse.prettyPrint().toString();
            Response qualifyingAppResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(qualifyingAppResponse.getStatusCode(), 200);
            Response updateQualifyingAppResponse = qUtils.updateQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(updateQualifyingAppResponse.prettyPrint().toString(),"true");
            Response getUpdatedResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(getUpdatedResponse.getStatusCode(), 200);
            qUtils.verifyQualifyingAppAnswser(getUpdatedResponse.prettyPrint());
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }

    }

    /**
     * Description          : propertyInformationSectionQuestionTest
     * Includes             : 1. Questionnaire flow  :
     *                          a. Create questionnaire
     *                          b. Answer the question in propertyInformation Section
     *                          c. Verify the questionnaire in mongodb that answer is updated
     *                          d. Verify answer
     */

    @Test(description = "", groups = {"docittapitests","questionnaireApiTests","propertyInformationSectionQuestionTest"})
    public void VerifyPropertyInformationSectionQuestion() throws Exception {
        String sTestID = "propertyInformationSectionQuestionTest";
        String result = "Failed";
        String formId = null;
        String tempAppId = null;
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod, sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl = apiPropertiesReader.getBaseresturl();
            String urlEndPoint = restUrl + ":" + apiParam.getPort();
            Response createTemplateResponse = qUtils.postCreateFormRequest(conType, urlEndPoint, apiParam.requestType);
            if(createTemplateResponse.getStatusCode() != 200) {
                qUtils.showErrorIfFailed(createTemplateResponse);
            }
            assertEquals(createTemplateResponse.getStatusCode(), 200);
            formId = qUtils.getJsonString(createTemplateResponse, "formId");
            logger.info("FormId extracted is :" + formId);
            Response addQuestionnaireResponse = qUtils.postGetStartedRequest(conType, urlEndPoint, apiParam.requestType, formId);
            assertEquals(addQuestionnaireResponse.getStatusCode(), 200);
            tempAppId = addQuestionnaireResponse.prettyPrint().toString();
            Response qualifyingAppResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType, tempAppId);
            assertEquals(qualifyingAppResponse.getStatusCode(), 200);
            Response updateQualifyingAppResponse = qUtils.updateQualifyingApp(conType, urlEndPoint, apiParam.requestType, tempAppId);
            assertEquals(updateQualifyingAppResponse.prettyPrint().toString(), "true");
            Response getUpdatedResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType, tempAppId);
            assertEquals(getUpdatedResponse.getStatusCode(), 200);
            qUtils.verifyQualifyingAppAnswser(getUpdatedResponse.prettyPrint());
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }

    /**
     * Description          : newHomeSectionQuestionTest
     * Includes             : 1. Questionnaire flow  :
     *                          a. Create questionnaire
     *                          b. Answer the question in newHome Section
     *                          c. Verify the questionnaire in mongodb that answer is updated
     *                          d. Verify answer
     */

    @Test(description = "", groups = {"docittapitests","questionnaireApiTests","newHomeSectionQuestionTest"})
    public void VerifyNewHomeSectionQuestion() throws Exception{
        String sTestID = "newHomeSectionQuestionTest";
        String result = "Failed";
        String formId=null;
        String tempAppId=null;
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            String urlEndPoint = restUrl + ":"+ apiParam.getPort();
            Response createTemplateResponse = qUtils.postCreateFormRequest(conType, urlEndPoint, apiParam.requestType);
            if(createTemplateResponse.getStatusCode() != 200) {
                qUtils.showErrorIfFailed(createTemplateResponse);
            }
            assertEquals(createTemplateResponse.getStatusCode(), 200);
            formId = qUtils.getJsonString(createTemplateResponse,"formId");
            logger.info("FormId extracted is :"+formId);
            Response addQuestionnaireResponse = qUtils.postGetStartedRequest(conType, urlEndPoint, apiParam.requestType,formId);
            assertEquals(addQuestionnaireResponse.getStatusCode(), 200);
            tempAppId = addQuestionnaireResponse.prettyPrint().toString();
            Response qualifyingAppResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(qualifyingAppResponse.getStatusCode(), 200);
            Response updateQualifyingAppResponse = qUtils.updateQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(updateQualifyingAppResponse.prettyPrint().toString(),"true");
            Response getUpdatedResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(getUpdatedResponse.getStatusCode(), 200);
            qUtils.verifyQualifyingAppAnswser(getUpdatedResponse.prettyPrint());
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }

    /**
     * Description          : titleInfoSectionQuestionTest
     * Includes             : 1. Questionnaire flow  :
     *                          a. Create questionnaire
     *                          b. Answer the question in title Section
     *                          c. Verify the questionnaire in mongodb that answer is updated
     *                          d. Verify answer
     */

    @Test(description = "", groups = {"docittapitests","questionnaireApiTests","titleInfoSectionQuestionTest"})
    public void VerifyTitleInfoSectionQuestion() throws Exception{
        String sTestID = "titleInfoSectionQuestionTest";
        String result = "Failed";
        String formId=null;
        String tempAppId=null;
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            String urlEndPoint = restUrl + ":"+ apiParam.getPort();
            Response createTemplateResponse = qUtils.postCreateFormRequest(conType, urlEndPoint, apiParam.requestType);
            if(createTemplateResponse.getStatusCode() != 200) {
                qUtils.showErrorIfFailed(createTemplateResponse);
            }
            assertEquals(createTemplateResponse.getStatusCode(), 200);
            formId = qUtils.getJsonString(createTemplateResponse,"formId");
            logger.info("FormId extracted is :"+formId);
            Response addQuestionnaireResponse = qUtils.postGetStartedRequest(conType, urlEndPoint, apiParam.requestType,formId);
            assertEquals(addQuestionnaireResponse.getStatusCode(), 200);
            tempAppId = addQuestionnaireResponse.prettyPrint().toString();
            Response qualifyingAppResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(qualifyingAppResponse.getStatusCode(), 200);
            Response updateQualifyingAppResponse = qUtils.updateQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(updateQualifyingAppResponse.prettyPrint().toString(),"true");
            Response getUpdatedResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(getUpdatedResponse.getStatusCode(), 200);
            qUtils.verifyQualifyingAppAnswser(getUpdatedResponse.prettyPrint());
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }

    /**
     * Description          : downPaymentSectionQuestionTest
     * Includes             : 1. Questionnaire flow  :
     *                          a. Create questionnaire
     *                          b. Answer the question in down payment Section
     *                          c. Verify the questionnaire in mongodb that answer is updated
     *                          d. Verify answer
     */

    @Test(description = "", groups = {"docittapitests","questionnaireApiTests","downPaymentSectionQuestionTest"})
    public void VerifyDownPaymentSectionQuestion() throws Exception{
        String sTestID = "downPaymentSectionQuestionTest";
        String result = "Failed";
        String formId=null;
        String tempAppId=null;
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            String urlEndPoint = restUrl + ":"+ apiParam.getPort();
            Response createTemplateResponse = qUtils.postCreateFormRequest(conType, urlEndPoint, apiParam.requestType);
            if(createTemplateResponse.getStatusCode() != 200) {
                qUtils.showErrorIfFailed(createTemplateResponse);
            }
            assertEquals(createTemplateResponse.getStatusCode(), 200);
            formId = qUtils.getJsonString(createTemplateResponse,"formId");
            logger.info("FormId extracted is :"+formId);
            Response addQuestionnaireResponse = qUtils.postGetStartedRequest(conType, urlEndPoint, apiParam.requestType,formId);
            assertEquals(addQuestionnaireResponse.getStatusCode(), 200);
            tempAppId = addQuestionnaireResponse.prettyPrint().toString();
            Response qualifyingAppResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(qualifyingAppResponse.getStatusCode(), 200);
            Response updateQualifyingAppResponse = qUtils.updateQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(updateQualifyingAppResponse.prettyPrint().toString(),"true");
            Response getUpdatedResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(getUpdatedResponse.getStatusCode(), 200);
            qUtils.verifyQualifyingAppAnswser(getUpdatedResponse.prettyPrint());
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }

    /**
     * Description          : employmentHistorySectionQuestionTest
     * Includes             : 1. Questionnaire flow  :
     *                          a. Create questionnaire
     *                          b. Answer the question in employmentHistory Section
     *                          c. Verify the questionnaire in mongodb that answer is updated
     *                          d. Verify answer
     */

    @Test(description = "", groups = {"docittapitests","questionnaireApiTests","employmentHistorySectionQuestionTest"})
    public void VerifyEmploymentHistorySectionQuestion() throws Exception{
        String sTestID = "employmentHistorySectionQuestionTest";
        String result = "Failed";
        String formId=null;
        String tempAppId=null;
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            String urlEndPoint = restUrl + ":"+ apiParam.getPort();
            Response createTemplateResponse = qUtils.postCreateFormRequest(conType, urlEndPoint, apiParam.requestType);
            if(createTemplateResponse.getStatusCode() != 200) {
                qUtils.showErrorIfFailed(createTemplateResponse);
            }
            assertEquals(createTemplateResponse.getStatusCode(), 200);
            formId = qUtils.getJsonString(createTemplateResponse,"formId");
            logger.info("FormId extracted is :"+formId);
            Response addQuestionnaireResponse = qUtils.postGetStartedRequest(conType, urlEndPoint, apiParam.requestType,formId);
            assertEquals(addQuestionnaireResponse.getStatusCode(), 200);
            tempAppId = addQuestionnaireResponse.prettyPrint().toString();
            Response qualifyingAppResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(qualifyingAppResponse.getStatusCode(), 200);
            Response updateQualifyingAppResponse = qUtils.updateQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(updateQualifyingAppResponse.prettyPrint().toString(),"true");
            Response getUpdatedResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(getUpdatedResponse.getStatusCode(), 200);
            qUtils.verifyQualifyingAppAnswser(getUpdatedResponse.prettyPrint());
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }

    /**
     * Description          : monthlyIncomeSectionQuestionTest
     * Includes             : 1. Questionnaire flow  :
     *                          a. Create questionnaire
     *                          b. Answer the question in monthlyIncome Section
     *                          c. Verify the questionnaire in mongodb that answer is updated
     *                          d. Verify answer
     */

    @Test(description = "", groups = {"docittapitests","questionnaireApiTests","monthlyIncomeSectionQuestionTest"})
    public void VerifyMonthlyIncomeSectionQuestion() throws Exception{
        String sTestID = "monthlyIncomeSectionQuestionTest";
        String result = "Failed";
        String formId=null;
        String tempAppId=null;
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            String urlEndPoint = restUrl + ":"+ apiParam.getPort();
            Response createTemplateResponse = qUtils.postCreateFormRequest(conType, urlEndPoint, apiParam.requestType);
            if(createTemplateResponse.getStatusCode() != 200) {
                qUtils.showErrorIfFailed(createTemplateResponse);
            }
            assertEquals(createTemplateResponse.getStatusCode(), 200);
            formId = qUtils.getJsonString(createTemplateResponse,"formId");
            logger.info("FormId extracted is :"+formId);
            Response addQuestionnaireResponse = qUtils.postGetStartedRequest(conType, urlEndPoint, apiParam.requestType,formId);
            assertEquals(addQuestionnaireResponse.getStatusCode(), 200);
            tempAppId = addQuestionnaireResponse.prettyPrint().toString();
            Response qualifyingAppResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(qualifyingAppResponse.getStatusCode(), 200);
            Response updateQualifyingAppResponse = qUtils.updateQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(updateQualifyingAppResponse.prettyPrint().toString(),"true");
            Response getUpdatedResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(getUpdatedResponse.getStatusCode(), 200);
            qUtils.verifyQualifyingAppAnswser(getUpdatedResponse.prettyPrint());
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }

    /**
     * Description          : monthlyExpenseSectionQuestionTest
     * Includes             : 1. Questionnaire flow  :
     *                          a. Create questionnaire
     *                          b. Answer the question in monthlyExpense Section
     *                          c. Verify the questionnaire in mongodb that answer is updated
     *                          d. Verify answer
     */

    @Test(description = "", groups = {"docittapitests","questionnaireApiTests","monthlyExpenseSectionQuestionTest"})
    public void VerifyMonthlyExpenseSectionQuestion() throws Exception{
        String sTestID = "monthlyExpenseSectionQuestionTest";
        String result = "Failed";
        String formId=null;
        String tempAppId=null;
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            String urlEndPoint = restUrl + ":"+ apiParam.getPort();
            Response createTemplateResponse = qUtils.postCreateFormRequest(conType, urlEndPoint, apiParam.requestType);
            if(createTemplateResponse.getStatusCode() != 200) {
                qUtils.showErrorIfFailed(createTemplateResponse);
            }
            assertEquals(createTemplateResponse.getStatusCode(), 200);
            formId = qUtils.getJsonString(createTemplateResponse,"formId");
            logger.info("FormId extracted is :"+formId);
            Response addQuestionnaireResponse = qUtils.postGetStartedRequest(conType, urlEndPoint, apiParam.requestType,formId);
            assertEquals(addQuestionnaireResponse.getStatusCode(), 200);
            tempAppId = addQuestionnaireResponse.prettyPrint().toString();
            Response qualifyingAppResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(qualifyingAppResponse.getStatusCode(), 200);
            Response updateQualifyingAppResponse = qUtils.updateQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(updateQualifyingAppResponse.prettyPrint().toString(),"true");
            Response getUpdatedResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(getUpdatedResponse.getStatusCode(), 200);
            qUtils.verifyQualifyingAppAnswser(getUpdatedResponse.prettyPrint());
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }

    /**
     * Description          : bankInfoSectionQuestionTest
     * Includes             : 1. Questionnaire flow  :
     *                          a. Create questionnaire
     *                          b. Answer the question in Bank Info Section
     *                          c. Verify the questionnaire in mongodb that answer is updated
     *                          d. Verify answer
     */

    @Test(description = "", groups = {"docittapitests","questionnaireApiTests","bankInfoSectionQuestionTest"})
    public void VerifyBankInfoSectionQuestion() throws Exception{
        String sTestID = "bankInfoSectionQuestionTest";
        String result = "Failed";
        String formId=null;
        String tempAppId=null;
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            String urlEndPoint = restUrl + ":"+ apiParam.getPort();
            Response createTemplateResponse = qUtils.postCreateFormRequest(conType, urlEndPoint, apiParam.requestType);
            if(createTemplateResponse.getStatusCode() != 200) {
                qUtils.showErrorIfFailed(createTemplateResponse);
            }
            assertEquals(createTemplateResponse.getStatusCode(), 200);
            formId = qUtils.getJsonString(createTemplateResponse,"formId");
            logger.info("FormId extracted is :"+formId);
            Response addQuestionnaireResponse = qUtils.postGetStartedRequest(conType, urlEndPoint, apiParam.requestType,formId);
            assertEquals(addQuestionnaireResponse.getStatusCode(), 200);
            tempAppId = addQuestionnaireResponse.prettyPrint().toString();
            Response qualifyingAppResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(qualifyingAppResponse.getStatusCode(), 200);
            Response updateQualifyingAppResponse = qUtils.updateQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(updateQualifyingAppResponse.prettyPrint().toString(),"true");
            Response getUpdatedResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(getUpdatedResponse.getStatusCode(), 200);
            qUtils.verifyQualifyingAppAnswser(getUpdatedResponse.prettyPrint());
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }

    /**
     * Description          : bankTypeSectionQuestionTest
     * Includes             : 1. Questionnaire flow  :
     *                          a. Create questionnaire
     *                          b. Answer the question in Bank type Section
     *                          c. Verify the questionnaire in mongodb that answer is updated
     *                          d. Verify answer
     */

    @Test(description = "", groups = {"docittapitests","questionnaireApiTests","bankTypeSectionQuestionTest"})
    public void VerifyBankTypeSectionQuestion() throws Exception{
        String sTestID = "bankTypeSectionQuestionTest";
        String result = "Failed";
        String formId=null;
        String tempAppId=null;
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            String urlEndPoint = restUrl + ":"+ apiParam.getPort();
            Response createTemplateResponse = qUtils.postCreateFormRequest(conType, urlEndPoint, apiParam.requestType);
            if(createTemplateResponse.getStatusCode() != 200) {
                qUtils.showErrorIfFailed(createTemplateResponse);
            }
            assertEquals(createTemplateResponse.getStatusCode(), 200);
            formId = qUtils.getJsonString(createTemplateResponse,"formId");
            logger.info("FormId extracted is :"+formId);
            Response addQuestionnaireResponse = qUtils.postGetStartedRequest(conType, urlEndPoint, apiParam.requestType,formId);
            assertEquals(addQuestionnaireResponse.getStatusCode(), 200);
            tempAppId = addQuestionnaireResponse.prettyPrint().toString();
            Response qualifyingAppResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(qualifyingAppResponse.getStatusCode(), 200);
            Response updateQualifyingAppResponse = qUtils.updateQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(updateQualifyingAppResponse.prettyPrint().toString(),"true");
            Response getUpdatedResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(getUpdatedResponse.getStatusCode(), 200);
            qUtils.verifyQualifyingAppAnswser(getUpdatedResponse.prettyPrint());
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }

    /**
     * Description          : InvestmentInfoSectionQuestionTest
     * Includes             : 1. Questionnaire flow  :
     *                          a. Create questionnaire
     *                          b. Answer the question in Investment Section
     *                          c. Verify the questionnaire in mongodb that answer is updated
     *                          d. Verify answer
     */

    @Test(description = "", groups = {"docittapitests","questionnaireApiTests","InvestmentInfoSectionQuestionTest"})
    public void VerifyInvestmentInfoSectionQuestion() throws Exception{
        String sTestID = "InvestmentInfoSectionQuestionTest";
        String result = "Failed";
        String formId=null;
        String tempAppId=null;
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            String urlEndPoint = restUrl + ":"+ apiParam.getPort();
            Response createTemplateResponse = qUtils.postCreateFormRequest(conType, urlEndPoint, apiParam.requestType);
            if(createTemplateResponse.getStatusCode() != 200) {
                qUtils.showErrorIfFailed(createTemplateResponse);
            }
            assertEquals(createTemplateResponse.getStatusCode(), 200);
            formId = qUtils.getJsonString(createTemplateResponse,"formId");
            logger.info("FormId extracted is :"+formId);
            Response addQuestionnaireResponse = qUtils.postGetStartedRequest(conType, urlEndPoint, apiParam.requestType,formId);
            assertEquals(addQuestionnaireResponse.getStatusCode(), 200);
            tempAppId = addQuestionnaireResponse.prettyPrint().toString();
            Response qualifyingAppResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(qualifyingAppResponse.getStatusCode(), 200);
            Response updateQualifyingAppResponse = qUtils.updateQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(updateQualifyingAppResponse.prettyPrint().toString(),"true");
            Response getUpdatedResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(getUpdatedResponse.getStatusCode(), 200);
            qUtils.verifyQualifyingAppAnswser(getUpdatedResponse.prettyPrint());
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }

    /**
     * Description          : liabilitiesSectionQuestionTest
     * Includes             : 1. Questionnaire flow  :
     *                          a. Create questionnaire
     *                          b. Answer the question in liabilities Section
     *                          c. Verify the questionnaire in mongodb that answer is updated
     *                          d. Verify answer
     */

    @Test(description = "", groups = {"docittapitests","questionnaireApiTests","liabilitiesSectionQuestionTest"})
    public void VerifyLiabilitiesSectionQuestion() throws Exception{
        String sTestID = "liabilitiesSectionQuestionTest";
        String result = "Failed";
        String formId=null;
        String tempAppId=null;
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            String urlEndPoint = restUrl + ":"+ apiParam.getPort();
            Response createTemplateResponse = qUtils.postCreateFormRequest(conType, urlEndPoint, apiParam.requestType);
            if(createTemplateResponse.getStatusCode() != 200) {
                qUtils.showErrorIfFailed(createTemplateResponse);
            }
            assertEquals(createTemplateResponse.getStatusCode(), 200);
            formId = qUtils.getJsonString(createTemplateResponse,"formId");
            logger.info("FormId extracted is :"+formId);
            Response addQuestionnaireResponse = qUtils.postGetStartedRequest(conType, urlEndPoint, apiParam.requestType,formId);
            assertEquals(addQuestionnaireResponse.getStatusCode(), 200);
            tempAppId = addQuestionnaireResponse.prettyPrint().toString();
            Response qualifyingAppResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(qualifyingAppResponse.getStatusCode(), 200);
            Response updateQualifyingAppResponse = qUtils.updateQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(updateQualifyingAppResponse.prettyPrint().toString(),"true");
            Response getUpdatedResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(getUpdatedResponse.getStatusCode(), 200);
            qUtils.verifyQualifyingAppAnswser(getUpdatedResponse.prettyPrint());
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }

    /**
     * Description          : accountBalanceSectionQuestionTest
     * Includes             : 1. Questionnaire flow  :
     *                          a. Create questionnaire
     *                          b. Answer the question in Account Balance Section
     *                          c. Verify the questionnaire in mongodb that answer is updated
     *                          d. Verify answer
     */

    @Test(description = "", groups = {"docittapitests","questionnaireApiTests","accountBalanceSectionQuestionTest"})
    public void VerifyAccountBalanceSectionQuestion() throws Exception{
        String sTestID = "accountBalanceSectionQuestionTest";
        String result = "Failed";
        String formId=null;
        String tempAppId=null;
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            String urlEndPoint = restUrl + ":"+ apiParam.getPort();
            Response createTemplateResponse = qUtils.postCreateFormRequest(conType, urlEndPoint, apiParam.requestType);
            if(createTemplateResponse.getStatusCode() != 200) {
                qUtils.showErrorIfFailed(createTemplateResponse);
            }
            assertEquals(createTemplateResponse.getStatusCode(), 200);
            formId = qUtils.getJsonString(createTemplateResponse,"formId");
            logger.info("FormId extracted is :"+formId);
            Response addQuestionnaireResponse = qUtils.postGetStartedRequest(conType, urlEndPoint, apiParam.requestType,formId);
            assertEquals(addQuestionnaireResponse.getStatusCode(), 200);
            tempAppId = addQuestionnaireResponse.prettyPrint().toString();
            Response qualifyingAppResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(qualifyingAppResponse.getStatusCode(), 200);
            Response updateQualifyingAppResponse = qUtils.updateQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(updateQualifyingAppResponse.prettyPrint().toString(),"true");
            Response getUpdatedResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(getUpdatedResponse.getStatusCode(), 200);
            qUtils.verifyQualifyingAppAnswser(getUpdatedResponse.prettyPrint());
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }

    /**
     * Description          : CreditSectionQuestionTest
     * Includes             : 1. Questionnaire flow  :
     *                          a. Create questionnaire
     *                          b. Answer the question in Credit Section
     *                          c. Verify the questionnaire in mongodb that answer is updated
     *                          d. Verify answer
     */

    @Test(description = "", groups = {"docittapitests","questionnaireApiTests","CreditSectionQuestionTest"})
    public void VerifyCreditSectionQuestion() throws Exception{
        String sTestID = "CreditSectionQuestionTest";
        String result = "Failed";
        String formId=null;
        String tempAppId=null;
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            String urlEndPoint = restUrl + ":"+ apiParam.getPort();
            Response createTemplateResponse = qUtils.postCreateFormRequest(conType, urlEndPoint, apiParam.requestType);
            if(createTemplateResponse.getStatusCode() != 200) {
                qUtils.showErrorIfFailed(createTemplateResponse);
            }
            assertEquals(createTemplateResponse.getStatusCode(), 200);
            formId = qUtils.getJsonString(createTemplateResponse,"formId");
            logger.info("FormId extracted is :"+formId);
            Response addQuestionnaireResponse = qUtils.postGetStartedRequest(conType, urlEndPoint, apiParam.requestType,formId);
            assertEquals(addQuestionnaireResponse.getStatusCode(), 200);
            tempAppId = addQuestionnaireResponse.prettyPrint().toString();
            Response qualifyingAppResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(qualifyingAppResponse.getStatusCode(), 200);
            Response updateQualifyingAppResponse = qUtils.updateQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(updateQualifyingAppResponse.prettyPrint().toString(),"true");
            Response getUpdatedResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(getUpdatedResponse.getStatusCode(), 200);
            qUtils.verifyQualifyingAppAnswser(getUpdatedResponse.prettyPrint());
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }

    /**
     * Description          : declarationSectionQuestionTest
     * Includes             : 1. Questionnaire flow  :
     *                          a. Create questionnaire
     *                          b. Answer the question in declaration Section
     *                          c. Verify the questionnaire in mongodb that answer is updated
     *                          d. Verify answer
     */

    @Test(description = "", groups = {"docittapitests","questionnaireApiTests","declarationSectionQuestionTest"})
    public void VerifyDeclarationSectionQuestion() throws Exception{
        String sTestID = "declarationSectionQuestionTest";
        String result = "Failed";
        String formId=null;
        String tempAppId=null;
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            String urlEndPoint = restUrl + ":"+ apiParam.getPort();
            Response createTemplateResponse = qUtils.postCreateFormRequest(conType, urlEndPoint, apiParam.requestType);
            if(createTemplateResponse.getStatusCode() != 200) {
                qUtils.showErrorIfFailed(createTemplateResponse);
            }
            assertEquals(createTemplateResponse.getStatusCode(), 200);
            formId = qUtils.getJsonString(createTemplateResponse,"formId");
            logger.info("FormId extracted is :"+formId);
            Response addQuestionnaireResponse = qUtils.postGetStartedRequest(conType, urlEndPoint, apiParam.requestType,formId);
            assertEquals(addQuestionnaireResponse.getStatusCode(), 200);
            tempAppId = addQuestionnaireResponse.prettyPrint().toString();
            Response qualifyingAppResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(qualifyingAppResponse.getStatusCode(), 200);
            Response updateQualifyingAppResponse = qUtils.updateQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(updateQualifyingAppResponse.prettyPrint().toString(),"true");
            Response getUpdatedResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(getUpdatedResponse.getStatusCode(), 200);
            qUtils.verifyQualifyingAppAnswser(getUpdatedResponse.prettyPrint());
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }

    /**
     * Description          : phoneNumberQuestionTest
     * Includes             : 1. Questionnaire flow  :
     *                          a. Create questionnaire
     *                          b. Answer the question related to phone number
     *                          c. Verify the questionnaire in mongodb that answer is updated
     *                          d. Verify answer
     */

    @Test(description = "", groups = {"docittapitests","questionnaireApiTests","phoneNumberQuestionTest"})
    public void VerifyPhoneNumberSectionQuestion() throws Exception{
        String sTestID = "phoneNumberQuestionTest";
        String result = "Failed";
        String formId=null;
        String tempAppId=null;
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            String urlEndPoint = restUrl + ":"+ apiParam.getPort();
            Response createTemplateResponse = qUtils.postCreateFormRequest(conType, urlEndPoint, apiParam.requestType);
            if(createTemplateResponse.getStatusCode() != 200) {
                qUtils.showErrorIfFailed(createTemplateResponse);
            }
            assertEquals(createTemplateResponse.getStatusCode(), 200);
            formId = qUtils.getJsonString(createTemplateResponse,"formId");
            logger.info("FormId extracted is :"+formId);
            Response addQuestionnaireResponse = qUtils.postGetStartedRequest(conType, urlEndPoint, apiParam.requestType,formId);
            assertEquals(addQuestionnaireResponse.getStatusCode(), 200);
            tempAppId = addQuestionnaireResponse.prettyPrint().toString();
            Response qualifyingAppResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(qualifyingAppResponse.getStatusCode(), 200);
            Response updateQualifyingAppResponse = qUtils.updateQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(updateQualifyingAppResponse.prettyPrint().toString(),"true");
            Response getUpdatedResponse = qUtils.getQualifyingApp(conType, urlEndPoint, apiParam.requestType,tempAppId);
            assertEquals(getUpdatedResponse.getStatusCode(), 200);
            qUtils.verifyQualifyingAppAnswser(getUpdatedResponse.prettyPrint());
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }
}
