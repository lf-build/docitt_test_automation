package net.sigmainfo.lf.automation.api.tests;

import com.jayway.restassured.response.Response;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import net.sigmainfo.lf.automation.api.function.APIFuncUtils;
import net.sigmainfo.lf.automation.api.constant.ApiParam;
import net.sigmainfo.lf.automation.api.function.ApiPropertiesReader;
import net.sigmainfo.lf.automation.api.function.QuestionnaireUtils;
import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.common.TestResults;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import java.net.UnknownHostException;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

/**
 * Created by shaishav.s on 28-03-2017.
 */
public class TeamMemberAssignmentTests extends AbstractTests {
    @Autowired
    TestResults testResults;

    @Autowired
    APIFuncUtils apiFuncUtils;

    @Autowired
    ApiParam apiParam;

    @Autowired
    ApiPropertiesReader apiPropertiesReader;

    @Autowired
    QuestionnaireUtils qUtils;

    public static String funcMod="Docitt_Api";

    @AfterClass(alwaysRun = true)
    private void endCasereport() throws Exception {

        deleteAddedAssignments();
        String funcModule = "Docitt_Api";
        String className = org.apache.commons.lang.StringUtils.substringAfterLast(this.getClass().getName(), ".");
        String description = "Verify  " + org.apache.commons.lang.StringUtils.substringAfterLast(this.getClass().getName(), ".");
        generateReport(className, description,funcMod);
    }

    private void deleteAddedAssignments() throws UnknownHostException {
        logger.info("Querying mongoDb for users added during this test collection");
        logger.info("==================");
        MongoClient mongoClient = null;

        mongoClient = new MongoClient(apiParam.MongoHost, apiParam.MongoPort);
        DB db = mongoClient.getDB("assignment");
        //auth = db.authenticate(portalPropertiesReader.getMongoDbUser(), portalPropertiesReader.getMongoDbPwd().toCharArray());
        DBCollection collection = db.getCollection("Invites");
        BasicDBObject b1 = new BasicDBObject();
        b1.append("InviteEmail", "aab@bacd.com");
        collection.remove(b1);

        DBCollection c2 = db.getCollection("assignments");
        BasicDBObject b2 = new BasicDBObject();
        b2.append("EntityId", "99");
        c2.remove(b2);

        DBCollection c3 = db.getCollection("assignments");
        BasicDBObject b3 = new BasicDBObject();
        b3.append("EntityId", "98");
        c3.remove(b3);

        logger.info("Invites and Assignments are deleted.");
    }

    /**
     * Description          : getMembersInTeamFlow
     * Includes             : 1. verifies whether team member is added successfully
     *                        a. Get Current Team Assignment
     *                        b. Send Invite
     *                        c. Verify Token
     *                        d. Update Invitation
     *                        e. Get Invited Information
     *                        f. Reverify whether member added
     */

    @Test(priority=1,description = "", groups = {"docittapitests","teamMemberAssignmenttests","getMembersInTeamFlow"})
    public void VerifyAddTeamMembersFlow() throws Exception{
        String sTestID = "getMembersInTeamFlow";
        String result = "Failed";
        int teamNum=99;
        boolean isCorrectEmail=true;
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            String urlEndPoint = restUrl + ":"+ apiParam.getPort() + apiParam.requestType;
            // Get Current Team Assignment
            Response getTeamAssignmentResp = qUtils.getTeamAssignmentResposne(conType, urlEndPoint, apiParam.requestType,teamNum);
            if(getTeamAssignmentResp.getStatusCode() != 200) {
                qUtils.showErrorIfFailed(getTeamAssignmentResp);
            }
            assertEquals(getTeamAssignmentResp.getStatusCode(), 200);
            //Send Invite
            Response sendInviteResp = qUtils.postSendInviteRequest(conType, urlEndPoint, apiParam.requestType,teamNum);
            if(sendInviteResp.getStatusCode() != 200) {
                qUtils.showErrorIfFailed(getTeamAssignmentResp);
            }
            assertEquals(getTeamAssignmentResp.getStatusCode(), 200);
            String invitationToken=new JSONObject(sendInviteResp.prettyPrint().toString()).getString("invitationToken").toString();
            String userId=new JSONObject(sendInviteResp.prettyPrint().toString()).getString("id").toString();
            logger.info("invitationToken: "+invitationToken);
            //Verify invitation token
            Response verifyTokenResp = qUtils.postTokenVerificationRequest(conType, urlEndPoint, apiParam.requestType,invitationToken);
            if(verifyTokenResp.getStatusCode() != 200) {
                qUtils.showErrorIfFailed(getTeamAssignmentResp);
            }
            assertEquals(getTeamAssignmentResp.getStatusCode(), 200);
            // Update Invitation
            Response updateInviteResp = qUtils.postUpdateInviteRequest(conType, urlEndPoint, apiParam.requestType,invitationToken,userId,isCorrectEmail);
            if(updateInviteResp.getStatusCode() != 200) {
                qUtils.showErrorIfFailed(getTeamAssignmentResp);
            }
            assertEquals(getTeamAssignmentResp.getStatusCode(), 200);
            // Get Invited Information
            Response invitedInfoResp = qUtils.getInviteInfoRequest(conType, urlEndPoint, apiParam.requestType,invitationToken,userId);
            if(invitedInfoResp.getStatusCode() != 200) {
                qUtils.showErrorIfFailed(getTeamAssignmentResp);
            }
            assertEquals(getTeamAssignmentResp.getStatusCode(), 200);
            // Reverify whether member added
            Response getTeamAssignmentResp2 = qUtils.getTeamAssignmentResposne(conType, urlEndPoint, apiParam.requestType,teamNum);
            if(getTeamAssignmentResp2.getStatusCode() != 200) {
                qUtils.showErrorIfFailed(getTeamAssignmentResp2);
            }
            assertEquals(getTeamAssignmentResp2.getStatusCode(), 200);
            JSONArray  JSONResponseBody = new   JSONArray (getTeamAssignmentResp2.prettyPrint().toString());
            assertEquals(JSONResponseBody.getJSONObject(0).getString("assignee").toString(), apiParam.getInviteEmail());
            assertEquals(JSONResponseBody.getJSONObject(0).getString("userId"), userId);
            assertEquals(JSONResponseBody.getJSONObject(0).getString("role"), apiParam.getRoles());
            assertEquals(JSONResponseBody.getJSONObject(0).getBoolean("isActive"), true);
            assertEquals(JSONResponseBody.getJSONObject(0).getBoolean("isDisable"), false);
            assertEquals(JSONResponseBody.getJSONObject(0).getString("entityId"), String.valueOf(teamNum));

            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }

    /**
     * Description          : duplicateInviteTest
     * Includes             : 1. verifies whether team member is added successfully
     *                        a. Get Current Team Assignment
     *                        b. Send Invite to already existing member
     *                        c. Verify response code
     */

    @Test(priority=2,description = "", groups = {"docittapitests","teamMemberAssignmenttests","duplicateInviteTest"})
    public void VerifyDuplicateInviteTest() throws Exception{
        String sTestID = "duplicateInviteTest";
        String result = "Failed";
        int teamNum=99;
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            String urlEndPoint = restUrl + ":"+ apiParam.getPort() + apiParam.requestType;
            // Get Current Team Assignment
            Response getTeamAssignmentResp = qUtils.getTeamAssignmentResposne(conType, urlEndPoint, apiParam.requestType,teamNum);
            if(getTeamAssignmentResp.getStatusCode() != 200) {
                qUtils.showErrorIfFailed(getTeamAssignmentResp);
            }
            assertEquals(getTeamAssignmentResp.getStatusCode(), 200);
            //Send Invite
            Response sendInviteResp = qUtils.postSendInviteRequest(conType, urlEndPoint, apiParam.requestType,teamNum);
            assertEquals(sendInviteResp.getStatusCode(),400);
            assertTrue(new JSONObject(sendInviteResp.prettyPrint().toString()).get("message").toString().contains("User already invited"));
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }

    /**
     * Description          : disableMemberTest
     * Includes             : 1. verifies whether team member is added successfully
     *                        a. Disable the member
     *                        b. Verify response code
     */

    @Test(priority=3,description = "", groups = {"docittapitests","teamMemberAssignmenttests","disableMemberTest"})
    public void VerifyDisableMemberTest() throws Exception{
        String sTestID = "disableMemberTest";
        String result = "Failed";
        int teamNum=99;
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            String urlEndPoint = restUrl + ":"+ apiParam.getPort() + apiParam.requestType;
            // Disable the member
            Response disableMemberResp = qUtils.deleteMemberRequest(conType, urlEndPoint, apiParam.requestType,teamNum);
            if(disableMemberResp.getStatusCode() != 204) {
                qUtils.showErrorIfFailed(disableMemberResp);
            }
            assertEquals(disableMemberResp.getStatusCode(), 204);
            //Verify by getting team assignments
            Response getTeamAssignmentResp = qUtils.getTeamAssignmentResposne(conType, urlEndPoint, apiParam.requestType,teamNum);
            if(getTeamAssignmentResp.getStatusCode() != 200) {
                qUtils.showErrorIfFailed(getTeamAssignmentResp);
            }
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }

    /**
     * Description          : singleInviteTest
     * Includes             : 1. Send Single Invite Request
     *                        2. Verify by getting team assignments
     *                        b. Verify response body
     */

    @Test(priority=3,description = "", groups = {"docittapitests","teamMemberAssignmenttests","singleInviteTest"})
    public void VerifysingleInviteTest() throws Exception{
        String sTestID = "singleInviteTest";
        String result = "Failed";
        int teamNum=98;
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            String urlEndPoint = restUrl + ":"+ apiParam.getPort() + "/application";
            // Single Invite Request
            Response singleInviteResp = qUtils.postSingleInviteRequest(conType, urlEndPoint, apiParam.requestType,teamNum);
            if(singleInviteResp.getStatusCode() != 200) {
                qUtils.showErrorIfFailed(singleInviteResp);
            }
            assertEquals(singleInviteResp.getStatusCode(), 200);
            //Verify by getting team assignments
            Response getTeamAssignmentResp = qUtils.getTeamAssignmentResposne(conType, urlEndPoint, apiParam.requestType,teamNum);
            if(getTeamAssignmentResp.getStatusCode() != 200) {
                qUtils.showErrorIfFailed(getTeamAssignmentResp);
            }
            JSONArray  JSONResponseBody = new   JSONArray (getTeamAssignmentResp.prettyPrint().toString());
            //assertEquals(JSONResponseBody.getJSONObject(0).getString("userId"), apiParam.getName());
            assertEquals(JSONResponseBody.getJSONObject(0).getString("assignee"), apiParam.getInviteName());
            assertEquals(JSONResponseBody.getJSONObject(0).getString("role"), apiParam.getRoles());
            assertEquals(JSONResponseBody.getJSONObject(0).getBoolean("isActive"), true);
            assertEquals(JSONResponseBody.getJSONObject(0).getBoolean("isDisable"), false);
            assertEquals(JSONResponseBody.getJSONObject(0).getString("entityId"), String.valueOf(teamNum));
            assertNotNull(JSONResponseBody.getJSONObject(0).getString("assignedOn"));
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }

    /**
     * Description          : multiInviteTest
     * Includes             : 1. Send multi Invite Request
     *                        2. Verify response code
     */

    @Test(priority=3,description = "", groups = {"docittapitests","teamMemberAssignmenttests","multiInviteTest"})
    public void VerifymultiInviteTest() throws Exception{
        String sTestID = "multiInviteTest";
        String result = "Failed";
        int teamNum=99;
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            String urlEndPoint = restUrl + ":"+ apiParam.getPort() + "/application";
            // Single Invite Request
            Response multiAssignResp = qUtils.postSingleInviteRequest(conType, urlEndPoint, apiParam.requestType,teamNum);
            if(multiAssignResp.getStatusCode() != 200) {
                qUtils.showErrorIfFailed(multiAssignResp);
            }
            assertEquals(multiAssignResp.getStatusCode(), 200);
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }

    /**
     * Description          : incorrectEmailSignUp
     * Includes             : 1. Get Current Team Assignment
     *                        2. Send Invite
     *                        3. Verify invitation token
     *                        4. Update Invitation with user id other than the one through which the user was invited
     */

    @Test(priority=1,description = "", groups = {"docittapitests","teamMemberAssignmenttests","incorrectEmailSignUp"})
    public void VerifyincorrectEmailSignUpFlow() throws Exception{
        String sTestID = "incorrectEmailSignUp";
        String result = "Failed";
        int teamNum=99;
        boolean isCorrectEmail=false;
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
        try {
            String conType = "application/json";
            String restUrl=apiPropertiesReader.getBaseresturl();
            String urlEndPoint = restUrl + ":"+ apiParam.getPort() + apiParam.requestType;
            // Get Current Team Assignment
            Response getTeamAssignmentResp = qUtils.getTeamAssignmentResposne(conType, urlEndPoint, apiParam.requestType,teamNum);
            if(getTeamAssignmentResp.getStatusCode() != 200) {
                qUtils.showErrorIfFailed(getTeamAssignmentResp);
            }
            assertEquals(getTeamAssignmentResp.getStatusCode(), 200);
            //Send Invite
            Response sendInviteResp = qUtils.postSendInviteRequest(conType, urlEndPoint, apiParam.requestType,teamNum);
            if(sendInviteResp.getStatusCode() != 200) {
                logger.info(sendInviteResp.prettyPrint().toString());
            }
            assertEquals(getTeamAssignmentResp.getStatusCode(), 200);
            String invitationToken=new JSONObject(sendInviteResp.prettyPrint().toString()).getString("invitationToken").toString();
            String userId=new JSONObject(sendInviteResp.prettyPrint().toString()).getString("id").toString();
            logger.info("invitationToken: "+invitationToken);
            //Verify invitation token
            Response verifyTokenResp = qUtils.postTokenVerificationRequest(conType, urlEndPoint, apiParam.requestType,invitationToken);
            if(verifyTokenResp.getStatusCode() != 200) {
                qUtils.showErrorIfFailed(getTeamAssignmentResp);
            }
            assertEquals(getTeamAssignmentResp.getStatusCode(), 200);
            // Update Invitation
            Response updateInviteResp = qUtils.postUpdateInviteRequest(conType, urlEndPoint, apiParam.requestType,invitationToken,userId,isCorrectEmail);
            assertEquals(updateInviteResp.getStatusCode(), 400);
            JSONObject jsonObjResp = new JSONObject(updateInviteResp.prettyPrint().toString());
            assertEquals(jsonObjResp.getString("message").contains("Invitation email does not match"), true);
            result = "Passed";
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }
}
