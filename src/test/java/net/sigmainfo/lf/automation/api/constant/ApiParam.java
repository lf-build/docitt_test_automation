package net.sigmainfo.lf.automation.api.constant;

import org.springframework.stereotype.Component;

/**
 * Created by           : Shaishav.s on 08-02-2017.
 * Test class           : ApiParam.java
 * Description          : Contains all placeholders declared within the test package
 * Includes             : 1. Declares the place holders being used
 *                        2. Contains getter and setter methods
 */
@Component
public class ApiParam {

    public static String userType;
    public static String requestType;
    public static String contentType;
    public static String serviceName;
    public static String baseresturl;
    public static String testid;
    public static String port;
    public static String bearerToken;
    public static String custom_report_location;
    public static String name;
    public static String email;
    public static String roles;
    public static String password;
    public static String passwordSalt;
    public static String username;
    public static int sectionId;
    public static String subSectionName;
    public static String questionSectionName;
    public static String questionId;
    public static String answer;
    public static String InvitedBy;
    public static String InviteEmail;
    public static String InviteName;
    public static String MongoHost;
    public static int MongoPort;
    public static boolean lenderTeam;
    public static String typeOfRequest;
    public static String priority;
    public static String applicant;
    public static String city;

    public static String getEntityId() {
        return entityId;
    }

    public static void setEntityId(String entityId) {
        ApiParam.entityId = entityId;
    }

    public static String entityId;

    public static String getEventTitle() {
        return eventTitle;
    }

    public static void setEventTitle(String eventTitle) {
        ApiParam.eventTitle = eventTitle;
    }

    public static String getEventDesc() {
        return eventDesc;
    }

    public static void setEventDesc(String eventDesc) {
        ApiParam.eventDesc = eventDesc;
    }

    public static String eventTitle;
    public static String eventDesc;



    public static String getTypeOfRequest() {
        return typeOfRequest;
    }

    public static void setTypeOfRequest(String typeOfRequest) {
        ApiParam.typeOfRequest = typeOfRequest;
    }

    public static String getPriority() {
        return priority;
    }

    public static void setPriority(String priority) {
        ApiParam.priority = priority;
    }

    public static String getApplicant() {
        return applicant;
    }

    public static void setApplicant(String applicant) {
        ApiParam.applicant = applicant;
    }

    public static String getCity() {
        return city;
    }

    public static void setCity(String city) {
        ApiParam.city = city;
    }

    public static boolean isLenderTeam() {
        return lenderTeam;
    }

    public static void setLenderTeam(boolean lenderTeam) {
        ApiParam.lenderTeam = lenderTeam;
    }



    public static String getBeareruser() {
        return beareruser;
    }

    public static void setBeareruser(String beareruser) {
        ApiParam.beareruser = beareruser;
    }

    public static String beareruser;

    public static String getUserType() {
        return userType;
    }

    public static void setUserType(String userType) {
        ApiParam.userType = userType;
    }

    public static String getRequestType() {
        return requestType;
    }

    public static void setRequestType(String requestType) {
        ApiParam.requestType = requestType;
    }

    public static String getContentType() {
        return contentType;
    }

    public static void setContentType(String contentType) {
        ApiParam.contentType = contentType;
    }

    public static String getServiceName() {
        return serviceName;
    }

    public static void setServiceName(String serviceName) {
        ApiParam.serviceName = serviceName;
    }

    public static String getBaseresturl() {
        return baseresturl;
    }

    public static void setBaseresturl(String baseresturl) {
        ApiParam.baseresturl = baseresturl;
    }

    public static String getTestid() {
        return testid;
    }

    public static void setTestid(String testid) {
        ApiParam.testid = testid;
    }

    public static String getPort() {
        return port;
    }

    public static void setPort(String port) {
        ApiParam.port = port;
    }

    public static String getBearerToken() {
        return bearerToken;
    }

    public static void setBearerToken(String bearerToken) {
        ApiParam.bearerToken = bearerToken;
    }

    public static String getCustom_report_location() {
        return custom_report_location;
    }

    public static void setCustom_report_location(String custom_report_location) {
        ApiParam.custom_report_location = custom_report_location;
    }

    public static String getName() {
        return name;
    }

    public static void setName(String name) {
        ApiParam.name = name;
    }

    public static String getEmail() {
        return email;
    }

    public static void setEmail(String email) {
        ApiParam.email = email;
    }

    public static String getRoles() {
        return roles;
    }

    public static void setRoles(String roles) {
        ApiParam.roles = roles;
    }

    public static String getPassword() {
        return password;
    }

    public static void setPassword(String password) {
        ApiParam.password = password;
    }

    public static String getPasswordSalt() {
        return passwordSalt;
    }

    public static void setPasswordSalt(String passwordSalt) {
        ApiParam.passwordSalt = passwordSalt;
    }

    public static String getUsername() {
        return username;
    }

    public static void setUsername(String username) {
        ApiParam.username = username;
    }



    public static int getSectionId() {
        return sectionId;
    }

    public static void setSectionId(int sectionId) {
        ApiParam.sectionId = sectionId;
    }

    public static String getSubSectionName() {
        return subSectionName;
    }

    public static void setSubSectionName(String subSectionName) {
        ApiParam.subSectionName = subSectionName;
    }

    public static String getQuestionSectionName() {
        return questionSectionName;
    }

    public static void setQuestionSectionName(String questionSectionName) {
        ApiParam.questionSectionName = questionSectionName;
    }

    public static String getQuestionId() {
        return questionId;
    }

    public static void setQuestionId(String questionId) {
        ApiParam.questionId = questionId;
    }

    public static String getAnswer() {
        return answer;
    }

    public static void setAnswer(String answer) {
        ApiParam.answer = answer;
    }



    public static String getInvitedBy() {
        return InvitedBy;
    }

    public static void setInvitedBy(String invitedBy) {
        InvitedBy = invitedBy;
    }

    public static String getInviteEmail() {
        return InviteEmail;
    }

    public static void setInviteEmail(String inviteEmail) {
        InviteEmail = inviteEmail;
    }

    public static String getInviteName() {
        return InviteName;
    }

    public static void setInviteName(String inviteName) {
        InviteName = inviteName;
    }

    public static String getInviteMobileNumber() {
        return InviteMobileNumber;
    }

    public static void setInviteMobileNumber(String inviteMobileNumber) {
        InviteMobileNumber = inviteMobileNumber;
    }

    public static String InviteMobileNumber;

    public static String getMongoHost() {
        return MongoHost;
    }

    public static void setMongoHost(String mongoHost) {
        MongoHost = mongoHost;
    }

    public static int getMongoPort() {
        return MongoPort;
    }

    public static void setMongoPort(int mongoPort) {
        MongoPort = mongoPort;
    }


}
