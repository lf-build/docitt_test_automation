package net.sigmainfo.lf.automation.api.function;

import org.springframework.stereotype.Component;

/**
 * Created by           : Shaishav.s on 23-02-2017.
 * Test class           : CheckUserAvailableRequest.java
 * Description          : CheckUserAvailableRequest
 * Includes             : 1. Declaration of members which makes this request
 *                        2. Getter and setter methods
 */
@Component
public class CheckUserAvailableRequest {

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    private String username;
}
